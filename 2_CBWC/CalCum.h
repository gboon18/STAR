class CalCum{

public:
  CalCum();
  ~CalCum();

  void Init();
  void SetMean(double, int);
  void SetCums(int);

  double Get_tc1() const {return tc1;}
  double Get_tc2() const {return tc2;}
  double Get_tc3() const {return tc3;}
  double Get_tc4() const {return tc4;}
  double Get_tc5() const {return tc5;}
  double Get_tc6() const {return tc6;}
  
  double Get_tk1() const {return tk1;}
  double Get_tk2() const {return tk2;}
  double Get_tk3() const {return tk3;}
  double Get_tk4() const {return tk4;}
  double Get_tk5() const {return tk5;}
  double Get_tk6() const {return tk6;}
  
  double Get_etc1() const {return etc1;}
  double Get_etc2() const {return etc2;}
  double Get_etc3() const {return etc3;}
  double Get_etc4() const {return etc4;}
  double Get_etc5() const {return etc5;}
  double Get_etc6() const {return etc6;}
  
  double Get_etk1() const {return etk1;}
  double Get_etk2() const {return etk2;}
  double Get_etk3() const {return etk3;}
  double Get_etk4() const {return etk4;}
  double Get_etk5() const {return etk5;}
  double Get_etk6() const {return etk6;}
  
  double Get_er21() const {return er21;}
  double Get_er31() const {return er31;}
  double Get_er32() const {return er32;}
  double Get_er42() const {return er42;}
  double Get_er51() const {return er51;}
  double Get_er62() const {return er62;}
  
  double Get_ek21() const {return ek21;}
  double Get_ek31() const {return ek31;}
  double Get_ek32() const {return ek32;}
  double Get_ek41() const {return ek41;}
  double Get_ek43() const {return ek43;}
  double Get_ek51() const {return ek51;}
  double Get_ek54() const {return ek54;}
  double Get_ek61() const {return ek61;}
  double Get_ek65() const {return ek65;}
  
private:

  static const int N_mean_term = 2535;
  double *mean_;
  double tc1, tc2, tc3, tc4, tc5, tc6;
  double tk1, tk2, tk3, tk4, tk5, tk6;
  double etc1, etc2, etc3, etc4, etc5, etc6;
  double etk1, etk2, etk3, etk4, etk5, etk6;
  double er21, er31, er32, er42, er51, er62;
  double ek21, ek31, ek32, ek41, ek43, ek51, ek54, ek61, ek65;

};
