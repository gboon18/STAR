//Based on arXiv:1702.07106v3

#include <iostream>
#include "Cum.h"

#include "TH1D.h"
#include "TFile.h"
#include "TKey.h"
#include "TProfile.h"
#include "TMath.h"
#include "TGraphErrors.h"

#include "CalCum.h"

#include <map>

using namespace std;

  //01072021(start)
// Cum::Cum(int maxmult, const char* NameRuZr) : maxMult(maxmult) {
// Cum::Cum(int maxmult, int maxmult_ref3, const char* NameRuZr) : maxMult(maxmult), maxMult_ref3(maxmult_ref3) {
  //01072021(finish)
Cum::Cum(int maxmult, int maxmult_ref3, int maxmult_ref3StPicoEvt, const char* NameRuZr, int mergetopcent) : maxMult(maxmult), maxMult_ref3(maxmult_ref3), maxMult_ref3StPicoEvt(maxmult_ref3StPicoEvt), MergeTopCent(mergetopcent) {

  hQ_Nevt = new TH1D(Form("hQ_Nevt_%s", "charge"), Form("hQ_Nevt_%s", "charge"), maxMult, 0, maxMult);
  hB_Nevt = new TH1D(Form("hB_Nevt_%s", "proton"), Form("hB_Nevt_%s", "proton"), maxMult, 0, maxMult);
  hS_Nevt = new TH1D(Form("hS_Nevt_%s", "kaon"),   Form("hS_Nevt_%s", "kaon"),   maxMult, 0, maxMult);
  hQ_Nevt->GetXaxis()->SetTitle("EPDmult");
  hB_Nevt->GetXaxis()->SetTitle("EPDmult");
  hS_Nevt->GetXaxis()->SetTitle("EPDmult");

  //unCorr
  hQ_Nevt_unCorr = new TH1D(Form("hQ_Nevt_%s_unCorr", "charge"), Form("hQ_Nevt_%s_unCorr", "charge"), maxMult, 0, maxMult);
  hB_Nevt_unCorr = new TH1D(Form("hB_Nevt_%s_unCorr", "proton"), Form("hB_Nevt_%s_unCorr", "proton"), maxMult, 0, maxMult);
  hS_Nevt_unCorr = new TH1D(Form("hS_Nevt_%s_unCorr", "kaon"),   Form("hS_Nevt_%s_unCorr", "kaon"),   maxMult, 0, maxMult);
  hQ_Nevt_unCorr->GetXaxis()->SetTitle("EPDmult");
  hB_Nevt_unCorr->GetXaxis()->SetTitle("EPDmult");
  hS_Nevt_unCorr->GetXaxis()->SetTitle("EPDmult");
  for(int i_cum = 0 ; i_cum < Ncums ; i_cum++ ) {
    hQ_C[i_cum] = new TH1D(Form("hQ_C%d_%s", i_cum+1, "charge"), Form("hQ_C%d_%s", i_cum+1, "charge"), maxMult, 0, maxMult);
    hB_C[i_cum] = new TH1D(Form("hB_C%d_%s", i_cum+1, "proton"), Form("hB_C%d_%s", i_cum+1, "proton"), maxMult, 0, maxMult);
    hS_C[i_cum] = new TH1D(Form("hS_C%d_%s", i_cum+1, "kaon"),   Form("hS_C%d_%s", i_cum+1, "kaon"),   maxMult, 0, maxMult);
    hQ_C[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hB_C[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hS_C[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hQ_C[i_cum]->GetYaxis()->SetTitle(cum_term_name[i_cum]);
    hB_C[i_cum]->GetYaxis()->SetTitle(cum_term_name[i_cum]);
    hS_C[i_cum]->GetYaxis()->SetTitle(cum_term_name[i_cum]);

    //unCorr
    hQ_C_unCorr[i_cum] = new TH1D(Form("hQ_C%d_%s_unCorr", i_cum+1, "charge"), Form("hQ_C%d_%s_unCorr", i_cum+1, "charge"), maxMult, 0, maxMult);
    hB_C_unCorr[i_cum] = new TH1D(Form("hB_C%d_%s_unCorr", i_cum+1, "proton"), Form("hB_C%d_%s_unCorr", i_cum+1, "proton"), maxMult, 0, maxMult);
    hS_C_unCorr[i_cum] = new TH1D(Form("hS_C%d_%s_unCorr", i_cum+1, "kaon"),   Form("hS_C%d_%s_unCorr", i_cum+1, "kaon"),   maxMult, 0, maxMult);
    hQ_C_unCorr[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hB_C_unCorr[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hS_C_unCorr[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hQ_C_unCorr[i_cum]->GetYaxis()->SetTitle(cum_term_name[i_cum]);
    hB_C_unCorr[i_cum]->GetYaxis()->SetTitle(cum_term_name[i_cum]);
    hS_C_unCorr[i_cum]->GetYaxis()->SetTitle(cum_term_name[i_cum]);

    ///////
    //Err//
    ///////
    hQ_C_err[i_cum] = new TH1D(Form("hQ_C%d_%s_err", i_cum+1, "charge"), Form("hQ_C%d_%s_err", i_cum+1, "charge"), maxMult, 0, maxMult);
    hB_C_err[i_cum] = new TH1D(Form("hB_C%d_%s_err", i_cum+1, "proton"), Form("hB_C%d_%s_err", i_cum+1, "proton"), maxMult, 0, maxMult);
    hS_C_err[i_cum] = new TH1D(Form("hS_C%d_%s_err", i_cum+1, "kaon"),   Form("hS_C%d_%s_err", i_cum+1, "kaon"),   maxMult, 0, maxMult);
    hQ_C_err[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hB_C_err[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hS_C_err[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hQ_C_err[i_cum]->GetYaxis()->SetTitle(cum_term_name[i_cum]);
    hB_C_err[i_cum]->GetYaxis()->SetTitle(cum_term_name[i_cum]);
    hS_C_err[i_cum]->GetYaxis()->SetTitle(cum_term_name[i_cum]);

    //unCorr
    hQ_C_unCorr_err[i_cum] = new TH1D(Form("hQ_C%d_%s_unCorr_err", i_cum+1, "charge"), Form("hQ_C%d_%s_unCorr_err", i_cum+1, "charge"), maxMult, 0, maxMult);
    hB_C_unCorr_err[i_cum] = new TH1D(Form("hB_C%d_%s_unCorr_err", i_cum+1, "proton"), Form("hB_C%d_%s_unCorr_err", i_cum+1, "proton"), maxMult, 0, maxMult);
    hS_C_unCorr_err[i_cum] = new TH1D(Form("hS_C%d_%s_unCorr_err", i_cum+1, "kaon"),   Form("hS_C%d_%s_unCorr_err", i_cum+1, "kaon"),   maxMult, 0, maxMult);
    hQ_C_unCorr_err[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hB_C_unCorr_err[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hS_C_unCorr_err[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hQ_C_unCorr_err[i_cum]->GetYaxis()->SetTitle(cum_term_name[i_cum]);
    hB_C_unCorr_err[i_cum]->GetYaxis()->SetTitle(cum_term_name[i_cum]);
    hS_C_unCorr_err[i_cum]->GetYaxis()->SetTitle(cum_term_name[i_cum]);
  }

  //Mixed cumulants
  for(int i_mix = 0 ; i_mix < N_mixed ; i_mix++){
    h_mixed_Nevt[i_mix] = new TH1D(Form("h_%s_Nevt%d", "mixed", i_mix), Form("h_%s_Nevt%d", "mixed", i_mix), maxMult, 0, maxMult);
    h_mixed_Nevt[i_mix]->GetXaxis()->SetTitle("EPDmult");

    //unCorr
    h_mixed_Nevt_unCorr[i_mix] = new TH1D(Form("h_%s_Nevt_unCorr%d", "mixed", i_mix), Form("h_%s_Nevt_unCorr%d", "mixed", i_mix), maxMult, 0, maxMult);
    h_mixed_Nevt_unCorr[i_mix]->GetXaxis()->SetTitle("EPDmult");
  }
  for(int i_cum = 0 ; i_cum < Ncums_mixed ; i_cum++ ) {
    h_mixed_C[i_cum] = new TH1D(Form("h_mixed_C%d_%s", i_cum+1, "mixed"), Form("h_mixed_C%d_%s", i_cum+1, "mixed"), maxMult, 0, maxMult);
    h_mixed_C[i_cum]->GetXaxis()->SetTitle("EPDmult");
    h_mixed_C[i_cum]->GetYaxis()->SetTitle(mixed_cum_term_name[i_cum]);

    //unCorr
    h_mixed_C_unCorr[i_cum] = new TH1D(Form("h_mixed_C%d_%s_unCorr", i_cum+1, "mixed"), Form("h_mixed_C%d_%s_unCorr", i_cum+1, "mixed"), maxMult, 0, maxMult);
    h_mixed_C_unCorr[i_cum]->GetXaxis()->SetTitle("EPDmult");
    h_mixed_C_unCorr[i_cum]->GetYaxis()->SetTitle(mixed_cum_term_name[i_cum]);

    ///////
    //Err//
    ///////
    h_mixed_C_err[i_cum] = new TH1D(Form("h_mixed_C%d_%s_err", i_cum+1, "mixed"), Form("h_mixed_C%d_%s_err", i_cum+1, "mixed"), maxMult, 0, maxMult);
    h_mixed_C_err[i_cum]->GetXaxis()->SetTitle("EPDmult");
    h_mixed_C_err[i_cum]->GetYaxis()->SetTitle(mixed_cum_term_name[i_cum]);

    //unCorr
    h_mixed_C_unCorr_err[i_cum] = new TH1D(Form("h_mixed_C%d_%s_unCorr_err", i_cum+1, "mixed"), Form("h_mixed_C%d_%s_unCorr_err", i_cum+1, "mixed"), maxMult, 0, maxMult);
    h_mixed_C_unCorr_err[i_cum]->GetXaxis()->SetTitle("EPDmult");
    h_mixed_C_unCorr_err[i_cum]->GetYaxis()->SetTitle(mixed_cum_term_name[i_cum]);

  }

  //======================================//

  //////////////////////////////
  ////////noErr up to C6////////
  //////////////////////////////
  //01072021(start)
  // hB_Nevt_ref3 = new TH1D(Form("hB_Nevt_%s_ref3", "proton"), Form("hB_Nevt_%s_ref3", "proton"), maxMult, 0, maxMult);
  hB_Nevt_ref3 = new TH1D(Form("hB_Nevt_%s_ref3", "proton"), Form("hB_Nevt_%s_ref3", "proton"), maxMult_ref3, 0, maxMult_ref3);
  //01072021(finish)
  hB_Nevt_ref3->GetXaxis()->SetTitle("RefMult3");

  // hB_Nevt_ref3StPicoEvt = new TH1D(Form("hB_Nevt_%s_ref3StPicoEvt", "proton"), Form("hB_Nevt_%s_ref3StPicoEvt", "proton"), maxMult_ref3StPicoEvt, 0, maxMult_ref3StPicoEvt);
  // hB_Nevt_ref3StPicoEvt->GetXaxis()->SetTitle("RefMult3StPicoEvt");

  hQ_Nevt_epd = new TH1D(Form("hQ_Nevt_%s_epd", "charge"), Form("hQ_Nevt_%s_epd", "charge"), maxMult, 0, maxMult);
  hB_Nevt_epd = new TH1D(Form("hB_Nevt_%s_epd", "proton"), Form("hB_Nevt_%s_epd", "proton"), maxMult, 0, maxMult);
  hS_Nevt_epd = new TH1D(Form("hS_Nevt_%s_epd", "kaon"),   Form("hS_Nevt_%s_epd", "kaon"),   maxMult, 0, maxMult);
  hQ_Nevt_epd->GetXaxis()->SetTitle("EPDmult");
  hB_Nevt_epd->GetXaxis()->SetTitle("EPDmult");
  hS_Nevt_epd->GetXaxis()->SetTitle("EPDmult");

  //unCorr
  //01072021(start)
  // hB_Nevt_ref3_unCorr = new TH1D(Form("hB_Nevt_%s_ref3_unCorr", "proton"), Form("hB_Nevt_%s_ref3_unCorr", "proton"), maxMult, 0, maxMult);
  hB_Nevt_ref3_unCorr = new TH1D(Form("hB_Nevt_%s_ref3_unCorr", "proton"), Form("hB_Nevt_%s_ref3_unCorr", "proton"), maxMult_ref3, 0, maxMult_ref3);
  //01072021(finish)
  hB_Nevt_ref3_unCorr->GetXaxis()->SetTitle("RefMult3");

  // hB_Nevt_ref3StPicoEvt_unCorr = new TH1D(Form("hB_Nevt_%s_ref3StPicoEvt_unCorr", "proton"), Form("hB_Nevt_%s_ref3StPicoEvt_unCorr", "proton"), maxMult_ref3StPicoEvt, 0, maxMult_ref3StPicoEvt);
  // hB_Nevt_ref3StPicoEvt_unCorr->GetXaxis()->SetTitle("RefMult3StPicoEvt");

  hQ_Nevt_epd_unCorr = new TH1D(Form("hQ_Nevt_%s_epd_unCorr", "charge"), Form("hQ_Nevt_%s_epd_unCorr", "charge"), maxMult, 0, maxMult);
  hB_Nevt_epd_unCorr = new TH1D(Form("hB_Nevt_%s_epd_unCorr", "proton"), Form("hB_Nevt_%s_epd_unCorr", "proton"), maxMult, 0, maxMult);
  hS_Nevt_epd_unCorr = new TH1D(Form("hS_Nevt_%s_epd_unCorr", "kaon"),   Form("hS_Nevt_%s_epd_unCorr", "kaon"),   maxMult, 0, maxMult);
  hQ_Nevt_epd_unCorr->GetXaxis()->SetTitle("EPDmult");
  hB_Nevt_epd_unCorr->GetXaxis()->SetTitle("EPDmult");
  hS_Nevt_epd_unCorr->GetXaxis()->SetTitle("EPDmult");

  for(int i_cum = 0 ; i_cum < Ncums_noErr ; i_cum++ ) {
    //01072021(start)
    // hB_C_ref3[i_cum] = new TH1D(Form("hB_C%d_%s_ref3", i_cum+1, "proton"), Form("hB_C%d_%s_ref3", i_cum+1, "proton"), maxMult, 0, maxMult);
    hB_C_ref3[i_cum] = new TH1D(Form("hB_C%d_%s_ref3", i_cum+1, "proton"), Form("hB_C%d_%s_ref3", i_cum+1, "proton"), maxMult_ref3, 0, maxMult_ref3);
    //01072021(finish)
    hB_C_ref3[i_cum]->GetXaxis()->SetTitle("RefMult3");

    // hB_C_ref3StPicoEvt[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);
    // hB_C_ref3StPicoEvt[i_cum] = new TH1D(Form("hB_C%d_%s_ref3StPicoEvt", i_cum+1, "proton"), Form("hB_C%d_%s_ref3StPicoEvt", i_cum+1, "proton"), maxMult_ref3StPicoEvt, 0, maxMult_ref3StPicoEvt);
    // hB_C_ref3StPicoEvt[i_cum]->GetXaxis()->SetTitle("RefMult3StPicoEvt");
    // hB_C_ref3StPicoEvt[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);

    hQ_C_epd[i_cum] = new TH1D(Form("hQ_C%d_%s_epd", i_cum+1, "charge"), Form("hQ_C%d_%s_epd", i_cum+1, "charge"), maxMult, 0, maxMult);
    hB_C_epd[i_cum] = new TH1D(Form("hB_C%d_%s_epd", i_cum+1, "proton"), Form("hB_C%d_%s_epd", i_cum+1, "proton"), maxMult, 0, maxMult);
    hS_C_epd[i_cum] = new TH1D(Form("hS_C%d_%s_epd", i_cum+1, "kaon"),   Form("hS_C%d_%s_epd", i_cum+1, "kaon"),   maxMult, 0, maxMult);
    hQ_C_epd[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hB_C_epd[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hS_C_epd[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hQ_C_epd[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);
    hB_C_epd[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);
    hS_C_epd[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);

    //unCorr
    //01072021(start)
    // hB_C_ref3_unCorr[i_cum] = new TH1D(Form("hB_C%d_%s_ref3_unCorr", i_cum+1, "proton"), Form("hB_C%d_%s_ref3_unCorr", i_cum+1, "proton"), maxMult, 0, maxMult);
    hB_C_ref3_unCorr[i_cum] = new TH1D(Form("hB_C%d_%s_ref3_unCorr", i_cum+1, "proton"), Form("hB_C%d_%s_ref3_unCorr", i_cum+1, "proton"), maxMult_ref3, 0, maxMult_ref3);
    //01072021(finish)
    hB_C_ref3_unCorr[i_cum]->GetXaxis()->SetTitle("RefMult3");
    hB_C_ref3_unCorr[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);

    // hB_C_ref3StPicoEvt_unCorr[i_cum] = new TH1D(Form("hB_C%d_%s_ref3StPicoEvt_unCorr", i_cum+1, "proton"), Form("hB_C%d_%s_ref3StPicoEvt_unCorr", i_cum+1, "proton"), maxMult_ref3StPicoEvt, 0, maxMult_ref3StPicoEvt);
    // hB_C_ref3StPicoEvt_unCorr[i_cum]->GetXaxis()->SetTitle("RefMult3StPicoEvt");
    // hB_C_ref3StPicoEvt_unCorr[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);

    hQ_C_epd_unCorr[i_cum] = new TH1D(Form("hQ_C%d_%s_epd_unCorr", i_cum+1, "charge"), Form("hQ_C%d_%s_epd_unCorr", i_cum+1, "charge"), maxMult, 0, maxMult);
    hB_C_epd_unCorr[i_cum] = new TH1D(Form("hB_C%d_%s_epd_unCorr", i_cum+1, "proton"), Form("hB_C%d_%s_epd_unCorr", i_cum+1, "proton"), maxMult, 0, maxMult);
    hS_C_epd_unCorr[i_cum] = new TH1D(Form("hS_C%d_%s_epd_unCorr", i_cum+1, "kaon"),   Form("hS_C%d_%s_epd_unCorr", i_cum+1, "kaon"),   maxMult, 0, maxMult);
    hQ_C_epd_unCorr[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hB_C_epd_unCorr[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hS_C_epd_unCorr[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hQ_C_epd_unCorr[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);
    hB_C_epd_unCorr[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);
    hS_C_epd_unCorr[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);

    ///////
    //Err//
    ///////
    //01072021(start)
    // hB_C_ref3_err[i_cum] = new TH1D(Form("hB_C%d_%s_ref3_err", i_cum+1, "proton"), Form("hB_C%d_%s_ref3_err", i_cum+1, "proton"), maxMult, 0, maxMult);
    hB_C_ref3_err[i_cum] = new TH1D(Form("hB_C%d_%s_ref3_err", i_cum+1, "proton"), Form("hB_C%d_%s_ref3_err", i_cum+1, "proton"), maxMult_ref3, 0, maxMult_ref3);
    //01072021(finish)
    hB_C_ref3_err[i_cum]->GetXaxis()->SetTitle("RefMult3");
    hB_C_ref3_err[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);

    // hB_C_ref3StPicoEvt_err[i_cum] = new TH1D(Form("hB_C%d_%s_ref3StPicoEvt_err", i_cum+1, "proton"), Form("hB_C%d_%s_ref3StPicoEvt_err", i_cum+1, "proton"), maxMult_ref3StPicoEvt, 0, maxMult_ref3StPicoEvt);
    // hB_C_ref3StPicoEvt_err[i_cum]->GetXaxis()->SetTitle("RefMult3StPicoEvt");
    // hB_C_ref3StPicoEvt_err[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);

    hQ_C_epd_err[i_cum] = new TH1D(Form("hQ_C%d_%s_epd_err", i_cum+1, "charge"), Form("hQ_C%d_%s_epd_err", i_cum+1, "charge"), maxMult, 0, maxMult);
    hB_C_epd_err[i_cum] = new TH1D(Form("hB_C%d_%s_epd_err", i_cum+1, "proton"), Form("hB_C%d_%s_epd_err", i_cum+1, "proton"), maxMult, 0, maxMult);
    hS_C_epd_err[i_cum] = new TH1D(Form("hS_C%d_%s_epd_err", i_cum+1, "kaon"),   Form("hS_C%d_%s_epd_err", i_cum+1, "kaon"),   maxMult, 0, maxMult);
    hQ_C_epd_err[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hB_C_epd_err[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hS_C_epd_err[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hQ_C_epd_err[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);
    hB_C_epd_err[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);
    hS_C_epd_err[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);

    //unCorr
    //01072021(start)
    // hB_C_ref3_unCorr_err[i_cum] = new TH1D(Form("hB_C%d_%s_ref3_unCorr_err", i_cum+1, "proton"), Form("hB_C%d_%s_ref3_unCorr_err", i_cum+1, "proton"), maxMult, 0, maxMult);
    hB_C_ref3_unCorr_err[i_cum] = new TH1D(Form("hB_C%d_%s_ref3_unCorr_err", i_cum+1, "proton"), Form("hB_C%d_%s_ref3_unCorr_err", i_cum+1, "proton"), maxMult_ref3, 0, maxMult_ref3);
    //01072021(finish)
    hB_C_ref3_unCorr_err[i_cum]->GetXaxis()->SetTitle("RefMult3");
    hB_C_ref3_unCorr_err[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);

    // hB_C_ref3StPicoEvt_unCorr_err[i_cum] = new TH1D(Form("hB_C%d_%s_ref3StPicoEvt_unCorr_err", i_cum+1, "proton"), Form("hB_C%d_%s_ref3StPicoEvt_unCorr_err", i_cum+1, "proton"), maxMult_ref3StPicoEvt, 0, maxMult_ref3StPicoEvt);
    // hB_C_ref3StPicoEvt_unCorr_err[i_cum]->GetXaxis()->SetTitle("RefMult3StPicoEvt");
    // hB_C_ref3StPicoEvt_unCorr_err[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);

    hQ_C_epd_unCorr_err[i_cum] = new TH1D(Form("hQ_C%d_%s_epd_unCorr_err", i_cum+1, "charge"), Form("hQ_C%d_%s_epd_unCorr_err", i_cum+1, "charge"), maxMult, 0, maxMult);
    hB_C_epd_unCorr_err[i_cum] = new TH1D(Form("hB_C%d_%s_epd_unCorr_err", i_cum+1, "proton"), Form("hB_C%d_%s_epd_unCorr_err", i_cum+1, "proton"), maxMult, 0, maxMult);
    hS_C_epd_unCorr_err[i_cum] = new TH1D(Form("hS_C%d_%s_epd_unCorr_err", i_cum+1, "kaon"),   Form("hS_C%d_%s_epd_unCorr_err", i_cum+1, "kaon"),   maxMult, 0, maxMult);
    hQ_C_epd_unCorr_err[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hB_C_epd_unCorr_err[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hS_C_epd_unCorr_err[i_cum]->GetXaxis()->SetTitle("EPDmult");
    hQ_C_epd_unCorr_err[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);
    hB_C_epd_unCorr_err[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);
    hS_C_epd_unCorr_err[i_cum]->GetYaxis()->SetTitle(cum_term_name_noErr[i_cum]);
  }

  //======================================//

  for(int i_CRK = 0 ; i_CRK < N_CRK ; i_CRK++){
    grQ_C[i_CRK] = new TGraphErrors(Ncent);
    grB_C[i_CRK] = new TGraphErrors(Ncent);
    grS_C[i_CRK] = new TGraphErrors(Ncent);
    grQ_C[i_CRK]->SetName(Form("%s_%s", "charge", CRK_name[i_CRK]));
    grB_C[i_CRK]->SetName(Form("%s_%s", "proton", CRK_name[i_CRK]));
    grS_C[i_CRK]->SetName(Form("%s_%s", "kaon",   CRK_name[i_CRK]));
    grQ_C[i_CRK]->SetTitle(CRK_label[i_CRK]);
    grB_C[i_CRK]->SetTitle(CRK_label[i_CRK]);
    grS_C[i_CRK]->SetTitle(CRK_label[i_CRK]);
    grQ_C[i_CRK]->SetMarkerStyle(20);
    grB_C[i_CRK]->SetMarkerStyle(20);
    grS_C[i_CRK]->SetMarkerStyle(20);
    grQ_C[i_CRK]->GetXaxis()->SetTitle("EPDmult");
    grB_C[i_CRK]->GetXaxis()->SetTitle("EPDmult");
    grS_C[i_CRK]->GetXaxis()->SetTitle("EPDmult");
    grQ_C[i_CRK]->GetYaxis()->SetTitle(CRK_label[i_CRK]);
    grB_C[i_CRK]->GetYaxis()->SetTitle(CRK_label[i_CRK]);
    grS_C[i_CRK]->GetYaxis()->SetTitle(CRK_label[i_CRK]);
  }

  //Mixed cumulants
  for(int i_mixed_CRK = 0 ; i_mixed_CRK < N_mixed_CRK ; i_mixed_CRK++){
    gr_mixed_C[i_mixed_CRK] = new TGraphErrors(Ncent);
    gr_mixed_C[i_mixed_CRK]->SetName(Form("%s_%s", "mixed", mixed_CRK_name[i_mixed_CRK]));
    gr_mixed_C[i_mixed_CRK]->SetTitle(mixed_CRK_label[i_mixed_CRK]);
    gr_mixed_C[i_mixed_CRK]->SetMarkerStyle(20);
    gr_mixed_C[i_mixed_CRK]->GetXaxis()->SetTitle("EPDmult");
    gr_mixed_C[i_mixed_CRK]->GetYaxis()->SetTitle(mixed_CRK_label[i_mixed_CRK]);
  }

  //////////////////////////////
  ////////noErr up to C6////////
  //////////////////////////////

  for(int i_CRK = 0 ; i_CRK < N_CRK_noErr ; i_CRK++){
    //EPDmult
    grQ_C_epd[i_CRK] = new TGraphErrors(Ncent);
    grB_C_epd[i_CRK] = new TGraphErrors(Ncent);
    grS_C_epd[i_CRK] = new TGraphErrors(Ncent);
    grQ_C_epd[i_CRK]->SetName(Form("%s_%s", "charge", CRK_name_noErr[i_CRK]));
    grB_C_epd[i_CRK]->SetName(Form("%s_%s", "proton", CRK_name_noErr[i_CRK]));
    grS_C_epd[i_CRK]->SetName(Form("%s_%s", "kaon",   CRK_name_noErr[i_CRK]));
    grQ_C_epd[i_CRK]->SetTitle(CRK_label_noErr[i_CRK]);
    grB_C_epd[i_CRK]->SetTitle(CRK_label_noErr[i_CRK]);
    grS_C_epd[i_CRK]->SetTitle(CRK_label_noErr[i_CRK]);
    grQ_C_epd[i_CRK]->SetMarkerStyle(20);
    grB_C_epd[i_CRK]->SetMarkerStyle(20);
    grS_C_epd[i_CRK]->SetMarkerStyle(20);
    grQ_C_epd[i_CRK]->GetXaxis()->SetTitle("EPDmult");
    grB_C_epd[i_CRK]->GetXaxis()->SetTitle("EPDmult");
    grS_C_epd[i_CRK]->GetXaxis()->SetTitle("EPDmult");
    grQ_C_epd[i_CRK]->GetYaxis()->SetTitle(CRK_label_noErr[i_CRK]);
    grB_C_epd[i_CRK]->GetYaxis()->SetTitle(CRK_label_noErr[i_CRK]);
    grS_C_epd[i_CRK]->GetYaxis()->SetTitle(CRK_label_noErr[i_CRK]);

    //RefMult3
    grB_C_ref3[i_CRK] = new TGraphErrors(Ncent);
    grB_C_ref3[i_CRK]->SetName(Form("%s_%s", "proton", CRK_name_noErr[i_CRK]));
    grB_C_ref3[i_CRK]->SetTitle(CRK_label_noErr[i_CRK]);
    grB_C_ref3[i_CRK]->SetMarkerStyle(20);
    grB_C_ref3[i_CRK]->GetXaxis()->SetTitle("RefMult3");
    grB_C_ref3[i_CRK]->GetYaxis()->SetTitle(CRK_label_noErr[i_CRK]);

    //RefMult3 UnCorr
    grB_C_ref3_unCorr[i_CRK] = new TGraphErrors(Ncent);
    grB_C_ref3_unCorr[i_CRK]->SetName(Form("%s_%s", "proton", CRK_name_noErr[i_CRK]));
    grB_C_ref3_unCorr[i_CRK]->SetTitle(CRK_label_noErr[i_CRK]);
    grB_C_ref3_unCorr[i_CRK]->SetMarkerStyle(20);
    grB_C_ref3_unCorr[i_CRK]->GetXaxis()->SetTitle("RefMult3");
    grB_C_ref3_unCorr[i_CRK]->GetYaxis()->SetTitle(CRK_label_noErr[i_CRK]);

    // //RefMult3 StPicoEvt
    // grB_C_ref3StPicoEvt[i_CRK] = new TGraphErrors(Ncent);
    // grB_C_ref3StPicoEvt[i_CRK]->SetName(Form("%s_%s", "proton", CRK_name_noErr[i_CRK]));
    // grB_C_ref3StPicoEvt[i_CRK]->SetTitle(CRK_label_noErr[i_CRK]);
    // grB_C_ref3StPicoEvt[i_CRK]->SetMarkerStyle(20);
    // grB_C_ref3StPicoEvt[i_CRK]->GetXaxis()->SetTitle("RefMult3StPicoEvt");
    // grB_C_ref3StPicoEvt[i_CRK]->GetYaxis()->SetTitle(CRK_label_noErr[i_CRK]);
  }

}

//////////////////////////////////////////


Cum::~Cum(){
  //delete stuffs

  delete hQ_Nevt;
  delete hB_Nevt;
  delete hS_Nevt;
  delete hQ_Nevt_unCorr;
  delete hB_Nevt_unCorr;
  delete hS_Nevt_unCorr;

  for(int i_cum = 0 ; i_cum < Ncums ; i_cum++) {
    delete hQ_C[i_cum];
    delete hB_C[i_cum];
    delete hS_C[i_cum];
    delete hQ_C_unCorr[i_cum];
    delete hB_C_unCorr[i_cum];
    delete hS_C_unCorr[i_cum];
    ///////
    //Err//
    ///////
    delete hQ_C_err[i_cum];
    delete hB_C_err[i_cum];
    delete hS_C_err[i_cum];
    delete hQ_C_unCorr_err[i_cum];
    delete hB_C_unCorr_err[i_cum];
    delete hS_C_unCorr_err[i_cum];
  }
 
  //Mixed cumulants
  for(int i_mix = 0 ; i_mix < N_mixed ; i_mix++){
    delete h_mixed_Nevt[i_mix];
    delete h_mixed_Nevt_unCorr[i_mix];
  }
  
  for(int i_cum = 0 ; i_cum < Ncums_mixed ; i_cum++) {
    delete h_mixed_C[i_cum];
    delete h_mixed_C_unCorr[i_cum];
    ///////
    //Err//
    ///////
    delete h_mixed_C_err[i_cum];
    delete h_mixed_C_unCorr_err[i_cum];
  }

  //======================================//

  //////////////////////////////
  ////////noErr up to C6////////
  //////////////////////////////
  delete hB_Nevt_ref3;
  delete hB_Nevt_ref3StPicoEvt;
  delete hQ_Nevt_epd;
  delete hB_Nevt_epd;
  delete hS_Nevt_epd;
  delete hB_Nevt_ref3_unCorr;
  delete hB_Nevt_ref3StPicoEvt_unCorr;
  delete hQ_Nevt_epd_unCorr;
  delete hB_Nevt_epd_unCorr;
  delete hS_Nevt_epd_unCorr;

  for(int i_cum = 0 ; i_cum < Ncums_noErr ; i_cum++){
    delete hB_C_ref3[i_cum];
    delete hB_C_ref3StPicoEvt[i_cum];
    delete hQ_C_epd[i_cum];
    delete hB_C_epd[i_cum];
    delete hS_C_epd[i_cum];
    delete hB_C_ref3_unCorr[i_cum];
    delete hB_C_ref3StPicoEvt_unCorr[i_cum];
    delete hQ_C_epd_unCorr[i_cum];
    delete hB_C_epd_unCorr[i_cum];
    delete hS_C_epd_unCorr[i_cum];

    ///////
    //Err//
    ///////
    delete hB_C_ref3_err[i_cum];
    delete hB_C_ref3StPicoEvt_err[i_cum];
    delete hQ_C_epd_err[i_cum];
    delete hB_C_epd_err[i_cum];
    delete hS_C_epd_err[i_cum];
    delete hB_C_ref3_unCorr_err[i_cum];
    delete hB_C_ref3StPicoEvt_unCorr_err[i_cum];
    delete hQ_C_epd_unCorr_err[i_cum];
    delete hB_C_epd_unCorr_err[i_cum];
    delete hS_C_epd_unCorr_err[i_cum];

  }
  
  //======================================//

  for(int i_CRK = 0 ; i_CRK < N_CRK ; i_CRK++)
    {
      delete grQ_C[i_CRK];
      delete grB_C[i_CRK];
      delete grS_C[i_CRK];
    }
  for(int i_mixed_CRK = 0 ; i_mixed_CRK < N_mixed_CRK ; i_mixed_CRK++)
    {
      delete gr_mixed_C[i_mixed_CRK];
    }
  // for(int i_cent = 0 ; i_cent < Ncent ; i_cent++) delete centrality[i_cent];

  //////////////////////////////
  ////////noErr up to C6////////
  //////////////////////////////
  for(int i_CRK = 0 ; i_CRK < N_CRK_noErr ; i_CRK++)
    {
      //EPDmult
      delete grQ_C_epd[i_CRK];
      delete grB_C_epd[i_CRK];
      delete grS_C_epd[i_CRK];
      //RefMul3
      delete grB_C_ref3[i_CRK];
      delete grB_C_ref3_unCorr[i_CRK];
      //RefMul3 StPicoEvt
      delete grB_C_ref3StPicoEvt[i_CRK];
    }
}

//////////////////////////////////////////

void Cum::GetMeans(const char* inFile){
  //get terms in Eq. 62 ~ 67 & 69 in arXiv:1702.07106v3
  TFile *f_in = new TFile(inFile, "READ");
  std::map<const char*, TProfile*>::iterator iter;
  ///charge///
  TIter nextkeyQ(f_in->GetListOfKeys());
  TKey *keyQ;
  while((keyQ = (TKey*)nextkeyQ())){
    TString strName = keyQ->ReadObj()->GetName();
    if(strName.Contains("charge")){
      strName.Replace(0, strlen("charge")+1,"");
    // cout<<strName<<endl;
    } else {
      continue;
    }
    int iT = 0;
    while (true) {
      if(iT == N_mean_term) {
	std::cout<<"In Cum::GetMeans"<<std::endl;
	std::cout<<"Found bad terms"<<std::endl;
	abort();
      }
      if(strcmp(strName, mean_term_name[iT])==0){
	// cout<<iT<<endl;
	h_mean_qQ_[iT] = (TProfile*) keyQ->ReadObj()->Clone();
	break;
      } else {
	iT++;
      }
    }
  }
  ///proton///
  TIter nextkeyB(f_in->GetListOfKeys());
  TKey *keyB;
  while((keyB = (TKey*)nextkeyB())){
    TString strName = keyB->ReadObj()->GetName();
    if(strName.Contains("proton")){
      strName.Replace(0, strlen("proton")+1,"");
    // cout<<strName<<endl;
    } else {
      continue;
    }
    int iT = 0;
    while (true) {
      if(iT == N_mean_term) {
	std::cout<<"In Cum::GetMeans"<<std::endl;
	std::cout<<"Found bad terms"<<std::endl;
	abort();
      }
      if(strcmp(strName, mean_term_name[iT])==0){
	h_mean_qB_[iT] = (TProfile*) keyB->ReadObj()->Clone();
	break;
      } else {
	iT++;
      }
    }
  }
  ///kaon///
  TIter nextkeyS(f_in->GetListOfKeys());
  TKey *keyS;
  while((keyS = (TKey*)nextkeyS())){
    TString strName = keyS->ReadObj()->GetName();
    if(strName.Contains("kaon")){
      strName.Replace(0, strlen("kaon")+1,"");
    } else {
      continue;
    }
    int iT = 0;
    while (true) {
      if(iT == N_mean_term) {
	std::cout<<"In Cum::GetMeans"<<std::endl;
	std::cout<<"Found bad terms"<<std::endl;
	abort();
      }
      if(strcmp(strName, mean_term_name[iT])==0){
	h_mean_qS_[iT] = (TProfile*) keyS->ReadObj()->Clone();
	break;
      } else {
	iT++;
      }
    }
  }
  ///mixed///
  TIter nextkeyM(f_in->GetListOfKeys());
  TKey *keyM;
  while((keyM = (TKey*)nextkeyM())){
    TString strName = keyM->ReadObj()->GetName();
    if(strName.Contains("mixed")){
      strName.Replace(0, strlen("mixed")+1,"");
    } else {
      continue;
    }
    int iT = 0;
    while (true) {
      if(iT == N_mixed_mean_term) {
	std::cout<<"In Cum::GetMeans"<<std::endl;
	std::cout<<"Found bad terms"<<std::endl;
	abort();
      }
      if(strcmp(strName, mixed_mean_term_name[iT])==0){
	h_mixed_mean_q_[iT] = (TProfile*) keyM->ReadObj()->Clone();
	break;
      } else {
	iT++;
      }
    }
  }
  TProfile* entQ = (TProfile*)f_in->Get(Form("%s_q_11","charge"));
  TProfile* entB = (TProfile*)f_in->Get(Form("%s_q_11","proton"));
  TProfile* entS = (TProfile*)f_in->Get(Form("%s_q_11","kaon"));
  TProfile* entQB = (TProfile*)f_in->Get(Form("%s_qQB_112","mixed"));
  TProfile* entBS = (TProfile*)f_in->Get(Form("%s_qBS_112","mixed"));
  TProfile* entSQ = (TProfile*)f_in->Get(Form("%s_qSQ_112","mixed"));
  for(int i=1;i<entQ->GetNbinsX();++i) {
    hQ_Nevt->SetBinContent(i, entQ->GetBinEntries(i));//getting No. of Evt per refMult
    hB_Nevt->SetBinContent(i, entB->GetBinEntries(i));//getting No. of Evt per refMult
    hS_Nevt->SetBinContent(i, entS->GetBinEntries(i));//getting No. of Evt per refMult

    h_mixed_Nevt[0]->SetBinContent(i, entQB->GetBinEntries(i));//getting No. of Evt per refMult
    h_mixed_Nevt[1]->SetBinContent(i, entBS->GetBinEntries(i));//getting No. of Evt per refMult
    h_mixed_Nevt[2]->SetBinContent(i, entSQ->GetBinEntries(i));//getting No. of Evt per refMult
    // cout<<ent->GetBinEntries(i)<<endl;
  }
}

//////////////////////////////////////////

void Cum::CalculateEffCorrCum(const char* NameRuZr){
  //calculate eff corr cumulant
  //charge
  double C_Q_1, C_Q_2, C_Q_3, C_Q_4, C_Q_5, C_Q_6;
  double k_Q_1, k_Q_2, k_Q_3, k_Q_4, k_Q_5, k_Q_6;
  double Cerr_Q_1, Cerr_Q_2, Cerr_Q_3, Cerr_Q_4, Cerr_Q_5, Cerr_Q_6;
  double kerr_Q_1, kerr_Q_2, kerr_Q_3, kerr_Q_4, kerr_Q_5, kerr_Q_6;
  double CRerr_Q_21, CRerr_Q_31, CRerr_Q_32, CRerr_Q_42, CRerr_Q_51, CRerr_Q_62;
  double kRerr_Q_21, kRerr_Q_31, kRerr_Q_32, kRerr_Q_41, kRerr_Q_43, kRerr_Q_51, kRerr_Q_54, kRerr_Q_61, kRerr_Q_65;
  //proton
  double C_B_1, C_B_2, C_B_3, C_B_4, C_B_5, C_B_6;
  double k_B_1, k_B_2, k_B_3, k_B_4, k_B_5, k_B_6;
  double Cerr_B_1, Cerr_B_2, Cerr_B_3, Cerr_B_4, Cerr_B_5, Cerr_B_6;
  double kerr_B_1, kerr_B_2, kerr_B_3, kerr_B_4, kerr_B_5, kerr_B_6;
  double CRerr_B_21, CRerr_B_31, CRerr_B_32, CRerr_B_42, CRerr_B_51, CRerr_B_62;
  double kRerr_B_21, kRerr_B_31, kRerr_B_32, kRerr_B_41, kRerr_B_43, kRerr_B_51, kRerr_B_54, kRerr_B_61, kRerr_B_65;
  //kaon
  double C_S_1, C_S_2, C_S_3, C_S_4, C_S_5, C_S_6;
  double k_S_1, k_S_2, k_S_3, k_S_4, k_S_5, k_S_6;
  double Cerr_S_1, Cerr_S_2, Cerr_S_3, Cerr_S_4, Cerr_S_5, Cerr_S_6;
  double kerr_S_1, kerr_S_2, kerr_S_3, kerr_S_4, kerr_S_5, kerr_S_6;
  double CRerr_S_21, CRerr_S_31, CRerr_S_32, CRerr_S_42, CRerr_S_51, CRerr_S_62;
  double kRerr_S_21, kRerr_S_31, kRerr_S_32, kRerr_S_41, kRerr_S_43, kRerr_S_51, kRerr_S_54, kRerr_S_61, kRerr_S_65;

  double mean_qQ_[N_mean_term];
  double mean_qB_[N_mean_term];
  double mean_qS_[N_mean_term];

  //Mixed terms
  double C_QB_11, C_BS_11, C_SQ_11;
  double Cerr_QB_11, Cerr_BS_11, Cerr_SQ_11;
  double mixed_mean_q_[N_mixed_mean_term];

  for(int i_RM = 0 ; i_RM < maxMult ; i_RM++){
    cout<<"Cum::CalculateEffCorrCum(). Multiplicity: "<<i_RM<<"/"<<maxMult<<endl;
    //initialize
    //charge
    C_Q_1 = C_Q_2 = C_Q_3 = C_Q_4 = C_Q_5 = C_Q_6 = 0. ;
    k_Q_1 = k_Q_2 = k_Q_3 = k_Q_4 = k_Q_5 = k_Q_6 = 0. ;
    Cerr_Q_1 = Cerr_Q_2 = Cerr_Q_3 = Cerr_Q_4 = Cerr_Q_5 = Cerr_Q_6 = 0. ;
    kerr_Q_1 = kerr_Q_2 = kerr_Q_3 = kerr_Q_4 = kerr_Q_5 = kerr_Q_6 = 0. ;
    CRerr_Q_21 = CRerr_Q_31 = CRerr_Q_32 = CRerr_Q_42 = CRerr_Q_51 = CRerr_Q_62 = 0. ;
    kRerr_Q_21 = kRerr_Q_31 = kRerr_Q_32 = kRerr_Q_41 = kRerr_Q_43 = kRerr_Q_51 = kRerr_Q_54 = kRerr_Q_61 = kRerr_Q_65 = 0. ;
    //proton
    C_B_1 = C_B_2 = C_B_3 = C_B_4 = C_B_5 = C_B_6 = 0. ;
    k_B_1 = k_B_2 = k_B_3 = k_B_4 = k_B_5 = k_B_6 = 0. ;
    Cerr_B_1 = Cerr_B_2 = Cerr_B_3 = Cerr_B_4 = Cerr_B_5 = Cerr_B_6 = 0. ;
    kerr_B_1 = kerr_B_2 = kerr_B_3 = kerr_B_4 = kerr_B_5 = kerr_B_6 = 0. ;
    CRerr_B_21 = CRerr_B_31 = CRerr_B_32 = CRerr_B_42 = CRerr_B_51 = CRerr_B_62 = 0. ;
    kRerr_B_21 = kRerr_B_31 = kRerr_B_32 = kRerr_B_41 = kRerr_B_43 = kRerr_B_51 = kRerr_B_54 = kRerr_B_61 = kRerr_B_65 = 0. ;
    //kaon
    C_S_1 = C_S_2 = C_S_3 = C_S_4 = C_S_5 = C_S_6 = 0. ;
    k_S_1 = k_S_2 = k_S_3 = k_S_4 = k_S_5 = k_S_6 = 0. ;
    Cerr_S_1 = Cerr_S_2 = Cerr_S_3 = Cerr_S_4 = Cerr_S_5 = Cerr_S_6 = 0. ;
    kerr_S_1 = kerr_S_2 = kerr_S_3 = kerr_S_4 = kerr_S_5 = kerr_S_6 = 0. ;
    CRerr_S_21 = CRerr_S_31 = CRerr_S_32 = CRerr_S_42 = CRerr_S_51 = CRerr_S_62 = 0. ;
    kRerr_S_21 = kRerr_S_31 = kRerr_S_32 = kRerr_S_41 = kRerr_S_43 = kRerr_S_51 = kRerr_S_54 = kRerr_S_61 = kRerr_S_65 = 0. ;

    //Mixed terms
    C_QB_11 = C_BS_11 = C_SQ_11 = 0.;
    Cerr_QB_11 = Cerr_BS_11 = Cerr_SQ_11 =0.;

    //get terms in Eq. 62 ~ 67 in arXiv:1702.07106v3
    // int NevtQ = h_mean_qQ_[0]->GetBinEntries(i_RM);
    // int NevtB = h_mean_qB_[0]->GetBinEntries(i_RM);
    // int NevtS = h_mean_qS_[0]->GetBinEntries(i_RM);
    // cout<<NevtQ<<endl;
    for(int i_term = 0 ; i_term < N_mean_term ; i_term++){
      mean_qQ_[i_term] = h_mean_qQ_[i_term]->GetBinContent(i_RM);
      mean_qB_[i_term] = h_mean_qB_[i_term]->GetBinContent(i_RM);
      mean_qS_[i_term] = h_mean_qS_[i_term]->GetBinContent(i_RM);
    }//95 terms

    //Mixed terms
    for(int i_term = 0 ; i_term < N_mixed_mean_term ; i_term++){
      mixed_mean_q_[i_term] = h_mixed_mean_q_[i_term]->GetBinContent(i_RM);
    }

    //C1 ~ C6
    C_Q_1 = mean_qQ_[0];
    C_B_1 = mean_qB_[0];
    C_S_1 = mean_qS_[0];

    C_Q_2 = mean_qQ_[3] -  TMath::Power(mean_qQ_[0], 2) + mean_qQ_[1] - mean_qQ_[2];
    C_B_2 = mean_qB_[3] -  TMath::Power(mean_qB_[0], 2) + mean_qB_[1] - mean_qB_[2];
    C_S_2 = mean_qS_[3] -  TMath::Power(mean_qS_[0], 2) + mean_qS_[1] - mean_qS_[2];

    // cout<<C_Q_1<<endl;
    // cout<<C_B_1<<endl;
    // cout<<C_S_1<<endl;

    //Mixed terms
    // C_QB_11 = mixed_mean_q_[0] + mixed_mean_q_[1] - mixed_mean_q_[2];
    // C_BS_11 = mixed_mean_q_[3] + mixed_mean_q_[4] - mixed_mean_q_[5];
    // C_SQ_11 = mixed_mean_q_[6] + mixed_mean_q_[7] - mixed_mean_q_[8];
    // C_QB_11 = (mixed_mean_q_[12]   - mixed_mean_q_[0+2*0]*mixed_mean_q_[1+2*0]) + mixed_mean_q_[6+2*0] - mixed_mean_q_[7+2*0];
    // C_BS_11 = (mixed_mean_q_[12+1] - mixed_mean_q_[0+2*1]*mixed_mean_q_[1+2*1]) + mixed_mean_q_[6+2*1] - mixed_mean_q_[7+2*1];
    // // C_SQ_11 = (mixed_mean_q_[12+3] - mixed_mean_q_[0+2*2]*mixed_mean_q_[1+2*2]) + mixed_mean_q_[6+2*2] - mixed_mean_q_[7+2*2];
    // C_SQ_11 = (mixed_mean_q_[12+2] - mixed_mean_q_[0+2*2]*mixed_mean_q_[1+2*2]) + mixed_mean_q_[6+2*2] - mixed_mean_q_[7+2*2];

    C_QB_11 = (mixed_mean_q_[5]   - mixed_mean_q_[0]*mixed_mean_q_[1]) + mixed_mean_q_[2] - mixed_mean_q_[3];
    C_BS_11 = (mixed_mean_q_[5 + 19] - mixed_mean_q_[0 + 19]*mixed_mean_q_[1 + 19]) + mixed_mean_q_[2 + 19] - mixed_mean_q_[3 + 19];
    C_SQ_11 = (mixed_mean_q_[5 + 2*19] - mixed_mean_q_[0 + 2*19]*mixed_mean_q_[1 + 2*19]) + mixed_mean_q_[2 + 2*19] - mixed_mean_q_[3 + 2*19];

    // cout<<mixed_mean_q_[0]<<endl;
    // cout<<mixed_mean_q_[3]<<endl;
    // cout<<mixed_mean_q_[6]<<endl;


    ///////
    //Err//
    ///////
    //??AM I getting confused with the 0th bin and 1st bin??//
    //charge
    Cerr_Q_1 = TMath::Sqrt((mean_qQ_[3] - TMath::Power(mean_qQ_[0], 2)) / (hQ_Nevt->GetBinContent(i_RM)));
    Cerr_Q_2 = TMath::Sqrt(
			   (
			    - 4. * mean_qQ_[0] * ( mean_qQ_[9] - mean_qQ_[3] * mean_qQ_[0] )
			    + 2. * ( mean_qQ_[10] - mean_qQ_[3] * mean_qQ_[1] )
			    - 2. * ( mean_qQ_[11] - mean_qQ_[3] * mean_qQ_[2] )
			    - 4. * mean_qQ_[0] * ( mean_qQ_[4] - mean_qQ_[0] * mean_qQ_[1] )
			    - 2. * ( mean_qQ_[7] - mean_qQ_[1] * mean_qQ_[2] )
			    + ( mean_qQ_[12] - TMath::Power(mean_qQ_[3], 2) )
			    + 4. * TMath::Power(mean_qQ_[0], 2) * ( mean_qQ_[3] - mean_qQ_[0] * mean_qQ_[0] )
			    + 1. * ( mean_qQ_[6] - TMath::Power(mean_qQ_[1], 2) )
			    + 1. * ( mean_qQ_[8] - TMath::Power(mean_qQ_[2], 2) )
			    + 4. * mean_qQ_[0] * ( mean_qQ_[5] - mean_qQ_[0] * mean_qQ_[2] )
			    )
			   / (hQ_Nevt->GetBinContent(i_RM)));
    //proton
    Cerr_B_1 = TMath::Sqrt((mean_qB_[3] - TMath::Power(mean_qB_[0], 2)) / (hB_Nevt->GetBinContent(i_RM)));
    Cerr_B_2 = TMath::Sqrt(
			   (
			    - 4. * mean_qB_[0] * ( mean_qB_[9] - mean_qB_[3] * mean_qB_[0] )
			    + 2. * ( mean_qB_[10] - mean_qB_[3] * mean_qB_[1] )
			    - 2. * ( mean_qB_[11] - mean_qB_[3] * mean_qB_[2] )
			    - 4. * mean_qB_[0] * ( mean_qB_[4] - mean_qB_[0] * mean_qB_[1] )
			    - 2. * ( mean_qB_[7] - mean_qB_[1] * mean_qB_[2] )
			    + ( mean_qB_[12] - TMath::Power(mean_qB_[3], 2) )
			    + 4. * TMath::Power(mean_qB_[0], 2) * ( mean_qB_[3] - mean_qB_[0] * mean_qB_[0] )
			    + 1. * ( mean_qB_[6] - TMath::Power(mean_qB_[1], 2) )
			    + 1. * ( mean_qB_[8] - TMath::Power(mean_qB_[2], 2) )
			    + 4. * mean_qB_[0] * ( mean_qB_[5] - mean_qB_[0] * mean_qB_[2] )
			    )
			   / (hB_Nevt->GetBinContent(i_RM)));
    //kaon
    Cerr_S_1 = TMath::Sqrt((mean_qS_[3] - TMath::Power(mean_qS_[0], 2)) / (hS_Nevt->GetBinContent(i_RM)));
    Cerr_S_2 = TMath::Sqrt(
			   (
			    - 4. * mean_qS_[0] * ( mean_qS_[9] - mean_qS_[3] * mean_qS_[0] )
			    + 2. * ( mean_qS_[10] - mean_qS_[3] * mean_qS_[1] )
			    - 2. * ( mean_qS_[11] - mean_qS_[3] * mean_qS_[2] )
			    - 4. * mean_qS_[0] * ( mean_qS_[4] - mean_qS_[0] * mean_qS_[1] )
			    - 2. * ( mean_qS_[7] - mean_qS_[1] * mean_qS_[2] )
			    + ( mean_qS_[12] - TMath::Power(mean_qS_[3], 2) )
			    + 4. * TMath::Power(mean_qS_[0], 2) * ( mean_qS_[3] - mean_qS_[0] * mean_qS_[0] )
			    + 1. * ( mean_qS_[6] - TMath::Power(mean_qS_[1], 2) )
			    + 1. * ( mean_qS_[8] - TMath::Power(mean_qS_[2], 2) )
			    + 4. * mean_qS_[0] * ( mean_qS_[5] - mean_qS_[0] * mean_qS_[2] )
			    )
			   / (hS_Nevt->GetBinContent(i_RM)));


    //Mixed terms
    //QB
    Cerr_QB_11 = TMath::Sqrt(
			     (
			      - 2. * mixed_mean_q_[1] * ( mixed_mean_q_[14] - mixed_mean_q_[5] * mixed_mean_q_[0] )
			      - 2. * mixed_mean_q_[0] * ( mixed_mean_q_[15] - mixed_mean_q_[5] * mixed_mean_q_[1] )
			      + 2. * ( mixed_mean_q_[16] - mixed_mean_q_[5] * mixed_mean_q_[2] )
			      - 2. * ( mixed_mean_q_[17] - mixed_mean_q_[5] * mixed_mean_q_[3] )
			      + 2. * mixed_mean_q_[1] * mixed_mean_q_[0] * ( mixed_mean_q_[5] - mixed_mean_q_[0] * mixed_mean_q_[1] )
			      - 2. * mixed_mean_q_[1] * ( mixed_mean_q_[6] - mixed_mean_q_[0] * mixed_mean_q_[2] )
			      + 2. * mixed_mean_q_[1] * ( mixed_mean_q_[7] - mixed_mean_q_[0] * mixed_mean_q_[3] )
			      - 2. * mixed_mean_q_[0] * ( mixed_mean_q_[9] - mixed_mean_q_[1] * mixed_mean_q_[2] )
			      + 2. * mixed_mean_q_[0] * ( mixed_mean_q_[10] - mixed_mean_q_[1] * mixed_mean_q_[3] )
			      + 2. * ( mixed_mean_q_[12] - mixed_mean_q_[2] * mixed_mean_q_[3] )
			      + 1. * ( mixed_mean_q_[18] - TMath::Power(mixed_mean_q_[5], 2) )
			      + 1. * TMath::Power(mixed_mean_q_[1] ,2) * ( mixed_mean_q_[4] - TMath::Power(mixed_mean_q_[0], 2) )
			      + 1. * TMath::Power(mixed_mean_q_[0] ,2) * ( mixed_mean_q_[8] - TMath::Power(mixed_mean_q_[1], 2) )
			      + 1. * ( mixed_mean_q_[11] - TMath::Power(mixed_mean_q_[2], 2) )
			      + 1. * ( mixed_mean_q_[13] - TMath::Power(mixed_mean_q_[3], 2) )
			      )
			     / (h_mixed_Nevt[0]->GetBinContent(i_RM)));
    //BS
    Cerr_BS_11 = TMath::Sqrt(
			     (
			      - 2. * mixed_mean_q_[1 + 19] * ( mixed_mean_q_[14 + 19] - mixed_mean_q_[5 + 19] * mixed_mean_q_[0 + 19] )
			      - 2. * mixed_mean_q_[0 + 19] * ( mixed_mean_q_[15 + 19] - mixed_mean_q_[5 + 19] * mixed_mean_q_[1 + 19] )
			      + 2. * ( mixed_mean_q_[16 + 19] - mixed_mean_q_[5 + 19] * mixed_mean_q_[2 + 19] )
			      - 2. * ( mixed_mean_q_[17 + 19] - mixed_mean_q_[5 + 19] * mixed_mean_q_[3 + 19] )
			      + 2. * mixed_mean_q_[1 + 19] * mixed_mean_q_[0 + 19] * ( mixed_mean_q_[5 + 19] - mixed_mean_q_[0 + 19] * mixed_mean_q_[1 + 19] )
			      - 2. * mixed_mean_q_[1 + 19] * ( mixed_mean_q_[6 + 19] - mixed_mean_q_[0 + 19] * mixed_mean_q_[2 + 19] )
			      + 2. * mixed_mean_q_[1 + 19] * ( mixed_mean_q_[7 + 19] - mixed_mean_q_[0 + 19] * mixed_mean_q_[3 + 19] )
			      - 2. * mixed_mean_q_[0 + 19] * ( mixed_mean_q_[9 + 19] - mixed_mean_q_[1 + 19] * mixed_mean_q_[2 + 19] )
			      + 2. * mixed_mean_q_[0 + 19] * ( mixed_mean_q_[10 + 19] - mixed_mean_q_[1 + 19] * mixed_mean_q_[3 + 19] )
			      + 2. * ( mixed_mean_q_[12 + 19] - mixed_mean_q_[2 + 19] * mixed_mean_q_[3 + 19] )
			      + 1. * ( mixed_mean_q_[18 + 19] - TMath::Power(mixed_mean_q_[5 + 19], 2) )
			      + 1. * TMath::Power(mixed_mean_q_[1 + 19] ,2) * ( mixed_mean_q_[4 + 19] - TMath::Power(mixed_mean_q_[0 + 19], 2) )
			      + 1. * TMath::Power(mixed_mean_q_[0 + 19] ,2) * ( mixed_mean_q_[8 + 19] - TMath::Power(mixed_mean_q_[1 + 19], 2) )
			      + 1. * ( mixed_mean_q_[11 + 19] - TMath::Power(mixed_mean_q_[2 + 19], 2) )
			      + 1. * ( mixed_mean_q_[13 + 19] - TMath::Power(mixed_mean_q_[3 + 19], 2) )
			      )
			     / (h_mixed_Nevt[1]->GetBinContent(i_RM)));
    
    //SQ
    Cerr_SQ_11 = TMath::Sqrt(
			     (
			      - 2. * mixed_mean_q_[1 + 2*19] * ( mixed_mean_q_[14 + 2*19] - mixed_mean_q_[5 + 2*19] * mixed_mean_q_[0 + 2*19] )
			      - 2. * mixed_mean_q_[0 + 2*19] * ( mixed_mean_q_[15 + 2*19] - mixed_mean_q_[5 + 2*19] * mixed_mean_q_[1 + 2*19] )
			      + 2. * ( mixed_mean_q_[16 + 2*19] - mixed_mean_q_[5 + 2*19] * mixed_mean_q_[2 + 2*19] )
			      - 2. * ( mixed_mean_q_[17 + 2*19] - mixed_mean_q_[5 + 2*19] * mixed_mean_q_[3 + 2*19] )
			      + 2. * mixed_mean_q_[1 + 2*19] * mixed_mean_q_[0 + 2*19] * ( mixed_mean_q_[5 + 2*19] - mixed_mean_q_[0 + 2*19] * mixed_mean_q_[1 + 2*19] )
			      - 2. * mixed_mean_q_[1 + 2*19] * ( mixed_mean_q_[6 + 2*19] - mixed_mean_q_[0 + 2*19] * mixed_mean_q_[2 + 2*19] )
			      + 2. * mixed_mean_q_[1 + 2*19] * ( mixed_mean_q_[7 + 2*19] - mixed_mean_q_[0 + 2*19] * mixed_mean_q_[3 + 2*19] )
			      - 2. * mixed_mean_q_[0 + 2*19] * ( mixed_mean_q_[9 + 2*19] - mixed_mean_q_[1 + 2*19] * mixed_mean_q_[2 + 2*19] )
			      + 2. * mixed_mean_q_[0 + 2*19] * ( mixed_mean_q_[10 + 2*19] - mixed_mean_q_[1 + 2*19] * mixed_mean_q_[3 + 2*19] )
			      + 2. * ( mixed_mean_q_[12 + 2*19] - mixed_mean_q_[2 + 2*19] * mixed_mean_q_[3 + 2*19] )
			      + 1. * ( mixed_mean_q_[18 + 2*19] - TMath::Power(mixed_mean_q_[5 + 2*19], 2) )
			      + 1. * TMath::Power(mixed_mean_q_[1 + 2*19] ,2) * ( mixed_mean_q_[4 + 2*19] - TMath::Power(mixed_mean_q_[0 + 2*19], 2) )
			      + 1. * TMath::Power(mixed_mean_q_[0 + 2*19] ,2) * ( mixed_mean_q_[8 + 2*19] - TMath::Power(mixed_mean_q_[1 + 2*19], 2) )
			      + 1. * ( mixed_mean_q_[11 + 2*19] - TMath::Power(mixed_mean_q_[2 + 2*19], 2) )
			      + 1. * ( mixed_mean_q_[13 + 2*19] - TMath::Power(mixed_mean_q_[3 + 2*19], 2) )
			      )
			     / (h_mixed_Nevt[2]->GetBinContent(i_RM)));
    

    //Fill it
    //charge
    hQ_C[0]->SetBinContent(i_RM, C_Q_1);
    hQ_C[0]->SetBinError(i_RM, Cerr_Q_1);
    hQ_C[1]->SetBinContent(i_RM, C_Q_2);
    hQ_C[1]->SetBinError(i_RM, Cerr_Q_2);
    //proton
    hB_C[0]->SetBinContent(i_RM, C_B_1);
    hB_C[0]->SetBinError(i_RM, Cerr_B_1);
    hB_C[1]->SetBinContent(i_RM, C_B_2);
    hB_C[1]->SetBinError(i_RM, Cerr_B_2);
    //kaon
    hS_C[0]->SetBinContent(i_RM, C_S_1);
    hS_C[0]->SetBinError(i_RM, Cerr_S_1);
    hS_C[1]->SetBinContent(i_RM, C_S_2);
    hS_C[1]->SetBinError(i_RM, Cerr_S_2);

    ///////
    //Err//
    ///////
    //charge
    if(!isnan(Cerr_Q_1)) hQ_C_err[0]->SetBinContent(i_RM, Cerr_Q_1);
    if(!isnan(Cerr_Q_2)) hQ_C_err[1]->SetBinContent(i_RM, Cerr_Q_2);
    //proton
    if(!isnan(Cerr_B_1)) hB_C_err[0]->SetBinContent(i_RM, Cerr_B_1);
    if(!isnan(Cerr_B_2)) hB_C_err[1]->SetBinContent(i_RM, Cerr_B_2);
    //kaon
    if(!isnan(Cerr_S_1)) hS_C_err[0]->SetBinContent(i_RM, Cerr_S_1);
    if(!isnan(Cerr_S_2)) hS_C_err[1]->SetBinContent(i_RM, Cerr_S_2);
  

    //Mixed terms
    h_mixed_C[0]->SetBinContent(i_RM, C_QB_11);
    h_mixed_C[1]->SetBinContent(i_RM, C_BS_11);
    h_mixed_C[2]->SetBinContent(i_RM, C_SQ_11);
    h_mixed_C[0]->SetBinError(i_RM, Cerr_QB_11);
    h_mixed_C[1]->SetBinError(i_RM, Cerr_BS_11);
    h_mixed_C[2]->SetBinError(i_RM, Cerr_SQ_11);

    // //test(start)
    // cout<<"C_QB_11 = "<<C_QB_11<<", Cerr_QB_11 = "<<Cerr_QB_11<<endl;
    // cout<<"C_B_2 = "<<C_QB_11<<", Cerr_B_2 = "<<Cerr_B_2<<endl;
    // //test(finish)

    if(C_S_2 != 0) {
      h_mixed_C[3]->SetBinContent(i_RM, C_B_2/C_S_2);
      h_mixed_C[3]->SetBinError(i_RM, TMath::Sqrt(TMath::Abs(TMath::Power(1/C_S_2, 2) * Cerr_B_2
							     + TMath::Power(-C_B_2/TMath::Power(C_S_2, 2), 2) * Cerr_S_2
							     + 2 * (1/C_S_2) * (-C_B_2/TMath::Power(C_S_2, 2)) * Cerr_B_2 * Cerr_S_2))
				);//THIS IS WRONG!!!
      // cout<<"C_B_2/C_S_2 = "<<C_B_2/C_S_2<<", err = "<<TMath::Sqrt(TMath::Abs(TMath::Power(1/C_S_2, 2) * Cerr_B_2
      // 	+ TMath::Power(-C_B_2/TMath::Power(C_S_2, 2), 2) * Cerr_S_2
      // 									      + 2 * (1/C_S_2) * (-C_B_2/TMath::Power(C_S_2, 2)) * Cerr_B_2 * Cerr_S_2))<<endl;
    }
    if(C_SQ_11 != 0) {
      h_mixed_C[4]->SetBinContent(i_RM, C_B_2/C_SQ_11);
      h_mixed_C[4]->SetBinError(i_RM, TMath::Sqrt(TMath::Abs(TMath::Power(1/C_SQ_11, 2) * Cerr_B_2
							     + TMath::Power(-C_B_2/TMath::Power(C_SQ_11, 2), 2) * Cerr_SQ_11
							     + 2 * (1/C_SQ_11) * (-C_B_2/TMath::Power(C_SQ_11, 2)) * Cerr_B_2 * Cerr_SQ_11))
				);//THIS IS WRONG!!!
    }
    if(C_B_2 != 0) {
      h_mixed_C[5]->SetBinContent(i_RM, C_QB_11/C_B_2);
      h_mixed_C[5]->SetBinError(i_RM, TMath::Sqrt(TMath::Abs(TMath::Power(1/C_B_2, 2) * Cerr_QB_11
							     + TMath::Power(-C_QB_11/TMath::Power(C_B_2, 2), 2) * Cerr_B_2
							     + 2 * (1/C_B_2) * (-C_QB_11/TMath::Power(C_B_2, 2)) * Cerr_QB_11 * Cerr_B_2))
				);//THIS IS WRONG!!!
    }
    if(C_S_2 != 0) {
      h_mixed_C[6]->SetBinContent(i_RM, C_BS_11/C_S_2);
      h_mixed_C[6]->SetBinError(i_RM, TMath::Sqrt(TMath::Abs(TMath::Power(1/C_S_2, 2) * Cerr_BS_11
							     + TMath::Power(-C_BS_11/TMath::Power(C_S_2, 2), 2) * Cerr_S_2
							     + 2 * (1/C_S_2) * (-C_BS_11/TMath::Power(C_S_2, 2)) * Cerr_BS_11 * Cerr_S_2))
				);//THIS IS WRONG!!!
    }
    if(C_B_2 != 0) {
      h_mixed_C[7]->SetBinContent(i_RM, C_BS_11/C_B_2);
      h_mixed_C[7]->SetBinError(i_RM, TMath::Sqrt(TMath::Abs(TMath::Power(1/C_B_2, 2) * Cerr_BS_11
							     + TMath::Power(-C_BS_11/TMath::Power(C_B_2, 2), 2) * Cerr_B_2
							     + 2 * (1/C_B_2) * (-C_BS_11/TMath::Power(C_B_2, 2)) * Cerr_BS_11 * Cerr_B_2))
				);//THIS IS WRONG!!!
    }
    if(C_S_2 != 0) {
      h_mixed_C[8]->SetBinContent(i_RM, C_SQ_11/C_S_2);
      h_mixed_C[8]->SetBinError(i_RM, TMath::Sqrt(TMath::Abs(TMath::Power(1/C_S_2, 2) * Cerr_SQ_11
							     + TMath::Power(-C_SQ_11/TMath::Power(C_S_2, 2), 2) * Cerr_S_2
							     + 2 * (1/C_S_2) * (-C_SQ_11/TMath::Power(C_S_2, 2)) * Cerr_SQ_11 * Cerr_S_2))
				);//THIS IS WRONG!!!
    }
    ///////
    //Err//
    ///////
    if(!isnan(Cerr_QB_11)) h_mixed_C_err[0]->SetBinContent(i_RM, Cerr_QB_11);
    if(!isnan(Cerr_BS_11)) h_mixed_C_err[1]->SetBinContent(i_RM, Cerr_BS_11);
    if(!isnan(Cerr_SQ_11)) h_mixed_C_err[2]->SetBinContent(i_RM, Cerr_SQ_11);

  }//refMult loop ends

  WriteEffCorrCum(Form("output/cum/%s_eff_corr_cum.root", NameRuZr));

}

//////////////////////////////////////////

void Cum::WriteEffCorrCum(const char* outFile){

  TFile *f_out = new TFile(outFile, "RECREATE");
  f_out->cd();
  for(int i = 0 ; i < Ncums ; i++){
    hQ_C[i]->Write();
    hB_C[i]->Write();
    hS_C[i]->Write();
    ///////
    //Err//
    ///////
    hQ_C_err[i]->Write();
    hB_C_err[i]->Write();
    hS_C_err[i]->Write();
  }
  //Mixed term
  for(int i = 0 ; i < Ncums_mixed ; i++){
    h_mixed_C[i]->Write();
    ///////
    //Err//
    ///////
    h_mixed_C_err[i]->Write();
  }

  hQ_Nevt->Write();
  hB_Nevt->Write();
  hS_Nevt->Write();

  for(int i_mix = 0 ; i_mix < N_mixed ; i_mix++){
    h_mixed_Nevt[i_mix]->Write();
  }
  
  f_out->Close();
  
}

//////////////////////////////////////////

void Cum::GetUnCorrMeans(const char* inFile){
  //get terms in Eq. 62 ~ 67 & 69 in arXiv:1702.07106v3
  TFile *f_in = new TFile(inFile, "READ");
  std::map<const char*, TProfile*>::iterator iter;
  ///charge///
  TIter nextkeyQ(f_in->GetListOfKeys());
  TKey *keyQ;
  while((keyQ = (TKey*)nextkeyQ())){
    TString strName = keyQ->ReadObj()->GetName();
    if(strName.Contains(Form("%s_unCorr", "charge"))){
      strName.Replace(0, strlen(Form("%s_unCorr", "charge"))+1,"");
      // cout<<strName<<endl;
    } else {
      continue;
    }
    int iT = 0;
    while (true) {
      if(iT == N_mean_term) {
	std::cout<<"In Cum::GetUnCorrMeans"<<std::endl;
	std::cout<<"Found bad terms"<<std::endl;
	abort();
      }
      if(strcmp(strName, mean_term_name[iT])==0){
	// cout<<iT<<endl;
	h_mean_qQ_unCorr_[iT] = (TProfile*) keyQ->ReadObj()->Clone();
	break;
      } else {
	iT++;
      }
    }
  }
  ///proton///
  TIter nextkeyB(f_in->GetListOfKeys());
  TKey *keyB;
  while((keyB = (TKey*)nextkeyB())){
    TString strName = keyB->ReadObj()->GetName();
    if(strName.Contains(Form("%s_unCorr", "proton"))){
      strName.Replace(0, strlen(Form("%s_unCorr", "proton"))+1,"");
      // cout<<strName<<endl;
    } else {
      continue;
    }
    int iT = 0;
    while (true) {
      if(iT == N_mean_term) {
	std::cout<<"In Cum::GetUnCorrMeans"<<std::endl;
	std::cout<<"Found bad terms"<<std::endl;
	abort();
      }
      if(strcmp(strName, mean_term_name[iT])==0){
	h_mean_qB_unCorr_[iT] = (TProfile*) keyB->ReadObj()->Clone();
	break;
      } else {
	iT++;
      }
    }
  }
  ///kaon///
  TIter nextkeyS(f_in->GetListOfKeys());
  TKey *keyS;
  while((keyS = (TKey*)nextkeyS())){
    TString strName = keyS->ReadObj()->GetName();
    if(strName.Contains(Form("%s_unCorr", "kaon"))){
      strName.Replace(0, strlen(Form("%s_unCorr", "kaon"))+1,"");
    } else {
      continue;
    }
    int iT = 0;
    while (true) {
      if(iT == N_mean_term) {
	std::cout<<"In Cum::GetUnCorrMeans"<<std::endl;
	std::cout<<"Found bad terms"<<std::endl;
	abort();
      }
      if(strcmp(strName, mean_term_name[iT])==0){
	h_mean_qS_unCorr_[iT] = (TProfile*) keyS->ReadObj()->Clone();
	break;
      } else {
	iT++;
      }
    }
  }
  ///mixed///
  TIter nextkeyM(f_in->GetListOfKeys());
  TKey *keyM;
  while((keyM = (TKey*)nextkeyM())){
    TString strName = keyM->ReadObj()->GetName();
    if(strName.Contains(Form("%s_unCorr", "mixed"))){
      strName.Replace(0, strlen(Form("%s_unCorr", "mixed"))+1,"");
    } else {
      continue;
    }
    int iT = 0;
    while (true) {
      if(iT == N_mixed_mean_term) {
	std::cout<<"In Cum::GetUnCorrMeans"<<std::endl;
	std::cout<<"Found bad terms"<<std::endl;
	abort();
      }
      if(strcmp(strName, mixed_mean_term_name[iT])==0){
	h_mixed_mean_q_unCorr_[iT] = (TProfile*) keyM->ReadObj()->Clone();
	break;
      } else {
	iT++;
      }
    }
  }
  TProfile* entQ = (TProfile*)f_in->Get(Form("%s_unCorr_q_11","charge"));
  TProfile* entB = (TProfile*)f_in->Get(Form("%s_unCorr_q_11","proton"));
  TProfile* entS = (TProfile*)f_in->Get(Form("%s_unCorr_q_11","kaon"));

  TProfile* entQB = (TProfile*)f_in->Get(Form("%s_unCorr_qQB_112","mixed"));//well, you can call anything...
  TProfile* entBS = (TProfile*)f_in->Get(Form("%s_unCorr_qBS_112","mixed"));
  TProfile* entSQ = (TProfile*)f_in->Get(Form("%s_unCorr_qSQ_112","mixed"));

  for(int i=1;i<entQ->GetNbinsX();++i) {
    hQ_Nevt_unCorr->SetBinContent(i, entQ->GetBinEntries(i));//getting No. of Evt per refMult
    hB_Nevt_unCorr->SetBinContent(i, entB->GetBinEntries(i));//getting No. of Evt per refMult
    hS_Nevt_unCorr->SetBinContent(i, entS->GetBinEntries(i));//getting No. of Evt per refMult

    h_mixed_Nevt_unCorr[0]->SetBinContent(i, entQB->GetBinEntries(i));//getting No. of Evt per refMult
    h_mixed_Nevt_unCorr[1]->SetBinContent(i, entBS->GetBinEntries(i));//getting No. of Evt per refMult
    h_mixed_Nevt_unCorr[2]->SetBinContent(i, entSQ->GetBinEntries(i));//getting No. of Evt per refMult
    // cout<<ent->GetBinEntries(i)<<endl;
  }

}

//////////////////////////////////////////

void Cum::CalculateEffUnCorrCum(const char* NameRuZr){
  //calculate eff corr cumulant
  //charge
  double C_Q_1, C_Q_2, C_Q_3, C_Q_4, C_Q_5, C_Q_6;
  double k_Q_1, k_Q_2, k_Q_3, k_Q_4, k_Q_5, k_Q_6;
  double Cerr_Q_1, Cerr_Q_2, Cerr_Q_3, Cerr_Q_4, Cerr_Q_5, Cerr_Q_6;
  double kerr_Q_1, kerr_Q_2, kerr_Q_3, kerr_Q_4, kerr_Q_5, kerr_Q_6;
  double CRerr_Q_21, CRerr_Q_31, CRerr_Q_32, CRerr_Q_42, CRerr_Q_51, CRerr_Q_62;
  double kRerr_Q_21, kRerr_Q_31, kRerr_Q_32, kRerr_Q_41, kRerr_Q_43, kRerr_Q_51, kRerr_Q_54, kRerr_Q_61, kRerr_Q_65;
  //proton
  double C_B_1, C_B_2, C_B_3, C_B_4, C_B_5, C_B_6;
  double k_B_1, k_B_2, k_B_3, k_B_4, k_B_5, k_B_6;
  double Cerr_B_1, Cerr_B_2, Cerr_B_3, Cerr_B_4, Cerr_B_5, Cerr_B_6;
  double kerr_B_1, kerr_B_2, kerr_B_3, kerr_B_4, kerr_B_5, kerr_B_6;
  double CRerr_B_21, CRerr_B_31, CRerr_B_32, CRerr_B_42, CRerr_B_51, CRerr_B_62;
  double kRerr_B_21, kRerr_B_31, kRerr_B_32, kRerr_B_41, kRerr_B_43, kRerr_B_51, kRerr_B_54, kRerr_B_61, kRerr_B_65;
  //kaon
  double C_S_1, C_S_2, C_S_3, C_S_4, C_S_5, C_S_6;
  double k_S_1, k_S_2, k_S_3, k_S_4, k_S_5, k_S_6;
  double Cerr_S_1, Cerr_S_2, Cerr_S_3, Cerr_S_4, Cerr_S_5, Cerr_S_6;
  double kerr_S_1, kerr_S_2, kerr_S_3, kerr_S_4, kerr_S_5, kerr_S_6;
  double CRerr_S_21, CRerr_S_31, CRerr_S_32, CRerr_S_42, CRerr_S_51, CRerr_S_62;
  double kRerr_S_21, kRerr_S_31, kRerr_S_32, kRerr_S_41, kRerr_S_43, kRerr_S_51, kRerr_S_54, kRerr_S_61, kRerr_S_65;

  double mean_qQ_unCorr_[N_mean_term];
  double mean_qB_unCorr_[N_mean_term];
  double mean_qS_unCorr_[N_mean_term];

  //Mixed terms
  double C_QB_11, C_BS_11, C_SQ_11;
  double Cerr_QB_11, Cerr_BS_11, Cerr_SQ_11;
  double mixed_mean_q_unCorr_[N_mixed_mean_term];

  for(int i_RM = 0 ; i_RM < maxMult ; i_RM++){
    cout<<"Cum::CalculateEffUnCorrCum(). Multiplicity: "<<i_RM<<"/"<<maxMult<<endl;
    //initialize
    //charge
    C_Q_1 = C_Q_2 = C_Q_3 = C_Q_4 = C_Q_5 = C_Q_6 = 0. ;
    k_Q_1 = k_Q_2 = k_Q_3 = k_Q_4 = k_Q_5 = k_Q_6 = 0. ;
    Cerr_Q_1 = Cerr_Q_2 = Cerr_Q_3 = Cerr_Q_4 = Cerr_Q_5 = Cerr_Q_6 = 0. ;
    kerr_Q_1 = kerr_Q_2 = kerr_Q_3 = kerr_Q_4 = kerr_Q_5 = kerr_Q_6 = 0. ;
    CRerr_Q_21 = CRerr_Q_31 = CRerr_Q_32 = CRerr_Q_42 = CRerr_Q_51 = CRerr_Q_62 = 0. ;
    kRerr_Q_21 = kRerr_Q_31 = kRerr_Q_32 = kRerr_Q_41 = kRerr_Q_43 = kRerr_Q_51 = kRerr_Q_54 = kRerr_Q_61 = kRerr_Q_65 = 0. ;
    //proton
    C_B_1 = C_B_2 = C_B_3 = C_B_4 = C_B_5 = C_B_6 = 0. ;
    k_B_1 = k_B_2 = k_B_3 = k_B_4 = k_B_5 = k_B_6 = 0. ;
    Cerr_B_1 = Cerr_B_2 = Cerr_B_3 = Cerr_B_4 = Cerr_B_5 = Cerr_B_6 = 0. ;
    kerr_B_1 = kerr_B_2 = kerr_B_3 = kerr_B_4 = kerr_B_5 = kerr_B_6 = 0. ;
    CRerr_B_21 = CRerr_B_31 = CRerr_B_32 = CRerr_B_42 = CRerr_B_51 = CRerr_B_62 = 0. ;
    kRerr_B_21 = kRerr_B_31 = kRerr_B_32 = kRerr_B_41 = kRerr_B_43 = kRerr_B_51 = kRerr_B_54 = kRerr_B_61 = kRerr_B_65 = 0. ;
    //kaon
    C_S_1 = C_S_2 = C_S_3 = C_S_4 = C_S_5 = C_S_6 = 0. ;
    k_S_1 = k_S_2 = k_S_3 = k_S_4 = k_S_5 = k_S_6 = 0. ;
    Cerr_S_1 = Cerr_S_2 = Cerr_S_3 = Cerr_S_4 = Cerr_S_5 = Cerr_S_6 = 0. ;
    kerr_S_1 = kerr_S_2 = kerr_S_3 = kerr_S_4 = kerr_S_5 = kerr_S_6 = 0. ;
    CRerr_S_21 = CRerr_S_31 = CRerr_S_32 = CRerr_S_42 = CRerr_S_51 = CRerr_S_62 = 0. ;
    kRerr_S_21 = kRerr_S_31 = kRerr_S_32 = kRerr_S_41 = kRerr_S_43 = kRerr_S_51 = kRerr_S_54 = kRerr_S_61 = kRerr_S_65 = 0. ;

    //Mixed terms
    C_QB_11 = C_BS_11 = C_SQ_11 = 0.;
    Cerr_QB_11 = Cerr_BS_11 = Cerr_SQ_11 =0.;

    //get terms in Eq. 62 ~ 67 in arXiv:1702.07106v3
    // int NevtQ = h_mean_qQ_[0]->GetBinEntries(i_RM);
    // int NevtB = h_mean_qB_[0]->GetBinEntries(i_RM);
    // int NevtS = h_mean_qS_[0]->GetBinEntries(i_RM);
    // cout<<NevtQ<<endl;
    for(int i_term = 0 ; i_term < N_mean_term ; i_term++){
      mean_qQ_unCorr_[i_term] = h_mean_qQ_unCorr_[i_term]->GetBinContent(i_RM);
      mean_qB_unCorr_[i_term] = h_mean_qB_unCorr_[i_term]->GetBinContent(i_RM);
      mean_qS_unCorr_[i_term] = h_mean_qS_unCorr_[i_term]->GetBinContent(i_RM);
    }//95 terms

    //Mixed terms
    for(int i_term = 0 ; i_term < N_mixed_mean_term ; i_term++){
      mixed_mean_q_unCorr_[i_term] = h_mixed_mean_q_unCorr_[i_term]->GetBinContent(i_RM);
      // cout<<i_term<<", "<<h_mixed_mean_q_unCorr_[i_term]->GetName()<<endl;
    }

    //C1 ~ C6
    C_Q_1 = mean_qQ_unCorr_[0];
    C_B_1 = mean_qB_unCorr_[0];
    C_S_1 = mean_qS_unCorr_[0];

    C_Q_2 = mean_qQ_unCorr_[3] -  TMath::Power(mean_qQ_unCorr_[0], 2) + mean_qQ_unCorr_[1] - mean_qQ_unCorr_[2];
    C_B_2 = mean_qB_unCorr_[3] -  TMath::Power(mean_qB_unCorr_[0], 2) + mean_qB_unCorr_[1] - mean_qB_unCorr_[2];
    C_S_2 = mean_qS_unCorr_[3] -  TMath::Power(mean_qS_unCorr_[0], 2) + mean_qS_unCorr_[1] - mean_qS_unCorr_[2];

    // cout<<C_Q_1<<endl;
    // cout<<C_B_1<<endl;
    // cout<<C_S_1<<endl;

    //Mixed term
    // C_QB_11 = mixed_mean_q_unCorr_[0] + mixed_mean_q_unCorr_[1] - mixed_mean_q_unCorr_[2];
    // C_BS_11 = mixed_mean_q_unCorr_[3] + mixed_mean_q_unCorr_[4] - mixed_mean_q_unCorr_[5];
    // C_SQ_11 = mixed_mean_q_unCorr_[6] + mixed_mean_q_unCorr_[7] - mixed_mean_q_unCorr_[8];

    // C_QB_11 = (mixed_mean_q_unCorr_[12]   - mixed_mean_q_unCorr_[0+2*0]*mixed_mean_q_unCorr_[1+2*0]) + mixed_mean_q_unCorr_[6+2*0] - mixed_mean_q_unCorr_[7+2*0];
    // C_BS_11 = (mixed_mean_q_unCorr_[12+1] - mixed_mean_q_unCorr_[0+2*1]*mixed_mean_q_unCorr_[1+2*1]) + mixed_mean_q_unCorr_[6+2*1] - mixed_mean_q_unCorr_[7+2*1];
    // C_SQ_11 = (mixed_mean_q_unCorr_[12+2] - mixed_mean_q_unCorr_[0+2*2]*mixed_mean_q_unCorr_[1+2*2]) + mixed_mean_q_unCorr_[6+2*2] - mixed_mean_q_unCorr_[7+2*2];

    C_QB_11 = (mixed_mean_q_unCorr_[5]   - mixed_mean_q_unCorr_[0]*mixed_mean_q_unCorr_[1]) + mixed_mean_q_unCorr_[2] - mixed_mean_q_unCorr_[3];
    C_BS_11 = (mixed_mean_q_unCorr_[5 + 19] - mixed_mean_q_unCorr_[0 + 19]*mixed_mean_q_unCorr_[1 + 19]) + mixed_mean_q_unCorr_[2 + 19] - mixed_mean_q_unCorr_[3 + 19];
    C_SQ_11 = (mixed_mean_q_unCorr_[5 + 2*19] - mixed_mean_q_unCorr_[0 + 2*19]*mixed_mean_q_unCorr_[1 + 2*19]) + mixed_mean_q_unCorr_[2 + 2*19] - mixed_mean_q_unCorr_[3 + 2*19];

    // cout<<C_QB_11<<endl;
    // cout<<C_BS_11<<endl;
    // cout<<C_SQ_11<<endl;
    // cout<<mixed_mean_q_unCorr_[0]<<endl;
    // cout<<mixed_mean_q_unCorr_[3]<<endl;
    // cout<<mixed_mean_q_unCorr_[6]<<endl;

    ///////
    //Err//
    ///////
    //??AM I getting confused with the 0th bin and 1st bin??//
    //charge
    Cerr_Q_1 = TMath::Sqrt((mean_qQ_unCorr_[3] - TMath::Power(mean_qQ_unCorr_[0], 2)) / (hQ_Nevt->GetBinContent(i_RM)));
    Cerr_Q_2 = TMath::Sqrt(
			   (
			    - 4. * mean_qQ_unCorr_[0] * ( mean_qQ_unCorr_[9] - mean_qQ_unCorr_[3] * mean_qQ_unCorr_[0] )
			    + 2. * ( mean_qQ_unCorr_[10] - mean_qQ_unCorr_[3] * mean_qQ_unCorr_[1] )
			    - 2. * ( mean_qQ_unCorr_[11] - mean_qQ_unCorr_[3] * mean_qQ_unCorr_[2] )
			    - 4. * mean_qQ_unCorr_[0] * ( mean_qQ_unCorr_[4] - mean_qQ_unCorr_[0] * mean_qQ_unCorr_[1] )
			    - 2. * ( mean_qQ_unCorr_[7] - mean_qQ_unCorr_[1] * mean_qQ_unCorr_[2] )
			    + ( mean_qQ_unCorr_[12] - TMath::Power(mean_qQ_unCorr_[3], 2) )
			    + 4. * TMath::Power(mean_qQ_unCorr_[0], 2) * ( mean_qQ_unCorr_[3] - mean_qQ_unCorr_[0] * mean_qQ_unCorr_[0] )
			    + 1. * ( mean_qQ_unCorr_[6] - TMath::Power(mean_qQ_unCorr_[1], 2) )
			    + 1. * ( mean_qQ_unCorr_[8] - TMath::Power(mean_qQ_unCorr_[2], 2) )
			    + 4. * mean_qQ_unCorr_[0] * ( mean_qQ_unCorr_[5] - mean_qQ_unCorr_[0] * mean_qQ_unCorr_[2] )
			    )
			   / (hQ_Nevt->GetBinContent(i_RM)));
    //proton
    Cerr_B_1 = TMath::Sqrt((mean_qB_unCorr_[3] - TMath::Power(mean_qB_unCorr_[0], 2)) / (hB_Nevt->GetBinContent(i_RM)));
    Cerr_B_2 = TMath::Sqrt(
			   (
			    - 4. * mean_qB_unCorr_[0] * ( mean_qB_unCorr_[9] - mean_qB_unCorr_[3] * mean_qB_unCorr_[0] )
			    + 2. * ( mean_qB_unCorr_[10] - mean_qB_unCorr_[3] * mean_qB_unCorr_[1] )
			    - 2. * ( mean_qB_unCorr_[11] - mean_qB_unCorr_[3] * mean_qB_unCorr_[2] )
			    - 4. * mean_qB_unCorr_[0] * ( mean_qB_unCorr_[4] - mean_qB_unCorr_[0] * mean_qB_unCorr_[1] )
			    - 2. * ( mean_qB_unCorr_[7] - mean_qB_unCorr_[1] * mean_qB_unCorr_[2] )
			    + ( mean_qB_unCorr_[12] - TMath::Power(mean_qB_unCorr_[3], 2) )
			    + 4. * TMath::Power(mean_qB_unCorr_[0], 2) * ( mean_qB_unCorr_[3] - mean_qB_unCorr_[0] * mean_qB_unCorr_[0] )
			    + 1. * ( mean_qB_unCorr_[6] - TMath::Power(mean_qB_unCorr_[1], 2) )
			    + 1. * ( mean_qB_unCorr_[8] - TMath::Power(mean_qB_unCorr_[2], 2) )
			    + 4. * mean_qB_unCorr_[0] * ( mean_qB_unCorr_[5] - mean_qB_unCorr_[0] * mean_qB_unCorr_[2] )
			    )
			   / (hB_Nevt->GetBinContent(i_RM)));
    //kaon
    Cerr_S_1 = TMath::Sqrt((mean_qS_unCorr_[3] - TMath::Power(mean_qS_unCorr_[0], 2)) / (hS_Nevt->GetBinContent(i_RM)));
    Cerr_S_2 = TMath::Sqrt(
			   (
			    - 4. * mean_qS_unCorr_[0] * ( mean_qS_unCorr_[9] - mean_qS_unCorr_[3] * mean_qS_unCorr_[0] )
			    + 2. * ( mean_qS_unCorr_[10] - mean_qS_unCorr_[3] * mean_qS_unCorr_[1] )
			    - 2. * ( mean_qS_unCorr_[11] - mean_qS_unCorr_[3] * mean_qS_unCorr_[2] )
			    - 4. * mean_qS_unCorr_[0] * ( mean_qS_unCorr_[4] - mean_qS_unCorr_[0] * mean_qS_unCorr_[1] )
			    - 2. * ( mean_qS_unCorr_[7] - mean_qS_unCorr_[1] * mean_qS_unCorr_[2] )
			    + ( mean_qS_unCorr_[12] - TMath::Power(mean_qS_unCorr_[3], 2) )
			    + 4. * TMath::Power(mean_qS_unCorr_[0], 2) * ( mean_qS_unCorr_[3] - mean_qS_unCorr_[0] * mean_qS_unCorr_[0] )
			    + 1. * ( mean_qS_unCorr_[6] - TMath::Power(mean_qS_unCorr_[1], 2) )
			    + 1. * ( mean_qS_unCorr_[8] - TMath::Power(mean_qS_unCorr_[2], 2) )
			    + 4. * mean_qS_unCorr_[0] * ( mean_qS_unCorr_[5] - mean_qS_unCorr_[0] * mean_qS_unCorr_[2] )
			    )
			   / (hS_Nevt->GetBinContent(i_RM)));


    //Mixed terms
    //QB
    Cerr_QB_11 = TMath::Sqrt(

			     - 2. * mixed_mean_q_unCorr_[1] * ( mixed_mean_q_unCorr_[14] - mixed_mean_q_unCorr_[5] * mixed_mean_q_unCorr_[0] )
			     - 2. * mixed_mean_q_unCorr_[0] * ( mixed_mean_q_unCorr_[15] - mixed_mean_q_unCorr_[5] * mixed_mean_q_unCorr_[1] )
			     + 2. * ( mixed_mean_q_unCorr_[16] - mixed_mean_q_unCorr_[5] * mixed_mean_q_unCorr_[2] )
			     - 2. * ( mixed_mean_q_unCorr_[17] - mixed_mean_q_unCorr_[5] * mixed_mean_q_unCorr_[3] )
			     + 2. * mixed_mean_q_unCorr_[1] * mixed_mean_q_unCorr_[0] * ( mixed_mean_q_unCorr_[5] - mixed_mean_q_unCorr_[0] * mixed_mean_q_unCorr_[1] )
			     - 2. * mixed_mean_q_unCorr_[1] * ( mixed_mean_q_unCorr_[6] - mixed_mean_q_unCorr_[0] * mixed_mean_q_unCorr_[2] )
			     + 2. * mixed_mean_q_unCorr_[1] * ( mixed_mean_q_unCorr_[7] - mixed_mean_q_unCorr_[0] * mixed_mean_q_unCorr_[3] )
			     - 2. * mixed_mean_q_unCorr_[0] * ( mixed_mean_q_unCorr_[9] - mixed_mean_q_unCorr_[1] * mixed_mean_q_unCorr_[2] )
			     + 2. * mixed_mean_q_unCorr_[0] * ( mixed_mean_q_unCorr_[10] - mixed_mean_q_unCorr_[1] * mixed_mean_q_unCorr_[3] )
			     + 2. * ( mixed_mean_q_unCorr_[12] - mixed_mean_q_unCorr_[2] * mixed_mean_q_unCorr_[3] )
			     + 1. * ( mixed_mean_q_unCorr_[18] - TMath::Power(mixed_mean_q_unCorr_[5], 2) )
			     + 1. * TMath::Power(mixed_mean_q_unCorr_[1] ,2) * ( mixed_mean_q_unCorr_[4] - TMath::Power(mixed_mean_q_unCorr_[0], 2) )
			     + 1. * TMath::Power(mixed_mean_q_unCorr_[0] ,2) * ( mixed_mean_q_unCorr_[8] - TMath::Power(mixed_mean_q_unCorr_[1], 2) )
			     + 1. * ( mixed_mean_q_unCorr_[11] - TMath::Power(mixed_mean_q_unCorr_[2], 2) )
			     + 1. * ( mixed_mean_q_unCorr_[13] - TMath::Power(mixed_mean_q_unCorr_[3], 2) )

			     / (h_mixed_Nevt[0]->GetBinContent(i_RM)));
    //BS
    Cerr_BS_11 = TMath::Sqrt(

			     - 2. * mixed_mean_q_unCorr_[1 + 19] * ( mixed_mean_q_unCorr_[14 + 19] - mixed_mean_q_unCorr_[5 + 19] * mixed_mean_q_unCorr_[0 + 19] )
			     - 2. * mixed_mean_q_unCorr_[0 + 19] * ( mixed_mean_q_unCorr_[15 + 19] - mixed_mean_q_unCorr_[5 + 19] * mixed_mean_q_unCorr_[1 + 19] )
			     + 2. * ( mixed_mean_q_unCorr_[16 + 19] - mixed_mean_q_unCorr_[5 + 19] * mixed_mean_q_unCorr_[2 + 19] )
			     - 2. * ( mixed_mean_q_unCorr_[17 + 19] - mixed_mean_q_unCorr_[5 + 19] * mixed_mean_q_unCorr_[3 + 19] )
			     + 2. * mixed_mean_q_unCorr_[1 + 19] * mixed_mean_q_unCorr_[0 + 19] * ( mixed_mean_q_unCorr_[5 + 19] - mixed_mean_q_unCorr_[0 + 19] * mixed_mean_q_unCorr_[1 + 19] )
			     - 2. * mixed_mean_q_unCorr_[1 + 19] * ( mixed_mean_q_unCorr_[6 + 19] - mixed_mean_q_unCorr_[0 + 19] * mixed_mean_q_unCorr_[2 + 19] )
			     + 2. * mixed_mean_q_unCorr_[1 + 19] * ( mixed_mean_q_unCorr_[7 + 19] - mixed_mean_q_unCorr_[0 + 19] * mixed_mean_q_unCorr_[3 + 19] )
			     - 2. * mixed_mean_q_unCorr_[0 + 19] * ( mixed_mean_q_unCorr_[9 + 19] - mixed_mean_q_unCorr_[1 + 19] * mixed_mean_q_unCorr_[2 + 19] )
			     + 2. * mixed_mean_q_unCorr_[0 + 19] * ( mixed_mean_q_unCorr_[10 + 19] - mixed_mean_q_unCorr_[1 + 19] * mixed_mean_q_unCorr_[3 + 19] )
			     + 2. * ( mixed_mean_q_unCorr_[12 + 19] - mixed_mean_q_unCorr_[2 + 19] * mixed_mean_q_unCorr_[3 + 19] )
			     + 1. * ( mixed_mean_q_unCorr_[18 + 19] - TMath::Power(mixed_mean_q_unCorr_[5 + 19], 2) )
			     + 1. * TMath::Power(mixed_mean_q_unCorr_[1 + 19] ,2) * ( mixed_mean_q_unCorr_[4 + 19] - TMath::Power(mixed_mean_q_unCorr_[0 + 19], 2) )
			     + 1. * TMath::Power(mixed_mean_q_unCorr_[0 + 19] ,2) * ( mixed_mean_q_unCorr_[8 + 19] - TMath::Power(mixed_mean_q_unCorr_[1 + 19], 2) )
			     + 1. * ( mixed_mean_q_unCorr_[11 + 19] - TMath::Power(mixed_mean_q_unCorr_[2 + 19], 2) )
			     + 1. * ( mixed_mean_q_unCorr_[13 + 19] - TMath::Power(mixed_mean_q_unCorr_[3 + 19], 2) )

			     / (h_mixed_Nevt[1]->GetBinContent(i_RM)));
    
    //SQ
    Cerr_SQ_11 = TMath::Sqrt(

			     - 2. * mixed_mean_q_unCorr_[1 + 2*19] * ( mixed_mean_q_unCorr_[14 + 2*19] - mixed_mean_q_unCorr_[5 + 2*19] * mixed_mean_q_unCorr_[0 + 2*19] )
			     - 2. * mixed_mean_q_unCorr_[0 + 2*19] * ( mixed_mean_q_unCorr_[15 + 2*19] - mixed_mean_q_unCorr_[5 + 2*19] * mixed_mean_q_unCorr_[1 + 2*19] )
			     + 2. * ( mixed_mean_q_unCorr_[16 + 2*19] - mixed_mean_q_unCorr_[5 + 2*19] * mixed_mean_q_unCorr_[2 + 2*19] )
			     - 2. * ( mixed_mean_q_unCorr_[17 + 2*19] - mixed_mean_q_unCorr_[5 + 2*19] * mixed_mean_q_unCorr_[3 + 2*19] )
			     + 2. * mixed_mean_q_unCorr_[1 + 2*19] * mixed_mean_q_unCorr_[0 + 2*19] * ( mixed_mean_q_unCorr_[5 + 2*19] - mixed_mean_q_unCorr_[0 + 2*19] * mixed_mean_q_unCorr_[1 + 2*19] )
			     - 2. * mixed_mean_q_unCorr_[1 + 2*19] * ( mixed_mean_q_unCorr_[6 + 2*19] - mixed_mean_q_unCorr_[0 + 2*19] * mixed_mean_q_unCorr_[2 + 2*19] )
			     + 2. * mixed_mean_q_unCorr_[1 + 2*19] * ( mixed_mean_q_unCorr_[7 + 2*19] - mixed_mean_q_unCorr_[0 + 2*19] * mixed_mean_q_unCorr_[3 + 2*19] )
			     - 2. * mixed_mean_q_unCorr_[0 + 2*19] * ( mixed_mean_q_unCorr_[9 + 2*19] - mixed_mean_q_unCorr_[1 + 2*19] * mixed_mean_q_unCorr_[2 + 2*19] )
			     + 2. * mixed_mean_q_unCorr_[0 + 2*19] * ( mixed_mean_q_unCorr_[10 + 2*19] - mixed_mean_q_unCorr_[1 + 2*19] * mixed_mean_q_unCorr_[3 + 2*19] )
			     + 2. * ( mixed_mean_q_unCorr_[12 + 2*19] - mixed_mean_q_unCorr_[2 + 2*19] * mixed_mean_q_unCorr_[3 + 2*19] )
			     + 1. * ( mixed_mean_q_unCorr_[18 + 2*19] - TMath::Power(mixed_mean_q_unCorr_[5 + 2*19], 2) )
			     + 1. * TMath::Power(mixed_mean_q_unCorr_[1 + 2*19] ,2) * ( mixed_mean_q_unCorr_[4 + 2*19] - TMath::Power(mixed_mean_q_unCorr_[0 + 2*19], 2) )
			     + 1. * TMath::Power(mixed_mean_q_unCorr_[0 + 2*19] ,2) * ( mixed_mean_q_unCorr_[8 + 2*19] - TMath::Power(mixed_mean_q_unCorr_[1 + 2*19], 2) )
			     + 1. * ( mixed_mean_q_unCorr_[11 + 2*19] - TMath::Power(mixed_mean_q_unCorr_[2 + 2*19], 2) )
			     + 1. * ( mixed_mean_q_unCorr_[13 + 2*19] - TMath::Power(mixed_mean_q_unCorr_[3 + 2*19], 2) )

			     / (h_mixed_Nevt[2]->GetBinContent(i_RM)));

    //Fill it
    //C1
    hQ_C_unCorr[0]->SetBinContent(i_RM, C_Q_1);
    hB_C_unCorr[0]->SetBinContent(i_RM, C_B_1);
    hS_C_unCorr[0]->SetBinContent(i_RM, C_S_1);
    //C2
    hQ_C_unCorr[1]->SetBinContent(i_RM, C_Q_2);
    hB_C_unCorr[1]->SetBinContent(i_RM, C_B_2);
    hS_C_unCorr[1]->SetBinContent(i_RM, C_S_2);

    ///////
    //Err//
    ///////
    //charge
    if(!isnan(Cerr_Q_1)) hQ_C_unCorr_err[0]->SetBinContent(i_RM, Cerr_Q_1);
    if(!isnan(Cerr_Q_2)) hQ_C_unCorr_err[1]->SetBinContent(i_RM, Cerr_Q_2);
    //proton
    if(!isnan(Cerr_B_1)) hB_C_unCorr_err[0]->SetBinContent(i_RM, Cerr_B_1);
    if(!isnan(Cerr_B_2)) hB_C_unCorr_err[1]->SetBinContent(i_RM, Cerr_B_2);
    //kaon
    if(!isnan(Cerr_S_1)) hS_C_unCorr_err[0]->SetBinContent(i_RM, Cerr_S_1);
    if(!isnan(Cerr_S_2)) hS_C_unCorr_err[1]->SetBinContent(i_RM, Cerr_S_2);

    //Mixed terms
    h_mixed_C_unCorr[0]->SetBinContent(i_RM, C_QB_11);
    h_mixed_C_unCorr[1]->SetBinContent(i_RM, C_BS_11);
    h_mixed_C_unCorr[2]->SetBinContent(i_RM, C_SQ_11);

    ///////
    //Err//
    ///////
    if(!isnan(Cerr_QB_11)) h_mixed_C_unCorr_err[0]->SetBinContent(i_RM, Cerr_QB_11);
    if(!isnan(Cerr_BS_11)) h_mixed_C_unCorr_err[1]->SetBinContent(i_RM, Cerr_BS_11);
    if(!isnan(Cerr_SQ_11)) h_mixed_C_unCorr_err[2]->SetBinContent(i_RM, Cerr_SQ_11);

  }//refMult loop ends

  WriteEffUnCorrCum(Form("output/cum/%s_eff_unCorr_cum.root", NameRuZr));
  
}

//////////////////////////////////////////

void Cum::WriteEffUnCorrCum(const char* outFile){

  TFile *f_out = new TFile(outFile, "RECREATE");
  f_out->cd();
  for(int i = 0 ; i < Ncums ; i++){
    hQ_C_unCorr[i]->Write();
    hB_C_unCorr[i]->Write();
    hS_C_unCorr[i]->Write();
    ///////
    //Err//
    ///////
    hQ_C_unCorr_err[i]->Write();
    hB_C_unCorr_err[i]->Write();
    hS_C_unCorr_err[i]->Write();
  }
  //Mixed term
  for(int i = 0 ; i < Ncums_mixed ; i++){
    h_mixed_C_unCorr[i]->Write();
    ///////
    //Err//
    ///////
    h_mixed_C_unCorr_err[i]->Write();
  }

  hQ_Nevt_unCorr->Write();
  hB_Nevt_unCorr->Write();
  hS_Nevt_unCorr->Write();

  for(int i_mix = 0 ; i_mix < N_mixed ; i_mix++){
    h_mixed_Nevt_unCorr[i_mix]->Write();
  }

  f_out->Close();
  
}

//======================================//
//////////////////////////////////////////
//======================================//

///////////
//EPDmult//
///////////
void Cum::GetEPDmultMeans(const char* inFile){
  //get terms in Eq. 62 ~ 67 & 69 in arXiv:1702.07106v3
  TFile *f_in = new TFile(inFile, "READ");
  std::map<const char*, TProfile*>::iterator iter;
  // ///charge///
  // TIter nextkeyQ(f_in->GetListOfKeys());
  // TKey *keyQ;
  // while((keyQ = (TKey*)nextkeyQ())){
  //   TString strName = keyQ->ReadObj()->GetName();
  //   if(strName.Contains(Form("%s_epd", "charge"))){
  //     strName.Replace(0, strlen(Form("%s_epd", "charge"))+1,"");
  //     // cout<<strName<<endl;
  //   } else {
  //     continue;
  //   }
  //   int iT = 0;
  //   while (true) {
  //     if(iT == N_mean_term_noErr) {
  // 	std::cout<<"In Cum::GetEPDmultMeans"<<std::endl;
  // 	std::cout<<"Found bad terms"<<std::endl;
  // 	abort();
  //     }
  //     if(strcmp(strName, mean_term_name_noErr[iT])==0){
  // 	h_mean_qQ_epd_[iT] = (TProfile*) keyQ->ReadObj()->Clone();
  // 	break;
  //     } else {
  // 	iT++;
  //     }
  //   }
  // }
  ///proton///
  TIter nextkeyB(f_in->GetListOfKeys());
  TKey *keyB;
  while((keyB = (TKey*)nextkeyB())){
    TString strName = keyB->ReadObj()->GetName();
    if(strName.Contains(Form("%s_epd", "proton"))){
      strName.Replace(0, strlen(Form("%s_epd", "proton"))+1,"");
      // cout<<strName<<endl;
    } else {
      continue;
    }
    int iT = 0;
    while (true) {
      if(iT == N_mean_term_noErr) {
	std::cout<<"In Cum::GetEPDmultMeans"<<std::endl;
	std::cout<<"Found bad terms"<<std::endl;
	abort();
      }
      if(strcmp(strName, mean_term_name_noErr[iT])==0){
	h_mean_qB_epd_[iT] = (TProfile*) keyB->ReadObj()->Clone();
	// cout<<iT<<", "<<h_mean_qB_epd_[iT]->GetEntries()<<endl;
	break;
      } else {
	iT++;
      }
    }
  }
  // ///kaon///
  // TIter nextkeyS(f_in->GetListOfKeys());
  // TKey *keyS;
  // while((keyS = (TKey*)nextkeyS())){
  //   TString strName = keyS->ReadObj()->GetName();
  //   if(strName.Contains(Form("%s_epd", "kaon"))){
  //     strName.Replace(0, strlen(Form("%s_epd", "kaon"))+1,"");
  //     // cout<<strName<<endl;
  //   } else {
  //     continue;
  //   }
  //   int iT = 0;
  //   while (true) {
  //     if(iT == N_mean_term_noErr) {
  // 	std::cout<<"In Cum::GetEPDmultMeans"<<std::endl;
  // 	std::cout<<"Found bad terms"<<std::endl;
  // 	abort();
  //     }
  //     if(strcmp(strName, mean_term_name_noErr[iT])==0){
  // 	h_mean_qS_epd_[iT] = (TProfile*) keyS->ReadObj()->Clone();
  // 	break;
  //     } else {
  // 	iT++;
  //     }
  //   }
  // }

  // TProfile* entQ = (TProfile*)f_in->Get(Form("%s_epd_q01_01","charge"));
  TProfile* entB = (TProfile*)f_in->Get(Form("%s_epd_q01_01","proton"));
  // TProfile* entS = (TProfile*)f_in->Get(Form("%s_epd_q01_01","kaon"));
  for(int i=1;i<entB->GetNbinsX();++i) {
    // hQ_Nevt_epd->SetBinContent(i, entQ->GetBinEntries(i));//getting No. of Evt per refMult
    hB_Nevt_epd->SetBinContent(i, entB->GetBinEntries(i));//getting No. of Evt per refMult
    // hS_Nevt_epd->SetBinContent(i, entS->GetBinEntries(i));//getting No. of Evt per refMult
    // cout<<ent->GetBinEntries(i)<<endl;
  }
}

//////////////////////////////////////////

void Cum::CalculateEffCorrEPDmultCum(const char* NameRuZr, int dca_cut, int nhitsfit_cut, int nsp_cut, int m2low_cut, int m2up_cut, int ptmax_cut, int ymax_cut, int detefflow_err, int deteffhigh_err){
  //calculate eff corr cumulant
  // //charge
  // double C_Q_1, C_Q_2, C_Q_3, C_Q_4, C_Q_5, C_Q_6;
  // double k_Q_1, k_Q_2, k_Q_3, k_Q_4, k_Q_5, k_Q_6;
  // double Cerr_Q_1, Cerr_Q_2, Cerr_Q_3, Cerr_Q_4, Cerr_Q_5, Cerr_Q_6;
  // double kerr_Q_1, kerr_Q_2, kerr_Q_3, kerr_Q_4, kerr_Q_5, kerr_Q_6;
  // double CRerr_Q_21, CRerr_Q_31, CRerr_Q_32, CRerr_Q_42, CRerr_Q_51, CRerr_Q_62;
  // double kRerr_Q_21, kRerr_Q_31, kRerr_Q_32, kRerr_Q_41, kRerr_Q_43, kRerr_Q_51, kRerr_Q_54, kRerr_Q_61, kRerr_Q_65;
  //proton
  double C_B_1, C_B_2, C_B_3, C_B_4, C_B_5, C_B_6;
  double k_B_1, k_B_2, k_B_3, k_B_4, k_B_5, k_B_6;
  double Cerr_B_1, Cerr_B_2, Cerr_B_3, Cerr_B_4, Cerr_B_5, Cerr_B_6;
  double kerr_B_1, kerr_B_2, kerr_B_3, kerr_B_4, kerr_B_5, kerr_B_6;
  double CRerr_B_21, CRerr_B_31, CRerr_B_32, CRerr_B_42, CRerr_B_51, CRerr_B_62;
  double kRerr_B_21, kRerr_B_31, kRerr_B_32, kRerr_B_41, kRerr_B_43, kRerr_B_51, kRerr_B_54, kRerr_B_61, kRerr_B_65;
  // //kaon
  // double C_S_1, C_S_2, C_S_3, C_S_4, C_S_5, C_S_6;
  // double k_S_1, k_S_2, k_S_3, k_S_4, k_S_5, k_S_6;
  // double Cerr_S_1, Cerr_S_2, Cerr_S_3, Cerr_S_4, Cerr_S_5, Cerr_S_6;
  // double kerr_S_1, kerr_S_2, kerr_S_3, kerr_S_4, kerr_S_5, kerr_S_6;
  // double CRerr_S_21, CRerr_S_31, CRerr_S_32, CRerr_S_42, CRerr_S_51, CRerr_S_62;
  // double kRerr_S_21, kRerr_S_31, kRerr_S_32, kRerr_S_41, kRerr_S_43, kRerr_S_51, kRerr_S_54, kRerr_S_61, kRerr_S_65;

  // double mean_qQ_epd_[N_mean_term_noErr];
  double mean_qB_epd_[N_mean_term_noErr];
  // double mean_qS_epd_[N_mean_term_noErr];

  // CalCum *mCalCumQ = new CalCum();//charge
  CalCum *mCalCumB = new CalCum();//proton
  // CalCum *mCalCumS = new CalCum();//kaon

  //temporary
  int nan_counter_cr51 = 0;
  int nan_counter_cr62 = 0;
  
  for(int i_RM = 0 ; i_RM < maxMult ; i_RM++){
    // mCalCumQ->Init();
    mCalCumB->Init();
    // mCalCumS->Init();
    cout<<"Cum::CalculateEffCorrEPDmultCum(). Multiplicity: "<<i_RM<<"/"<<maxMult<<endl;
    //initialize
    // //charge
    // C_Q_1 = C_Q_2 = C_Q_3 = C_Q_4 = C_Q_5 = C_Q_6 = 0. ;
    // k_Q_1 = k_Q_2 = k_Q_3 = k_Q_4 = k_Q_5 = k_Q_6 = 0. ;
    // Cerr_Q_1 = Cerr_Q_2 = Cerr_Q_3 = Cerr_Q_4 = Cerr_Q_5 = Cerr_Q_6 = 0. ;
    // kerr_Q_1 = kerr_Q_2 = kerr_Q_3 = kerr_Q_4 = kerr_Q_5 = kerr_Q_6 = 0. ;
    // CRerr_Q_21 = CRerr_Q_31 = CRerr_Q_32 = CRerr_Q_42 = CRerr_Q_51 = CRerr_Q_62 = 0. ;
    // kRerr_Q_21 = kRerr_Q_31 = kRerr_Q_32 = kRerr_Q_41 = kRerr_Q_43 = kRerr_Q_51 = kRerr_Q_54 = kRerr_Q_61 = kRerr_Q_65 = 0. ;
    //proton
    C_B_1 = C_B_2 = C_B_3 = C_B_4 = C_B_5 = C_B_6 = 0. ;
    k_B_1 = k_B_2 = k_B_3 = k_B_4 = k_B_5 = k_B_6 = 0. ;
    Cerr_B_1 = Cerr_B_2 = Cerr_B_3 = Cerr_B_4 = Cerr_B_5 = Cerr_B_6 = 0. ;
    kerr_B_1 = kerr_B_2 = kerr_B_3 = kerr_B_4 = kerr_B_5 = kerr_B_6 = 0. ;
    CRerr_B_21 = CRerr_B_31 = CRerr_B_32 = CRerr_B_42 = CRerr_B_51 = CRerr_B_62 = 0. ;
    kRerr_B_21 = kRerr_B_31 = kRerr_B_32 = kRerr_B_41 = kRerr_B_43 = kRerr_B_51 = kRerr_B_54 = kRerr_B_61 = kRerr_B_65 = 0. ;
    // //kaon
    // C_S_1 = C_S_2 = C_S_3 = C_S_4 = C_S_5 = C_S_6 = 0. ;
    // k_S_1 = k_S_2 = k_S_3 = k_S_4 = k_S_5 = k_S_6 = 0. ;
    // Cerr_S_1 = Cerr_S_2 = Cerr_S_3 = Cerr_S_4 = Cerr_S_5 = Cerr_S_6 = 0. ;
    // kerr_S_1 = kerr_S_2 = kerr_S_3 = kerr_S_4 = kerr_S_5 = kerr_S_6 = 0. ;
    // CRerr_S_21 = CRerr_S_31 = CRerr_S_32 = CRerr_S_42 = CRerr_S_51 = CRerr_S_62 = 0. ;
    // kRerr_S_21 = kRerr_S_31 = kRerr_S_32 = kRerr_S_41 = kRerr_S_43 = kRerr_S_51 = kRerr_S_54 = kRerr_S_61 = kRerr_S_65 = 0. ;
    
    // cout<<"hey0"<<endl;
    for(int i_term = 0 ; i_term < N_mean_term_noErr ; i_term++){
      // cout<<"hey0_1"<<endl;
      // mean_qQ_epd_[i_term] = h_mean_qQ_epd_[i_term]->GetBinContent(i_RM);
      mean_qB_epd_[i_term] = h_mean_qB_epd_[i_term]->GetBinContent(i_RM);
      // mean_qS_epd_[i_term] = h_mean_qS_epd_[i_term]->GetBinContent(i_RM);
      // cout<<"hey1"<<endl;
      // mCalCumQ->SetMean(mean_qQ_epd_[i_term], i_term);
      mCalCumB->SetMean(mean_qB_epd_[i_term], i_term);
      // mCalCumS->SetMean(mean_qS_epd_[i_term], i_term);
      // cout<<"hey2"<<endl;
    }//2535

    // cout<<"hey3"<<endl;
    // mCalCumQ->SetCums( hQ_Nevt_epd->GetBinContent(i_RM) );
    mCalCumB->SetCums( hB_Nevt_epd->GetBinContent(i_RM) );
    // mCalCumS->SetCums( hS_Nevt_epd->GetBinContent(i_RM) );
    // cout<<"hey4"<<endl;

    // //charge
    // C_Q_1 = mCalCumQ->Get_tc1();
    // C_Q_2 = mCalCumQ->Get_tc2();
    // C_Q_3 = mCalCumQ->Get_tc3();
    // C_Q_4 = mCalCumQ->Get_tc4();
    // C_Q_5 = mCalCumQ->Get_tc5();
    // C_Q_6 = mCalCumQ->Get_tc6();

    // k_Q_1 = mCalCumQ->Get_tk1();
    // k_Q_2 = mCalCumQ->Get_tk2();
    // k_Q_3 = mCalCumQ->Get_tk3();
    // k_Q_4 = mCalCumQ->Get_tk4();
    // k_Q_5 = mCalCumQ->Get_tk5();
    // k_Q_6 = mCalCumQ->Get_tk6();

    // if(!isnan(mCalCumQ->Get_etc1())) Cerr_Q_1 = mCalCumQ->Get_etc1();
    // if(!isnan(mCalCumQ->Get_etc2())) Cerr_Q_2 = mCalCumQ->Get_etc2();
    // if(!isnan(mCalCumQ->Get_etc3())) Cerr_Q_3 = mCalCumQ->Get_etc3();
    // if(!isnan(mCalCumQ->Get_etc4())) Cerr_Q_4 = mCalCumQ->Get_etc4();
    // if(!isnan(mCalCumQ->Get_etc5())) Cerr_Q_5 = mCalCumQ->Get_etc5();
    // if(!isnan(mCalCumQ->Get_etc6())) Cerr_Q_6 = mCalCumQ->Get_etc6();

    // if(!isnan(mCalCumQ->Get_etk1())) kerr_Q_1 = mCalCumQ->Get_etk1();
    // if(!isnan(mCalCumQ->Get_etk2())) kerr_Q_2 = mCalCumQ->Get_etk2();
    // if(!isnan(mCalCumQ->Get_etk3())) kerr_Q_3 = mCalCumQ->Get_etk3();
    // if(!isnan(mCalCumQ->Get_etk4())) kerr_Q_4 = mCalCumQ->Get_etk4();
    // if(!isnan(mCalCumQ->Get_etk5())) kerr_Q_5 = mCalCumQ->Get_etk5();
    // if(!isnan(mCalCumQ->Get_etk6())) kerr_Q_6 = mCalCumQ->Get_etk6();

    // if(!isnan(mCalCumQ->Get_er21())) CRerr_Q_21 = mCalCumQ->Get_er21();
    // if(!isnan(mCalCumQ->Get_er31())) CRerr_Q_31 = mCalCumQ->Get_er31();
    // if(!isnan(mCalCumQ->Get_er32())) CRerr_Q_32 = mCalCumQ->Get_er32();
    // if(!isnan(mCalCumQ->Get_er42())) CRerr_Q_42 = mCalCumQ->Get_er42();
    // if(!isnan(mCalCumQ->Get_er51())) CRerr_Q_51 = mCalCumQ->Get_er51();
    // if(!isnan(mCalCumQ->Get_er62())) CRerr_Q_62 = mCalCumQ->Get_er62();

    // if(!isnan(mCalCumQ->Get_ek21())) kRerr_Q_21 = mCalCumQ->Get_ek21();
    // if(!isnan(mCalCumQ->Get_ek31())) kRerr_Q_31 = mCalCumQ->Get_ek31();
    // if(!isnan(mCalCumQ->Get_ek32())) kRerr_Q_32 = mCalCumQ->Get_ek32();
    // if(!isnan(mCalCumQ->Get_ek41())) kRerr_Q_41 = mCalCumQ->Get_ek41();
    // if(!isnan(mCalCumQ->Get_ek43())) kRerr_Q_43 = mCalCumQ->Get_ek43();
    // if(!isnan(mCalCumQ->Get_ek51())) kRerr_Q_51 = mCalCumQ->Get_ek51();
    // if(!isnan(mCalCumQ->Get_ek54())) kRerr_Q_54 = mCalCumQ->Get_ek54();
    // if(!isnan(mCalCumQ->Get_ek61())) kRerr_Q_61 = mCalCumQ->Get_ek61();
    // if(!isnan(mCalCumQ->Get_ek65())) kRerr_Q_65 = mCalCumQ->Get_ek65();

    //proton
    C_B_1 = mCalCumB->Get_tc1();
    C_B_2 = mCalCumB->Get_tc2();
    C_B_3 = mCalCumB->Get_tc3();
    C_B_4 = mCalCumB->Get_tc4();
    C_B_5 = mCalCumB->Get_tc5();
    C_B_6 = mCalCumB->Get_tc6();

    k_B_1 = mCalCumB->Get_tk1();
    k_B_2 = mCalCumB->Get_tk2();
    k_B_3 = mCalCumB->Get_tk3();
    k_B_4 = mCalCumB->Get_tk4();
    k_B_5 = mCalCumB->Get_tk5();
    k_B_6 = mCalCumB->Get_tk6();

    if(!isnan(mCalCumB->Get_etc1())) Cerr_B_1 = mCalCumB->Get_etc1();
    if(!isnan(mCalCumB->Get_etc2())) Cerr_B_2 = mCalCumB->Get_etc2();
    if(!isnan(mCalCumB->Get_etc3())) Cerr_B_3 = mCalCumB->Get_etc3();
    if(!isnan(mCalCumB->Get_etc4())) Cerr_B_4 = mCalCumB->Get_etc4();
    if(!isnan(mCalCumB->Get_etc5())) Cerr_B_5 = mCalCumB->Get_etc5();
    if(!isnan(mCalCumB->Get_etc6())) Cerr_B_6 = mCalCumB->Get_etc6();

    if(!isnan(mCalCumB->Get_etk1())) kerr_B_1 = mCalCumB->Get_etk1();
    if(!isnan(mCalCumB->Get_etk2())) kerr_B_2 = mCalCumB->Get_etk2();
    if(!isnan(mCalCumB->Get_etk3())) kerr_B_3 = mCalCumB->Get_etk3();
    if(!isnan(mCalCumB->Get_etk4())) kerr_B_4 = mCalCumB->Get_etk4();
    if(!isnan(mCalCumB->Get_etk5())) kerr_B_5 = mCalCumB->Get_etk5();
    if(!isnan(mCalCumB->Get_etk6())) kerr_B_6 = mCalCumB->Get_etk6();

    if(!isnan(mCalCumB->Get_er21())) CRerr_B_21 = mCalCumB->Get_er21();
    if(!isnan(mCalCumB->Get_er31())) CRerr_B_31 = mCalCumB->Get_er31();
    if(!isnan(mCalCumB->Get_er32())) CRerr_B_32 = mCalCumB->Get_er32();
    if(!isnan(mCalCumB->Get_er42())) CRerr_B_42 = mCalCumB->Get_er42();
    if(!isnan(mCalCumB->Get_er51())) CRerr_B_51 = mCalCumB->Get_er51();
    if(!isnan(mCalCumB->Get_er62())) CRerr_B_62 = mCalCumB->Get_er62();

    if(!isnan(mCalCumB->Get_ek21())) kRerr_B_21 = mCalCumB->Get_ek21();
    if(!isnan(mCalCumB->Get_ek31())) kRerr_B_31 = mCalCumB->Get_ek31();
    if(!isnan(mCalCumB->Get_ek32())) kRerr_B_32 = mCalCumB->Get_ek32();
    if(!isnan(mCalCumB->Get_ek41())) kRerr_B_41 = mCalCumB->Get_ek41();
    if(!isnan(mCalCumB->Get_ek43())) kRerr_B_43 = mCalCumB->Get_ek43();
    if(!isnan(mCalCumB->Get_ek51())) kRerr_B_51 = mCalCumB->Get_ek51();
    if(!isnan(mCalCumB->Get_ek54())) kRerr_B_54 = mCalCumB->Get_ek54();
    if(!isnan(mCalCumB->Get_ek61())) kRerr_B_61 = mCalCumB->Get_ek61();
    if(!isnan(mCalCumB->Get_ek65())) kRerr_B_65 = mCalCumB->Get_ek65();

    // //kaon
    // C_S_1 = mCalCumS->Get_tc1();
    // C_S_2 = mCalCumS->Get_tc2();
    // C_S_3 = mCalCumS->Get_tc3();
    // C_S_4 = mCalCumS->Get_tc4();
    // C_S_5 = mCalCumS->Get_tc5();
    // C_S_6 = mCalCumS->Get_tc6();

    // k_S_1 = mCalCumS->Get_tk1();
    // k_S_2 = mCalCumS->Get_tk2();
    // k_S_3 = mCalCumS->Get_tk3();
    // k_S_4 = mCalCumS->Get_tk4();
    // k_S_5 = mCalCumS->Get_tk5();
    // k_S_6 = mCalCumS->Get_tk6();

    // if(!isnan(mCalCumS->Get_etc1())) Cerr_S_1 = mCalCumS->Get_etc1();
    // if(!isnan(mCalCumS->Get_etc2())) Cerr_S_2 = mCalCumS->Get_etc2();
    // if(!isnan(mCalCumS->Get_etc3())) Cerr_S_3 = mCalCumS->Get_etc3();
    // if(!isnan(mCalCumS->Get_etc4())) Cerr_S_4 = mCalCumS->Get_etc4();
    // if(!isnan(mCalCumS->Get_etc5())) Cerr_S_5 = mCalCumS->Get_etc5();
    // if(!isnan(mCalCumS->Get_etc6())) Cerr_S_6 = mCalCumS->Get_etc6();

    // if(!isnan(mCalCumS->Get_etk1())) kerr_S_1 = mCalCumS->Get_etk1();
    // if(!isnan(mCalCumS->Get_etk2())) kerr_S_2 = mCalCumS->Get_etk2();
    // if(!isnan(mCalCumS->Get_etk3())) kerr_S_3 = mCalCumS->Get_etk3();
    // if(!isnan(mCalCumS->Get_etk4())) kerr_S_4 = mCalCumS->Get_etk4();
    // if(!isnan(mCalCumS->Get_etk5())) kerr_S_5 = mCalCumS->Get_etk5();
    // if(!isnan(mCalCumS->Get_etk6())) kerr_S_6 = mCalCumS->Get_etk6();

    // if(!isnan(mCalCumS->Get_er21())) CRerr_S_21 = mCalCumS->Get_er21();
    // if(!isnan(mCalCumS->Get_er31())) CRerr_S_31 = mCalCumS->Get_er31();
    // if(!isnan(mCalCumS->Get_er32())) CRerr_S_32 = mCalCumS->Get_er32();
    // if(!isnan(mCalCumS->Get_er42())) CRerr_S_42 = mCalCumS->Get_er42();
    // if(!isnan(mCalCumS->Get_er51())) CRerr_S_51 = mCalCumS->Get_er51();
    // if(!isnan(mCalCumS->Get_er62())) CRerr_S_62 = mCalCumS->Get_er62();

    // if(!isnan(mCalCumS->Get_ek21())) kRerr_S_21 = mCalCumS->Get_ek21();
    // if(!isnan(mCalCumS->Get_ek31())) kRerr_S_31 = mCalCumS->Get_ek31();
    // if(!isnan(mCalCumS->Get_ek32())) kRerr_S_32 = mCalCumS->Get_ek32();
    // if(!isnan(mCalCumS->Get_ek41())) kRerr_S_41 = mCalCumS->Get_ek41();
    // if(!isnan(mCalCumS->Get_ek43())) kRerr_S_43 = mCalCumS->Get_ek43();
    // if(!isnan(mCalCumS->Get_ek51())) kRerr_S_51 = mCalCumS->Get_ek51();
    // if(!isnan(mCalCumS->Get_ek54())) kRerr_S_54 = mCalCumS->Get_ek54();
    // if(!isnan(mCalCumS->Get_ek61())) kRerr_S_61 = mCalCumS->Get_ek61();
    // if(!isnan(mCalCumS->Get_ek65())) kRerr_S_65 = mCalCumS->Get_ek65();

    //Fill it
    // //charge
    // hQ_C_epd[0]->SetBinContent(i_RM, C_Q_1);//C1
    // hQ_C_epd[1]->SetBinContent(i_RM, C_Q_2);//C2
    // hQ_C_epd[2]->SetBinContent(i_RM, C_Q_3);//C3
    // hQ_C_epd[3]->SetBinContent(i_RM, C_Q_4);//C4
    // hQ_C_epd[4]->SetBinContent(i_RM, C_Q_5);//C5
    // hQ_C_epd[5]->SetBinContent(i_RM, C_Q_6);//C6
    // hQ_C_epd[0]->SetBinError(i_RM, mCalCumQ->Get_etc1());//C1
    // hQ_C_epd[1]->SetBinError(i_RM, mCalCumQ->Get_etc2());//C2
    // hQ_C_epd[2]->SetBinError(i_RM, mCalCumQ->Get_etc3());//C3
    // hQ_C_epd[3]->SetBinError(i_RM, mCalCumQ->Get_etc4());//C4
    // hQ_C_epd[4]->SetBinError(i_RM, mCalCumQ->Get_etc5());//C5
    // hQ_C_epd[5]->SetBinError(i_RM, mCalCumQ->Get_etc6());//C6

    // if(C_Q_1 != 0) {
    //   hQ_C_epd[6]->SetBinContent(i_RM, C_Q_2/C_Q_1);// C2/C1
    //   hQ_C_epd[6]->SetBinError(i_RM, mCalCumQ->Get_er21());// C2/C1
    //   // if(mCalCumQ->Get_er21() > 1e5) cout<<"mCalCumQ->Get_er21() = "<<mCalCumQ->Get_er21()<<endl;
    // }
    // if(C_Q_1 != 0) {
    //   hQ_C_epd[7]->SetBinContent(i_RM, C_Q_3/C_Q_1);// C3/C1
    //   hQ_C_epd[7]->SetBinError(i_RM, mCalCumQ->Get_er31());// C3/C1
    //   // if(mCalCumQ->Get_er31() > 1e5) cout<<"mCalCumQ->Get_er31() = "<<mCalCumQ->Get_er31()<<endl;
    // }
    // if(C_Q_2 != 0) {
    //   hQ_C_epd[8]->SetBinContent(i_RM, C_Q_3/C_Q_2);// C3/C2
    //   hQ_C_epd[8]->SetBinError(i_RM, mCalCumQ->Get_er32());// C3/C2
    //   // if(mCalCumQ->Get_er32() > 1e5) cout<<"mCalCumQ->Get_er32() = "<<mCalCumQ->Get_er32()<<endl;
    // }
    // if(C_Q_2 != 0) {
    //   hQ_C_epd[9]->SetBinContent(i_RM, C_Q_4/C_Q_2);// C4/C2
    //   hQ_C_epd[9]->SetBinError(i_RM, mCalCumQ->Get_er42());// C4/C2
    //   // if(mCalCumQ->Get_er42() > 1e5) cout<<"mCalCumQ->Get_er42() = "<<mCalCumQ->Get_er42()<<endl;
    // }
    // if(C_Q_1 != 0) {
    //   hQ_C_epd[10]->SetBinContent(i_RM, C_Q_5/C_Q_1);// C5/C1
    //   hQ_C_epd[10]->SetBinError(i_RM, mCalCumQ->Get_er51());// C5/C1
    //   // if(mCalCumQ->Get_er51() > 1e5) cout<<"mCalCumQ->Get_er51() = "<<mCalCumQ->Get_er51()<<endl;
    // }
    // if(C_Q_2 != 0) {
    //   hQ_C_epd[11]->SetBinContent(i_RM, C_Q_6/C_Q_2);// C6/C2
    //   hQ_C_epd[11]->SetBinError(i_RM, mCalCumQ->Get_er62());// C6/C2
    //   // if(mCalCumQ->Get_er62() > 1e5) cout<<"mCalCumQ->Get_er62() = "<<mCalCumQ->Get_er62()<<endl;
    // }

    // hQ_C_epd[12]->SetBinContent(i_RM,  k_Q_1);//k1
    // hQ_C_epd[13]->SetBinContent(i_RM,  k_Q_2);//k2
    // hQ_C_epd[14]->SetBinContent(i_RM,  k_Q_3);//k3
    // hQ_C_epd[15]->SetBinContent(i_RM,  k_Q_4);//k4
    // hQ_C_epd[16]->SetBinContent(i_RM, k_Q_5);//k5
    // hQ_C_epd[17]->SetBinContent(i_RM, k_Q_6);//k6

    // if(k_Q_1 != 0) hQ_C_epd[18]->SetBinContent(i_RM, k_Q_2/k_Q_1);// k2/k1
    // if(k_Q_1 != 0) hQ_C_epd[19]->SetBinContent(i_RM, k_Q_3/k_Q_1);// k3/k1
    // if(k_Q_2 != 0) hQ_C_epd[20]->SetBinContent(i_RM, k_Q_3/k_Q_2);// k3/k2
    // if(k_Q_1 != 0) hQ_C_epd[21]->SetBinContent(i_RM, k_Q_4/k_Q_1);// k4/k1
    // if(k_Q_3 != 0) hQ_C_epd[22]->SetBinContent(i_RM, k_Q_4/k_Q_3);// k4/k3
    // if(k_Q_1 != 0) hQ_C_epd[23]->SetBinContent(i_RM, k_Q_5/k_Q_1);// k5/k1
    // if(k_Q_4 != 0) hQ_C_epd[24]->SetBinContent(i_RM, k_Q_5/k_Q_4);// k5/k4
    // if(k_Q_1 != 0) hQ_C_epd[25]->SetBinContent(i_RM, k_Q_6/k_Q_1);// k6/k1
    // if(k_Q_5 != 0) hQ_C_epd[26]->SetBinContent(i_RM, k_Q_6/k_Q_5);// k6/k5

    //proton
    hB_C_epd[0]->SetBinContent(i_RM, C_B_1);//C1
    hB_C_epd[1]->SetBinContent(i_RM, C_B_2);//C2
    hB_C_epd[2]->SetBinContent(i_RM, C_B_3);//C3
    hB_C_epd[3]->SetBinContent(i_RM, C_B_4);//C4
    hB_C_epd[4]->SetBinContent(i_RM, C_B_5);//C5
    hB_C_epd[5]->SetBinContent(i_RM, C_B_6);//C6
    hB_C_epd[0]->SetBinError(i_RM, mCalCumB->Get_etc1());//C1
    hB_C_epd[1]->SetBinError(i_RM, mCalCumB->Get_etc2());//C2
    hB_C_epd[2]->SetBinError(i_RM, mCalCumB->Get_etc3());//C3
    hB_C_epd[3]->SetBinError(i_RM, mCalCumB->Get_etc4());//C4
    hB_C_epd[4]->SetBinError(i_RM, mCalCumB->Get_etc5());//C5
    hB_C_epd[5]->SetBinError(i_RM, mCalCumB->Get_etc6());//C6

    // if(C_B_1 != 0) {
    //14092021(start)
    // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er21()) && mCalCumB->Get_er21() < 1e6) {//27082021 some of them is nan
    //15092021: commented out the if statement. Later during CBWC, reject the whole "nan" multbin.
    // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er21())) {//27082021 some of them is nan
      //14092021(finish)
      hB_C_epd[6]->SetBinContent(i_RM, C_B_2/C_B_1);// C2/C1
      hB_C_epd[6]->SetBinError(i_RM, mCalCumB->Get_er21());// C2/C1
    // }
    // if(C_B_1 != 0) {
    //14092021(start)
    // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er31()) && mCalCumB->Get_er31() < 1e6) {
    // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er31())) {
      //14092021(finish)
      hB_C_epd[7]->SetBinContent(i_RM, C_B_3/C_B_1);// C3/C1
      hB_C_epd[7]->SetBinError(i_RM, mCalCumB->Get_er31());// C3/C1
    // }
    // if(C_B_2 != 0) {
    //14092021(start)
    // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er32()) && mCalCumB->Get_er32() < 1e6) {
    // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er32())) {
      //14092021(finish)
      hB_C_epd[8]->SetBinContent(i_RM, C_B_3/C_B_2);// C3/C2
      hB_C_epd[8]->SetBinError(i_RM, mCalCumB->Get_er32());// C3/C2
    // }
    // if(C_B_2 != 0) {
    //14092021(start)
    // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er42()) && mCalCumB->Get_er42() < 1e6) {
    // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er42())) {
      //14092021(finish)
      hB_C_epd[9]->SetBinContent(i_RM, C_B_4/C_B_2);// C4/C2
      hB_C_epd[9]->SetBinError(i_RM, mCalCumB->Get_er42());// C4/C2
    // }
    // if(C_B_1 != 0) {
    //14092021(start)
    // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er51()) && mCalCumB->Get_er51() < 1e6) {
    // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er51())) {
      //14092021(finish)
      hB_C_epd[10]->SetBinContent(i_RM, C_B_5/C_B_1);// C5/C1
      hB_C_epd[10]->SetBinError(i_RM, mCalCumB->Get_er51());// C5/C1
      cout<<"i_RM, C_B_5/C_B_1, mCalCumB->Get_er51(): "<<i_RM<<", "<<C_B_5/C_B_1<<", "<<mCalCumB->Get_er51()<<endl;
      if(isnan(mCalCumB->Get_er51())) nan_counter_cr51++;
    // }
    // if(C_B_2 != 0) {
    //14092021(start)
    // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er62()) && mCalCumB->Get_er62() < 1e6) {
    // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er62())) {
      //14092021(finish)
      hB_C_epd[11]->SetBinContent(i_RM, C_B_6/C_B_2);// C6/C2
      hB_C_epd[11]->SetBinError(i_RM, mCalCumB->Get_er62());// C6/C2
      cout<<"i_RM, C_B_6/C_B_2, mCalCumB->Get_er62(): "<<i_RM<<", "<<C_B_6/C_B_2<<", "<<mCalCumB->Get_er62()<<endl;
    // }
      if(isnan(mCalCumB->Get_er62())) nan_counter_cr62++;

    hB_C_epd[12]->SetBinContent(i_RM,  k_B_1);//k1
    hB_C_epd[13]->SetBinContent(i_RM,  k_B_2);//k2
    hB_C_epd[14]->SetBinContent(i_RM,  k_B_3);//k3
    hB_C_epd[15]->SetBinContent(i_RM,  k_B_4);//k4
    hB_C_epd[16]->SetBinContent(i_RM, k_B_5);//k5
    hB_C_epd[17]->SetBinContent(i_RM, k_B_6);//k6

    if(k_B_1 != 0) hB_C_epd[18]->SetBinContent(i_RM, k_B_2/k_B_1);// k2/k1
    if(k_B_1 != 0) hB_C_epd[19]->SetBinContent(i_RM, k_B_3/k_B_1);// k3/k1
    if(k_B_2 != 0) hB_C_epd[20]->SetBinContent(i_RM, k_B_3/k_B_2);// k3/k2
    if(k_B_1 != 0) hB_C_epd[21]->SetBinContent(i_RM, k_B_4/k_B_1);// k4/k1
    if(k_B_3 != 0) hB_C_epd[22]->SetBinContent(i_RM, k_B_4/k_B_3);// k4/k3
    if(k_B_1 != 0) hB_C_epd[23]->SetBinContent(i_RM, k_B_5/k_B_1);// k5/k1
    if(k_B_4 != 0) hB_C_epd[24]->SetBinContent(i_RM, k_B_5/k_B_4);// k5/k4
    if(k_B_1 != 0) hB_C_epd[25]->SetBinContent(i_RM, k_B_6/k_B_1);// k6/k1
    if(k_B_5 != 0) hB_C_epd[26]->SetBinContent(i_RM, k_B_6/k_B_5);// k6/k5

    // //kaon
    // hS_C_epd[0]->SetBinContent(i_RM, C_S_1);//C1
    // hS_C_epd[1]->SetBinContent(i_RM, C_S_2);//C2
    // hS_C_epd[2]->SetBinContent(i_RM, C_S_3);//C3
    // hS_C_epd[3]->SetBinContent(i_RM, C_S_4);//C4
    // hS_C_epd[4]->SetBinContent(i_RM, C_S_5);//C5
    // hS_C_epd[5]->SetBinContent(i_RM, C_S_6);//C6
    // hS_C_epd[0]->SetBinError(i_RM, mCalCumS->Get_etc1());//C1
    // hS_C_epd[1]->SetBinError(i_RM, mCalCumS->Get_etc2());//C2
    // hS_C_epd[2]->SetBinError(i_RM, mCalCumS->Get_etc3());//C3
    // hS_C_epd[3]->SetBinError(i_RM, mCalCumS->Get_etc4());//C4
    // hS_C_epd[4]->SetBinError(i_RM, mCalCumS->Get_etc5());//C5
    // hS_C_epd[5]->SetBinError(i_RM, mCalCumS->Get_etc6());//C6

    // if(C_S_1 != 0) {
    //   hS_C_epd[6]->SetBinContent(i_RM, C_S_2/C_S_1);// C2/C1
    //   hS_C_epd[6]->SetBinError(i_RM, mCalCumS->Get_er21());// C2/C1
    // }
    // if(C_S_1 != 0) {
    //   hS_C_epd[7]->SetBinContent(i_RM, C_S_3/C_S_1);// C3/C1
    //   hS_C_epd[7]->SetBinError(i_RM, mCalCumS->Get_er31());// C3/C1
    // }
    // if(C_S_2 != 0) {
    //   hS_C_epd[8]->SetBinContent(i_RM, C_S_3/C_S_2);// C3/C2
    //   hS_C_epd[8]->SetBinError(i_RM, mCalCumS->Get_er32());// C3/C2
    // }
    // if(C_S_2 != 0) {
    //   hS_C_epd[9]->SetBinContent(i_RM, C_S_4/C_S_2);// C4/C2
    //   hS_C_epd[9]->SetBinError(i_RM, mCalCumS->Get_er42());// C4/C2
    // }
    // if(C_S_1 != 0) {
    //   hS_C_epd[10]->SetBinContent(i_RM, C_S_5/C_S_1);// C5/C1
    //   hS_C_epd[10]->SetBinError(i_RM, mCalCumS->Get_er51());// C5/C1
    // }
    // if(C_S_2 != 0) {
    //   hS_C_epd[11]->SetBinContent(i_RM, C_S_6/C_S_2);// C6/C2
    //   hS_C_epd[11]->SetBinError(i_RM, mCalCumS->Get_er62());// C6/C2
    // }

    // hS_C_epd[12]->SetBinContent(i_RM,  k_S_1);//k1
    // hS_C_epd[13]->SetBinContent(i_RM,  k_S_2);//k2
    // hS_C_epd[14]->SetBinContent(i_RM,  k_S_3);//k3
    // hS_C_epd[15]->SetBinContent(i_RM,  k_S_4);//k4
    // hS_C_epd[16]->SetBinContent(i_RM, k_S_5);//k5
    // hS_C_epd[17]->SetBinContent(i_RM, k_S_6);//k6

    // if(k_S_1 != 0) hS_C_epd[18]->SetBinContent(i_RM, k_S_2/k_S_1);// k2/k1
    // if(k_S_1 != 0) hS_C_epd[19]->SetBinContent(i_RM, k_S_3/k_S_1);// k3/k1
    // if(k_S_2 != 0) hS_C_epd[20]->SetBinContent(i_RM, k_S_3/k_S_2);// k3/k2
    // if(k_S_1 != 0) hS_C_epd[21]->SetBinContent(i_RM, k_S_4/k_S_1);// k4/k1
    // if(k_S_3 != 0) hS_C_epd[22]->SetBinContent(i_RM, k_S_4/k_S_3);// k4/k3
    // if(k_S_1 != 0) hS_C_epd[23]->SetBinContent(i_RM, k_S_5/k_S_1);// k5/k1
    // if(k_S_4 != 0) hS_C_epd[24]->SetBinContent(i_RM, k_S_5/k_S_4);// k5/k4
    // if(k_S_1 != 0) hS_C_epd[25]->SetBinContent(i_RM, k_S_6/k_S_1);// k6/k1
    // if(k_S_5 != 0) hS_C_epd[26]->SetBinContent(i_RM, k_S_6/k_S_5);// k6/k5

    ///////
    //Err//
    ///////
    // //charge
    // hQ_C_epd_err[0]->SetBinContent(i_RM, Cerr_Q_1);//c_err1
    // hQ_C_epd_err[1]->SetBinContent(i_RM, Cerr_Q_2);//c_err2
    // hQ_C_epd_err[2]->SetBinContent(i_RM, Cerr_Q_3);//c_err3
    // hQ_C_epd_err[3]->SetBinContent(i_RM, Cerr_Q_4);//c_err4
    // hQ_C_epd_err[4]->SetBinContent(i_RM, Cerr_Q_5);//c_err5
    // hQ_C_epd_err[5]->SetBinContent(i_RM, Cerr_Q_6);//c_err6

    // hQ_C_epd_err[6]->SetBinContent(i_RM, kerr_Q_1);//k_err1
    // hQ_C_epd_err[7]->SetBinContent(i_RM, kerr_Q_2);//k_err2
    // hQ_C_epd_err[8]->SetBinContent(i_RM, kerr_Q_3);//k_err3
    // hQ_C_epd_err[9]->SetBinContent(i_RM, kerr_Q_4);//k_err4
    // hQ_C_epd_err[10]->SetBinContent(i_RM, kerr_Q_5);//k_err5
    // hQ_C_epd_err[11]->SetBinContent(i_RM, kerr_Q_6);//k_err6

    // hQ_C_epd_err[12]->SetBinContent(i_RM, CRerr_Q_21);//CR_err21
    // hQ_C_epd_err[13]->SetBinContent(i_RM, CRerr_Q_31);//CR_err31
    // hQ_C_epd_err[14]->SetBinContent(i_RM, CRerr_Q_32);//CR_err32
    // hQ_C_epd_err[15]->SetBinContent(i_RM, CRerr_Q_42);//CR_err42
    // hQ_C_epd_err[16]->SetBinContent(i_RM, CRerr_Q_51);//CR_err51
    // hQ_C_epd_err[17]->SetBinContent(i_RM, CRerr_Q_62);//CR_err62

    // hQ_C_epd_err[18]->SetBinContent(i_RM, kRerr_Q_21);//kR_err21
    // hQ_C_epd_err[19]->SetBinContent(i_RM, kRerr_Q_31);//kR_err31
    // hQ_C_epd_err[20]->SetBinContent(i_RM, kRerr_Q_32);//kR_err32
    // hQ_C_epd_err[21]->SetBinContent(i_RM, kRerr_Q_41);//kR_err41
    // hQ_C_epd_err[22]->SetBinContent(i_RM, kRerr_Q_43);//kR_err43
    // hQ_C_epd_err[23]->SetBinContent(i_RM, kRerr_Q_51);//kR_err51
    // hQ_C_epd_err[24]->SetBinContent(i_RM, kRerr_Q_54);//kR_err54
    // hQ_C_epd_err[25]->SetBinContent(i_RM, kRerr_Q_61);//kR_err61
    // hQ_C_epd_err[26]->SetBinContent(i_RM, kRerr_Q_65);//kR_err65
    //proton    
    hB_C_epd_err[0]->SetBinContent(i_RM, Cerr_B_1);//c_err1
    hB_C_epd_err[1]->SetBinContent(i_RM, Cerr_B_2);//c_err2
    hB_C_epd_err[2]->SetBinContent(i_RM, Cerr_B_3);//c_err3
    hB_C_epd_err[3]->SetBinContent(i_RM, Cerr_B_4);//c_err4
    hB_C_epd_err[4]->SetBinContent(i_RM, Cerr_B_5);//c_err5
    hB_C_epd_err[5]->SetBinContent(i_RM, Cerr_B_6);//c_err6

    hB_C_epd_err[6]->SetBinContent(i_RM, kerr_B_1);//k_err1
    hB_C_epd_err[7]->SetBinContent(i_RM, kerr_B_2);//k_err2
    hB_C_epd_err[8]->SetBinContent(i_RM, kerr_B_3);//k_err3
    hB_C_epd_err[9]->SetBinContent(i_RM, kerr_B_4);//k_err4
    hB_C_epd_err[10]->SetBinContent(i_RM, kerr_B_5);//k_err5
    hB_C_epd_err[11]->SetBinContent(i_RM, kerr_B_6);//k_err6

    hB_C_epd_err[12]->SetBinContent(i_RM, CRerr_B_21);//CR_err21
    hB_C_epd_err[13]->SetBinContent(i_RM, CRerr_B_31);//CR_err31
    hB_C_epd_err[14]->SetBinContent(i_RM, CRerr_B_32);//CR_err32
    hB_C_epd_err[15]->SetBinContent(i_RM, CRerr_B_42);//CR_err42
    hB_C_epd_err[16]->SetBinContent(i_RM, CRerr_B_51);//CR_err51
    hB_C_epd_err[17]->SetBinContent(i_RM, CRerr_B_62);//CR_err62

    hB_C_epd_err[18]->SetBinContent(i_RM, kRerr_B_21);//kR_err21
    hB_C_epd_err[19]->SetBinContent(i_RM, kRerr_B_31);//kR_err31
    hB_C_epd_err[20]->SetBinContent(i_RM, kRerr_B_32);//kR_err32
    hB_C_epd_err[21]->SetBinContent(i_RM, kRerr_B_41);//kR_err41
    hB_C_epd_err[22]->SetBinContent(i_RM, kRerr_B_43);//kR_err43
    hB_C_epd_err[23]->SetBinContent(i_RM, kRerr_B_51);//kR_err51
    hB_C_epd_err[24]->SetBinContent(i_RM, kRerr_B_54);//kR_err54
    hB_C_epd_err[25]->SetBinContent(i_RM, kRerr_B_61);//kR_err61
    hB_C_epd_err[26]->SetBinContent(i_RM, kRerr_B_65);//kR_err65
    // //kaon
    // hS_C_epd_err[0]->SetBinContent(i_RM, Cerr_S_1);//c_err1
    // hS_C_epd_err[1]->SetBinContent(i_RM, Cerr_S_2);//c_err2
    // hS_C_epd_err[2]->SetBinContent(i_RM, Cerr_S_3);//c_err3
    // hS_C_epd_err[3]->SetBinContent(i_RM, Cerr_S_4);//c_err4
    // hS_C_epd_err[4]->SetBinContent(i_RM, Cerr_S_5);//c_err5
    // hS_C_epd_err[5]->SetBinContent(i_RM, Cerr_S_6);//c_err6

    // hS_C_epd_err[6]->SetBinContent(i_RM, kerr_S_1);//k_err1
    // hS_C_epd_err[7]->SetBinContent(i_RM, kerr_S_2);//k_err2
    // hS_C_epd_err[8]->SetBinContent(i_RM, kerr_S_3);//k_err3
    // hS_C_epd_err[9]->SetBinContent(i_RM, kerr_S_4);//k_err4
    // hS_C_epd_err[10]->SetBinContent(i_RM, kerr_S_5);//k_err5
    // hS_C_epd_err[11]->SetBinContent(i_RM, kerr_S_6);//k_err6

    // hS_C_epd_err[12]->SetBinContent(i_RM, CRerr_S_21);//CR_err21
    // hS_C_epd_err[13]->SetBinContent(i_RM, CRerr_S_31);//CR_err31
    // hS_C_epd_err[14]->SetBinContent(i_RM, CRerr_S_32);//CR_err32
    // hS_C_epd_err[15]->SetBinContent(i_RM, CRerr_S_42);//CR_err42
    // hS_C_epd_err[16]->SetBinContent(i_RM, CRerr_S_51);//CR_err51
    // hS_C_epd_err[17]->SetBinContent(i_RM, CRerr_S_62);//CR_err62

    // hS_C_epd_err[18]->SetBinContent(i_RM, kRerr_S_21);//kR_err21
    // hS_C_epd_err[19]->SetBinContent(i_RM, kRerr_S_31);//kR_err31
    // hS_C_epd_err[20]->SetBinContent(i_RM, kRerr_S_32);//kR_err32
    // hS_C_epd_err[21]->SetBinContent(i_RM, kRerr_S_41);//kR_err41
    // hS_C_epd_err[22]->SetBinContent(i_RM, kRerr_S_43);//kR_err43
    // hS_C_epd_err[23]->SetBinContent(i_RM, kRerr_S_51);//kR_err51
    // hS_C_epd_err[24]->SetBinContent(i_RM, kRerr_S_54);//kR_err54
    // hS_C_epd_err[25]->SetBinContent(i_RM, kRerr_S_61);//kR_err61
    // hS_C_epd_err[26]->SetBinContent(i_RM, kRerr_S_65);//kR_err65

  }//refMult loop ends

  WriteEffCorrEPDmultCum(Form("output/cum/%s_effcorr_ref3uncorr_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_ptmax_%d_ymax_%d_detefflow_%d_deteffhigh_%d_maxmult_%d_29012022_1_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, detefflow_err, deteffhigh_err, maxMult_ref3StPicoEvt, MergeTopCent));

  //temporary
  cout<<"Cum::CalculateEffCorrEPDmultCum"<<endl;
  cout<<"nan_counter_cr51 = "<<nan_counter_cr51<<endl;
  cout<<"nan_counter_cr62 = "<<nan_counter_cr62<<endl;

  // delete mCalCumQ;
  delete mCalCumB;
  // delete mCalCumS;
}

//////////////////////////////////////////

void Cum::WriteEffCorrEPDmultCum(const char* outFile){

  TFile *f_out = new TFile(outFile, "RECREATE");
  f_out->cd();
  for(int i = 0 ; i < Ncums_noErr ; i++){
    // hQ_C_epd[i]->Write();
    hB_C_epd[i]->Write();
    // hS_C_epd[i]->Write();
    // hQ_C_epd_err[i]->Write();
    hB_C_epd_err[i]->Write();
    // hS_C_epd_err[i]->Write();
  }

  // hQ_Nevt_epd->Write();
  hB_Nevt_epd->Write();
  // hS_Nevt_epd->Write();
  f_out->Close();
  
}

//////////////////////////////////////////

void Cum::GetEPDmultUnCorrMeans(const char* inFile){
  //get terms in Eq. 62 ~ 67 & 69 in arXiv:1702.07106v3
  TFile *f_in = new TFile(inFile, "READ");
  std::map<const char*, TProfile*>::iterator iter;
  ///charge///
  TIter nextkeyQ(f_in->GetListOfKeys());
  TKey *keyQ;
  while((keyQ = (TKey*)nextkeyQ())){
    TString strName = keyQ->ReadObj()->GetName();
    if(strName.Contains(Form("%s_epd_unCorr", "charge"))){
      strName.Replace(0, strlen(Form("%s_epd_unCorr", "charge"))+1,"");
      // cout<<strName<<endl;
    } else {
      continue;
    }
    int iT = 0;
    while (true) {
      if(iT == N_mean_term_noErr) {
	std::cout<<"In Cum::GetEPDmultUnCorrMeans"<<std::endl;
	std::cout<<"Found bad terms"<<std::endl;
	abort();
      }
      if(strcmp(strName, mean_term_name_noErr[iT])==0){
	h_mean_qQ_epd_unCorr_[iT] = (TProfile*) keyQ->ReadObj()->Clone();
	break;
      } else {
	iT++;
      }
    }
  }
  ///proton///
  TIter nextkeyB(f_in->GetListOfKeys());
  TKey *keyB;
  while((keyB = (TKey*)nextkeyB())){
    TString strName = keyB->ReadObj()->GetName();
    if(strName.Contains(Form("%s_epd_unCorr", "proton"))){
      strName.Replace(0, strlen(Form("%s_epd_unCorr", "proton"))+1,"");
      // cout<<strName<<endl;
    } else {
      continue;
    }
    int iT = 0;
    while (true) {
      if(iT == N_mean_term_noErr) {
	std::cout<<"In Cum::GetEPDmultUnCorrMeans"<<std::endl;
	std::cout<<"Found bad terms"<<std::endl;
	abort();
      }
      if(strcmp(strName, mean_term_name_noErr[iT])==0){
	h_mean_qB_epd_unCorr_[iT] = (TProfile*) keyB->ReadObj()->Clone();
	break;
      } else {
	iT++;
      }
    }
  }
  ///kaon///
  TIter nextkeyS(f_in->GetListOfKeys());
  TKey *keyS;
  while((keyS = (TKey*)nextkeyS())){
    TString strName = keyS->ReadObj()->GetName();
    if(strName.Contains(Form("%s_epd_unCorr", "kaon"))){
      strName.Replace(0, strlen(Form("%s_epd_unCorr", "kaon"))+1,"");
      // cout<<strName<<endl;
    } else {
      continue;
    }
    int iT = 0;
    while (true) {
      if(iT == N_mean_term_noErr) {
	std::cout<<"In Cum::GetEPDmultUnCorrMeans"<<std::endl;
	std::cout<<"Found bad terms"<<std::endl;
	abort();
      }
      if(strcmp(strName, mean_term_name_noErr[iT])==0){
	h_mean_qS_epd_unCorr_[iT] = (TProfile*) keyS->ReadObj()->Clone();
	break;
      } else {
	iT++;
      }
    }
  }

  TProfile* entQ = (TProfile*)f_in->Get(Form("%s_epd_unCorr_q01_01","charge"));
  TProfile* entB = (TProfile*)f_in->Get(Form("%s_epd_unCorr_q01_01","proton"));
  TProfile* entS = (TProfile*)f_in->Get(Form("%s_epd_unCorr_q01_01","kaon"));
  for(int i=1;i<entB->GetNbinsX();++i) {
    hQ_Nevt_epd_unCorr->SetBinContent(i, entQ->GetBinEntries(i));//getting No. of Evt per refMult
    hB_Nevt_epd_unCorr->SetBinContent(i, entB->GetBinEntries(i));//getting No. of Evt per refMult
    hS_Nevt_epd_unCorr->SetBinContent(i, entS->GetBinEntries(i));//getting No. of Evt per refMult
    // cout<<ent->GetBinEntries(i)<<endl;
  }
}

//////////////////////////////////////////

void Cum::CalculateEffUnCorrEPDmultCum(const char* NameRuZr){
  //calculate eff uncorr cumulant

  //charge
  double C_Q_1, C_Q_2, C_Q_3, C_Q_4, C_Q_5, C_Q_6;
  double k_Q_1, k_Q_2, k_Q_3, k_Q_4, k_Q_5, k_Q_6;
  double Cerr_Q_1, Cerr_Q_2, Cerr_Q_3, Cerr_Q_4, Cerr_Q_5, Cerr_Q_6;
  double kerr_Q_1, kerr_Q_2, kerr_Q_3, kerr_Q_4, kerr_Q_5, kerr_Q_6;
  double CRerr_Q_21, CRerr_Q_31, CRerr_Q_32, CRerr_Q_42, CRerr_Q_51, CRerr_Q_62;
  double kRerr_Q_21, kRerr_Q_31, kRerr_Q_32, kRerr_Q_41, kRerr_Q_43, kRerr_Q_51, kRerr_Q_54, kRerr_Q_61, kRerr_Q_65;
  //proton
  double C_B_1, C_B_2, C_B_3, C_B_4, C_B_5, C_B_6;
  double k_B_1, k_B_2, k_B_3, k_B_4, k_B_5, k_B_6;
  double Cerr_B_1, Cerr_B_2, Cerr_B_3, Cerr_B_4, Cerr_B_5, Cerr_B_6;
  double kerr_B_1, kerr_B_2, kerr_B_3, kerr_B_4, kerr_B_5, kerr_B_6;
  double CRerr_B_21, CRerr_B_31, CRerr_B_32, CRerr_B_42, CRerr_B_51, CRerr_B_62;
  double kRerr_B_21, kRerr_B_31, kRerr_B_32, kRerr_B_41, kRerr_B_43, kRerr_B_51, kRerr_B_54, kRerr_B_61, kRerr_B_65;
  //kaon
  double C_S_1, C_S_2, C_S_3, C_S_4, C_S_5, C_S_6;
  double k_S_1, k_S_2, k_S_3, k_S_4, k_S_5, k_S_6;
  double Cerr_S_1, Cerr_S_2, Cerr_S_3, Cerr_S_4, Cerr_S_5, Cerr_S_6;
  double kerr_S_1, kerr_S_2, kerr_S_3, kerr_S_4, kerr_S_5, kerr_S_6;
  double CRerr_S_21, CRerr_S_31, CRerr_S_32, CRerr_S_42, CRerr_S_51, CRerr_S_62;
  double kRerr_S_21, kRerr_S_31, kRerr_S_32, kRerr_S_41, kRerr_S_43, kRerr_S_51, kRerr_S_54, kRerr_S_61, kRerr_S_65;

  double mean_qQ_epd_unCorr_[N_mean_term_noErr];
  double mean_qB_epd_unCorr_[N_mean_term_noErr];
  double mean_qS_epd_unCorr_[N_mean_term_noErr];

  CalCum *mCalCumQ = new CalCum();//charge
  CalCum *mCalCumB = new CalCum();//proton
  CalCum *mCalCumS = new CalCum();//kaon

  for(int i_RM = 0 ; i_RM < maxMult ; i_RM++){
    mCalCumQ->Init();
    mCalCumB->Init();
    mCalCumS->Init();
    cout<<"Cum::CalculateEffUnCorrEPDmultCum(). Multiplicity: "<<i_RM<<"/"<<maxMult<<endl;
    //initialize
    //charge
    C_Q_1 = C_Q_2 = C_Q_3 = C_Q_4 = C_Q_5 = C_Q_6 = 0. ;
    k_Q_1 = k_Q_2 = k_Q_3 = k_Q_4 = k_Q_5 = k_Q_6 = 0. ;
    Cerr_Q_1 = Cerr_Q_2 = Cerr_Q_3 = Cerr_Q_4 = Cerr_Q_5 = Cerr_Q_6 = 0. ;
    kerr_Q_1 = kerr_Q_2 = kerr_Q_3 = kerr_Q_4 = kerr_Q_5 = kerr_Q_6 = 0. ;
    CRerr_Q_21 = CRerr_Q_31 = CRerr_Q_32 = CRerr_Q_42 = CRerr_Q_51 = CRerr_Q_62 = 0. ;
    kRerr_Q_21 = kRerr_Q_31 = kRerr_Q_32 = kRerr_Q_41 = kRerr_Q_43 = kRerr_Q_51 = kRerr_Q_54 = kRerr_Q_61 = kRerr_Q_65 = 0. ;
    //proton
    C_B_1 = C_B_2 = C_B_3 = C_B_4 = C_B_5 = C_B_6 = 0. ;
    k_B_1 = k_B_2 = k_B_3 = k_B_4 = k_B_5 = k_B_6 = 0. ;
    Cerr_B_1 = Cerr_B_2 = Cerr_B_3 = Cerr_B_4 = Cerr_B_5 = Cerr_B_6 = 0. ;
    kerr_B_1 = kerr_B_2 = kerr_B_3 = kerr_B_4 = kerr_B_5 = kerr_B_6 = 0. ;
    CRerr_B_21 = CRerr_B_31 = CRerr_B_32 = CRerr_B_42 = CRerr_B_51 = CRerr_B_62 = 0. ;
    kRerr_B_21 = kRerr_B_31 = kRerr_B_32 = kRerr_B_41 = kRerr_B_43 = kRerr_B_51 = kRerr_B_54 = kRerr_B_61 = kRerr_B_65 = 0. ;
    //kaon
    C_S_1 = C_S_2 = C_S_3 = C_S_4 = C_S_5 = C_S_6 = 0. ;
    k_S_1 = k_S_2 = k_S_3 = k_S_4 = k_S_5 = k_S_6 = 0. ;
    Cerr_S_1 = Cerr_S_2 = Cerr_S_3 = Cerr_S_4 = Cerr_S_5 = Cerr_S_6 = 0. ;
    kerr_S_1 = kerr_S_2 = kerr_S_3 = kerr_S_4 = kerr_S_5 = kerr_S_6 = 0. ;
    CRerr_S_21 = CRerr_S_31 = CRerr_S_32 = CRerr_S_42 = CRerr_S_51 = CRerr_S_62 = 0. ;
    kRerr_S_21 = kRerr_S_31 = kRerr_S_32 = kRerr_S_41 = kRerr_S_43 = kRerr_S_51 = kRerr_S_54 = kRerr_S_61 = kRerr_S_65 = 0. ;
    
      // cout<<"hey0"<<endl;
    for(int i_term = 0 ; i_term < N_mean_term_noErr ; i_term++){
      // cout<<"hey0_1"<<endl;
      mean_qQ_epd_unCorr_[i_term] = h_mean_qQ_epd_unCorr_[i_term]->GetBinContent(i_RM);
      mean_qB_epd_unCorr_[i_term] = h_mean_qB_epd_unCorr_[i_term]->GetBinContent(i_RM);
      mean_qS_epd_unCorr_[i_term] = h_mean_qS_epd_unCorr_[i_term]->GetBinContent(i_RM);
      // cout<<"hey1"<<endl;
      mCalCumQ->SetMean(mean_qQ_epd_unCorr_[i_term], i_term);
      mCalCumB->SetMean(mean_qB_epd_unCorr_[i_term], i_term);
      mCalCumS->SetMean(mean_qS_epd_unCorr_[i_term], i_term);
      // cout<<"hey2"<<endl;
    }//2535

    // cout<<"hey3"<<endl;
    mCalCumQ->SetCums( hQ_Nevt_epd_unCorr->GetBinContent(i_RM) );
    mCalCumB->SetCums( hB_Nevt_epd_unCorr->GetBinContent(i_RM) );
    mCalCumS->SetCums( hS_Nevt_epd_unCorr->GetBinContent(i_RM) );
    // cout<<"hey4"<<endl;

    //charge
    C_Q_1 = mCalCumQ->Get_tc1();
    C_Q_2 = mCalCumQ->Get_tc2();
    C_Q_3 = mCalCumQ->Get_tc3();
    C_Q_4 = mCalCumQ->Get_tc4();
    C_Q_5 = mCalCumQ->Get_tc5();
    C_Q_6 = mCalCumQ->Get_tc6();

    k_Q_1 = mCalCumQ->Get_tk1();
    k_Q_2 = mCalCumQ->Get_tk2();
    k_Q_3 = mCalCumQ->Get_tk3();
    k_Q_4 = mCalCumQ->Get_tk4();
    k_Q_5 = mCalCumQ->Get_tk5();
    k_Q_6 = mCalCumQ->Get_tk6();

    if(!isnan(mCalCumQ->Get_etc1())) Cerr_Q_1 = mCalCumQ->Get_etc1();
    if(!isnan(mCalCumQ->Get_etc2())) Cerr_Q_2 = mCalCumQ->Get_etc2();
    if(!isnan(mCalCumQ->Get_etc3())) Cerr_Q_3 = mCalCumQ->Get_etc3();
    if(!isnan(mCalCumQ->Get_etc4())) Cerr_Q_4 = mCalCumQ->Get_etc4();
    if(!isnan(mCalCumQ->Get_etc5())) Cerr_Q_5 = mCalCumQ->Get_etc5();
    if(!isnan(mCalCumQ->Get_etc6())) Cerr_Q_6 = mCalCumQ->Get_etc6();

    if(!isnan(mCalCumQ->Get_etk1())) kerr_Q_1 = mCalCumQ->Get_etk1();
    if(!isnan(mCalCumQ->Get_etk2())) kerr_Q_2 = mCalCumQ->Get_etk2();
    if(!isnan(mCalCumQ->Get_etk3())) kerr_Q_3 = mCalCumQ->Get_etk3();
    if(!isnan(mCalCumQ->Get_etk4())) kerr_Q_4 = mCalCumQ->Get_etk4();
    if(!isnan(mCalCumQ->Get_etk5())) kerr_Q_5 = mCalCumQ->Get_etk5();
    if(!isnan(mCalCumQ->Get_etk6())) kerr_Q_6 = mCalCumQ->Get_etk6();

    if(!isnan(mCalCumQ->Get_er21())) CRerr_Q_21 = mCalCumQ->Get_er21();
    if(!isnan(mCalCumQ->Get_er31())) CRerr_Q_31 = mCalCumQ->Get_er31();
    if(!isnan(mCalCumQ->Get_er32())) CRerr_Q_32 = mCalCumQ->Get_er32();
    if(!isnan(mCalCumQ->Get_er42())) CRerr_Q_42 = mCalCumQ->Get_er42();
    if(!isnan(mCalCumQ->Get_er51())) CRerr_Q_51 = mCalCumQ->Get_er51();
    if(!isnan(mCalCumQ->Get_er62())) CRerr_Q_62 = mCalCumQ->Get_er62();

    if(!isnan(mCalCumQ->Get_ek21())) kRerr_Q_21 = mCalCumQ->Get_ek21();
    if(!isnan(mCalCumQ->Get_ek31())) kRerr_Q_31 = mCalCumQ->Get_ek31();
    if(!isnan(mCalCumQ->Get_ek32())) kRerr_Q_32 = mCalCumQ->Get_ek32();
    if(!isnan(mCalCumQ->Get_ek41())) kRerr_Q_41 = mCalCumQ->Get_ek41();
    if(!isnan(mCalCumQ->Get_ek43())) kRerr_Q_43 = mCalCumQ->Get_ek43();
    if(!isnan(mCalCumQ->Get_ek51())) kRerr_Q_51 = mCalCumQ->Get_ek51();
    if(!isnan(mCalCumQ->Get_ek54())) kRerr_Q_54 = mCalCumQ->Get_ek54();
    if(!isnan(mCalCumQ->Get_ek61())) kRerr_Q_61 = mCalCumQ->Get_ek61();
    if(!isnan(mCalCumQ->Get_ek65())) kRerr_Q_65 = mCalCumQ->Get_ek65();

    //proton
    C_B_1 = mCalCumB->Get_tc1();
    C_B_2 = mCalCumB->Get_tc2();
    C_B_3 = mCalCumB->Get_tc3();
    C_B_4 = mCalCumB->Get_tc4();
    C_B_5 = mCalCumB->Get_tc5();
    C_B_6 = mCalCumB->Get_tc6();

    k_B_1 = mCalCumB->Get_tk1();
    k_B_2 = mCalCumB->Get_tk2();
    k_B_3 = mCalCumB->Get_tk3();
    k_B_4 = mCalCumB->Get_tk4();
    k_B_5 = mCalCumB->Get_tk5();
    k_B_6 = mCalCumB->Get_tk6();

    if(!isnan(mCalCumB->Get_etc1())) Cerr_B_1 = mCalCumB->Get_etc1();
    if(!isnan(mCalCumB->Get_etc2())) Cerr_B_2 = mCalCumB->Get_etc2();
    if(!isnan(mCalCumB->Get_etc3())) Cerr_B_3 = mCalCumB->Get_etc3();
    if(!isnan(mCalCumB->Get_etc4())) Cerr_B_4 = mCalCumB->Get_etc4();
    if(!isnan(mCalCumB->Get_etc5())) Cerr_B_5 = mCalCumB->Get_etc5();
    if(!isnan(mCalCumB->Get_etc6())) Cerr_B_6 = mCalCumB->Get_etc6();

    if(!isnan(mCalCumB->Get_etk1())) kerr_B_1 = mCalCumB->Get_etk1();
    if(!isnan(mCalCumB->Get_etk2())) kerr_B_2 = mCalCumB->Get_etk2();
    if(!isnan(mCalCumB->Get_etk3())) kerr_B_3 = mCalCumB->Get_etk3();
    if(!isnan(mCalCumB->Get_etk4())) kerr_B_4 = mCalCumB->Get_etk4();
    if(!isnan(mCalCumB->Get_etk5())) kerr_B_5 = mCalCumB->Get_etk5();
    if(!isnan(mCalCumB->Get_etk6())) kerr_B_6 = mCalCumB->Get_etk6();

    if(!isnan(mCalCumB->Get_er21())) CRerr_B_21 = mCalCumB->Get_er21();
    if(!isnan(mCalCumB->Get_er31())) CRerr_B_31 = mCalCumB->Get_er31();
    if(!isnan(mCalCumB->Get_er32())) CRerr_B_32 = mCalCumB->Get_er32();
    if(!isnan(mCalCumB->Get_er42())) CRerr_B_42 = mCalCumB->Get_er42();
    if(!isnan(mCalCumB->Get_er51())) CRerr_B_51 = mCalCumB->Get_er51();
    if(!isnan(mCalCumB->Get_er62())) CRerr_B_62 = mCalCumB->Get_er62();

    if(!isnan(mCalCumB->Get_ek21())) kRerr_B_21 = mCalCumB->Get_ek21();
    if(!isnan(mCalCumB->Get_ek31())) kRerr_B_31 = mCalCumB->Get_ek31();
    if(!isnan(mCalCumB->Get_ek32())) kRerr_B_32 = mCalCumB->Get_ek32();
    if(!isnan(mCalCumB->Get_ek41())) kRerr_B_41 = mCalCumB->Get_ek41();
    if(!isnan(mCalCumB->Get_ek43())) kRerr_B_43 = mCalCumB->Get_ek43();
    if(!isnan(mCalCumB->Get_ek51())) kRerr_B_51 = mCalCumB->Get_ek51();
    if(!isnan(mCalCumB->Get_ek54())) kRerr_B_54 = mCalCumB->Get_ek54();
    if(!isnan(mCalCumB->Get_ek61())) kRerr_B_61 = mCalCumB->Get_ek61();
    if(!isnan(mCalCumB->Get_ek65())) kRerr_B_65 = mCalCumB->Get_ek65();

    //kaon
    C_S_1 = mCalCumS->Get_tc1();
    C_S_2 = mCalCumS->Get_tc2();
    C_S_3 = mCalCumS->Get_tc3();
    C_S_4 = mCalCumS->Get_tc4();
    C_S_5 = mCalCumS->Get_tc5();
    C_S_6 = mCalCumS->Get_tc6();

    k_S_1 = mCalCumS->Get_tk1();
    k_S_2 = mCalCumS->Get_tk2();
    k_S_3 = mCalCumS->Get_tk3();
    k_S_4 = mCalCumS->Get_tk4();
    k_S_5 = mCalCumS->Get_tk5();
    k_S_6 = mCalCumS->Get_tk6();

    if(!isnan(mCalCumS->Get_etc1())) Cerr_S_1 = mCalCumS->Get_etc1();
    if(!isnan(mCalCumS->Get_etc2())) Cerr_S_2 = mCalCumS->Get_etc2();
    if(!isnan(mCalCumS->Get_etc3())) Cerr_S_3 = mCalCumS->Get_etc3();
    if(!isnan(mCalCumS->Get_etc4())) Cerr_S_4 = mCalCumS->Get_etc4();
    if(!isnan(mCalCumS->Get_etc5())) Cerr_S_5 = mCalCumS->Get_etc5();
    if(!isnan(mCalCumS->Get_etc6())) Cerr_S_6 = mCalCumS->Get_etc6();

    if(!isnan(mCalCumS->Get_etk1())) kerr_S_1 = mCalCumS->Get_etk1();
    if(!isnan(mCalCumS->Get_etk2())) kerr_S_2 = mCalCumS->Get_etk2();
    if(!isnan(mCalCumS->Get_etk3())) kerr_S_3 = mCalCumS->Get_etk3();
    if(!isnan(mCalCumS->Get_etk4())) kerr_S_4 = mCalCumS->Get_etk4();
    if(!isnan(mCalCumS->Get_etk5())) kerr_S_5 = mCalCumS->Get_etk5();
    if(!isnan(mCalCumS->Get_etk6())) kerr_S_6 = mCalCumS->Get_etk6();

    if(!isnan(mCalCumS->Get_er21())) CRerr_S_21 = mCalCumS->Get_er21();
    if(!isnan(mCalCumS->Get_er31())) CRerr_S_31 = mCalCumS->Get_er31();
    if(!isnan(mCalCumS->Get_er32())) CRerr_S_32 = mCalCumS->Get_er32();
    if(!isnan(mCalCumS->Get_er42())) CRerr_S_42 = mCalCumS->Get_er42();
    if(!isnan(mCalCumS->Get_er51())) CRerr_S_51 = mCalCumS->Get_er51();
    if(!isnan(mCalCumS->Get_er62())) CRerr_S_62 = mCalCumS->Get_er62();

    if(!isnan(mCalCumS->Get_ek21())) kRerr_S_21 = mCalCumS->Get_ek21();
    if(!isnan(mCalCumS->Get_ek31())) kRerr_S_31 = mCalCumS->Get_ek31();
    if(!isnan(mCalCumS->Get_ek32())) kRerr_S_32 = mCalCumS->Get_ek32();
    if(!isnan(mCalCumS->Get_ek41())) kRerr_S_41 = mCalCumS->Get_ek41();
    if(!isnan(mCalCumS->Get_ek43())) kRerr_S_43 = mCalCumS->Get_ek43();
    if(!isnan(mCalCumS->Get_ek51())) kRerr_S_51 = mCalCumS->Get_ek51();
    if(!isnan(mCalCumS->Get_ek54())) kRerr_S_54 = mCalCumS->Get_ek54();
    if(!isnan(mCalCumS->Get_ek61())) kRerr_S_61 = mCalCumS->Get_ek61();
    if(!isnan(mCalCumS->Get_ek65())) kRerr_S_65 = mCalCumS->Get_ek65();

    //Fill it
    //charge
    hQ_C_epd_unCorr[0]->SetBinContent(i_RM, C_Q_1);//C1
    hQ_C_epd_unCorr[1]->SetBinContent(i_RM, C_Q_2);//C2
    hQ_C_epd_unCorr[2]->SetBinContent(i_RM, C_Q_3);//C3
    hQ_C_epd_unCorr[3]->SetBinContent(i_RM, C_Q_4);//C4
    hQ_C_epd_unCorr[4]->SetBinContent(i_RM, C_Q_5);//C5
    hQ_C_epd_unCorr[5]->SetBinContent(i_RM, C_Q_6);//C6

    if(C_Q_1 != 0) hQ_C_epd_unCorr[6]->SetBinContent(i_RM, C_Q_2/C_Q_1);// C2/C1
    if(C_Q_1 != 0) hQ_C_epd_unCorr[7]->SetBinContent(i_RM, C_Q_3/C_Q_1);// C3/C1
    if(C_Q_2 != 0) hQ_C_epd_unCorr[8]->SetBinContent(i_RM, C_Q_3/C_Q_2);// C3/C2
    if(C_Q_2 != 0) hQ_C_epd_unCorr[9]->SetBinContent(i_RM, C_Q_4/C_Q_2);// C4/C2
    if(C_Q_1 != 0) hQ_C_epd_unCorr[10]->SetBinContent(i_RM, C_Q_5/C_Q_1);// C5/C1
    if(C_Q_2 != 0) hQ_C_epd_unCorr[11]->SetBinContent(i_RM, C_Q_6/C_Q_2);// C6/C2

    hQ_C_epd_unCorr[12]->SetBinContent(i_RM,  k_Q_1);//k1
    hQ_C_epd_unCorr[13]->SetBinContent(i_RM,  k_Q_2);//k2
    hQ_C_epd_unCorr[14]->SetBinContent(i_RM,  k_Q_3);//k3
    hQ_C_epd_unCorr[15]->SetBinContent(i_RM,  k_Q_4);//k4
    hQ_C_epd_unCorr[16]->SetBinContent(i_RM, k_Q_5);//k5
    hQ_C_epd_unCorr[17]->SetBinContent(i_RM, k_Q_6);//k6

    if(k_Q_1 != 0) hQ_C_epd_unCorr[18]->SetBinContent(i_RM, k_Q_2/k_Q_1);// k2/k1
    if(k_Q_1 != 0) hQ_C_epd_unCorr[19]->SetBinContent(i_RM, k_Q_3/k_Q_1);// k3/k1
    if(k_Q_2 != 0) hQ_C_epd_unCorr[20]->SetBinContent(i_RM, k_Q_3/k_Q_2);// k3/k2
    if(k_Q_1 != 0) hQ_C_epd_unCorr[21]->SetBinContent(i_RM, k_Q_4/k_Q_1);// k4/k1
    if(k_Q_3 != 0) hQ_C_epd_unCorr[22]->SetBinContent(i_RM, k_Q_4/k_Q_3);// k4/k3
    if(k_Q_1 != 0) hQ_C_epd_unCorr[23]->SetBinContent(i_RM, k_Q_5/k_Q_1);// k5/k1
    if(k_Q_4 != 0) hQ_C_epd_unCorr[24]->SetBinContent(i_RM, k_Q_5/k_Q_4);// k5/k4
    if(k_Q_1 != 0) hQ_C_epd_unCorr[25]->SetBinContent(i_RM, k_Q_6/k_Q_1);// k6/k1
    if(k_Q_5 != 0) hQ_C_epd_unCorr[26]->SetBinContent(i_RM, k_Q_6/k_Q_5);// k6/k5

    //proton
    hB_C_epd_unCorr[0]->SetBinContent(i_RM, C_B_1);//C1
    hB_C_epd_unCorr[1]->SetBinContent(i_RM, C_B_2);//C2
    hB_C_epd_unCorr[2]->SetBinContent(i_RM, C_B_3);//C3
    hB_C_epd_unCorr[3]->SetBinContent(i_RM, C_B_4);//C4
    hB_C_epd_unCorr[4]->SetBinContent(i_RM, C_B_5);//C5
    hB_C_epd_unCorr[5]->SetBinContent(i_RM, C_B_6);//C6

    if(C_B_1 != 0) hB_C_epd_unCorr[6]->SetBinContent(i_RM, C_B_2/C_B_1);// C2/C1
    if(C_B_1 != 0) hB_C_epd_unCorr[7]->SetBinContent(i_RM, C_B_3/C_B_1);// C3/C1
    if(C_B_2 != 0) hB_C_epd_unCorr[8]->SetBinContent(i_RM, C_B_3/C_B_2);// C3/C2
    if(C_B_2 != 0) hB_C_epd_unCorr[9]->SetBinContent(i_RM, C_B_4/C_B_2);// C4/C2
    if(C_B_1 != 0) hB_C_epd_unCorr[10]->SetBinContent(i_RM, C_B_5/C_B_1);// C5/C1
    if(C_B_2 != 0) hB_C_epd_unCorr[11]->SetBinContent(i_RM, C_B_6/C_B_2);// C6/C2

    hB_C_epd_unCorr[12]->SetBinContent(i_RM,  k_B_1);//k1
    hB_C_epd_unCorr[13]->SetBinContent(i_RM,  k_B_2);//k2
    hB_C_epd_unCorr[14]->SetBinContent(i_RM,  k_B_3);//k3
    hB_C_epd_unCorr[15]->SetBinContent(i_RM,  k_B_4);//k4
    hB_C_epd_unCorr[16]->SetBinContent(i_RM, k_B_5);//k5
    hB_C_epd_unCorr[17]->SetBinContent(i_RM, k_B_6);//k6

    if(k_B_1 != 0) hB_C_epd_unCorr[18]->SetBinContent(i_RM, k_B_2/k_B_1);// k2/k1
    if(k_B_1 != 0) hB_C_epd_unCorr[19]->SetBinContent(i_RM, k_B_3/k_B_1);// k3/k1
    if(k_B_2 != 0) hB_C_epd_unCorr[20]->SetBinContent(i_RM, k_B_3/k_B_2);// k3/k2
    if(k_B_1 != 0) hB_C_epd_unCorr[21]->SetBinContent(i_RM, k_B_4/k_B_1);// k4/k1
    if(k_B_3 != 0) hB_C_epd_unCorr[22]->SetBinContent(i_RM, k_B_4/k_B_3);// k4/k3
    if(k_B_1 != 0) hB_C_epd_unCorr[23]->SetBinContent(i_RM, k_B_5/k_B_1);// k5/k1
    if(k_B_4 != 0) hB_C_epd_unCorr[24]->SetBinContent(i_RM, k_B_5/k_B_4);// k5/k4
    if(k_B_1 != 0) hB_C_epd_unCorr[25]->SetBinContent(i_RM, k_B_6/k_B_1);// k6/k1
    if(k_B_5 != 0) hB_C_epd_unCorr[26]->SetBinContent(i_RM, k_B_6/k_B_5);// k6/k5

    //kaon
    hS_C_epd_unCorr[0]->SetBinContent(i_RM, C_S_1);//C1
    hS_C_epd_unCorr[1]->SetBinContent(i_RM, C_S_2);//C2
    hS_C_epd_unCorr[2]->SetBinContent(i_RM, C_S_3);//C3
    hS_C_epd_unCorr[3]->SetBinContent(i_RM, C_S_4);//C4
    hS_C_epd_unCorr[4]->SetBinContent(i_RM, C_S_5);//C5
    hS_C_epd_unCorr[5]->SetBinContent(i_RM, C_S_6);//C6

    if(C_S_1 != 0) hS_C_epd_unCorr[6]->SetBinContent(i_RM, C_S_2/C_S_1);// C2/C1
    if(C_S_1 != 0) hS_C_epd_unCorr[7]->SetBinContent(i_RM, C_S_3/C_S_1);// C3/C1
    if(C_S_2 != 0) hS_C_epd_unCorr[8]->SetBinContent(i_RM, C_S_3/C_S_2);// C3/C2
    if(C_S_2 != 0) hS_C_epd_unCorr[9]->SetBinContent(i_RM, C_S_4/C_S_2);// C4/C2
    if(C_S_1 != 0) hS_C_epd_unCorr[10]->SetBinContent(i_RM, C_S_5/C_S_1);// C5/C1
    if(C_S_2 != 0) hS_C_epd_unCorr[11]->SetBinContent(i_RM, C_S_6/C_S_2);// C6/C2

    hS_C_epd_unCorr[12]->SetBinContent(i_RM,  k_S_1);//k1
    hS_C_epd_unCorr[13]->SetBinContent(i_RM,  k_S_2);//k2
    hS_C_epd_unCorr[14]->SetBinContent(i_RM,  k_S_3);//k3
    hS_C_epd_unCorr[15]->SetBinContent(i_RM,  k_S_4);//k4
    hS_C_epd_unCorr[16]->SetBinContent(i_RM, k_S_5);//k5
    hS_C_epd_unCorr[17]->SetBinContent(i_RM, k_S_6);//k6

    if(k_S_1 != 0) hS_C_epd_unCorr[18]->SetBinContent(i_RM, k_S_2/k_S_1);// k2/k1
    if(k_S_1 != 0) hS_C_epd_unCorr[19]->SetBinContent(i_RM, k_S_3/k_S_1);// k3/k1
    if(k_S_2 != 0) hS_C_epd_unCorr[20]->SetBinContent(i_RM, k_S_3/k_S_2);// k3/k2
    if(k_S_1 != 0) hS_C_epd_unCorr[21]->SetBinContent(i_RM, k_S_4/k_S_1);// k4/k1
    if(k_S_3 != 0) hS_C_epd_unCorr[22]->SetBinContent(i_RM, k_S_4/k_S_3);// k4/k3
    if(k_S_1 != 0) hS_C_epd_unCorr[23]->SetBinContent(i_RM, k_S_5/k_S_1);// k5/k1
    if(k_S_4 != 0) hS_C_epd_unCorr[24]->SetBinContent(i_RM, k_S_5/k_S_4);// k5/k4
    if(k_S_1 != 0) hS_C_epd_unCorr[25]->SetBinContent(i_RM, k_S_6/k_S_1);// k6/k1
    if(k_S_5 != 0) hS_C_epd_unCorr[26]->SetBinContent(i_RM, k_S_6/k_S_5);// k6/k5

    ///////
    //Err//
    ///////
    //charge
    hQ_C_epd_unCorr_err[0]->SetBinContent(i_RM, Cerr_Q_1);//c_err1
    hQ_C_epd_unCorr_err[1]->SetBinContent(i_RM, Cerr_Q_2);//c_err2
    hQ_C_epd_unCorr_err[2]->SetBinContent(i_RM, Cerr_Q_3);//c_err3
    hQ_C_epd_unCorr_err[3]->SetBinContent(i_RM, Cerr_Q_4);//c_err4
    hQ_C_epd_unCorr_err[4]->SetBinContent(i_RM, Cerr_Q_5);//c_err5
    hQ_C_epd_unCorr_err[5]->SetBinContent(i_RM, Cerr_Q_6);//c_err6

    hQ_C_epd_unCorr_err[6]->SetBinContent(i_RM, kerr_Q_1);//k_err1
    hQ_C_epd_unCorr_err[7]->SetBinContent(i_RM, kerr_Q_2);//k_err2
    hQ_C_epd_unCorr_err[8]->SetBinContent(i_RM, kerr_Q_3);//k_err3
    hQ_C_epd_unCorr_err[9]->SetBinContent(i_RM, kerr_Q_4);//k_err4
    hQ_C_epd_unCorr_err[10]->SetBinContent(i_RM, kerr_Q_5);//k_err5
    hQ_C_epd_unCorr_err[11]->SetBinContent(i_RM, kerr_Q_6);//k_err6

    hQ_C_epd_unCorr_err[12]->SetBinContent(i_RM, CRerr_Q_21);//CR_err21
    hQ_C_epd_unCorr_err[13]->SetBinContent(i_RM, CRerr_Q_31);//CR_err31
    hQ_C_epd_unCorr_err[14]->SetBinContent(i_RM, CRerr_Q_32);//CR_err32
    hQ_C_epd_unCorr_err[15]->SetBinContent(i_RM, CRerr_Q_42);//CR_err42
    hQ_C_epd_unCorr_err[16]->SetBinContent(i_RM, CRerr_Q_51);//CR_err51
    hQ_C_epd_unCorr_err[17]->SetBinContent(i_RM, CRerr_Q_62);//CR_err62

    hQ_C_epd_unCorr_err[18]->SetBinContent(i_RM, kRerr_Q_21);//kR_err21
    hQ_C_epd_unCorr_err[19]->SetBinContent(i_RM, kRerr_Q_31);//kR_err31
    hQ_C_epd_unCorr_err[20]->SetBinContent(i_RM, kRerr_Q_32);//kR_err32
    hQ_C_epd_unCorr_err[21]->SetBinContent(i_RM, kRerr_Q_41);//kR_err41
    hQ_C_epd_unCorr_err[22]->SetBinContent(i_RM, kRerr_Q_43);//kR_err43
    hQ_C_epd_unCorr_err[23]->SetBinContent(i_RM, kRerr_Q_51);//kR_err51
    hQ_C_epd_unCorr_err[24]->SetBinContent(i_RM, kRerr_Q_54);//kR_err54
    hQ_C_epd_unCorr_err[25]->SetBinContent(i_RM, kRerr_Q_61);//kR_err61
    hQ_C_epd_unCorr_err[26]->SetBinContent(i_RM, kRerr_Q_65);//kR_err65
    //proton    
    hB_C_epd_unCorr_err[0]->SetBinContent(i_RM, Cerr_B_1);//c_err1
    hB_C_epd_unCorr_err[1]->SetBinContent(i_RM, Cerr_B_2);//c_err2
    hB_C_epd_unCorr_err[2]->SetBinContent(i_RM, Cerr_B_3);//c_err3
    hB_C_epd_unCorr_err[3]->SetBinContent(i_RM, Cerr_B_4);//c_err4
    hB_C_epd_unCorr_err[4]->SetBinContent(i_RM, Cerr_B_5);//c_err5
    hB_C_epd_unCorr_err[5]->SetBinContent(i_RM, Cerr_B_6);//c_err6

    hB_C_epd_unCorr_err[6]->SetBinContent(i_RM, kerr_B_1);//k_err1
    hB_C_epd_unCorr_err[7]->SetBinContent(i_RM, kerr_B_2);//k_err2
    hB_C_epd_unCorr_err[8]->SetBinContent(i_RM, kerr_B_3);//k_err3
    hB_C_epd_unCorr_err[9]->SetBinContent(i_RM, kerr_B_4);//k_err4
    hB_C_epd_unCorr_err[10]->SetBinContent(i_RM, kerr_B_5);//k_err5
    hB_C_epd_unCorr_err[11]->SetBinContent(i_RM, kerr_B_6);//k_err6

    hB_C_epd_unCorr_err[12]->SetBinContent(i_RM, CRerr_B_21);//CR_err21
    hB_C_epd_unCorr_err[13]->SetBinContent(i_RM, CRerr_B_31);//CR_err31
    hB_C_epd_unCorr_err[14]->SetBinContent(i_RM, CRerr_B_32);//CR_err32
    hB_C_epd_unCorr_err[15]->SetBinContent(i_RM, CRerr_B_42);//CR_err42
    hB_C_epd_unCorr_err[16]->SetBinContent(i_RM, CRerr_B_51);//CR_err51
    hB_C_epd_unCorr_err[17]->SetBinContent(i_RM, CRerr_B_62);//CR_err62

    hB_C_epd_unCorr_err[18]->SetBinContent(i_RM, kRerr_B_21);//kR_err21
    hB_C_epd_unCorr_err[19]->SetBinContent(i_RM, kRerr_B_31);//kR_err31
    hB_C_epd_unCorr_err[20]->SetBinContent(i_RM, kRerr_B_32);//kR_err32
    hB_C_epd_unCorr_err[21]->SetBinContent(i_RM, kRerr_B_41);//kR_err41
    hB_C_epd_unCorr_err[22]->SetBinContent(i_RM, kRerr_B_43);//kR_err43
    hB_C_epd_unCorr_err[23]->SetBinContent(i_RM, kRerr_B_51);//kR_err51
    hB_C_epd_unCorr_err[24]->SetBinContent(i_RM, kRerr_B_54);//kR_err54
    hB_C_epd_unCorr_err[25]->SetBinContent(i_RM, kRerr_B_61);//kR_err61
    hB_C_epd_unCorr_err[26]->SetBinContent(i_RM, kRerr_B_65);//kR_err65
    //kaon
    hS_C_epd_unCorr_err[0]->SetBinContent(i_RM, Cerr_S_1);//c_err1
    hS_C_epd_unCorr_err[1]->SetBinContent(i_RM, Cerr_S_2);//c_err2
    hS_C_epd_unCorr_err[2]->SetBinContent(i_RM, Cerr_S_3);//c_err3
    hS_C_epd_unCorr_err[3]->SetBinContent(i_RM, Cerr_S_4);//c_err4
    hS_C_epd_unCorr_err[4]->SetBinContent(i_RM, Cerr_S_5);//c_err5
    hS_C_epd_unCorr_err[5]->SetBinContent(i_RM, Cerr_S_6);//c_err6

    hS_C_epd_unCorr_err[6]->SetBinContent(i_RM, kerr_S_1);//k_err1
    hS_C_epd_unCorr_err[7]->SetBinContent(i_RM, kerr_S_2);//k_err2
    hS_C_epd_unCorr_err[8]->SetBinContent(i_RM, kerr_S_3);//k_err3
    hS_C_epd_unCorr_err[9]->SetBinContent(i_RM, kerr_S_4);//k_err4
    hS_C_epd_unCorr_err[10]->SetBinContent(i_RM, kerr_S_5);//k_err5
    hS_C_epd_unCorr_err[11]->SetBinContent(i_RM, kerr_S_6);//k_err6

    hS_C_epd_unCorr_err[12]->SetBinContent(i_RM, CRerr_S_21);//CR_err21
    hS_C_epd_unCorr_err[13]->SetBinContent(i_RM, CRerr_S_31);//CR_err31
    hS_C_epd_unCorr_err[14]->SetBinContent(i_RM, CRerr_S_32);//CR_err32
    hS_C_epd_unCorr_err[15]->SetBinContent(i_RM, CRerr_S_42);//CR_err42
    hS_C_epd_unCorr_err[16]->SetBinContent(i_RM, CRerr_S_51);//CR_err51
    hS_C_epd_unCorr_err[17]->SetBinContent(i_RM, CRerr_S_62);//CR_err62

    hS_C_epd_unCorr_err[18]->SetBinContent(i_RM, kRerr_S_21);//kR_err21
    hS_C_epd_unCorr_err[19]->SetBinContent(i_RM, kRerr_S_31);//kR_err31
    hS_C_epd_unCorr_err[20]->SetBinContent(i_RM, kRerr_S_32);//kR_err32
    hS_C_epd_unCorr_err[21]->SetBinContent(i_RM, kRerr_S_41);//kR_err41
    hS_C_epd_unCorr_err[22]->SetBinContent(i_RM, kRerr_S_43);//kR_err43
    hS_C_epd_unCorr_err[23]->SetBinContent(i_RM, kRerr_S_51);//kR_err51
    hS_C_epd_unCorr_err[24]->SetBinContent(i_RM, kRerr_S_54);//kR_err54
    hS_C_epd_unCorr_err[25]->SetBinContent(i_RM, kRerr_S_61);//kR_err61
    hS_C_epd_unCorr_err[26]->SetBinContent(i_RM, kRerr_S_65);//kR_err65

  }//refMult loop ends

  WriteEffUnCorrEPDmultCum(Form("output/cum/%s_eff_EPDmult_unCorr_cum.root", NameRuZr));
  
  delete mCalCumQ;
  delete mCalCumB;
  delete mCalCumS;
}

//////////////////////////////////////////

void Cum::WriteEffUnCorrEPDmultCum(const char* outFile){

  TFile *f_out = new TFile(outFile, "RECREATE");
  f_out->cd();
  for(int i = 0 ; i < Ncums_noErr ; i++){
    // hQ_C_epd_unCorr[i]->Write();
    hB_C_epd_unCorr[i]->Write();
    // hS_C_epd_unCorr[i]->Write();
    // hQ_C_epd_unCorr_err[i]->Write();
    hB_C_epd_unCorr_err[i]->Write();
    // hS_C_epd_unCorr_err[i]->Write();
  }

  // hQ_Nevt_epd_unCorr->Write();
  hB_Nevt_epd_unCorr->Write();
  // hS_Nevt_epd_unCorr->Write();
  f_out->Close();
  
}

//////////////////////////////////////////

////////////
//RefMult3//
////////////
void Cum::GetRefMult3Means(const char* inFile){
  //get terms in Eq. 62 ~ 67 & 69 in arXiv:1702.07106v3
  TFile *f_in = new TFile(inFile, "READ");
  std::map<const char*, TProfile*>::iterator iter;
  ///proton///
  TIter nextkeyB(f_in->GetListOfKeys());
  TKey *keyB;
  while((keyB = (TKey*)nextkeyB())){
    TString strName = keyB->ReadObj()->GetName();
    // if(strName.Contains(Form("%s_ref3", "proton"))){
    //   strName.Replace(0, strlen(Form("%s_ref3", "proton"))+1,"");
    //parasite
    if(strName.Contains(Form("%s_ref3StPicoEvt", "proton"))){
      strName.Replace(0, strlen(Form("%s_ref3StPicoEvt", "proton"))+1,"");
      // cout<<strName<<endl;
    } else {
      continue;
    }
    int iT = 0;
    while (true) {
      if(iT == N_mean_term_noErr) {
	std::cout<<"In Cum::GetRefMult3Means"<<std::endl;
	std::cout<<"Found bad terms"<<std::endl;
	abort();
      }
      if(strcmp(strName, mean_term_name_noErr[iT])==0){
	h_mean_qB_ref3_[iT] = (TProfile*) keyB->ReadObj()->Clone();
	break;
      } else {
	iT++;
      }
    }
  }

  // TProfile* entB = (TProfile*)f_in->Get(Form("%s_ref3_q01_01","proton"));
  //parasite
  TProfile* entB = (TProfile*)f_in->Get(Form("%s_ref3StPicoEvt_q01_01","proton"));
  for(int i=1;i<entB->GetNbinsX();++i) {
    hB_Nevt_ref3->SetBinContent(i, entB->GetBinEntries(i));//getting No. of Evt per refMult
    // cout<<ent->GetBinEntries(i)<<endl;
  }
}

//////////////////////////////////////////

void Cum::CalculateEffCorrRefMult3Cum(const char* NameRuZr, int dca_cut, int nhitsfit_cut, int nsp_cut, int m2low_cut, int m2up_cut, int ptmax_cut, int ymax_cut, int detefflow_err, int deteffhigh_err){
  //calculate eff corr cumulant

  //proton
  double C_B_1, C_B_2, C_B_3, C_B_4, C_B_5, C_B_6;
  double k_B_1, k_B_2, k_B_3, k_B_4, k_B_5, k_B_6;
  double Cerr_B_1, Cerr_B_2, Cerr_B_3, Cerr_B_4, Cerr_B_5, Cerr_B_6;
  double kerr_B_1, kerr_B_2, kerr_B_3, kerr_B_4, kerr_B_5, kerr_B_6;
  double CRerr_B_21, CRerr_B_31, CRerr_B_32, CRerr_B_42, CRerr_B_51, CRerr_B_62;
  double kRerr_B_21, kRerr_B_31, kRerr_B_32, kRerr_B_41, kRerr_B_43, kRerr_B_51, kRerr_B_54, kRerr_B_61, kRerr_B_65;

  double mean_qB_ref3_[N_mean_term_noErr];

  CalCum *mCalCumB = new CalCum();//proton

  //temporary
  int nan_counter_cr51 = 0;
  int nan_counter_cr62 = 0;
  
  //01072021(start)
  // for(int i_RM = 0 ; i_RM < maxMult ; i_RM++){
  for(int i_RM = 0 ; i_RM < maxMult_ref3 ; i_RM++){
    //01072021(finish)
    mCalCumB->Init();
    //01072021(start)
    // cout<<"Cum::CalculateEffCorrRefMult3Cum(). Multiplicity: "<<i_RM<<"/"<<maxMult<<endl;
    cout<<"Cum::CalculateEffCorrRefMult3Cum(). Multiplicity: "<<i_RM<<"/"<<maxMult_ref3<<endl;
    //01072021(finish)
    //initialize
    //proton
    C_B_1 = C_B_2 = C_B_3 = C_B_4 = C_B_5 = C_B_6 = 0. ;
    k_B_1 = k_B_2 = k_B_3 = k_B_4 = k_B_5 = k_B_6 = 0. ;
    Cerr_B_1 = Cerr_B_2 = Cerr_B_3 = Cerr_B_4 = Cerr_B_5 = Cerr_B_6 = 0. ;
    kerr_B_1 = kerr_B_2 = kerr_B_3 = kerr_B_4 = kerr_B_5 = kerr_B_6 = 0. ;
    CRerr_B_21 = CRerr_B_31 = CRerr_B_32 = CRerr_B_42 = CRerr_B_51 = CRerr_B_62 = 0. ;
    kRerr_B_21 = kRerr_B_31 = kRerr_B_32 = kRerr_B_41 = kRerr_B_43 = kRerr_B_51 = kRerr_B_54 = kRerr_B_61 = kRerr_B_65 = 0. ;

    //01122021(start)
    int Nevt = h_mean_qB_ref3_[0]->GetBinEntries(i_RM);
    if(Nevt < 1) continue;
    //01122021(finish)

    for(int i_term = 0 ; i_term < N_mean_term_noErr ; i_term++){
      mean_qB_ref3_[i_term] = h_mean_qB_ref3_[i_term]->GetBinContent(i_RM);
      mCalCumB->SetMean(mean_qB_ref3_[i_term], i_term);
    }//2535

    //01122021(start)
    // mCalCumB->SetCums( hB_Nevt_ref3->GetBinContent(i_RM) );
    mCalCumB->SetCums( Nevt );
    //01122021(finish)

    //proton
    C_B_1 = mCalCumB->Get_tc1();
    C_B_2 = mCalCumB->Get_tc2();
    C_B_3 = mCalCumB->Get_tc3();
    C_B_4 = mCalCumB->Get_tc4();
    C_B_5 = mCalCumB->Get_tc5();
    C_B_6 = mCalCumB->Get_tc6();

    k_B_1 = mCalCumB->Get_tk1();
    k_B_2 = mCalCumB->Get_tk2();
    k_B_3 = mCalCumB->Get_tk3();
    k_B_4 = mCalCumB->Get_tk4();
    k_B_5 = mCalCumB->Get_tk5();
    k_B_6 = mCalCumB->Get_tk6();

    //06082021(start)
    // if(!isnan(mCalCumB->Get_etc1())) Cerr_B_1 = mCalCumB->Get_etc1();
    // if(!isnan(mCalCumB->Get_etc2())) Cerr_B_2 = mCalCumB->Get_etc2();
    // if(!isnan(mCalCumB->Get_etc3())) Cerr_B_3 = mCalCumB->Get_etc3();
    // if(!isnan(mCalCumB->Get_etc4())) Cerr_B_4 = mCalCumB->Get_etc4();
    // if(!isnan(mCalCumB->Get_etc5())) Cerr_B_5 = mCalCumB->Get_etc5();
    // if(!isnan(mCalCumB->Get_etc6())) Cerr_B_6 = mCalCumB->Get_etc6();

    // if(!isnan(mCalCumB->Get_etk1())) kerr_B_1 = mCalCumB->Get_etk1();
    // if(!isnan(mCalCumB->Get_etk2())) kerr_B_2 = mCalCumB->Get_etk2();
    // if(!isnan(mCalCumB->Get_etk3())) kerr_B_3 = mCalCumB->Get_etk3();
    // if(!isnan(mCalCumB->Get_etk4())) kerr_B_4 = mCalCumB->Get_etk4();
    // if(!isnan(mCalCumB->Get_etk5())) kerr_B_5 = mCalCumB->Get_etk5();
    // if(!isnan(mCalCumB->Get_etk6())) kerr_B_6 = mCalCumB->Get_etk6();

    // if(!isnan(mCalCumB->Get_er21())) CRerr_B_21 = mCalCumB->Get_er21();
    // if(!isnan(mCalCumB->Get_er31())) CRerr_B_31 = mCalCumB->Get_er31();
    // if(!isnan(mCalCumB->Get_er32())) CRerr_B_32 = mCalCumB->Get_er32();
    // if(!isnan(mCalCumB->Get_er42())) CRerr_B_42 = mCalCumB->Get_er42();
    // if(!isnan(mCalCumB->Get_er51())) CRerr_B_51 = mCalCumB->Get_er51();
    // if(!isnan(mCalCumB->Get_er62())) CRerr_B_62 = mCalCumB->Get_er62();

    // if(!isnan(mCalCumB->Get_ek21())) kRerr_B_21 = mCalCumB->Get_ek21();
    // if(!isnan(mCalCumB->Get_ek31())) kRerr_B_31 = mCalCumB->Get_ek31();
    // if(!isnan(mCalCumB->Get_ek32())) kRerr_B_32 = mCalCumB->Get_ek32();
    // if(!isnan(mCalCumB->Get_ek41())) kRerr_B_41 = mCalCumB->Get_ek41();
    // if(!isnan(mCalCumB->Get_ek43())) kRerr_B_43 = mCalCumB->Get_ek43();
    // if(!isnan(mCalCumB->Get_ek51())) kRerr_B_51 = mCalCumB->Get_ek51();
    // if(!isnan(mCalCumB->Get_ek54())) kRerr_B_54 = mCalCumB->Get_ek54();
    // if(!isnan(mCalCumB->Get_ek61())) kRerr_B_61 = mCalCumB->Get_ek61();
    // if(!isnan(mCalCumB->Get_ek65())) kRerr_B_65 = mCalCumB->Get_ek65();
    //06082021(finish)

    //01122012(start)
    // //14092021(start)
    // if(!isnan(mCalCumB->Get_etc1())) Cerr_B_1 = mCalCumB->Get_etc1();
    // if(!isnan(mCalCumB->Get_etc2())) Cerr_B_2 = mCalCumB->Get_etc2();
    // if(!isnan(mCalCumB->Get_etc3())) Cerr_B_3 = mCalCumB->Get_etc3();
    // if(!isnan(mCalCumB->Get_etc4())) Cerr_B_4 = mCalCumB->Get_etc4();
    // if(!isnan(mCalCumB->Get_etc5())) {
    //   Cerr_B_5 = mCalCumB->Get_etc5();
    //   cout<<"i_RM, C_B_5, mCalCumB->Get_etc5(): "<<i_RM<<", "<<C_B_5<<", "<<mCalCumB->Get_etc5()<<endl;
    // }
    // if(!isnan(mCalCumB->Get_etc6())) {
    //   Cerr_B_6 = mCalCumB->Get_etc6();
    //   cout<<"i_RM, C_B_6, mCalCumB->Get_etc6(): "<<i_RM<<", "<<C_B_6<<", "<<mCalCumB->Get_etc6()<<endl;
    // }
    // //14092021(finish)
    Cerr_B_1 = mCalCumB->Get_etc1();
    Cerr_B_2 = mCalCumB->Get_etc2();
    Cerr_B_3 = mCalCumB->Get_etc3();
    Cerr_B_4 = mCalCumB->Get_etc4();
    Cerr_B_5 = mCalCumB->Get_etc5();
    Cerr_B_6 = mCalCumB->Get_etc6();
    //01122012(finish)

    kerr_B_1 = mCalCumB->Get_etk1();
    kerr_B_2 = mCalCumB->Get_etk2();
    kerr_B_3 = mCalCumB->Get_etk3();
    kerr_B_4 = mCalCumB->Get_etk4();
    kerr_B_5 = mCalCumB->Get_etk5();
    kerr_B_6 = mCalCumB->Get_etk6();

    if(C_B_1 != 0) CRerr_B_21 = mCalCumB->Get_er21();
    if(C_B_1 != 0) CRerr_B_31 = mCalCumB->Get_er31();
    if(C_B_2 != 0) CRerr_B_32 = mCalCumB->Get_er32();
    if(C_B_2 != 0) CRerr_B_42 = mCalCumB->Get_er42();
    if(C_B_1 != 0) CRerr_B_51 = mCalCumB->Get_er51();
    if(C_B_2 != 0) CRerr_B_62 = mCalCumB->Get_er62();

    if(k_B_1 != 0) kRerr_B_21 = mCalCumB->Get_ek21();
    if(k_B_1 != 0) kRerr_B_31 = mCalCumB->Get_ek31();
    if(k_B_2 != 0) kRerr_B_32 = mCalCumB->Get_ek32();
    if(k_B_1 != 0) kRerr_B_41 = mCalCumB->Get_ek41();
    if(k_B_3 != 0) kRerr_B_43 = mCalCumB->Get_ek43();
    if(k_B_1 != 0) kRerr_B_51 = mCalCumB->Get_ek51();
    if(k_B_4 != 0) kRerr_B_54 = mCalCumB->Get_ek54();
    if(k_B_1 != 0) kRerr_B_61 = mCalCumB->Get_ek61();
    if(k_B_5 != 0) kRerr_B_65 = mCalCumB->Get_ek65();

    //Fill it
    //proton
    hB_C_ref3[0]->SetBinContent(i_RM, C_B_1);//C1
    hB_C_ref3[1]->SetBinContent(i_RM, C_B_2);//C2
    hB_C_ref3[2]->SetBinContent(i_RM, C_B_3);//C3
    hB_C_ref3[3]->SetBinContent(i_RM, C_B_4);//C4
    hB_C_ref3[4]->SetBinContent(i_RM, C_B_5);//C5
    hB_C_ref3[5]->SetBinContent(i_RM, C_B_6);//C6
    hB_C_ref3[0]->SetBinError(i_RM, mCalCumB->Get_etc1());//C1
    hB_C_ref3[1]->SetBinError(i_RM, mCalCumB->Get_etc2());//C2
    hB_C_ref3[2]->SetBinError(i_RM, mCalCumB->Get_etc3());//C3
    hB_C_ref3[3]->SetBinError(i_RM, mCalCumB->Get_etc4());//C4
    hB_C_ref3[4]->SetBinError(i_RM, mCalCumB->Get_etc5());//C5
    hB_C_ref3[5]->SetBinError(i_RM, mCalCumB->Get_etc6());//C6

    // if(C_B_1 != 0) {
    //14092021(start)
    // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er21()) && mCalCumB->Get_er21() < 1e6) {//27082021 some of them is nan
    //15092021: commented out the if statement. Later during CBWC, reject the whole "nan" multbin.
    //01122021(start)
    // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er21())) {//27082021 some of them is nan
    if(C_B_1 != 0) {//27082021 some of them is nan
    //01122021(finish)
      //14092021(finish)
      hB_C_ref3[6]->SetBinContent(i_RM, C_B_2/C_B_1);// C2/C1
      hB_C_ref3[6]->SetBinError(i_RM, mCalCumB->Get_er21());// C2/C1
    }
    // if(C_B_1 != 0){
    //14092021(start)
    // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er31()) && mCalCumB->Get_er31() < 1e6) {
    //01122021(start)
    // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er31())) {
    if(C_B_1 != 0) {
    //01122021(finish)
      //14092021(finish)
      hB_C_ref3[7]->SetBinContent(i_RM, C_B_3/C_B_1);// C3/C1
      hB_C_ref3[7]->SetBinError(i_RM, mCalCumB->Get_er31());// C3/C1
    }
    // if(C_B_2 != 0){
    //14092021(start)
    // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er32()) && mCalCumB->Get_er32() < 1e6) {
    //01122021(start)
    // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er32())) {
    if(C_B_2 != 0) {
    //01122021(finish)
      //14092021(finish)
      hB_C_ref3[8]->SetBinContent(i_RM, C_B_3/C_B_2);// C3/C2
      hB_C_ref3[8]->SetBinError(i_RM, mCalCumB->Get_er32());// C3/C2
    }
    // if(C_B_2 != 0){
    //14092021(start)
    // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er42()) && mCalCumB->Get_er42() < 1e6) {
    //01122021(start)
    // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er42())) {
    if(C_B_2 != 0) {
    //01122021(finish)
      //14092021(finish)
      hB_C_ref3[9]->SetBinContent(i_RM, C_B_4/C_B_2);// C4/C2
      hB_C_ref3[9]->SetBinError(i_RM, mCalCumB->Get_er42());// C4/C2
    }
    // if(C_B_1 != 0){
    //14092021(start)
    // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er51()) && mCalCumB->Get_er51() < 1e6) {
    //01122021(start)
    // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er51())) {
    if(C_B_1 != 0){
    //01122021(finish)
      //14092021(finish)
      hB_C_ref3[10]->SetBinContent(i_RM, C_B_5/C_B_1);// C5/C1
      hB_C_ref3[10]->SetBinError(i_RM, mCalCumB->Get_er51());// C5/C1
      cout<<"i_RM, C_B_5/C_B_1, mCalCumB->Get_er51(): "<<i_RM<<", "<<C_B_5/C_B_1<<", "<<mCalCumB->Get_er51()<<endl;
      if(isnan(mCalCumB->Get_er51())) nan_counter_cr51++;
    }
    // if(C_B_2 != 0){
    //14092021(start)
    // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er62()) && mCalCumB->Get_er62() < 1e6) {
    //01122021(start)
    // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er62())) {
    if(C_B_2 != 0){
    //01122021(finish)
      //14092021(finish)
      hB_C_ref3[11]->SetBinContent(i_RM, C_B_6/C_B_2);// C6/C2
      hB_C_ref3[11]->SetBinError(i_RM, mCalCumB->Get_er62());// C6/C2
      cout<<"i_RM, C_B_6/C_B_2, mCalCumB->Get_er62(): "<<i_RM<<", "<<C_B_6/C_B_2<<", "<<mCalCumB->Get_er62()<<endl;
      if(isnan(mCalCumB->Get_er62())) {
	cout<<"!!!!i_RM = "<<i_RM<<", mCalCumB->Get_er62() = "<<mCalCumB->Get_er62()<<endl;
	nan_counter_cr62++;
      }
    }

    hB_C_ref3[12]->SetBinContent(i_RM,  k_B_1);//k1
    hB_C_ref3[13]->SetBinContent(i_RM,  k_B_2);//k2
    hB_C_ref3[14]->SetBinContent(i_RM,  k_B_3);//k3
    hB_C_ref3[15]->SetBinContent(i_RM,  k_B_4);//k4
    hB_C_ref3[16]->SetBinContent(i_RM, k_B_5);//k5
    hB_C_ref3[17]->SetBinContent(i_RM, k_B_6);//k6

    if(k_B_1 != 0) hB_C_ref3[18]->SetBinContent(i_RM, k_B_2/k_B_1);// k2/k1
    if(k_B_1 != 0) hB_C_ref3[19]->SetBinContent(i_RM, k_B_3/k_B_1);// k3/k1
    if(k_B_2 != 0) hB_C_ref3[20]->SetBinContent(i_RM, k_B_3/k_B_2);// k3/k2
    if(k_B_1 != 0) hB_C_ref3[21]->SetBinContent(i_RM, k_B_4/k_B_1);// k4/k1
    if(k_B_3 != 0) hB_C_ref3[22]->SetBinContent(i_RM, k_B_4/k_B_3);// k4/k3
    if(k_B_1 != 0) hB_C_ref3[23]->SetBinContent(i_RM, k_B_5/k_B_1);// k5/k1
    if(k_B_4 != 0) hB_C_ref3[24]->SetBinContent(i_RM, k_B_5/k_B_4);// k5/k4
    if(k_B_1 != 0) hB_C_ref3[25]->SetBinContent(i_RM, k_B_6/k_B_1);// k6/k1
    if(k_B_5 != 0) hB_C_ref3[26]->SetBinContent(i_RM, k_B_6/k_B_5);// k6/k5

    ///////
    //Err//
    ///////
    //proton    
    hB_C_ref3_err[0]->SetBinContent(i_RM, Cerr_B_1);//c_err1
    hB_C_ref3_err[1]->SetBinContent(i_RM, Cerr_B_2);//c_err2
    hB_C_ref3_err[2]->SetBinContent(i_RM, Cerr_B_3);//c_err3
    hB_C_ref3_err[3]->SetBinContent(i_RM, Cerr_B_4);//c_err4
    hB_C_ref3_err[4]->SetBinContent(i_RM, Cerr_B_5);//c_err5
    hB_C_ref3_err[5]->SetBinContent(i_RM, Cerr_B_6);//c_err6

    hB_C_ref3_err[6]->SetBinContent(i_RM, kerr_B_1);//k_err1
    hB_C_ref3_err[7]->SetBinContent(i_RM, kerr_B_2);//k_err2
    hB_C_ref3_err[8]->SetBinContent(i_RM, kerr_B_3);//k_err3
    hB_C_ref3_err[9]->SetBinContent(i_RM, kerr_B_4);//k_err4
    hB_C_ref3_err[10]->SetBinContent(i_RM, kerr_B_5);//k_err5
    hB_C_ref3_err[11]->SetBinContent(i_RM, kerr_B_6);//k_err6

    hB_C_ref3_err[12]->SetBinContent(i_RM, CRerr_B_21);//CR_err21
    hB_C_ref3_err[13]->SetBinContent(i_RM, CRerr_B_31);//CR_err31
    hB_C_ref3_err[14]->SetBinContent(i_RM, CRerr_B_32);//CR_err32
    hB_C_ref3_err[15]->SetBinContent(i_RM, CRerr_B_42);//CR_err42
    hB_C_ref3_err[16]->SetBinContent(i_RM, CRerr_B_51);//CR_err51
    hB_C_ref3_err[17]->SetBinContent(i_RM, CRerr_B_62);//CR_err62

    hB_C_ref3_err[18]->SetBinContent(i_RM, kRerr_B_21);//kR_err21
    hB_C_ref3_err[19]->SetBinContent(i_RM, kRerr_B_31);//kR_err31
    hB_C_ref3_err[20]->SetBinContent(i_RM, kRerr_B_32);//kR_err32
    hB_C_ref3_err[21]->SetBinContent(i_RM, kRerr_B_41);//kR_err41
    hB_C_ref3_err[22]->SetBinContent(i_RM, kRerr_B_43);//kR_err43
    hB_C_ref3_err[23]->SetBinContent(i_RM, kRerr_B_51);//kR_err51
    hB_C_ref3_err[24]->SetBinContent(i_RM, kRerr_B_54);//kR_err54
    hB_C_ref3_err[25]->SetBinContent(i_RM, kRerr_B_61);//kR_err61
    hB_C_ref3_err[26]->SetBinContent(i_RM, kRerr_B_65);//kR_err65

  }//refMult loop ends

  //parasite
  WriteEffCorrRefMult3Cum(Form("output/cum/%s_effcorr_ref3corr_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_ptmax_%d_ymax_%d_detefflow_%d_deteffhigh_%d_maxmult_%d_29012022_1_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, detefflow_err, deteffhigh_err, maxMult_ref3StPicoEvt, MergeTopCent));
  
  //temporary
  cout<<"Cum::CalculateEffCorrRefMult3Cum"<<endl;
  cout<<"nan_counter_cr51 = "<<nan_counter_cr51<<endl;
  cout<<"nan_counter_cr62 = "<<nan_counter_cr62<<endl;
  
  delete mCalCumB;
}

//////////////////////////////////////////

void Cum::WriteEffCorrRefMult3Cum(const char* outFile){

  TFile *f_out = new TFile(outFile, "RECREATE");
  f_out->cd();
  for(int i = 0 ; i < Ncums_noErr ; i++){
    hB_C_ref3[i]->Write();
    hB_C_ref3_err[i]->Write();
  }

  hB_Nevt_ref3->Write();
  f_out->Close();
  
}

//////////////////////////////////////////

void Cum::GetRefMult3UnCorrMeans(const char* inFile){
  cout<<"hey"<<endl;
  //get terms in Eq. 62 ~ 67 & 69 in arXiv:1702.07106v3
  TFile *f_in = new TFile(inFile, "READ");
  std::map<const char*, TProfile*>::iterator iter;
  ///proton///
  TIter nextkeyB(f_in->GetListOfKeys());
  TKey *keyB;
  while((keyB = (TKey*)nextkeyB())){
    TString strName = keyB->ReadObj()->GetName();
    // if(strName.Contains(Form("%s_ref3_unCorr", "proton"))){
    //   strName.Replace(0, strlen(Form("%s_ref3_unCorr", "proton"))+1,"");
    //parasite
    if(strName.Contains(Form("%s_ref3StPicoEvt_unCorr", "proton"))){
      strName.Replace(0, strlen(Form("%s_ref3StPicoEvt_unCorr", "proton"))+1,"");
      // cout<<strName<<endl;
    } else {
      continue;
    }
    int iT = 0;
    while (true) {
      if(iT == N_mean_term_noErr) {
	std::cout<<"In Cum::GetRefMult3UnCorrMeans"<<std::endl;
	std::cout<<"Found bad terms"<<std::endl;
	abort();
      }
      if(strcmp(strName, mean_term_name_noErr[iT])==0){
	h_mean_qB_ref3_unCorr_[iT] = (TProfile*) keyB->ReadObj()->Clone();
	break;
      } else {
	iT++;
      }
    }
  }

  // TProfile* entB = (TProfile*)f_in->Get(Form("%s_ref3_unCorr_q01_01","proton"));
  //parasite
  TProfile* entB = (TProfile*)f_in->Get(Form("%s_ref3StPicoEvt_unCorr_q01_01","proton"));
  for(int i=1;i<entB->GetNbinsX();++i) {
    hB_Nevt_ref3_unCorr->SetBinContent(i, entB->GetBinEntries(i));//getting No. of Evt per refMult
    // cout<<ent->GetBinEntries(i)<<endl;
  }
}

//////////////////////////////////////////

// // void Cum::CalculateEffUnCorrRefMult3Cum(const char* NameRuZr){
// void Cum::CalculateEffUnCorrRefMult3Cum(const char* NameRuZr){
//   //calculate eff corr cumulant

//   //proton
//   double C_B_1, C_B_2, C_B_3, C_B_4, C_B_5, C_B_6;
//   double k_B_1, k_B_2, k_B_3, k_B_4, k_B_5, k_B_6;
//   double Cerr_B_1, Cerr_B_2, Cerr_B_3, Cerr_B_4, Cerr_B_5, Cerr_B_6;
//   double kerr_B_1, kerr_B_2, kerr_B_3, kerr_B_4, kerr_B_5, kerr_B_6;
//   double CRerr_B_21, CRerr_B_31, CRerr_B_32, CRerr_B_42, CRerr_B_51, CRerr_B_62;
//   double kRerr_B_21, kRerr_B_31, kRerr_B_32, kRerr_B_41, kRerr_B_43, kRerr_B_51, kRerr_B_54, kRerr_B_61, kRerr_B_65;

//   double mean_qB_ref3_unCorr_[N_mean_term_noErr];

//   CalCum *mCalCumB = new CalCum();//proton

//   //01072021(start)
//   // for(int i_RM = 0 ; i_RM < maxMult ; i_RM++){
//   for(int i_RM = 0 ; i_RM < maxMult_ref3 ; i_RM++){
//   //01072021(finish)
//     mCalCumB->Init();
//   //01072021(start)
//     // cout<<"Cum::CalculateEffUnCorrRefMult3Cum(). Multiplicity: "<<i_RM<<"/"<<maxMult<<endl;
//     cout<<"Cum::CalculateEffUnCorrRefMult3Cum(). Multiplicity: "<<i_RM<<"/"<<maxMult_ref3<<endl;
//   //01072021(finish)
//     //initialize
//     //proton
//     C_B_1 = C_B_2 = C_B_3 = C_B_4 = C_B_5 = C_B_6 = 0. ;
//     k_B_1 = k_B_2 = k_B_3 = k_B_4 = k_B_5 = k_B_6 = 0. ;
//     Cerr_B_1 = Cerr_B_2 = Cerr_B_3 = Cerr_B_4 = Cerr_B_5 = Cerr_B_6 = 0. ;
//     kerr_B_1 = kerr_B_2 = kerr_B_3 = kerr_B_4 = kerr_B_5 = kerr_B_6 = 0. ;
//     CRerr_B_21 = CRerr_B_31 = CRerr_B_32 = CRerr_B_42 = CRerr_B_51 = CRerr_B_62 = 0. ;
//     kRerr_B_21 = kRerr_B_31 = kRerr_B_32 = kRerr_B_41 = kRerr_B_43 = kRerr_B_51 = kRerr_B_54 = kRerr_B_61 = kRerr_B_65 = 0. ;

//     for(int i_term = 0 ; i_term < N_mean_term_noErr ; i_term++){
//       mean_qB_ref3_unCorr_[i_term] = h_mean_qB_ref3_unCorr_[i_term]->GetBinContent(i_RM);
//       mCalCumB->SetMean(mean_qB_ref3_unCorr_[i_term], i_term);
//     }//2535

//     mCalCumB->SetCums( hB_Nevt_ref3_unCorr->GetBinContent(i_RM) );

//     //proton
//     C_B_1 = mCalCumB->Get_tc1();
//     C_B_2 = mCalCumB->Get_tc2();
//     C_B_3 = mCalCumB->Get_tc3();
//     C_B_4 = mCalCumB->Get_tc4();
//     C_B_5 = mCalCumB->Get_tc5();
//     C_B_6 = mCalCumB->Get_tc6();

//     k_B_1 = mCalCumB->Get_tk1();
//     k_B_2 = mCalCumB->Get_tk2();
//     k_B_3 = mCalCumB->Get_tk3();
//     k_B_4 = mCalCumB->Get_tk4();
//     k_B_5 = mCalCumB->Get_tk5();
//     k_B_6 = mCalCumB->Get_tk6();

//     if(!isnan(mCalCumB->Get_etc1())) Cerr_B_1 = mCalCumB->Get_etc1();
//     if(!isnan(mCalCumB->Get_etc2())) Cerr_B_2 = mCalCumB->Get_etc2();
//     if(!isnan(mCalCumB->Get_etc3())) Cerr_B_3 = mCalCumB->Get_etc3();
//     if(!isnan(mCalCumB->Get_etc4())) Cerr_B_4 = mCalCumB->Get_etc4();
//     if(!isnan(mCalCumB->Get_etc5())) Cerr_B_5 = mCalCumB->Get_etc5();
//     if(!isnan(mCalCumB->Get_etc6())) Cerr_B_6 = mCalCumB->Get_etc6();

//     if(!isnan(mCalCumB->Get_etk1())) kerr_B_1 = mCalCumB->Get_etk1();
//     if(!isnan(mCalCumB->Get_etk2())) kerr_B_2 = mCalCumB->Get_etk2();
//     if(!isnan(mCalCumB->Get_etk3())) kerr_B_3 = mCalCumB->Get_etk3();
//     if(!isnan(mCalCumB->Get_etk4())) kerr_B_4 = mCalCumB->Get_etk4();
//     if(!isnan(mCalCumB->Get_etk5())) kerr_B_5 = mCalCumB->Get_etk5();
//     if(!isnan(mCalCumB->Get_etk6())) kerr_B_6 = mCalCumB->Get_etk6();

//     if(!isnan(mCalCumB->Get_er21())) CRerr_B_21 = mCalCumB->Get_er21();
//     if(!isnan(mCalCumB->Get_er31())) CRerr_B_31 = mCalCumB->Get_er31();
//     if(!isnan(mCalCumB->Get_er32())) CRerr_B_32 = mCalCumB->Get_er32();
//     if(!isnan(mCalCumB->Get_er42())) CRerr_B_42 = mCalCumB->Get_er42();
//     if(!isnan(mCalCumB->Get_er51())) CRerr_B_51 = mCalCumB->Get_er51();
//     if(!isnan(mCalCumB->Get_er62())) CRerr_B_62 = mCalCumB->Get_er62();

//     if(!isnan(mCalCumB->Get_ek21())) kRerr_B_21 = mCalCumB->Get_ek21();
//     if(!isnan(mCalCumB->Get_ek31())) kRerr_B_31 = mCalCumB->Get_ek31();
//     if(!isnan(mCalCumB->Get_ek32())) kRerr_B_32 = mCalCumB->Get_ek32();
//     if(!isnan(mCalCumB->Get_ek41())) kRerr_B_41 = mCalCumB->Get_ek41();
//     if(!isnan(mCalCumB->Get_ek43())) kRerr_B_43 = mCalCumB->Get_ek43();
//     if(!isnan(mCalCumB->Get_ek51())) kRerr_B_51 = mCalCumB->Get_ek51();
//     if(!isnan(mCalCumB->Get_ek54())) kRerr_B_54 = mCalCumB->Get_ek54();
//     if(!isnan(mCalCumB->Get_ek61())) kRerr_B_61 = mCalCumB->Get_ek61();
//     if(!isnan(mCalCumB->Get_ek65())) kRerr_B_65 = mCalCumB->Get_ek65();

//     //Fill it
//     hB_C_ref3_unCorr[0]->SetBinContent(i_RM, C_B_1);//C1
//     hB_C_ref3_unCorr[1]->SetBinContent(i_RM, C_B_2);//C2
//     hB_C_ref3_unCorr[2]->SetBinContent(i_RM, C_B_3);//C3
//     hB_C_ref3_unCorr[3]->SetBinContent(i_RM, C_B_4);//C4
//     hB_C_ref3_unCorr[4]->SetBinContent(i_RM, C_B_5);//C5
//     hB_C_ref3_unCorr[5]->SetBinContent(i_RM, C_B_6);//C6

//     if(C_B_1 != 0) hB_C_ref3_unCorr[6]->SetBinContent(i_RM, C_B_2/C_B_1);// C2/C1
//     if(C_B_1 != 0) hB_C_ref3_unCorr[7]->SetBinContent(i_RM, C_B_3/C_B_1);// C3/C1
//     if(C_B_2 != 0) hB_C_ref3_unCorr[8]->SetBinContent(i_RM, C_B_3/C_B_2);// C3/C2
//     if(C_B_2 != 0) hB_C_ref3_unCorr[9]->SetBinContent(i_RM, C_B_4/C_B_2);// C4/C2
//     if(C_B_1 != 0) hB_C_ref3_unCorr[10]->SetBinContent(i_RM, C_B_5/C_B_1);// C5/C1
//     if(C_B_2 != 0) hB_C_ref3_unCorr[11]->SetBinContent(i_RM, C_B_6/C_B_2);// C6/C2

//     hB_C_ref3_unCorr[12]->SetBinContent(i_RM,  k_B_1);//k1
//     hB_C_ref3_unCorr[13]->SetBinContent(i_RM,  k_B_2);//k2
//     hB_C_ref3_unCorr[14]->SetBinContent(i_RM,  k_B_3);//k3
//     hB_C_ref3_unCorr[15]->SetBinContent(i_RM,  k_B_4);//k4
//     hB_C_ref3_unCorr[16]->SetBinContent(i_RM, k_B_5);//k5
//     hB_C_ref3_unCorr[17]->SetBinContent(i_RM, k_B_6);//k6

//     if(k_B_1 != 0) hB_C_ref3_unCorr[18]->SetBinContent(i_RM, k_B_2/k_B_1);// k2/k1
//     if(k_B_1 != 0) hB_C_ref3_unCorr[19]->SetBinContent(i_RM, k_B_3/k_B_1);// k3/k1
//     if(k_B_2 != 0) hB_C_ref3_unCorr[20]->SetBinContent(i_RM, k_B_3/k_B_2);// k3/k2
//     if(k_B_1 != 0) hB_C_ref3_unCorr[21]->SetBinContent(i_RM, k_B_4/k_B_1);// k4/k1
//     if(k_B_3 != 0) hB_C_ref3_unCorr[22]->SetBinContent(i_RM, k_B_4/k_B_3);// k4/k3
//     if(k_B_1 != 0) hB_C_ref3_unCorr[23]->SetBinContent(i_RM, k_B_5/k_B_1);// k5/k1
//     if(k_B_4 != 0) hB_C_ref3_unCorr[24]->SetBinContent(i_RM, k_B_5/k_B_4);// k5/k4
//     if(k_B_1 != 0) hB_C_ref3_unCorr[25]->SetBinContent(i_RM, k_B_6/k_B_1);// k6/k1
//     if(k_B_5 != 0) hB_C_ref3_unCorr[26]->SetBinContent(i_RM, k_B_6/k_B_5);// k6/k5


//     ///////
//     //Err//
//     ///////
//     //proton    
//     hB_C_ref3_unCorr_err[0]->SetBinContent(i_RM, Cerr_B_1);//c_err1
//     hB_C_ref3_unCorr_err[1]->SetBinContent(i_RM, Cerr_B_2);//c_err2
//     hB_C_ref3_unCorr_err[2]->SetBinContent(i_RM, Cerr_B_3);//c_err3
//     hB_C_ref3_unCorr_err[3]->SetBinContent(i_RM, Cerr_B_4);//c_err4
//     hB_C_ref3_unCorr_err[4]->SetBinContent(i_RM, Cerr_B_5);//c_err5
//     hB_C_ref3_unCorr_err[5]->SetBinContent(i_RM, Cerr_B_6);//c_err6

//     hB_C_ref3_unCorr_err[6]->SetBinContent(i_RM, kerr_B_1);//k_err1
//     hB_C_ref3_unCorr_err[7]->SetBinContent(i_RM, kerr_B_2);//k_err2
//     hB_C_ref3_unCorr_err[8]->SetBinContent(i_RM, kerr_B_3);//k_err3
//     hB_C_ref3_unCorr_err[9]->SetBinContent(i_RM, kerr_B_4);//k_err4
//     hB_C_ref3_unCorr_err[10]->SetBinContent(i_RM, kerr_B_5);//k_err5
//     hB_C_ref3_unCorr_err[11]->SetBinContent(i_RM, kerr_B_6);//k_err6

//     hB_C_ref3_unCorr_err[12]->SetBinContent(i_RM, CRerr_B_21);//CR_err21
//     hB_C_ref3_unCorr_err[13]->SetBinContent(i_RM, CRerr_B_31);//CR_err31
//     hB_C_ref3_unCorr_err[14]->SetBinContent(i_RM, CRerr_B_32);//CR_err32
//     hB_C_ref3_unCorr_err[15]->SetBinContent(i_RM, CRerr_B_42);//CR_err42
//     hB_C_ref3_unCorr_err[16]->SetBinContent(i_RM, CRerr_B_51);//CR_err51
//     hB_C_ref3_unCorr_err[17]->SetBinContent(i_RM, CRerr_B_62);//CR_err62

//     hB_C_ref3_unCorr_err[18]->SetBinContent(i_RM, kRerr_B_21);//kR_err21
//     hB_C_ref3_unCorr_err[19]->SetBinContent(i_RM, kRerr_B_31);//kR_err31
//     hB_C_ref3_unCorr_err[20]->SetBinContent(i_RM, kRerr_B_32);//kR_err32
//     hB_C_ref3_unCorr_err[21]->SetBinContent(i_RM, kRerr_B_41);//kR_err41
//     hB_C_ref3_unCorr_err[22]->SetBinContent(i_RM, kRerr_B_43);//kR_err43
//     hB_C_ref3_unCorr_err[23]->SetBinContent(i_RM, kRerr_B_51);//kR_err51
//     hB_C_ref3_unCorr_err[24]->SetBinContent(i_RM, kRerr_B_54);//kR_err54
//     hB_C_ref3_unCorr_err[25]->SetBinContent(i_RM, kRerr_B_61);//kR_err61
//     hB_C_ref3_unCorr_err[26]->SetBinContent(i_RM, kRerr_B_65);//kR_err65

//   }//refMult loop ends

//   WriteEffUnCorrRefMult3Cum(Form("output/cum/%s_eff_refMult3StPicoEvt_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut));
  
//   delete mCalCumB;
// }


//////////////////////////////////////////

void Cum::CalculateEffUnCorrRefMult3Cum(const char* NameRuZr, int dca_cut, int nhitsfit_cut, int nsp_cut, int m2low_cut, int m2up_cut, int ptmax_cut, int ymax_cut){
  //calculate eff corr cumulant

  //proton
  double C_B_1, C_B_2, C_B_3, C_B_4, C_B_5, C_B_6;
  double k_B_1, k_B_2, k_B_3, k_B_4, k_B_5, k_B_6;
  double Cerr_B_1, Cerr_B_2, Cerr_B_3, Cerr_B_4, Cerr_B_5, Cerr_B_6;
  double kerr_B_1, kerr_B_2, kerr_B_3, kerr_B_4, kerr_B_5, kerr_B_6;
  double CRerr_B_21, CRerr_B_31, CRerr_B_32, CRerr_B_42, CRerr_B_51, CRerr_B_62;
  double kRerr_B_21, kRerr_B_31, kRerr_B_32, kRerr_B_41, kRerr_B_43, kRerr_B_51, kRerr_B_54, kRerr_B_61, kRerr_B_65;

  double mean_qB_ref3_[N_mean_term_noErr];

  CalCum *mCalCumB = new CalCum();//proton

  //temporary
  int nan_counter_cr51 = 0;
  int nan_counter_cr62 = 0;
  
  //01072021(start)
  // for(int i_RM = 0 ; i_RM < maxMult ; i_RM++){
  for(int i_RM = 0 ; i_RM < maxMult_ref3 ; i_RM++){
    //01072021(finish)
    mCalCumB->Init();
    //01072021(start)
    // cout<<"Cum::CalculateEffUnCorrRefMult3Cum(). Multiplicity: "<<i_RM<<"/"<<maxMult<<endl;
    cout<<"Cum::CalculateEffUnCorrRefMult3Cum(). Multiplicity: "<<i_RM<<"/"<<maxMult_ref3<<endl;
    //01072021(finish)
    //initialize
    //proton
    C_B_1 = C_B_2 = C_B_3 = C_B_4 = C_B_5 = C_B_6 = 0. ;
    k_B_1 = k_B_2 = k_B_3 = k_B_4 = k_B_5 = k_B_6 = 0. ;
    Cerr_B_1 = Cerr_B_2 = Cerr_B_3 = Cerr_B_4 = Cerr_B_5 = Cerr_B_6 = 0. ;
    kerr_B_1 = kerr_B_2 = kerr_B_3 = kerr_B_4 = kerr_B_5 = kerr_B_6 = 0. ;
    CRerr_B_21 = CRerr_B_31 = CRerr_B_32 = CRerr_B_42 = CRerr_B_51 = CRerr_B_62 = 0. ;
    kRerr_B_21 = kRerr_B_31 = kRerr_B_32 = kRerr_B_41 = kRerr_B_43 = kRerr_B_51 = kRerr_B_54 = kRerr_B_61 = kRerr_B_65 = 0. ;

    for(int i_term = 0 ; i_term < N_mean_term_noErr ; i_term++){
      mean_qB_ref3_[i_term] = h_mean_qB_ref3_unCorr_[i_term]->GetBinContent(i_RM);
      mCalCumB->SetMean(mean_qB_ref3_[i_term], i_term);
    }//2535

    mCalCumB->SetCums( hB_Nevt_ref3->GetBinContent(i_RM) );

    //proton
    C_B_1 = mCalCumB->Get_tc1();
    C_B_2 = mCalCumB->Get_tc2();
    C_B_3 = mCalCumB->Get_tc3();
    C_B_4 = mCalCumB->Get_tc4();
    C_B_5 = mCalCumB->Get_tc5();
    C_B_6 = mCalCumB->Get_tc6();

    k_B_1 = mCalCumB->Get_tk1();
    k_B_2 = mCalCumB->Get_tk2();
    k_B_3 = mCalCumB->Get_tk3();
    k_B_4 = mCalCumB->Get_tk4();
    k_B_5 = mCalCumB->Get_tk5();
    k_B_6 = mCalCumB->Get_tk6();

    //06082021(start)
    // if(!isnan(mCalCumB->Get_etc1())) Cerr_B_1 = mCalCumB->Get_etc1();
    // if(!isnan(mCalCumB->Get_etc2())) Cerr_B_2 = mCalCumB->Get_etc2();
    // if(!isnan(mCalCumB->Get_etc3())) Cerr_B_3 = mCalCumB->Get_etc3();
    // if(!isnan(mCalCumB->Get_etc4())) Cerr_B_4 = mCalCumB->Get_etc4();
    // if(!isnan(mCalCumB->Get_etc5())) Cerr_B_5 = mCalCumB->Get_etc5();
    // if(!isnan(mCalCumB->Get_etc6())) Cerr_B_6 = mCalCumB->Get_etc6();

    // if(!isnan(mCalCumB->Get_etk1())) kerr_B_1 = mCalCumB->Get_etk1();
    // if(!isnan(mCalCumB->Get_etk2())) kerr_B_2 = mCalCumB->Get_etk2();
    // if(!isnan(mCalCumB->Get_etk3())) kerr_B_3 = mCalCumB->Get_etk3();
    // if(!isnan(mCalCumB->Get_etk4())) kerr_B_4 = mCalCumB->Get_etk4();
    // if(!isnan(mCalCumB->Get_etk5())) kerr_B_5 = mCalCumB->Get_etk5();
    // if(!isnan(mCalCumB->Get_etk6())) kerr_B_6 = mCalCumB->Get_etk6();

    // if(!isnan(mCalCumB->Get_er21())) CRerr_B_21 = mCalCumB->Get_er21();
    // if(!isnan(mCalCumB->Get_er31())) CRerr_B_31 = mCalCumB->Get_er31();
    // if(!isnan(mCalCumB->Get_er32())) CRerr_B_32 = mCalCumB->Get_er32();
    // if(!isnan(mCalCumB->Get_er42())) CRerr_B_42 = mCalCumB->Get_er42();
    // if(!isnan(mCalCumB->Get_er51())) CRerr_B_51 = mCalCumB->Get_er51();
    // if(!isnan(mCalCumB->Get_er62())) CRerr_B_62 = mCalCumB->Get_er62();

    // if(!isnan(mCalCumB->Get_ek21())) kRerr_B_21 = mCalCumB->Get_ek21();
    // if(!isnan(mCalCumB->Get_ek31())) kRerr_B_31 = mCalCumB->Get_ek31();
    // if(!isnan(mCalCumB->Get_ek32())) kRerr_B_32 = mCalCumB->Get_ek32();
    // if(!isnan(mCalCumB->Get_ek41())) kRerr_B_41 = mCalCumB->Get_ek41();
    // if(!isnan(mCalCumB->Get_ek43())) kRerr_B_43 = mCalCumB->Get_ek43();
    // if(!isnan(mCalCumB->Get_ek51())) kRerr_B_51 = mCalCumB->Get_ek51();
    // if(!isnan(mCalCumB->Get_ek54())) kRerr_B_54 = mCalCumB->Get_ek54();
    // if(!isnan(mCalCumB->Get_ek61())) kRerr_B_61 = mCalCumB->Get_ek61();
    // if(!isnan(mCalCumB->Get_ek65())) kRerr_B_65 = mCalCumB->Get_ek65();
    //06082021(finish)

    //14092021(start)
    if(!isnan(mCalCumB->Get_etc1())) Cerr_B_1 = mCalCumB->Get_etc1();
    if(!isnan(mCalCumB->Get_etc2())) Cerr_B_2 = mCalCumB->Get_etc2();
    if(!isnan(mCalCumB->Get_etc3())) Cerr_B_3 = mCalCumB->Get_etc3();
    if(!isnan(mCalCumB->Get_etc4())) Cerr_B_4 = mCalCumB->Get_etc4();
    if(!isnan(mCalCumB->Get_etc5())) {
      Cerr_B_5 = mCalCumB->Get_etc5();
      cout<<"i_RM, C_B_5, mCalCumB->Get_etc5(): "<<i_RM<<", "<<C_B_5<<", "<<mCalCumB->Get_etc5()<<endl;
    }
    if(!isnan(mCalCumB->Get_etc6())) {
      Cerr_B_6 = mCalCumB->Get_etc6();
      cout<<"i_RM, C_B_6, mCalCumB->Get_etc6(): "<<i_RM<<", "<<C_B_6<<", "<<mCalCumB->Get_etc6()<<endl;
    }
    //14092021(finish)

    kerr_B_1 = mCalCumB->Get_etk1();
    kerr_B_2 = mCalCumB->Get_etk2();
    kerr_B_3 = mCalCumB->Get_etk3();
    kerr_B_4 = mCalCumB->Get_etk4();
    kerr_B_5 = mCalCumB->Get_etk5();
    kerr_B_6 = mCalCumB->Get_etk6();

    if(C_B_1 != 0) CRerr_B_21 = mCalCumB->Get_er21();
    if(C_B_1 != 0) CRerr_B_31 = mCalCumB->Get_er31();
    if(C_B_2 != 0) CRerr_B_32 = mCalCumB->Get_er32();
    if(C_B_2 != 0) CRerr_B_42 = mCalCumB->Get_er42();
    if(C_B_1 != 0) CRerr_B_51 = mCalCumB->Get_er51();
    if(C_B_2 != 0) CRerr_B_62 = mCalCumB->Get_er62();

    if(k_B_1 != 0) kRerr_B_21 = mCalCumB->Get_ek21();
    if(k_B_1 != 0) kRerr_B_31 = mCalCumB->Get_ek31();
    if(k_B_2 != 0) kRerr_B_32 = mCalCumB->Get_ek32();
    if(k_B_1 != 0) kRerr_B_41 = mCalCumB->Get_ek41();
    if(k_B_3 != 0) kRerr_B_43 = mCalCumB->Get_ek43();
    if(k_B_1 != 0) kRerr_B_51 = mCalCumB->Get_ek51();
    if(k_B_4 != 0) kRerr_B_54 = mCalCumB->Get_ek54();
    if(k_B_1 != 0) kRerr_B_61 = mCalCumB->Get_ek61();
    if(k_B_5 != 0) kRerr_B_65 = mCalCumB->Get_ek65();

    //Fill it
    //proton
    hB_C_ref3_unCorr[0]->SetBinContent(i_RM, C_B_1);//C1
    hB_C_ref3_unCorr[1]->SetBinContent(i_RM, C_B_2);//C2
    hB_C_ref3_unCorr[2]->SetBinContent(i_RM, C_B_3);//C3
    hB_C_ref3_unCorr[3]->SetBinContent(i_RM, C_B_4);//C4
    hB_C_ref3_unCorr[4]->SetBinContent(i_RM, C_B_5);//C5
    hB_C_ref3_unCorr[5]->SetBinContent(i_RM, C_B_6);//C6
    hB_C_ref3_unCorr[0]->SetBinError(i_RM, mCalCumB->Get_etc1());//C1
    hB_C_ref3_unCorr[1]->SetBinError(i_RM, mCalCumB->Get_etc2());//C2
    hB_C_ref3_unCorr[2]->SetBinError(i_RM, mCalCumB->Get_etc3());//C3
    hB_C_ref3_unCorr[3]->SetBinError(i_RM, mCalCumB->Get_etc4());//C4
    hB_C_ref3_unCorr[4]->SetBinError(i_RM, mCalCumB->Get_etc5());//C5
    hB_C_ref3_unCorr[5]->SetBinError(i_RM, mCalCumB->Get_etc6());//C6

    // if(C_B_1 != 0) {
    //14092021(start)
    // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er21()) && mCalCumB->Get_er21() < 1e6) {//27082021 some of them is nan
    //15092021: commented out the if statement. Later during CBWC, reject the whole "nan" multbin.
    if(C_B_1 != 0 && !isnan(mCalCumB->Get_er21())) {//27082021 some of them is nan
      //14092021(finish)
      hB_C_ref3_unCorr[6]->SetBinContent(i_RM, C_B_2/C_B_1);// C2/C1
      hB_C_ref3_unCorr[6]->SetBinError(i_RM, mCalCumB->Get_er21());// C2/C1
    }
    // if(C_B_1 != 0){
    //14092021(start)
    // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er31()) && mCalCumB->Get_er31() < 1e6) {
    if(C_B_1 != 0 && !isnan(mCalCumB->Get_er31())) {
      //14092021(finish)
      hB_C_ref3_unCorr[7]->SetBinContent(i_RM, C_B_3/C_B_1);// C3/C1
      hB_C_ref3_unCorr[7]->SetBinError(i_RM, mCalCumB->Get_er31());// C3/C1
    }
    // if(C_B_2 != 0){
    //14092021(start)
    // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er32()) && mCalCumB->Get_er32() < 1e6) {
    if(C_B_2 != 0 && !isnan(mCalCumB->Get_er32())) {
      //14092021(finish)
      hB_C_ref3_unCorr[8]->SetBinContent(i_RM, C_B_3/C_B_2);// C3/C2
      hB_C_ref3_unCorr[8]->SetBinError(i_RM, mCalCumB->Get_er32());// C3/C2
    }
    // if(C_B_2 != 0){
    //14092021(start)
    // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er42()) && mCalCumB->Get_er42() < 1e6) {
    if(C_B_2 != 0 && !isnan(mCalCumB->Get_er42())) {
      //14092021(finish)
      hB_C_ref3_unCorr[9]->SetBinContent(i_RM, C_B_4/C_B_2);// C4/C2
      hB_C_ref3_unCorr[9]->SetBinError(i_RM, mCalCumB->Get_er42());// C4/C2
    }
    // if(C_B_1 != 0){
    //14092021(start)
    // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er51()) && mCalCumB->Get_er51() < 1e6) {
    if(C_B_1 != 0 && !isnan(mCalCumB->Get_er51())) {
      //14092021(finish)
      hB_C_ref3_unCorr[10]->SetBinContent(i_RM, C_B_5/C_B_1);// C5/C1
      hB_C_ref3_unCorr[10]->SetBinError(i_RM, mCalCumB->Get_er51());// C5/C1
      cout<<"i_RM, C_B_5/C_B_1, mCalCumB->Get_er51(): "<<i_RM<<", "<<C_B_5/C_B_1<<", "<<mCalCumB->Get_er51()<<endl;
      if(isnan(mCalCumB->Get_er51())) nan_counter_cr51++;
    }
    // if(C_B_2 != 0){
    //14092021(start)
    // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er62()) && mCalCumB->Get_er62() < 1e6) {
    if(C_B_2 != 0 && !isnan(mCalCumB->Get_er62())) {
      //14092021(finish)
      hB_C_ref3_unCorr[11]->SetBinContent(i_RM, C_B_6/C_B_2);// C6/C2
      hB_C_ref3_unCorr[11]->SetBinError(i_RM, mCalCumB->Get_er62());// C6/C2
      cout<<"i_RM, C_B_6/C_B_2, mCalCumB->Get_er62(): "<<i_RM<<", "<<C_B_6/C_B_2<<", "<<mCalCumB->Get_er62()<<endl;
      if(isnan(mCalCumB->Get_er62())) {
	cout<<"!!!!i_RM = "<<i_RM<<", mCalCumB->Get_er62() = "<<mCalCumB->Get_er62()<<endl;
	nan_counter_cr62++;
      }
    }

    hB_C_ref3_unCorr[12]->SetBinContent(i_RM,  k_B_1);//k1
    hB_C_ref3_unCorr[13]->SetBinContent(i_RM,  k_B_2);//k2
    hB_C_ref3_unCorr[14]->SetBinContent(i_RM,  k_B_3);//k3
    hB_C_ref3_unCorr[15]->SetBinContent(i_RM,  k_B_4);//k4
    hB_C_ref3_unCorr[16]->SetBinContent(i_RM, k_B_5);//k5
    hB_C_ref3_unCorr[17]->SetBinContent(i_RM, k_B_6);//k6

    if(k_B_1 != 0) hB_C_ref3_unCorr[18]->SetBinContent(i_RM, k_B_2/k_B_1);// k2/k1
    if(k_B_1 != 0) hB_C_ref3_unCorr[19]->SetBinContent(i_RM, k_B_3/k_B_1);// k3/k1
    if(k_B_2 != 0) hB_C_ref3_unCorr[20]->SetBinContent(i_RM, k_B_3/k_B_2);// k3/k2
    if(k_B_1 != 0) hB_C_ref3_unCorr[21]->SetBinContent(i_RM, k_B_4/k_B_1);// k4/k1
    if(k_B_3 != 0) hB_C_ref3_unCorr[22]->SetBinContent(i_RM, k_B_4/k_B_3);// k4/k3
    if(k_B_1 != 0) hB_C_ref3_unCorr[23]->SetBinContent(i_RM, k_B_5/k_B_1);// k5/k1
    if(k_B_4 != 0) hB_C_ref3_unCorr[24]->SetBinContent(i_RM, k_B_5/k_B_4);// k5/k4
    if(k_B_1 != 0) hB_C_ref3_unCorr[25]->SetBinContent(i_RM, k_B_6/k_B_1);// k6/k1
    if(k_B_5 != 0) hB_C_ref3_unCorr[26]->SetBinContent(i_RM, k_B_6/k_B_5);// k6/k5

    ///////
    //Err//
    ///////
    //proton    
    hB_C_ref3_unCorr_err[0]->SetBinContent(i_RM, Cerr_B_1);//c_err1
    hB_C_ref3_unCorr_err[1]->SetBinContent(i_RM, Cerr_B_2);//c_err2
    hB_C_ref3_unCorr_err[2]->SetBinContent(i_RM, Cerr_B_3);//c_err3
    hB_C_ref3_unCorr_err[3]->SetBinContent(i_RM, Cerr_B_4);//c_err4
    hB_C_ref3_unCorr_err[4]->SetBinContent(i_RM, Cerr_B_5);//c_err5
    hB_C_ref3_unCorr_err[5]->SetBinContent(i_RM, Cerr_B_6);//c_err6

    hB_C_ref3_unCorr_err[6]->SetBinContent(i_RM, kerr_B_1);//k_err1
    hB_C_ref3_unCorr_err[7]->SetBinContent(i_RM, kerr_B_2);//k_err2
    hB_C_ref3_unCorr_err[8]->SetBinContent(i_RM, kerr_B_3);//k_err3
    hB_C_ref3_unCorr_err[9]->SetBinContent(i_RM, kerr_B_4);//k_err4
    hB_C_ref3_unCorr_err[10]->SetBinContent(i_RM, kerr_B_5);//k_err5
    hB_C_ref3_unCorr_err[11]->SetBinContent(i_RM, kerr_B_6);//k_err6

    hB_C_ref3_unCorr_err[12]->SetBinContent(i_RM, CRerr_B_21);//CR_err21
    hB_C_ref3_unCorr_err[13]->SetBinContent(i_RM, CRerr_B_31);//CR_err31
    hB_C_ref3_unCorr_err[14]->SetBinContent(i_RM, CRerr_B_32);//CR_err32
    hB_C_ref3_unCorr_err[15]->SetBinContent(i_RM, CRerr_B_42);//CR_err42
    hB_C_ref3_unCorr_err[16]->SetBinContent(i_RM, CRerr_B_51);//CR_err51
    hB_C_ref3_unCorr_err[17]->SetBinContent(i_RM, CRerr_B_62);//CR_err62

    hB_C_ref3_unCorr_err[18]->SetBinContent(i_RM, kRerr_B_21);//kR_err21
    hB_C_ref3_unCorr_err[19]->SetBinContent(i_RM, kRerr_B_31);//kR_err31
    hB_C_ref3_unCorr_err[20]->SetBinContent(i_RM, kRerr_B_32);//kR_err32
    hB_C_ref3_unCorr_err[21]->SetBinContent(i_RM, kRerr_B_41);//kR_err41
    hB_C_ref3_unCorr_err[22]->SetBinContent(i_RM, kRerr_B_43);//kR_err43
    hB_C_ref3_unCorr_err[23]->SetBinContent(i_RM, kRerr_B_51);//kR_err51
    hB_C_ref3_unCorr_err[24]->SetBinContent(i_RM, kRerr_B_54);//kR_err54
    hB_C_ref3_unCorr_err[25]->SetBinContent(i_RM, kRerr_B_61);//kR_err61
    hB_C_ref3_unCorr_err[26]->SetBinContent(i_RM, kRerr_B_65);//kR_err65

  }//refMult loop ends

  // WriteEffUnCorrRefMult3Cum(Form("output/cum/%s_eff_refMult3_cum.root", NameRuZr));
  //parasite
  // WriteEffUnCorrRefMult3Cum(Form("output/cum/%s_eff_unCorr_refMult3StPicoEvt_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_multeff_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, MergeTopCent));
  // WriteEffUnCorrRefMult3Cum(Form("output/cum/%s_eff_unCorr_refMult3StPicoEvt_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_ruzrtrackeff_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, MergeTopCent));
  // WriteEffUnCorrRefMult3Cum(Form("output/cum/%s_eff_unCorr_refMult3StPicoEvt_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_multeff_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, MergeTopCent));
  // WriteEffUnCorrRefMult3Cum(Form("output/cum/%s_eff_unCorr_refMult3StPicoEvt_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_noepdcut_narrowref3cut_multeff_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, MergeTopCent));
  // WriteEffUnCorrRefMult3Cum(Form("output/cum/%s_eff_unCorr_refMult3StPicoEvt_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_ptmax_%d_ymax_%d_noepdcut_narrowref3cut_yptcenteff_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, MergeTopCent));
  WriteEffUnCorrRefMult3Cum(Form("output/cum/%s_eff_unCorr_refMult3StPicoEvt_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_ptmax_%d_ymax_%d_noepdcut_multeff_%d_29112021_1.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, MergeTopCent));
  // WriteEffUnCorrRefMult3Cum(Form("output/cum/%s_eff_unCorr_refMult3StPicoEvt_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_ptmax_%d_ymax_%d_noepdcut_narrowref3cut_multeff_%d_30112021_1.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, MergeTopCent));
  // WriteEffUnCorrRefMult3Cum(Form("output/cum/%s_eff_unCorr_refMult3StPicoEvt_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_noepdcut_narrowref3cut_yptmulteff_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, MergeTopCent));
  // WriteEffUnCorrRefMult3Cum(Form("output/cum/%s_eff_unCorr_refMult3StPicoEvt_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_auaueff_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, MergeTopCent));

  //temporary
  cout<<"Cum::CalculateEffUnCorrRefMult3Cum"<<endl;
  cout<<"nan_counter_cr51 = "<<nan_counter_cr51<<endl;
  cout<<"nan_counter_cr62 = "<<nan_counter_cr62<<endl;
  
  delete mCalCumB;
}

//////////////////////////////////////////

void Cum::WriteEffUnCorrRefMult3Cum(const char* outFile){

  TFile *f_out = new TFile(outFile, "RECREATE");
  f_out->cd();
  for(int i = 0 ; i < Ncums_noErr ; i++){
    hB_C_ref3_unCorr[i]->Write();
    hB_C_ref3_unCorr_err[i]->Write();
  }

  hB_Nevt_ref3_unCorr->Write();
  f_out->Close();
  
}

// //////////////////////
// //RefMult3 StPicoEvt//
// //////////////////////
// void Cum::GetRefMult3StPicoEvtMeans(const char* inFile){
//   //get terms in Eq. 62 ~ 67 & 69 in arXiv:1702.07106v3
//   TFile *f_in = new TFile(inFile, "READ");
//   std::map<const char*, TProfile*>::iterator iter;
//   ///proton///
//   TIter nextkeyB(f_in->GetListOfKeys());
//   TKey *keyB;
//   while((keyB = (TKey*)nextkeyB())){
//     TString strName = keyB->ReadObj()->GetName();
//     if(strName.Contains(Form("%s_ref3StPicoEvt", "proton"))){
//       strName.Replace(0, strlen(Form("%s_ref3StPicoEvt", "proton"))+1,"");
//       // cout<<strName<<endl;
//     } else {
//       continue;
//     }
//     int iT = 0;
//     while (true) {
//       if(iT == N_mean_term_noErr) {
// 	std::cout<<"In Cum::GetRefMult3StPicoEvtMeans"<<std::endl;
// 	std::cout<<"Found bad terms"<<std::endl;
// 	abort();
//       }
//       if(strcmp(strName, mean_term_name_noErr[iT])==0){
// 	h_mean_qB_ref3StPicoEvt_[iT] = (TProfile*) keyB->ReadObj()->Clone();
// 	break;
//       } else {
// 	iT++;
//       }
//     }
//   }

//   TProfile* entB = (TProfile*)f_in->Get(Form("%s_ref3StPicoEvt_q01_01","proton"));
//   for(int i=1;i<entB->GetNbinsX();++i) {
//     hB_Nevt_ref3StPicoEvt->SetBinContent(i, entB->GetBinEntries(i));//getting No. of Evt per refMult
//     // cout<<ent->GetBinEntries(i)<<endl;
//   }
// }

// //////////////////////////////////////////

// void Cum::CalculateEffCorrRefMult3StPicoEvtCum(const char* NameRuZr){
//   //calculate eff corr cumulant

//   //proton
//   double C_B_1, C_B_2, C_B_3, C_B_4, C_B_5, C_B_6;
//   double k_B_1, k_B_2, k_B_3, k_B_4, k_B_5, k_B_6;
//   double Cerr_B_1, Cerr_B_2, Cerr_B_3, Cerr_B_4, Cerr_B_5, Cerr_B_6;
//   double kerr_B_1, kerr_B_2, kerr_B_3, kerr_B_4, kerr_B_5, kerr_B_6;
//   double CRerr_B_21, CRerr_B_31, CRerr_B_32, CRerr_B_42, CRerr_B_51, CRerr_B_62;
//   double kRerr_B_21, kRerr_B_31, kRerr_B_32, kRerr_B_41, kRerr_B_43, kRerr_B_51, kRerr_B_54, kRerr_B_61, kRerr_B_65;

//   double mean_qB_ref3StPicoEvt_[N_mean_term_noErr];

//   CalCum *mCalCumB = new CalCum();//proton

//   //01072021(start)
//   // for(int i_RM = 0 ; i_RM < maxMult ; i_RM++){
//   for(int i_RM = 0 ; i_RM < maxMult_ref3StPicoEvt ; i_RM++){
//     //01072021(finish)
//     mCalCumB->Init();
//     //01072021(start)
//     // cout<<"Cum::CalculateEffCorrRefMult3StPicoEvtCum(). Multiplicity: "<<i_RM<<"/"<<maxMult<<endl;
//     cout<<"Cum::CalculateEffCorrRefMult3StPicoEvtCum(). Multiplicity: "<<i_RM<<"/"<<maxMult_ref3StPicoEvt<<endl;
//     //01072021(finish)
//     //initialize
//     //proton
//     C_B_1 = C_B_2 = C_B_3 = C_B_4 = C_B_5 = C_B_6 = 0. ;
//     k_B_1 = k_B_2 = k_B_3 = k_B_4 = k_B_5 = k_B_6 = 0. ;
//     Cerr_B_1 = Cerr_B_2 = Cerr_B_3 = Cerr_B_4 = Cerr_B_5 = Cerr_B_6 = 0. ;
//     kerr_B_1 = kerr_B_2 = kerr_B_3 = kerr_B_4 = kerr_B_5 = kerr_B_6 = 0. ;
//     CRerr_B_21 = CRerr_B_31 = CRerr_B_32 = CRerr_B_42 = CRerr_B_51 = CRerr_B_62 = 0. ;
//     kRerr_B_21 = kRerr_B_31 = kRerr_B_32 = kRerr_B_41 = kRerr_B_43 = kRerr_B_51 = kRerr_B_54 = kRerr_B_61 = kRerr_B_65 = 0. ;

//     for(int i_term = 0 ; i_term < N_mean_term_noErr ; i_term++){
//       mean_qB_ref3StPicoEvt_[i_term] = h_mean_qB_ref3StPicoEvt_[i_term]->GetBinContent(i_RM);
//       mCalCumB->SetMean(mean_qB_ref3StPicoEvt_[i_term], i_term);
//     }//2535

//     mCalCumB->SetCums( hB_Nevt_ref3StPicoEvt->GetBinContent(i_RM) );

//     //proton
//     C_B_1 = mCalCumB->Get_tc1();
//     C_B_2 = mCalCumB->Get_tc2();
//     C_B_3 = mCalCumB->Get_tc3();
//     C_B_4 = mCalCumB->Get_tc4();
//     C_B_5 = mCalCumB->Get_tc5();
//     C_B_6 = mCalCumB->Get_tc6();

//     k_B_1 = mCalCumB->Get_tk1();
//     k_B_2 = mCalCumB->Get_tk2();
//     k_B_3 = mCalCumB->Get_tk3();
//     k_B_4 = mCalCumB->Get_tk4();
//     k_B_5 = mCalCumB->Get_tk5();
//     k_B_6 = mCalCumB->Get_tk6();

//     //06082021(start)
//     // if(!isnan(mCalCumB->Get_etc1())) Cerr_B_1 = mCalCumB->Get_etc1();
//     // if(!isnan(mCalCumB->Get_etc2())) Cerr_B_2 = mCalCumB->Get_etc2();
//     // if(!isnan(mCalCumB->Get_etc3())) Cerr_B_3 = mCalCumB->Get_etc3();
//     // if(!isnan(mCalCumB->Get_etc4())) Cerr_B_4 = mCalCumB->Get_etc4();
//     // if(!isnan(mCalCumB->Get_etc5())) Cerr_B_5 = mCalCumB->Get_etc5();
//     // if(!isnan(mCalCumB->Get_etc6())) Cerr_B_6 = mCalCumB->Get_etc6();

//     // if(!isnan(mCalCumB->Get_etk1())) kerr_B_1 = mCalCumB->Get_etk1();
//     // if(!isnan(mCalCumB->Get_etk2())) kerr_B_2 = mCalCumB->Get_etk2();
//     // if(!isnan(mCalCumB->Get_etk3())) kerr_B_3 = mCalCumB->Get_etk3();
//     // if(!isnan(mCalCumB->Get_etk4())) kerr_B_4 = mCalCumB->Get_etk4();
//     // if(!isnan(mCalCumB->Get_etk5())) kerr_B_5 = mCalCumB->Get_etk5();
//     // if(!isnan(mCalCumB->Get_etk6())) kerr_B_6 = mCalCumB->Get_etk6();

//     // if(!isnan(mCalCumB->Get_er21())) CRerr_B_21 = mCalCumB->Get_er21();
//     // if(!isnan(mCalCumB->Get_er31())) CRerr_B_31 = mCalCumB->Get_er31();
//     // if(!isnan(mCalCumB->Get_er32())) CRerr_B_32 = mCalCumB->Get_er32();
//     // if(!isnan(mCalCumB->Get_er42())) CRerr_B_42 = mCalCumB->Get_er42();
//     // if(!isnan(mCalCumB->Get_er51())) CRerr_B_51 = mCalCumB->Get_er51();
//     // if(!isnan(mCalCumB->Get_er62())) CRerr_B_62 = mCalCumB->Get_er62();

//     // if(!isnan(mCalCumB->Get_ek21())) kRerr_B_21 = mCalCumB->Get_ek21();
//     // if(!isnan(mCalCumB->Get_ek31())) kRerr_B_31 = mCalCumB->Get_ek31();
//     // if(!isnan(mCalCumB->Get_ek32())) kRerr_B_32 = mCalCumB->Get_ek32();
//     // if(!isnan(mCalCumB->Get_ek41())) kRerr_B_41 = mCalCumB->Get_ek41();
//     // if(!isnan(mCalCumB->Get_ek43())) kRerr_B_43 = mCalCumB->Get_ek43();
//     // if(!isnan(mCalCumB->Get_ek51())) kRerr_B_51 = mCalCumB->Get_ek51();
//     // if(!isnan(mCalCumB->Get_ek54())) kRerr_B_54 = mCalCumB->Get_ek54();
//     // if(!isnan(mCalCumB->Get_ek61())) kRerr_B_61 = mCalCumB->Get_ek61();
//     // if(!isnan(mCalCumB->Get_ek65())) kRerr_B_65 = mCalCumB->Get_ek65();
//     //06082021(finish)

//     Cerr_B_1 = mCalCumB->Get_etc1();
//     Cerr_B_2 = mCalCumB->Get_etc2();
//     Cerr_B_3 = mCalCumB->Get_etc3();
//     Cerr_B_4 = mCalCumB->Get_etc4();
//     Cerr_B_5 = mCalCumB->Get_etc5();
//     Cerr_B_6 = mCalCumB->Get_etc6();

//     kerr_B_1 = mCalCumB->Get_etk1();
//     kerr_B_2 = mCalCumB->Get_etk2();
//     kerr_B_3 = mCalCumB->Get_etk3();
//     kerr_B_4 = mCalCumB->Get_etk4();
//     kerr_B_5 = mCalCumB->Get_etk5();
//     kerr_B_6 = mCalCumB->Get_etk6();

//     if(C_B_1 != 0) CRerr_B_21 = mCalCumB->Get_er21();
//     if(C_B_1 != 0) CRerr_B_31 = mCalCumB->Get_er31();
//     if(C_B_2 != 0) CRerr_B_32 = mCalCumB->Get_er32();
//     if(C_B_2 != 0) CRerr_B_42 = mCalCumB->Get_er42();
//     if(C_B_1 != 0) CRerr_B_51 = mCalCumB->Get_er51();
//     if(C_B_2 != 0) CRerr_B_62 = mCalCumB->Get_er62();

//     if(k_B_1 != 0) kRerr_B_21 = mCalCumB->Get_ek21();
//     if(k_B_1 != 0) kRerr_B_31 = mCalCumB->Get_ek31();
//     if(k_B_2 != 0) kRerr_B_32 = mCalCumB->Get_ek32();
//     if(k_B_1 != 0) kRerr_B_41 = mCalCumB->Get_ek41();
//     if(k_B_3 != 0) kRerr_B_43 = mCalCumB->Get_ek43();
//     if(k_B_1 != 0) kRerr_B_51 = mCalCumB->Get_ek51();
//     if(k_B_4 != 0) kRerr_B_54 = mCalCumB->Get_ek54();
//     if(k_B_1 != 0) kRerr_B_61 = mCalCumB->Get_ek61();
//     if(k_B_5 != 0) kRerr_B_65 = mCalCumB->Get_ek65();

//     //Fill it
//     //proton
//     hB_C_ref3StPicoEvt[0]->SetBinContent(i_RM, C_B_1);//C1
//     hB_C_ref3StPicoEvt[1]->SetBinContent(i_RM, C_B_2);//C2
//     hB_C_ref3StPicoEvt[2]->SetBinContent(i_RM, C_B_3);//C3
//     hB_C_ref3StPicoEvt[3]->SetBinContent(i_RM, C_B_4);//C4
//     hB_C_ref3StPicoEvt[4]->SetBinContent(i_RM, C_B_5);//C5
//     hB_C_ref3StPicoEvt[5]->SetBinContent(i_RM, C_B_6);//C6
//     hB_C_ref3StPicoEvt[0]->SetBinError(i_RM, mCalCumB->Get_etc1());//C1
//     hB_C_ref3StPicoEvt[1]->SetBinError(i_RM, mCalCumB->Get_etc2());//C2
//     hB_C_ref3StPicoEvt[2]->SetBinError(i_RM, mCalCumB->Get_etc3());//C3
//     hB_C_ref3StPicoEvt[3]->SetBinError(i_RM, mCalCumB->Get_etc4());//C4
//     hB_C_ref3StPicoEvt[4]->SetBinError(i_RM, mCalCumB->Get_etc5());//C5
//     hB_C_ref3StPicoEvt[5]->SetBinError(i_RM, mCalCumB->Get_etc6());//C6

//     if(C_B_1 != 0) {
//     // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er21())) {//27082021 some of them is nan
//       hB_C_ref3StPicoEvt[6]->SetBinContent(i_RM, C_B_2/C_B_1);// C2/C1
//       hB_C_ref3StPicoEvt[6]->SetBinError(i_RM, mCalCumB->Get_er21());// C2/C1
//     }
//     if(C_B_1 != 0){
//     // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er31())) {
//       hB_C_ref3StPicoEvt[7]->SetBinContent(i_RM, C_B_3/C_B_1);// C3/C1
//       hB_C_ref3StPicoEvt[7]->SetBinError(i_RM, mCalCumB->Get_er31());// C3/C1
//     }
//     if(C_B_2 != 0){
//     // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er32())) {
//       hB_C_ref3StPicoEvt[8]->SetBinContent(i_RM, C_B_3/C_B_2);// C3/C2
//       hB_C_ref3StPicoEvt[8]->SetBinError(i_RM, mCalCumB->Get_er32());// C3/C2
//     }
//     if(C_B_2 != 0){
//     // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er42())) {
//       hB_C_ref3StPicoEvt[9]->SetBinContent(i_RM, C_B_4/C_B_2);// C4/C2
//       hB_C_ref3StPicoEvt[9]->SetBinError(i_RM, mCalCumB->Get_er42());// C4/C2
//     }
//     if(C_B_1 != 0){
//     // if(C_B_1 != 0 && !isnan(mCalCumB->Get_er51())) {
//       hB_C_ref3StPicoEvt[10]->SetBinContent(i_RM, C_B_5/C_B_1);// C5/C1
//       hB_C_ref3StPicoEvt[10]->SetBinError(i_RM, mCalCumB->Get_er51());// C5/C1
//     }
//     if(C_B_2 != 0){
//     // if(C_B_2 != 0 && !isnan(mCalCumB->Get_er62())) {
//       hB_C_ref3StPicoEvt[11]->SetBinContent(i_RM, C_B_6/C_B_2);// C6/C2
//       hB_C_ref3StPicoEvt[11]->SetBinError(i_RM, mCalCumB->Get_er62());// C6/C2
//     }

//     hB_C_ref3StPicoEvt[12]->SetBinContent(i_RM,  k_B_1);//k1
//     hB_C_ref3StPicoEvt[13]->SetBinContent(i_RM,  k_B_2);//k2
//     hB_C_ref3StPicoEvt[14]->SetBinContent(i_RM,  k_B_3);//k3
//     hB_C_ref3StPicoEvt[15]->SetBinContent(i_RM,  k_B_4);//k4
//     hB_C_ref3StPicoEvt[16]->SetBinContent(i_RM, k_B_5);//k5
//     hB_C_ref3StPicoEvt[17]->SetBinContent(i_RM, k_B_6);//k6

//     if(k_B_1 != 0) hB_C_ref3StPicoEvt[18]->SetBinContent(i_RM, k_B_2/k_B_1);// k2/k1
//     if(k_B_1 != 0) hB_C_ref3StPicoEvt[19]->SetBinContent(i_RM, k_B_3/k_B_1);// k3/k1
//     if(k_B_2 != 0) hB_C_ref3StPicoEvt[20]->SetBinContent(i_RM, k_B_3/k_B_2);// k3/k2
//     if(k_B_1 != 0) hB_C_ref3StPicoEvt[21]->SetBinContent(i_RM, k_B_4/k_B_1);// k4/k1
//     if(k_B_3 != 0) hB_C_ref3StPicoEvt[22]->SetBinContent(i_RM, k_B_4/k_B_3);// k4/k3
//     if(k_B_1 != 0) hB_C_ref3StPicoEvt[23]->SetBinContent(i_RM, k_B_5/k_B_1);// k5/k1
//     if(k_B_4 != 0) hB_C_ref3StPicoEvt[24]->SetBinContent(i_RM, k_B_5/k_B_4);// k5/k4
//     if(k_B_1 != 0) hB_C_ref3StPicoEvt[25]->SetBinContent(i_RM, k_B_6/k_B_1);// k6/k1
//     if(k_B_5 != 0) hB_C_ref3StPicoEvt[26]->SetBinContent(i_RM, k_B_6/k_B_5);// k6/k5

//     ///////
//     //Err//
//     ///////
//     //proton    
//     hB_C_ref3StPicoEvt_err[0]->SetBinContent(i_RM, Cerr_B_1);//c_err1
//     hB_C_ref3StPicoEvt_err[1]->SetBinContent(i_RM, Cerr_B_2);//c_err2
//     hB_C_ref3StPicoEvt_err[2]->SetBinContent(i_RM, Cerr_B_3);//c_err3
//     hB_C_ref3StPicoEvt_err[3]->SetBinContent(i_RM, Cerr_B_4);//c_err4
//     hB_C_ref3StPicoEvt_err[4]->SetBinContent(i_RM, Cerr_B_5);//c_err5
//     hB_C_ref3StPicoEvt_err[5]->SetBinContent(i_RM, Cerr_B_6);//c_err6

//     hB_C_ref3StPicoEvt_err[6]->SetBinContent(i_RM, kerr_B_1);//k_err1
//     hB_C_ref3StPicoEvt_err[7]->SetBinContent(i_RM, kerr_B_2);//k_err2
//     hB_C_ref3StPicoEvt_err[8]->SetBinContent(i_RM, kerr_B_3);//k_err3
//     hB_C_ref3StPicoEvt_err[9]->SetBinContent(i_RM, kerr_B_4);//k_err4
//     hB_C_ref3StPicoEvt_err[10]->SetBinContent(i_RM, kerr_B_5);//k_err5
//     hB_C_ref3StPicoEvt_err[11]->SetBinContent(i_RM, kerr_B_6);//k_err6

//     hB_C_ref3StPicoEvt_err[12]->SetBinContent(i_RM, CRerr_B_21);//CR_err21
//     hB_C_ref3StPicoEvt_err[13]->SetBinContent(i_RM, CRerr_B_31);//CR_err31
//     hB_C_ref3StPicoEvt_err[14]->SetBinContent(i_RM, CRerr_B_32);//CR_err32
//     hB_C_ref3StPicoEvt_err[15]->SetBinContent(i_RM, CRerr_B_42);//CR_err42
//     hB_C_ref3StPicoEvt_err[16]->SetBinContent(i_RM, CRerr_B_51);//CR_err51
//     hB_C_ref3StPicoEvt_err[17]->SetBinContent(i_RM, CRerr_B_62);//CR_err62

//     hB_C_ref3StPicoEvt_err[18]->SetBinContent(i_RM, kRerr_B_21);//kR_err21
//     hB_C_ref3StPicoEvt_err[19]->SetBinContent(i_RM, kRerr_B_31);//kR_err31
//     hB_C_ref3StPicoEvt_err[20]->SetBinContent(i_RM, kRerr_B_32);//kR_err32
//     hB_C_ref3StPicoEvt_err[21]->SetBinContent(i_RM, kRerr_B_41);//kR_err41
//     hB_C_ref3StPicoEvt_err[22]->SetBinContent(i_RM, kRerr_B_43);//kR_err43
//     hB_C_ref3StPicoEvt_err[23]->SetBinContent(i_RM, kRerr_B_51);//kR_err51
//     hB_C_ref3StPicoEvt_err[24]->SetBinContent(i_RM, kRerr_B_54);//kR_err54
//     hB_C_ref3StPicoEvt_err[25]->SetBinContent(i_RM, kRerr_B_61);//kR_err61
//     hB_C_ref3StPicoEvt_err[26]->SetBinContent(i_RM, kRerr_B_65);//kR_err65

//   }//refMult loop ends

//   WriteEffCorrRefMult3StPicoEvtCum(Form("output/cum/%s_eff_refMult3StPicoEvt_cum.root", NameRuZr));
  
//   delete mCalCumB;
// }

// //////////////////////////////////////////

// void Cum::WriteEffCorrRefMult3StPicoEvtCum(const char* outFile){

//   TFile *f_out = new TFile(outFile, "RECREATE");
//   f_out->cd();
//   for(int i = 0 ; i < Ncums_noErr ; i++){
//     hB_C_ref3StPicoEvt[i]->Write();
//     hB_C_ref3StPicoEvt_err[i]->Write();
//   }

//   hB_Nevt_ref3StPicoEvt->Write();
//   f_out->Close();
  
// }

// //////////////////////////////////////////

// void Cum::GetRefMult3StPicoEvtUnCorrMeans(const char* inFile){
//   //get terms in Eq. 62 ~ 67 & 69 in arXiv:1702.07106v3
//   TFile *f_in = new TFile(inFile, "READ");
//   std::map<const char*, TProfile*>::iterator iter;
//   ///proton///
//   TIter nextkeyB(f_in->GetListOfKeys());
//   TKey *keyB;
//   while((keyB = (TKey*)nextkeyB())){
//     TString strName = keyB->ReadObj()->GetName();
//     if(strName.Contains(Form("%s_ref3StPicoEvt_unCorr", "proton"))){
//       strName.Replace(0, strlen(Form("%s_ref3StPicoEvt_unCorr", "proton"))+1,"");
//       // cout<<strName<<endl;
//     } else {
//       continue;
//     }
//     int iT = 0;
//     while (true) {
//       if(iT == N_mean_term_noErr) {
// 	std::cout<<"In Cum::GetRefMult3StPicoEvtUnCorrMeans"<<std::endl;
// 	std::cout<<"Found bad terms"<<std::endl;
// 	abort();
//       }
//       if(strcmp(strName, mean_term_name_noErr[iT])==0){
// 	h_mean_qB_ref3StPicoEvt_unCorr_[iT] = (TProfile*) keyB->ReadObj()->Clone();
// 	break;
//       } else {
// 	iT++;
//       }
//     }
//   }

//   TProfile* entB = (TProfile*)f_in->Get(Form("%s_ref3StPicoEvt_unCorr_q01_01","proton"));
//   for(int i=1;i<entB->GetNbinsX();++i) {
//     hB_Nevt_ref3StPicoEvt_unCorr->SetBinContent(i, entB->GetBinEntries(i));//getting No. of Evt per refMult
//     // cout<<ent->GetBinEntries(i)<<endl;
//   }
// }

// //////////////////////////////////////////

// void Cum::CalculateEffUnCorrRefMult3StPicoEvtCum(const char* NameRuZr){
//   //calculate eff corr cumulant

//   //proton
//   double C_B_1, C_B_2, C_B_3, C_B_4, C_B_5, C_B_6;
//   double k_B_1, k_B_2, k_B_3, k_B_4, k_B_5, k_B_6;
//   double Cerr_B_1, Cerr_B_2, Cerr_B_3, Cerr_B_4, Cerr_B_5, Cerr_B_6;
//   double kerr_B_1, kerr_B_2, kerr_B_3, kerr_B_4, kerr_B_5, kerr_B_6;
//   double CRerr_B_21, CRerr_B_31, CRerr_B_32, CRerr_B_42, CRerr_B_51, CRerr_B_62;
//   double kRerr_B_21, kRerr_B_31, kRerr_B_32, kRerr_B_41, kRerr_B_43, kRerr_B_51, kRerr_B_54, kRerr_B_61, kRerr_B_65;

//   double mean_qB_ref3StPicoEvt_unCorr_[N_mean_term_noErr];

//   CalCum *mCalCumB = new CalCum();//proton

//   //01072021(start)
//   // for(int i_RM = 0 ; i_RM < maxMult ; i_RM++){
//   for(int i_RM = 0 ; i_RM < maxMult_ref3StPicoEvt ; i_RM++){
//   //01072021(finish)
//     mCalCumB->Init();
//   //01072021(start)
//     // cout<<"Cum::CalculateEffUnCorrRefMult3StPicoEvtCum(). Multiplicity: "<<i_RM<<"/"<<maxMult<<endl;
//     cout<<"Cum::CalculateEffUnCorrRefMult3StPicoEvtCum(). Multiplicity: "<<i_RM<<"/"<<maxMult_ref3StPicoEvt<<endl;
//   //01072021(finish)
//     //initialize
//     //proton
//     C_B_1 = C_B_2 = C_B_3 = C_B_4 = C_B_5 = C_B_6 = 0. ;
//     k_B_1 = k_B_2 = k_B_3 = k_B_4 = k_B_5 = k_B_6 = 0. ;
//     Cerr_B_1 = Cerr_B_2 = Cerr_B_3 = Cerr_B_4 = Cerr_B_5 = Cerr_B_6 = 0. ;
//     kerr_B_1 = kerr_B_2 = kerr_B_3 = kerr_B_4 = kerr_B_5 = kerr_B_6 = 0. ;
//     CRerr_B_21 = CRerr_B_31 = CRerr_B_32 = CRerr_B_42 = CRerr_B_51 = CRerr_B_62 = 0. ;
//     kRerr_B_21 = kRerr_B_31 = kRerr_B_32 = kRerr_B_41 = kRerr_B_43 = kRerr_B_51 = kRerr_B_54 = kRerr_B_61 = kRerr_B_65 = 0. ;

//     for(int i_term = 0 ; i_term < N_mean_term_noErr ; i_term++){
//       mean_qB_ref3StPicoEvt_unCorr_[i_term] = h_mean_qB_ref3StPicoEvt_unCorr_[i_term]->GetBinContent(i_RM);
//       mCalCumB->SetMean(mean_qB_ref3StPicoEvt_unCorr_[i_term], i_term);
//     }//2535

//     mCalCumB->SetCums( hB_Nevt_ref3StPicoEvt_unCorr->GetBinContent(i_RM) );

//     //proton
//     C_B_1 = mCalCumB->Get_tc1();
//     C_B_2 = mCalCumB->Get_tc2();
//     C_B_3 = mCalCumB->Get_tc3();
//     C_B_4 = mCalCumB->Get_tc4();
//     C_B_5 = mCalCumB->Get_tc5();
//     C_B_6 = mCalCumB->Get_tc6();

//     k_B_1 = mCalCumB->Get_tk1();
//     k_B_2 = mCalCumB->Get_tk2();
//     k_B_3 = mCalCumB->Get_tk3();
//     k_B_4 = mCalCumB->Get_tk4();
//     k_B_5 = mCalCumB->Get_tk5();
//     k_B_6 = mCalCumB->Get_tk6();

//     if(!isnan(mCalCumB->Get_etc1())) Cerr_B_1 = mCalCumB->Get_etc1();
//     if(!isnan(mCalCumB->Get_etc2())) Cerr_B_2 = mCalCumB->Get_etc2();
//     if(!isnan(mCalCumB->Get_etc3())) Cerr_B_3 = mCalCumB->Get_etc3();
//     if(!isnan(mCalCumB->Get_etc4())) Cerr_B_4 = mCalCumB->Get_etc4();
//     if(!isnan(mCalCumB->Get_etc5())) Cerr_B_5 = mCalCumB->Get_etc5();
//     if(!isnan(mCalCumB->Get_etc6())) Cerr_B_6 = mCalCumB->Get_etc6();
 
//    if(!isnan(mCalCumB->Get_etk1())) kerr_B_1 = mCalCumB->Get_etk1();
//     if(!isnan(mCalCumB->Get_etk2())) kerr_B_2 = mCalCumB->Get_etk2();
//     if(!isnan(mCalCumB->Get_etk3())) kerr_B_3 = mCalCumB->Get_etk3();
//     if(!isnan(mCalCumB->Get_etk4())) kerr_B_4 = mCalCumB->Get_etk4();
//     if(!isnan(mCalCumB->Get_etk5())) kerr_B_5 = mCalCumB->Get_etk5();
//     if(!isnan(mCalCumB->Get_etk6())) kerr_B_6 = mCalCumB->Get_etk6();

//     if(!isnan(mCalCumB->Get_er21())) CRerr_B_21 = mCalCumB->Get_er21();
//     if(!isnan(mCalCumB->Get_er31())) CRerr_B_31 = mCalCumB->Get_er31();
//     if(!isnan(mCalCumB->Get_er32())) CRerr_B_32 = mCalCumB->Get_er32();
//     if(!isnan(mCalCumB->Get_er42())) CRerr_B_42 = mCalCumB->Get_er42();
//     if(!isnan(mCalCumB->Get_er51())) CRerr_B_51 = mCalCumB->Get_er51();
//     if(!isnan(mCalCumB->Get_er62())) CRerr_B_62 = mCalCumB->Get_er62();

//     if(!isnan(mCalCumB->Get_ek21())) kRerr_B_21 = mCalCumB->Get_ek21();
//     if(!isnan(mCalCumB->Get_ek31())) kRerr_B_31 = mCalCumB->Get_ek31();
//     if(!isnan(mCalCumB->Get_ek32())) kRerr_B_32 = mCalCumB->Get_ek32();
//     if(!isnan(mCalCumB->Get_ek41())) kRerr_B_41 = mCalCumB->Get_ek41();
//     if(!isnan(mCalCumB->Get_ek43())) kRerr_B_43 = mCalCumB->Get_ek43();
//     if(!isnan(mCalCumB->Get_ek51())) kRerr_B_51 = mCalCumB->Get_ek51();
//     if(!isnan(mCalCumB->Get_ek54())) kRerr_B_54 = mCalCumB->Get_ek54();
//     if(!isnan(mCalCumB->Get_ek61())) kRerr_B_61 = mCalCumB->Get_ek61();
//     if(!isnan(mCalCumB->Get_ek65())) kRerr_B_65 = mCalCumB->Get_ek65();

//     //Fill it
//     hB_C_ref3StPicoEvt_unCorr[0]->SetBinContent(i_RM, C_B_1);//C1
//     hB_C_ref3StPicoEvt_unCorr[1]->SetBinContent(i_RM, C_B_2);//C2
//     hB_C_ref3StPicoEvt_unCorr[2]->SetBinContent(i_RM, C_B_3);//C3
//     hB_C_ref3StPicoEvt_unCorr[3]->SetBinContent(i_RM, C_B_4);//C4
//     hB_C_ref3StPicoEvt_unCorr[4]->SetBinContent(i_RM, C_B_5);//C5
//     hB_C_ref3StPicoEvt_unCorr[5]->SetBinContent(i_RM, C_B_6);//C6

//     if(C_B_1 != 0) hB_C_ref3StPicoEvt_unCorr[6]->SetBinContent(i_RM, C_B_2/C_B_1);// C2/C1
//     if(C_B_1 != 0) hB_C_ref3StPicoEvt_unCorr[7]->SetBinContent(i_RM, C_B_3/C_B_1);// C3/C1
//     if(C_B_2 != 0) hB_C_ref3StPicoEvt_unCorr[8]->SetBinContent(i_RM, C_B_3/C_B_2);// C3/C2
//     if(C_B_2 != 0) hB_C_ref3StPicoEvt_unCorr[9]->SetBinContent(i_RM, C_B_4/C_B_2);// C4/C2
//     if(C_B_1 != 0) hB_C_ref3StPicoEvt_unCorr[10]->SetBinContent(i_RM, C_B_5/C_B_1);// C5/C1
//     if(C_B_2 != 0) hB_C_ref3StPicoEvt_unCorr[11]->SetBinContent(i_RM, C_B_6/C_B_2);// C6/C2

//     hB_C_ref3StPicoEvt_unCorr[12]->SetBinContent(i_RM,  k_B_1);//k1
//     hB_C_ref3StPicoEvt_unCorr[13]->SetBinContent(i_RM,  k_B_2);//k2
//     hB_C_ref3StPicoEvt_unCorr[14]->SetBinContent(i_RM,  k_B_3);//k3
//     hB_C_ref3StPicoEvt_unCorr[15]->SetBinContent(i_RM,  k_B_4);//k4
//     hB_C_ref3StPicoEvt_unCorr[16]->SetBinContent(i_RM, k_B_5);//k5
//     hB_C_ref3StPicoEvt_unCorr[17]->SetBinContent(i_RM, k_B_6);//k6

//     if(k_B_1 != 0) hB_C_ref3StPicoEvt_unCorr[18]->SetBinContent(i_RM, k_B_2/k_B_1);// k2/k1
//     if(k_B_1 != 0) hB_C_ref3StPicoEvt_unCorr[19]->SetBinContent(i_RM, k_B_3/k_B_1);// k3/k1
//     if(k_B_2 != 0) hB_C_ref3StPicoEvt_unCorr[20]->SetBinContent(i_RM, k_B_3/k_B_2);// k3/k2
//     if(k_B_1 != 0) hB_C_ref3StPicoEvt_unCorr[21]->SetBinContent(i_RM, k_B_4/k_B_1);// k4/k1
//     if(k_B_3 != 0) hB_C_ref3StPicoEvt_unCorr[22]->SetBinContent(i_RM, k_B_4/k_B_3);// k4/k3
//     if(k_B_1 != 0) hB_C_ref3StPicoEvt_unCorr[23]->SetBinContent(i_RM, k_B_5/k_B_1);// k5/k1
//     if(k_B_4 != 0) hB_C_ref3StPicoEvt_unCorr[24]->SetBinContent(i_RM, k_B_5/k_B_4);// k5/k4
//     if(k_B_1 != 0) hB_C_ref3StPicoEvt_unCorr[25]->SetBinContent(i_RM, k_B_6/k_B_1);// k6/k1
//     if(k_B_5 != 0) hB_C_ref3StPicoEvt_unCorr[26]->SetBinContent(i_RM, k_B_6/k_B_5);// k6/k5


//     ///////
//     //Err//
//     ///////
//     //proton    
//     hB_C_ref3StPicoEvt_unCorr_err[0]->SetBinContent(i_RM, Cerr_B_1);//c_err1
//     hB_C_ref3StPicoEvt_unCorr_err[1]->SetBinContent(i_RM, Cerr_B_2);//c_err2
//     hB_C_ref3StPicoEvt_unCorr_err[2]->SetBinContent(i_RM, Cerr_B_3);//c_err3
//     hB_C_ref3StPicoEvt_unCorr_err[3]->SetBinContent(i_RM, Cerr_B_4);//c_err4
//     hB_C_ref3StPicoEvt_unCorr_err[4]->SetBinContent(i_RM, Cerr_B_5);//c_err5
//     hB_C_ref3StPicoEvt_unCorr_err[5]->SetBinContent(i_RM, Cerr_B_6);//c_err6

//     hB_C_ref3StPicoEvt_unCorr_err[6]->SetBinContent(i_RM, kerr_B_1);//k_err1
//     hB_C_ref3StPicoEvt_unCorr_err[7]->SetBinContent(i_RM, kerr_B_2);//k_err2
//     hB_C_ref3StPicoEvt_unCorr_err[8]->SetBinContent(i_RM, kerr_B_3);//k_err3
//     hB_C_ref3StPicoEvt_unCorr_err[9]->SetBinContent(i_RM, kerr_B_4);//k_err4
//     hB_C_ref3StPicoEvt_unCorr_err[10]->SetBinContent(i_RM, kerr_B_5);//k_err5
//     hB_C_ref3StPicoEvt_unCorr_err[11]->SetBinContent(i_RM, kerr_B_6);//k_err6

//     hB_C_ref3StPicoEvt_unCorr_err[12]->SetBinContent(i_RM, CRerr_B_21);//CR_err21
//     hB_C_ref3StPicoEvt_unCorr_err[13]->SetBinContent(i_RM, CRerr_B_31);//CR_err31
//     hB_C_ref3StPicoEvt_unCorr_err[14]->SetBinContent(i_RM, CRerr_B_32);//CR_err32
//     hB_C_ref3StPicoEvt_unCorr_err[15]->SetBinContent(i_RM, CRerr_B_42);//CR_err42
//     hB_C_ref3StPicoEvt_unCorr_err[16]->SetBinContent(i_RM, CRerr_B_51);//CR_err51
//     hB_C_ref3StPicoEvt_unCorr_err[17]->SetBinContent(i_RM, CRerr_B_62);//CR_err62

//     hB_C_ref3StPicoEvt_unCorr_err[18]->SetBinContent(i_RM, kRerr_B_21);//kR_err21
//     hB_C_ref3StPicoEvt_unCorr_err[19]->SetBinContent(i_RM, kRerr_B_31);//kR_err31
//     hB_C_ref3StPicoEvt_unCorr_err[20]->SetBinContent(i_RM, kRerr_B_32);//kR_err32
//     hB_C_ref3StPicoEvt_unCorr_err[21]->SetBinContent(i_RM, kRerr_B_41);//kR_err41
//     hB_C_ref3StPicoEvt_unCorr_err[22]->SetBinContent(i_RM, kRerr_B_43);//kR_err43
//     hB_C_ref3StPicoEvt_unCorr_err[23]->SetBinContent(i_RM, kRerr_B_51);//kR_err51
//     hB_C_ref3StPicoEvt_unCorr_err[24]->SetBinContent(i_RM, kRerr_B_54);//kR_err54
//     hB_C_ref3StPicoEvt_unCorr_err[25]->SetBinContent(i_RM, kRerr_B_61);//kR_err61
//     hB_C_ref3StPicoEvt_unCorr_err[26]->SetBinContent(i_RM, kRerr_B_65);//kR_err65

//   }//refMult loop ends

//   WriteEffUnCorrRefMult3StPicoEvtCum(Form("output/cum/%s_eff_refMult3StPicoEvt_unCorr_cum.root", NameRuZr));
  
//   delete mCalCumB;
// }

// //////////////////////////////////////////

// void Cum::WriteEffUnCorrRefMult3StPicoEvtCum(const char* outFile){

//   TFile *f_out = new TFile(outFile, "RECREATE");
//   f_out->cd();
//   for(int i = 0 ; i < Ncums_noErr ; i++){
//     hB_C_ref3StPicoEvt_unCorr[i]->Write();
//     hB_C_ref3StPicoEvt_unCorr_err[i]->Write();
//   }

//   hB_Nevt_ref3StPicoEvt_unCorr->Write();
//   f_out->Close();
  
// }

 //======================================//
//////////////////////////////////////////
//======================================//

void Cum::CalculateCBWCcum(const char* NameRuZr){

  //20082021(start)
  // int NmergedBin = 240;
  // int NmultBin = (int)maxMult/NmergedBin+1;
  //20082021(finish)

  //20082021(start)
  double CentEvent[Ncent] = {0.};

  double AvgCent[Ncent] = {0.};

  double CumSumQ[Ncent] = {0.};
  double CumSumB[Ncent] = {0.}; 
  double CumSumS[Ncent] = {0.};
  double CumErrSumQ[Ncent] = {0.};
  double CumErrSumB[Ncent] = {0.};
  double CumErrSumS[Ncent] = {0.};
  //Mixed cumulants
  double MixedCumSum[Ncent] = {0.};
  double MixedCumErrSum[Ncent] = {0.};
  //////////
  // double CentEvent[9] = {0.};

  // double AvgCent[9] = {0.};

  // double CumSumQ[9] = {0.};
  // double CumSumB[9] = {0.};
  // double CumSumS[9] = {0.};
  // double CumErrSumQ[9] = {0.};
  // double CumErrSumB[9] = {0.};
  // double CumErrSumS[9] = {0.};
  // //Mixed cumulants
  // double MixedCumSum[9] = {0.};
  // double MixedCumErrSum[9] = {0.};
  //20082021(finish)

  for(int i_CRK = 0 ; i_CRK < N_CRK ; i_CRK++){

    //initialize
    int Nevt = 0;
    //20082021(start)
    for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
    // for(int i_cent = 0 ; i_cent < NmultBin ; i_cent++){
      //20082021(finish)
      CentEvent[i_cent] = 0.;
    
      AvgCent[i_cent] = 0.;
      
      CumSumQ[i_cent] = 0.;
      CumSumB[i_cent] = 0.;
      CumSumS[i_cent] = 0.;
      CumErrSumQ[i_cent] = 0.;
      CumErrSumB[i_cent] = 0.;
      CumErrSumS[i_cent] = 0.;
    }//centrality bin loop ends

    for(int i_mult = 0 ; i_mult < maxMult ; i_mult++){
      Nevt = hQ_Nevt->GetBinContent(i_mult);

      //20082021(start)
      int CentBin = getCent(i_mult, NameRuZr);
      // cout<<Form("%d, %d",i_mult, CentBin)<<endl;
      if(CentBin == -1) continue;
      /////
      // int CentBin = (int)i_mult/NmergedBin;
      // // cout<<Form("%d, %d",i_mult, CentBin)<<endl;
      // if(CentBin == -1) continue;
      //20082021(finish)

      CentEvent[CentBin] += Nevt;//denominator

      AvgCent[CentBin] += i_mult*Nevt;

      CumSumQ[CentBin] += (hQ_C[i_CRK]->GetBinContent(i_mult) * Nevt);//numerator
      CumSumB[CentBin] += (hB_C[i_CRK]->GetBinContent(i_mult) * Nevt);//numerator
      CumSumS[CentBin] += (hS_C[i_CRK]->GetBinContent(i_mult) * Nevt);//numerator
      CumErrSumQ[CentBin] += TMath::Power(hQ_C[i_CRK]->GetBinError(i_mult) * Nevt, 2);
      CumErrSumB[CentBin] += TMath::Power(hB_C[i_CRK]->GetBinError(i_mult) * Nevt, 2);
      CumErrSumS[CentBin] += TMath::Power(hS_C[i_CRK]->GetBinError(i_mult) * Nevt, 2);
      // CumErrSumQ[CentBin] += TMath::Power(hQ_C_err[i_CRK]->GetBinContent(i_mult) * Nevt, 2);
      // CumErrSumB[CentBin] += TMath::Power(hB_C_err[i_CRK]->GetBinContent(i_mult) * Nevt, 2);
      // CumErrSumS[CentBin] += TMath::Power(hS_C_err[i_CRK]->GetBinContent(i_mult) * Nevt, 2);
    }//mult loop ends
    //20082021(start)
    for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
    // for(int i_cent = 0 ; i_cent < NmultBin ; i_cent++){
      //20082021(finish)
      // cout<<"diagonal i_CRK & CentEvent[i_cent]: "<<i_CRK<<",\t"<<CentEvent[i_cent]<<endl;
      AvgCent[i_cent] /= CentEvent[i_cent];

      CumSumQ[i_cent] /= CentEvent[i_cent];
      CumSumB[i_cent] /= CentEvent[i_cent];
      CumSumS[i_cent] /= CentEvent[i_cent];
      // CumErrSumQ[i_cent] = ( TMath::Sqrt(CumErrSumQ[i_cent]) / CentEvent[i_cent] );
      // CumErrSumB[i_cent] = ( TMath::Sqrt(CumErrSumB[i_cent]) / CentEvent[i_cent] );
      // CumErrSumS[i_cent] = ( TMath::Sqrt(CumErrSumS[i_cent]) / CentEvent[i_cent] );
      CumErrSumQ[i_cent] = ( TMath::Sqrt(CumErrSumQ[i_cent]) / CentEvent[i_cent] );
      CumErrSumB[i_cent] = ( TMath::Sqrt(CumErrSumB[i_cent]) / CentEvent[i_cent] );
      CumErrSumS[i_cent] = ( TMath::Sqrt(CumErrSumS[i_cent]) / CentEvent[i_cent] );
      // grQ_C[i_CRK]->SetPoint(i_cent, centrality[i_cent], CumSumQ[i_cent]);
      // grB_C[i_CRK]->SetPoint(i_cent, centrality[i_cent], CumSumB[i_cent]);
      // grS_C[i_CRK]->SetPoint(i_cent, centrality[i_cent], CumSumS[i_cent]);
      grQ_C[i_CRK]->SetPoint(i_cent, AvgCent[i_cent], CumSumQ[i_cent]);
      grB_C[i_CRK]->SetPoint(i_cent, AvgCent[i_cent], CumSumB[i_cent]);
      grS_C[i_CRK]->SetPoint(i_cent, AvgCent[i_cent], CumSumS[i_cent]);
      grQ_C[i_CRK]->SetPointError(i_cent, 0, CumErrSumQ[i_cent]);
      grB_C[i_CRK]->SetPointError(i_cent, 0, CumErrSumB[i_cent]);
      grS_C[i_CRK]->SetPointError(i_cent, 0, CumErrSumS[i_cent]);

      // cout<<Form("%d,\t %f, \t %f", centrality[i_cent], CentEvent[i_cent], CumSumQ[i_cent])<<endl;

    }//centrality bin loop ends

  }//CRK loop ends

  //Mixed cumulants

  for(int i_mixed_CRK = 0 ; i_mixed_CRK < N_mixed_CRK ; i_mixed_CRK++){

    //initialize
    int Nevt = 0;
    //20082021(start)
    for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
    // for(int i_cent = 0 ; i_cent < NmultBin ; i_cent++){
      //20082021(finish)
      CentEvent[i_cent] = 0.;

      AvgCent[i_cent] = 0.;  
      
      MixedCumSum[i_cent] = 0.;
      MixedCumErrSum[i_cent] = 0.;
    }//centrality bin loop ends

    for(int i_mult = 0 ; i_mult < maxMult ; i_mult++){
      Nevt = hQ_Nevt->GetBinContent(i_mult);

      //20082021(start)
      int CentBin = getCent(i_mult, NameRuZr);
      // cout<<Form("%d, %d",i_mult, CentBin)<<endl;
      if(CentBin == -1) continue;
      //////
      // int CentBin = (int)i_mult/NmergedBin;
      // // cout<<Form("%d, %d",i_mult, CentBin)<<endl;
      // if(CentBin == -1) continue;
      //20082021(finish)

      CentEvent[CentBin] += Nevt;//denominator

      AvgCent[CentBin] += i_mult*Nevt;

      MixedCumSum[CentBin] += (h_mixed_C[i_mixed_CRK]->GetBinContent(i_mult) * Nevt);//numerator
      MixedCumErrSum[CentBin] += TMath::Power(h_mixed_C[i_mixed_CRK]->GetBinError(i_mult) * Nevt, 2);
      // MixedCumErrSum[CentBin] += TMath::Power(h_mixed_C_err[i_mixed_CRK]->GetBinContent(i_mult) * Nevt, 2);
    }//mult loop ends
    //16082021(start)
    // for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
    //20082021(start)
    for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
    // for(int i_cent = 0 ; i_cent < NmultBin ; i_cent++){
      //20082021(finish)
      //16082021(finish)
      // cout<<"off-diagonal CentEvent[i_cent]: "<<CentEvent[i_cent]<<endl;
      AvgCent[i_cent] /= CentEvent[i_cent];

      MixedCumSum[i_cent] /= CentEvent[i_cent];
      // MixedCumErrSum[i_cent] = ( TMath::Sqrt(MixedCumErrSum[i_cent]) / CentEvent[i_cent] );
      MixedCumErrSum[i_cent] = ( TMath::Sqrt(MixedCumErrSum[i_cent]) / CentEvent[i_cent] );
      // gr_mixed_C[i_mixed_CRK]->SetPoint(i_cent, centrality[i_cent], MixedCumSum[i_cent]);
      gr_mixed_C[i_mixed_CRK]->SetPoint(i_cent, AvgCent[i_cent], MixedCumSum[i_cent]);
      gr_mixed_C[i_mixed_CRK]->SetPointError(i_cent, 0, MixedCumErrSum[i_cent]);
      
      //20082021(start)
      // cout<<Form("%d,\t %f, \t %f, \t %f", centrality[i_cent], CentEvent[i_cent], MixedCumSum[i_cent], MixedCumErrSum[i_cent])<<endl;
      // cout<<Form("%d,\t %f,\t %f, \t %f, \t %f", i_cent, AvgCent[i_cent], CentEvent[i_cent], MixedCumSum[i_cent], MixedCumErrSum[i_cent])<<endl;
      //20082021(finish)
    }//centrality bin loop ends

  }//mixed_CRK loop ends

  WriteCBWCcum(Form("output/cum/%s_eff_corr_CBWC_cum.root", NameRuZr));
  
}

//////////////////////////////////////////

void Cum::WriteCBWCcum(const char* outFile){
  TFile *f_out = new TFile(outFile, "RECREATE");
  f_out->cd();
  // for(int i = 0 ; i < Ncums ; i++){
  for(int i = 0 ; i < N_CRK ; i++){
    grQ_C[i]->Write();
    grB_C[i]->Write();
    grS_C[i]->Write();
  }
  // for(int i = 0 ; i < Ncums_mixed ; i++){
  for(int i = 0 ; i < N_mixed_CRK ; i++){
    gr_mixed_C[i]->Write();
  }
  f_out->Close();

}

//////////////////////////////////////////

  //////////////////////////////
  ////////noErr up to C6////////
  //////////////////////////////

//EPDmult
void Cum::CalculateEPDmultCBWCcum(const char* NameRuZr, int dca_cut, int nhitsfit_cut, int nsp_cut, int m2low_cut, int m2up_cut, int ptmax_cut, int ymax_cut, int detefflow_err, int deteffhigh_err){

  //20082021(start)
  // int NmergedBin = 50;
  // int NmultBin = (int)maxMult_ref3/NmergedBin;
  //20082021(finish)

  //20082021(start)
  double CentEvent[Ncent] = {0.};

  double AvgCent[Ncent] = {0.};
  
  double CumSumQ[Ncent] = {0.};
  double CumSumB[Ncent] = {0.};
  double CumSumS[Ncent] = {0.};
  double CumErrSumQ[Ncent] = {0.};
  double CumErrSumB[Ncent] = {0.};
  double CumErrSumS[Ncent] = {0.};
  //////////
  // double CentEvent[16] = {0.};

  // double AvgCent[16] = {0.};
  
  // double CumSumQ[16] = {0.};
  // double CumSumB[16] = {0.};
  // double CumSumS[16] = {0.};
  // double CumErrSumQ[16] = {0.};
  // double CumErrSumB[16] = {0.};
  // double CumErrSumS[16] = {0.};
 //20082021(finish)

  for(int i_CRK = 0 ; i_CRK < N_CRK_noErr ; i_CRK++){

    //initialize
    int Nevt = 0;
    //20082021(start)
    for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
    ////////
    // for(int i_cent = 0 ; i_cent < NmultBin ; i_cent++){
      //20082021(finish)
      CentEvent[i_cent] = 0.;
    
      AvgCent[i_cent] = 0.;
      
      CumSumQ[i_cent] = 0.;
      CumSumB[i_cent] = 0.;
      CumSumS[i_cent] = 0.;
      CumErrSumQ[i_cent] = 0.;
      CumErrSumB[i_cent] = 0.;
      CumErrSumS[i_cent] = 0.;
    }//centrality bin loop ends

    for(int i_mult = 0 ; i_mult < maxMult ; i_mult++){
      Nevt = hB_Nevt_epd->GetBinContent(i_mult);

      //20082021(start)
      int CentBin = getCent(i_mult, NameRuZr);
      // cout<<Form("%d, %d",i_mult, CentBin)<<endl;
      if(CentBin == -1) continue;
      /////
      // int CentBin = (int)i_mult/NmergedBin;
      // if(CentBin == -1) continue;
      //20082021(finish)

      // //30092021(start)(wrong)
      // //2D centrality binning so that select only the events that
      // //belongs to the same centrality class of EPDmult and RefMult3
      // int CentBin_ref3 = getRefMult3Cent(i_mult, NameRuZr);
      // if(CentBin_ref3 == -1) continue;
      // if(CentBin_ref3 != CentBin) {cout<<i_mult<<endl; continue;}
      // //30092021(finish)(wrong)

      cout<<"i_CRK = "<<i_CRK<<", i_mult = "<<i_mult<<", Nevt = "<<Nevt<<", hB_C_epd[i_CRK]->GetBinContent(i_mult) = "<<hB_C_epd[i_CRK]->GetBinContent(i_mult)<<", hB_C_epd[i_CRK]->GetBinError(i_mult) = "<<hB_C_epd[i_CRK]->GetBinError(i_mult)<<endl;
      //15092021(start)(not sure)
      if( !isnan(Nevt) && !isnan(i_mult)
          // && !isnan(hQ_C_epd[i_CRK]->GetBinContent(i_mult)) && !isnan(hB_C_epd[i_CRK]->GetBinContent(i_mult)) && !isnan(hS_C_epd[i_CRK]->GetBinContent(i_mult))
          // && !isnan(hQ_C_epd[i_CRK]->GetBinError(i_mult)) && !isnan(hB_C_epd[i_CRK]->GetBinError(i_mult)) && !isnan(hS_C_epd[i_CRK]->GetBinError(i_mult))
	  //I care about net-proton only.
          && !isnan(hB_C_epd[i_CRK]->GetBinContent(i_mult))
          && !isnan(hB_C_epd[i_CRK]->GetBinError(i_mult))
          ){

	CentEvent[CentBin] += Nevt;//denominator

	AvgCent[CentBin] += i_mult*Nevt;

	CumSumQ[CentBin] += (hQ_C_epd[i_CRK]->GetBinContent(i_mult) * Nevt);//numerator
	CumSumB[CentBin] += (hB_C_epd[i_CRK]->GetBinContent(i_mult) * Nevt);//numerator
	CumSumS[CentBin] += (hS_C_epd[i_CRK]->GetBinContent(i_mult) * Nevt);//numerator

	CumErrSumQ[CentBin] += TMath::Power(hQ_C_epd[i_CRK]->GetBinError(i_mult) * Nevt, 2);
	CumErrSumB[CentBin] += TMath::Power(hB_C_epd[i_CRK]->GetBinError(i_mult) * Nevt, 2);
	CumErrSumS[CentBin] += TMath::Power(hS_C_epd[i_CRK]->GetBinError(i_mult) * Nevt, 2);

	//15092021(start)
	// if(!isnan(TMath::Power(hQ_C_epd[i_CRK]->GetBinError(i_mult) * Nevt, 2))) CumErrSumQ[CentBin] += TMath::Power(hQ_C_epd[i_CRK]->GetBinError(i_mult) * Nevt, 2);
	// if(!isnan(TMath::Power(hB_C_epd[i_CRK]->GetBinError(i_mult) * Nevt, 2))) CumErrSumB[CentBin] += TMath::Power(hB_C_epd[i_CRK]->GetBinError(i_mult) * Nevt, 2);
	// if(!isnan(TMath::Power(hS_C_epd[i_CRK]->GetBinError(i_mult) * Nevt, 2))) CumErrSumS[CentBin] += TMath::Power(hS_C_epd[i_CRK]->GetBinError(i_mult) * Nevt, 2);
	//15092021(finish)
      }
      //15092021(finish)(not sure)
      // CumErrSumQ[CentBin] += TMath::Power(hQ_C_epd_err[i_CRK]->GetBinContent(i_mult) * Nevt, 2);
      // CumErrSumB[CentBin] += TMath::Power(hB_C_epd_err[i_CRK]->GetBinContent(i_mult) * Nevt, 2);
      // CumErrSumS[CentBin] += TMath::Power(hS_C_epd_err[i_CRK]->GetBinContent(i_mult) * Nevt, 2);
    }//mult loop ends
    //16082021(start)
    // for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
    //20082021(start)
    for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
    //////
    // for(int i_cent = 0 ; i_cent < NmultBin ; i_cent++){
      //20082021(finish)
      //16082021(finish)
      // cout<<"diagonal i_CRK & CentEvent[i_cent]: "<<i_CRK<<",\t"<<CentEvent[i_cent]<<endl;
      AvgCent[i_cent] /= CentEvent[i_cent];

      CumSumQ[i_cent] /= CentEvent[i_cent];
      CumSumB[i_cent] /= CentEvent[i_cent];
      CumSumS[i_cent] /= CentEvent[i_cent];
      // CumErrSumQ[i_cent] = ( TMath::Sqrt(CumErrSumQ[i_cent]) / CentEvent[i_cent] );
      // CumErrSumB[i_cent] = ( TMath::Sqrt(CumErrSumB[i_cent]) / CentEvent[i_cent] );
      // CumErrSumS[i_cent] = ( TMath::Sqrt(CumErrSumS[i_cent]) / CentEvent[i_cent] );
      CumErrSumQ[i_cent] = ( TMath::Sqrt(CumErrSumQ[i_cent]) / CentEvent[i_cent] );
      CumErrSumB[i_cent] = ( TMath::Sqrt(CumErrSumB[i_cent]) / CentEvent[i_cent] );
      CumErrSumS[i_cent] = ( TMath::Sqrt(CumErrSumS[i_cent]) / CentEvent[i_cent] );
      // grQ_C_epd[i_CRK]->SetPoint(i_cent, centrality[i_cent], CumSumQ[i_cent]);
      // grB_C_epd[i_CRK]->SetPoint(i_cent, centrality[i_cent], CumSumB[i_cent]);
      // grS_C_epd[i_CRK]->SetPoint(i_cent, centrality[i_cent], CumSumS[i_cent]);

      if(!isnan(AvgCent[i_cent]) && !isnan(CumSumB[i_cent]) && !isnan(CumErrSumB[i_cent])){
	//I care about net-proton only.	
	grQ_C_epd[i_CRK]->SetPoint(i_cent, AvgCent[i_cent], CumSumQ[i_cent]);
	grB_C_epd[i_CRK]->SetPoint(i_cent, AvgCent[i_cent], CumSumB[i_cent]);
	grS_C_epd[i_CRK]->SetPoint(i_cent, AvgCent[i_cent], CumSumS[i_cent]);
	grQ_C_epd[i_CRK]->SetPointError(i_cent, 0, CumErrSumQ[i_cent]);
	grB_C_epd[i_CRK]->SetPointError(i_cent, 0, CumErrSumB[i_cent]);
	grS_C_epd[i_CRK]->SetPointError(i_cent, 0, CumErrSumS[i_cent]);
      }
    }//centrality bin loop ends

  }//CRK loop ends

  WriteEPDmultCBWCcum(Form("output/cum/%s_effcorr_ref3uncorr_CBWC_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_ptmax_%d_ymax_%d_detefflow_%d_deteffhigh_%d_maxmult_%d_29012022_1_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, detefflow_err, deteffhigh_err, maxMult_ref3StPicoEvt, MergeTopCent));
  
}

//////////////////////////////////////////

void Cum::WriteEPDmultCBWCcum(const char* outFile){
  TFile *f_out = new TFile(outFile, "RECREATE");
  f_out->cd();
  // for(int i = 0 ; i < Ncums ; i++){
  for(int i = 0 ; i < N_CRK_noErr ; i++){
    grQ_C_epd[i]->Write();
    grB_C_epd[i]->Write();
    grS_C_epd[i]->Write();
  }
  f_out->Close();

}

//////////////////////////////////////////
 
//RefMult3
void Cum::CalculateRefMult3CBWCcum(const char* NameRuZr, int dca_cut, int nhitsfit_cut, int nsp_cut, int m2low_cut, int m2up_cut, int ptmax_cut, int ymax_cut, int detefflow_err, int deteffhigh_err){

  //20082021(start)
  // int NmergedBin = 50;
  // int NmultBin = (int)maxMult_ref3/NmergedBin;
  //20082021(finish)

  //20082021(start)
  double CentEvent[Ncent] = {0.};
  double AvgCent[Ncent] = {0.};
  double CumSumB[Ncent] = {0.};
  double CumErrSumB[Ncent] = {0.};
  //////
  // double CentEvent[16] = {0.};
  // double AvgCent[16] = {0.};
  // double CumSumB[16] = {0.};
  // double CumErrSumB[16] = {0.};
  //20082021(finish)

  for(int i_CRK = 0 ; i_CRK < N_CRK_noErr ; i_CRK++){

    //initialize
    int Nevt = 0;
    //20082021(start)
    for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
      ///////
      // for(int i_cent = 0 ; i_cent < NmultBin ; i_cent++){
      //20082021(finish)
      CentEvent[i_cent] = 0.;

      AvgCent[i_cent] = 0.;
      
      CumSumB[i_cent] = 0.;
      CumErrSumB[i_cent] = 0.;
    }//centrality bin loop ends

    //01072021(start)
    // for(int i_mult = 0 ; i_mult < maxMult ; i_mult++){
    for(int i_mult = 0 ; i_mult < maxMult_ref3 ; i_mult++){
      //22102021: CHEAT~!!!! RuZr_mean_refMult3StPicoEvt_mergedFile_dca_10_nhitsfit_20_nsp_25_m2low_60_m2up_120.root has a weird structure @RefMult3 = 393. Let's ignore 392 for now!!!! (start)
      // if(i_mult != 393){
      //01072021(finish)
      Nevt = hB_Nevt_ref3->GetBinContent(i_mult);
      // cout<<i_mult<<", "<<Nevt<<endl;
      //20082021(start)
      int CentBin = getRefMult3Cent(i_mult, NameRuZr);
      // cout<<Form("%d, %d",i_mult, CentBin)<<endl;
      if(CentBin == -1) continue;
      //////////
      // int CentBin = (int)i_mult/NmergedBin;
      // if(CentBin == -1) continue;
      //20082021(finish)

      // //30092021(start)(wrong)
      // //2D centrality binning so that select only the events that
      // //belongs to the same centrality class of EPDmult and RefMult3
      // int CentBin_epd = getCent(i_mult, NameRuZr);
      // if(CentBin_epd == -1) continue;
      // if(CentBin_epd != CentBin) continue;
      // //30092021(finish)(wrong)

      //15092021(start)(not sure)
      //01122021(start)
      if( isnan(Nevt) || isnan(i_mult)
	  || isnan(hB_C_ref3[i_CRK]->GetBinContent(i_mult))
	  || isnan(hB_C_ref3[i_CRK]->GetBinError(i_mult))
	  ) {
	cout<<"i_CRK = "<<i_CRK<<", i_mult = "<<i_mult<<", Nevt = "<<Nevt<<", hB_C_ref3[i_CRK]->GetBinContent(i_mult) = "<<hB_C_ref3[i_CRK]->GetBinContent(i_mult)<<", hB_C_ref3[i_CRK]->GetBinError(i_mult) = "<<hB_C_ref3[i_CRK]->GetBinError(i_mult)<<endl;
      }
      if( !isnan(Nevt) && !isnan(i_mult)
	  && !isnan(hB_C_ref3[i_CRK]->GetBinContent(i_mult))
	  && !isnan(hB_C_ref3[i_CRK]->GetBinError(i_mult))
	  ) {
	//01122021(finish)
	CentEvent[CentBin] += Nevt;//denominator

	AvgCent[CentBin] += i_mult*Nevt;

	CumSumB[CentBin] += (hB_C_ref3[i_CRK]->GetBinContent(i_mult) * Nevt);//numerator
	// if(i_CRK == 10 /* CR_5_1 */ && CentBin == 8) cout<<"Cum::CalculateRefMult3CBWCcum: "<<"i_CRK = "<<i_CRK<<", CentBin = "<<CentBin<<", "<<"hB_C_ref3[i_CRK]->GetBinError(i_mult) * Nevt: "<<hB_C_ref3[i_CRK]->GetBinError(i_mult)<<" * "<<Nevt<<" = "<<TMath::Power(hB_C_ref3[i_CRK]->GetBinError(i_mult) * Nevt, 2)<<endl;
	// if(i_CRK == 10 /* CR_5_1 */ && CentBin == 7) cout<<"Cum::CalculateRefMult3CBWCcum: "<<"i_CRK = "<<i_CRK<<", CentBin = "<<CentBin<<", "<<"hB_C_ref3[i_CRK]->GetBinError(i_mult) * Nevt: "<<hB_C_ref3[i_CRK]->GetBinError(i_mult)<<" * "<<Nevt<<" = "<<TMath::Power(hB_C_ref3[i_CRK]->GetBinError(i_mult) * Nevt, 2)<<endl;

	CumErrSumB[CentBin] += TMath::Power(hB_C_ref3[i_CRK]->GetBinError(i_mult) * Nevt, 2);

      
	//15092021(start)
	// //14092021(start)
	// // CumErrSumB[CentBin] += TMath::Power(hB_C_ref3[i_CRK]->GetBinError(i_mult) * Nevt, 2);
	// //14092021(finish)
	//15092021(finish)
	//01122021(start)
      }
      //01122021(finish)
      //15092021(finish)(not sure)
      
      // cout<<"i_CRK = "<<i_CRK<<", CentBin = "<<CentBin<<", CentEvent[CentBin] = "<<CentEvent[CentBin]<<", AvgCent[CentBin] = "<<AvgCent[CentBin]<<",  CumSumB[CentBin] = "<<CumSumB[CentBin]<<",  CumErrSumB[CentBin] = "<<CumErrSumB[CentBin]<<endl;
      // CumErrSumB[CentBin] += TMath::Power(hB_C_ref3_err[i_CRK]->GetBinContent(i_mult) * Nevt, 2);
      // }//@RefMult3 = 393 removal statement ends
      //22102021: CHEAT~!!!! RuZr_mean_refMult3StPicoEvt_mergedFile_dca_10_nhitsfit_20_nsp_25_m2low_60_m2up_120.root has a weird structure @RefMult3 = 392. Let's ignore 392 for now!!!! (finish)
    }//mult loop ends
    //16082021(start)
    // for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
    //20082021(start)
    for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
      ///////////
      // for(int i_cent = 0 ; i_cent < NmultBin ; i_cent++){
      //20082021(finish)
      //16082021(finish)
      // cout<<"diagonal i_CRK & AvgCent[i_cent]: "<<i_CRK<<",\t"<<AvgCent[i_cent]<<endl;
      // cout<<"diagonal i_CRK & CentEvent[i_cent]: "<<i_CRK<<",\t"<<CentEvent[i_cent]<<endl;
      AvgCent[i_cent] /= CentEvent[i_cent];
      // cout<<Form("AvgCent[%d] = %f", i_cent, AvgCent[i_cent])<<endl;
      CumSumB[i_cent] /= CentEvent[i_cent];
      // CumErrSumB[i_cent] = ( TMath::Sqrt(CumErrSumB[i_cent]) / CentEvent[i_cent] );
      // cout<<"i_CRK = "<<i_CRK<<", i_cent = "<<i_cent<<",  CentEvent[i_cent] = "<<CentEvent[i_cent]<<",  CumErrSumB[i_cent] = "<<CumErrSumB[i_cent]<<endl;
      CumErrSumB[i_cent] = ( TMath::Sqrt(CumErrSumB[i_cent]) / CentEvent[i_cent] );
      // grB_C_ref3[i_CRK]->SetPoint(i_cent, centrality_ref3[i_cent], CumSumB[i_cent]);
      // if(isnan(AvgCent[i_cent]) || isnan(CumSumB[i_cent] || isnan(CumErrSumB[i_cent]))) cout<<"i_cent, AvgCent[i_cent], CumSumB[i_cent], CumErrSumB[i_cent]: "<<i_cent<<", "<<AvgCent[i_cent]<<", "<<CumSumB[i_cent]<<", "<<CumErrSumB[i_cent]<<endl;
      //01122021(start)
      if(!isnan(AvgCent[i_cent]) && !isnan(CumSumB[i_cent]) && !isnan(CumErrSumB[i_cent])){
	//01122021(finish)
	grB_C_ref3[i_CRK]->SetPoint(i_cent, AvgCent[i_cent], CumSumB[i_cent]);
	grB_C_ref3[i_CRK]->SetPointError(i_cent, 0, CumErrSumB[i_cent]);
	// cout<<"i_CRK = "<<i_CRK<<", i_cent = "<<i_cent<<", AvgCent[i_cent] = "<<AvgCent[i_cent]<<", CumSumB[i_cent] = "<<CumSumB[i_cent]<<", CumErrSumB[i_cent] = "<<CumErrSumB[i_cent]<<endl;
	//01122021(start)
      }
      //01122021(finish)

    }//centrality bin loop ends

  }//CRK loop ends

  //parasite
  WriteRefMult3CBWCcum(Form("output/cum/%s_effcorr_ref3corr_CBWC_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_ptmax_%d_ymax_%d_detefflow_%d_deteffhigh_%d_maxmult_%d_29012022_1_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, detefflow_err, deteffhigh_err, maxMult_ref3StPicoEvt, MergeTopCent));
}
  //////////////////////////////////////////

  void Cum::WriteRefMult3CBWCcum(const char* outFile){
    TFile *f_out = new TFile(outFile, "RECREATE");
    f_out->cd();
    // for(int i = 0 ; i < Ncums ; i++){
    for(int i = 0 ; i < N_CRK_noErr ; i++){
      grB_C_ref3[i]->Write();
    }
    f_out->Close();

  }

  ///////////////////////////////

  //RefMult3 unCorr
void Cum::CalculateRefMult3CBWCcumUnCorr(const char* NameRuZr, int dca_cut, int nhitsfit_cut, int nsp_cut, int m2low_cut, int m2up_cut, int ptmax_cut, int ymax_cut){
  double CentEvent[Ncent] = {0.};

  double AvgCent[Ncent] = {0.};
  
  double CumSumB[Ncent] = {0.};
  double CumErrSumB[Ncent] = {0.};
  for(int i_CRK = 0 ; i_CRK < N_CRK_noErr ; i_CRK++){

    //initialize
    int Nevt = 0;
    for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
      CentEvent[i_cent] = 0.;

      AvgCent[i_cent] = 0.;
      
      CumSumB[i_cent] = 0.;
      CumErrSumB[i_cent] = 0.;
    }//centrality bin loop ends

    for(int i_mult = 0 ; i_mult < maxMult_ref3 ; i_mult++){
      //22102021: CHEAT~!!!! RuZr_mean_refMult3StPicoEvt_mergedFile_dca_10_nhitsfit_20_nsp_25_m2low_60_m2up_120.root has a weird structure @RefMult3 = 393. Let's ignore 392 for now!!!! (start)
      // if(i_mult != 393){
      Nevt = hB_Nevt_ref3_unCorr->GetBinContent(i_mult);
      int CentBin = getRefMult3Cent(i_mult, NameRuZr);
      // cout<<Form("%d, %d",i_mult, CentBin)<<endl;
      if(CentBin == -1) continue;

      if( !isnan(Nevt) && !isnan(i_mult) //not sure this is the way to do
	  && !isnan(hB_C_ref3_unCorr[i_CRK]->GetBinContent(i_mult))
	  && !isnan(hB_C_ref3_unCorr[i_CRK]->GetBinError(i_mult))
	  ) {
	CentEvent[CentBin] += Nevt;//denominator

	AvgCent[CentBin] += i_mult*Nevt;

	CumSumB[CentBin] += (hB_C_ref3_unCorr[i_CRK]->GetBinContent(i_mult) * Nevt);//numerator
	CumErrSumB[CentBin] += TMath::Power(hB_C_ref3_unCorr[i_CRK]->GetBinError(i_mult) * Nevt, 2);
      }//if(!isnan
    }//mult loop ends
    for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
      AvgCent[i_cent] /= CentEvent[i_cent];
      CumSumB[i_cent] /= CentEvent[i_cent];
      CumErrSumB[i_cent] = ( TMath::Sqrt(CumErrSumB[i_cent]) / CentEvent[i_cent] );

      if(!isnan(AvgCent[i_cent]) && !isnan(CumSumB[i_cent]) && !isnan(CumErrSumB[i_cent])){//not sure this is the way to do
	grB_C_ref3_unCorr[i_CRK]->SetPoint(i_cent, AvgCent[i_cent], CumSumB[i_cent]);
	grB_C_ref3_unCorr[i_CRK]->SetPointError(i_cent, 0, CumErrSumB[i_cent]);
	cout<<"i_CRK = "<<i_CRK<<", i_cent = "<<i_cent<<", AvgCent[i_cent] = "<<AvgCent[i_cent]<<", CumSumB[i_cent] = "<<CumSumB[i_cent]<<", CumErrSumB[i_cent] = "<<CumErrSumB[i_cent]<<endl;
      }//if(!isnan(AvgCent[i_cent]) && !isnan(CumSumB[i_cent]) && !isnan(CumErrSumB[i_cent])){
    }//centrality bin loop ends

  }//CRK loop ends

  //parasite
  // WriteRefMult3CBWCcumUnCorr(Form("output/cum/%s_eff_UnCorr_refMult3StPicoEvt_CBWC_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_ruzrtrackeff_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, MergeTopCent));
  // WriteRefMult3CBWCcumUnCorr(Form("output/cum/%s_eff_UnCorr_refMult3StPicoEvt_CBWC_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_multeff_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, MergeTopCent));
  // WriteRefMult3CBWCcumUnCorr(Form("output/cum/%s_eff_UnCorr_refMult3StPicoEvt_CBWC_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_ptmax_%d_ymax_%d_noepdcut_narrowref3cut_yptcenteff_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut,  MergeTopCent));
  WriteRefMult3CBWCcumUnCorr(Form("output/cum/%s_eff_UnCorr_refMult3StPicoEvt_CBWC_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_ptmax_%d_ymax_%d_noepdcut_multeff_%d_29112021_1.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut,  MergeTopCent));
  // WriteRefMult3CBWCcumUnCorr(Form("output/cum/%s_eff_UnCorr_refMult3StPicoEvt_CBWC_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_ptmax_%d_ymax_%d_noepdcut_narrowref3cut_multeff_%d_30112021_1.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut,  MergeTopCent));
  // WriteRefMult3CBWCcumUnCorr(Form("output/cum/%s_eff_UnCorr_refMult3StPicoEvt_CBWC_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_noepdcut_narrowref3cut_yptmulteff_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, MergeTopCent));
  // WriteRefMult3CBWCcumUnCorr(Form("output/cum/%s_eff_UnCorr_refMult3StPicoEvt_CBWC_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_auaueff_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, MergeTopCent));
  
}

  //////////////////////////////////////////

  void Cum::WriteRefMult3CBWCcumUnCorr(const char* outFile){
    TFile *f_out = new TFile(outFile, "RECREATE");
    f_out->cd();
    // for(int i = 0 ; i < Ncums ; i++){
    for(int i = 0 ; i < N_CRK_noErr ; i++){
      grB_C_ref3_unCorr[i]->Write();
    }
    f_out->Close();

  }

  //////////////////////////////////////////

void Cum::CBWC_RuZr(const char* inFile_Ru, const char* inFile_Zr, const char* outFile){
  TFile *outf = new TFile(outFile, "RECREATE");
  TFile *inf_Ru = new TFile(inFile_Ru, "READ");
  TFile *inf_Zr = new TFile(inFile_Zr, "READ");
  TH1D *hB_C_ref3_Ru[Ncums_noErr];
  TH1D *hB_C_ref3_Zr[Ncums_noErr];
  TH1D *hB_Nevt_ref3_Ru = (TH1D*)inf_Ru->Get(Form("hB_Nevt_%s_ref3", "proton")); hB_Nevt_ref3_Ru->SetName(Form("hB_Nevt_%s_ref3_%s", "proton", "Ru"));
  TH1D *hB_Nevt_ref3_Zr = (TH1D*)inf_Zr->Get(Form("hB_Nevt_%s_ref3", "proton")); hB_Nevt_ref3_Zr->SetName(Form("hB_Nevt_%s_ref3_%s", "proton", "Zr"));
  for(int i_cum = 0 ; i_cum < Ncums_noErr ; i_cum++){
    hB_C_ref3_Ru[i_cum] = (TH1D*)inf_Ru->Get(Form("hB_C%d_%s_ref3", i_cum+1, "proton"));hB_C_ref3_Ru[i_cum]->SetName(Form("hB_C%d_%s_ref3_Ru", i_cum+1, "proton"));
    hB_C_ref3_Zr[i_cum] = (TH1D*)inf_Zr->Get(Form("hB_C%d_%s_ref3", i_cum+1, "proton"));hB_C_ref3_Zr[i_cum]->SetName(Form("hB_C%d_%s_ref3_Zr", i_cum+1, "proton"));
  }

  TGraphErrors *grB_C_ref3_RuZr[N_CRK_noErr];
  for(int i_CRK = 0 ; i_CRK < N_CRK_noErr ; i_CRK++){
    grB_C_ref3_RuZr[i_CRK] = new TGraphErrors(Ncent);
    grB_C_ref3_RuZr[i_CRK]->SetName(Form("%s_%s_%s", "proton", CRK_name_noErr[i_CRK], "RuZr"));
    grB_C_ref3_RuZr[i_CRK]->SetTitle(CRK_label_noErr[i_CRK]);
    grB_C_ref3_RuZr[i_CRK]->SetMarkerStyle(20);
    grB_C_ref3_RuZr[i_CRK]->GetXaxis()->SetTitle("RefMult3");
    grB_C_ref3_RuZr[i_CRK]->GetYaxis()->SetTitle(CRK_label_noErr[i_CRK]);
  }
 
  double CentEvent[Ncent] = {0.};
  double AvgCent[Ncent] = {0.};
  double CumSumB[Ncent] = {0.};
  double CumErrSumB[Ncent] = {0.};

  for(int i_CRK = 0 ; i_CRK < N_CRK_noErr ; i_CRK++){

    //initialize
    int Nevt_Ru = 0;
    int Nevt_Zr = 0;
    for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
      CentEvent[i_cent] = 0.;

      AvgCent[i_cent] = 0.;
      
      CumSumB[i_cent] = 0.;
      CumErrSumB[i_cent] = 0.;
    }//centrality bin loop ends

    for(int i_mult = 0 ; i_mult < maxMult_ref3 ; i_mult++){
      Nevt_Ru = hB_Nevt_ref3_Ru->GetBinContent(i_mult);
      Nevt_Zr = hB_Nevt_ref3_Zr->GetBinContent(i_mult);

      int CentBin = getRefMult3Cent(i_mult, "RuZr");

      if(CentBin == -1) continue;

      int CentBin_Ru = getRefMult3Cent(i_mult, "Ru");
      int CentBin_Zr = getRefMult3Cent(i_mult, "Zr");

      if(CentBin == CentBin_Ru && CentBin != CentBin_Zr){
	if( isnan(Nevt_Ru) || isnan(i_mult)
	    || isnan(hB_C_ref3_Ru[i_CRK]->GetBinContent(i_mult))
	    || isnan(hB_C_ref3_Ru[i_CRK]->GetBinError(i_mult))
	    ) {
	  cout<<"i_CRK = "<<i_CRK<<", i_mult = "<<i_mult<<", Nevt_Ru = "<<Nevt_Ru<<", hB_C_ref3_Ru[i_CRK]->GetBinContent(i_mult) = "<<hB_C_ref3_Ru[i_CRK]->GetBinContent(i_mult)<<", hB_C_ref3_Ru[i_CRK]->GetBinError(i_mult) = "<<hB_C_ref3_Ru[i_CRK]->GetBinError(i_mult)<<endl;
	}
	if( !isnan(Nevt_Ru) && !isnan(i_mult)
	    && !isnan(hB_C_ref3_Ru[i_CRK]->GetBinContent(i_mult))
	    && !isnan(hB_C_ref3_Ru[i_CRK]->GetBinError(i_mult))
	    ) {
	  CentEvent[CentBin] += Nevt_Ru;//denominator

	  AvgCent[CentBin] += i_mult*Nevt_Ru;

	  CumSumB[CentBin] += (hB_C_ref3_Ru[i_CRK]->GetBinContent(i_mult) * Nevt_Ru);//numerator

	  CumErrSumB[CentBin] += TMath::Power(hB_C_ref3_Ru[i_CRK]->GetBinError(i_mult) * Nevt_Ru, 2);

	}// isnan
      }// if(CentBin == CentBin_Ru && CentBin != CentBin_Zr)
      else if(CentBin != CentBin_Ru && CentBin == CentBin_Zr){
	if( isnan(Nevt_Zr) || isnan(i_mult)
	    || isnan(hB_C_ref3_Zr[i_CRK]->GetBinContent(i_mult))
	    || isnan(hB_C_ref3_Zr[i_CRK]->GetBinError(i_mult))
	    ) {
	  cout<<"i_CRK = "<<i_CRK<<", i_mult = "<<i_mult<<", Nevt_Zr = "<<Nevt_Zr<<", hB_C_ref3_Zr[i_CRK]->GetBinContent(i_mult) = "<<hB_C_ref3_Zr[i_CRK]->GetBinContent(i_mult)<<", hB_C_ref3_Zr[i_CRK]->GetBinError(i_mult) = "<<hB_C_ref3_Zr[i_CRK]->GetBinError(i_mult)<<endl;
	}
	if( !isnan(Nevt_Zr) && !isnan(i_mult)
	    && !isnan(hB_C_ref3_Zr[i_CRK]->GetBinContent(i_mult))
	    && !isnan(hB_C_ref3_Zr[i_CRK]->GetBinError(i_mult))
	    ) {
	  CentEvent[CentBin] += Nevt_Zr;//denominator

	  AvgCent[CentBin] += i_mult*Nevt_Zr;

	  CumSumB[CentBin] += (hB_C_ref3_Zr[i_CRK]->GetBinContent(i_mult) * Nevt_Zr);//numerator

	  CumErrSumB[CentBin] += TMath::Power(hB_C_ref3_Zr[i_CRK]->GetBinError(i_mult) * Nevt_Zr, 2);

	}// isnan
      }// if(CentBin != CentBin_Ru && CentBin == CentBin_Zr)
      else if(CentBin == CentBin_Ru && CentBin == CentBin_Zr){
	if(
	   (
	    isnan(Nevt_Ru) || isnan(i_mult)
	    || isnan(hB_C_ref3_Ru[i_CRK]->GetBinContent(i_mult))
	    || isnan(hB_C_ref3_Ru[i_CRK]->GetBinError(i_mult))
	    )
	   ||
	   (
	    isnan(Nevt_Zr) || isnan(i_mult)
	    || isnan(hB_C_ref3_Zr[i_CRK]->GetBinContent(i_mult))
	    || isnan(hB_C_ref3_Zr[i_CRK]->GetBinError(i_mult))
	    )

	   ) {
	  cout<<"i_CRK = "<<i_CRK<<", i_mult = "<<i_mult<<", Nevt_Ru = "<<Nevt_Ru<<", hB_C_ref3_Ru[i_CRK]->GetBinContent(i_mult) = "<<hB_C_ref3_Ru[i_CRK]->GetBinContent(i_mult)<<", hB_C_ref3_Ru[i_CRK]->GetBinError(i_mult) = "<<hB_C_ref3_Ru[i_CRK]->GetBinError(i_mult)<<", Nevt_Zr = "<<Nevt_Zr<<", hB_C_ref3_Zr[i_CRK]->GetBinContent(i_mult) = "<<hB_C_ref3_Zr[i_CRK]->GetBinContent(i_mult)<<", hB_C_ref3_Zr[i_CRK]->GetBinError(i_mult) = "<<hB_C_ref3_Zr[i_CRK]->GetBinError(i_mult)<<endl;
	}
	if(
	   (!isnan(Nevt_Ru) && !isnan(i_mult)
	    && !isnan(hB_C_ref3_Ru[i_CRK]->GetBinContent(i_mult))
	    && !isnan(hB_C_ref3_Ru[i_CRK]->GetBinError(i_mult))
	    )
	   &&
	   (!isnan(Nevt_Zr) && !isnan(i_mult)
	    && !isnan(hB_C_ref3_Zr[i_CRK]->GetBinContent(i_mult))
	    && !isnan(hB_C_ref3_Zr[i_CRK]->GetBinError(i_mult))
	    )
	   ) {
	  CentEvent[CentBin] += (Nevt_Ru + Nevt_Zr);//denominator

	  AvgCent[CentBin] += i_mult*(Nevt_Ru + Nevt_Zr) ;

	  CumSumB[CentBin] += (hB_C_ref3_Ru[i_CRK]->GetBinContent(i_mult) * Nevt_Ru) + (hB_C_ref3_Zr[i_CRK]->GetBinContent(i_mult) * Nevt_Zr);//numerator

	  CumErrSumB[CentBin] += TMath::Power(hB_C_ref3_Ru[i_CRK]->GetBinError(i_mult) * Nevt_Ru, 2) + TMath::Power(hB_C_ref3_Zr[i_CRK]->GetBinError(i_mult) * Nevt_Zr, 2);

	}// isnan
      }// if(CentBin == CentBin_Ru && CentBin == CentBin_Zr)
      else {cout<<"CentBin = "<<CentBin<<", CentBin_Ru = "<<CentBin_Ru<<", CentBin_Zr = "<<CentBin_Zr<<endl;}

    }//mult loop ends

    for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
      AvgCent[i_cent] /= CentEvent[i_cent];
      CumSumB[i_cent] /= CentEvent[i_cent];
      CumErrSumB[i_cent] = ( TMath::Sqrt(CumErrSumB[i_cent]) / CentEvent[i_cent] );
      if(!isnan(AvgCent[i_cent]) && !isnan(CumSumB[i_cent]) && !isnan(CumErrSumB[i_cent])){
	grB_C_ref3_RuZr[i_CRK]->SetPoint(i_cent, AvgCent[i_cent], CumSumB[i_cent]);
	grB_C_ref3_RuZr[i_CRK]->SetPointError(i_cent, 0, CumErrSumB[i_cent]);
      }
      //01122021(finish)

    }//centrality bin loop ends

  }//CRK loop ends

  outf->cd();
  for(int i_CRK = 0 ; i_CRK < N_CRK_noErr ; i_CRK++){
    grB_C_ref3_RuZr[i_CRK]->Write();
  }
  outf->Write();
  outf->Close();
  
}

  //////////////////////////////////////////


  // //RefMult3 StPicoEvt
  // void Cum::CalculateRefMult3StPicoEvtCBWCcum(const char* NameRuZr){

  //   //20082021(start)
  //   // int NmergedBin = 70;
  //   // int NmultBin = (int)maxMult_ref3StPicoEvt/NmergedBin;
  //   //20082021(finish)

  //   //20082021(start)
  //   double CentEvent[Ncent] = {0.};

  //   double AvgCent[Ncent] = {0.};
  
  //   double CumSumB[Ncent] = {0.};
  //   double CumErrSumB[Ncent] = {0.};
  //   //////
  //   // double CentEvent[10] = {0.};

  //   // double AvgCent[10] = {0.};
  
  //   // double CumSumB[10] = {0.};
  //   // double CumErrSumB[10] = {0.};
  //   //20082021(finish)

  //   for(int i_CRK = 0 ; i_CRK < N_CRK_noErr ; i_CRK++){

  //     //initialize
  //     int Nevt = 0;
  //     //20082021(start)
  //     for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
  //     // for(int i_cent = 0 ; i_cent < NmultBin ; i_cent++){
  //       //20082021(finish)
  //       CentEvent[i_cent] = 0.;

  //       AvgCent[i_cent] = 0.;
      
  //       CumSumB[i_cent] = 0.;
  //       CumErrSumB[i_cent] = 0.;
  //     }//centrality bin loop ends

  //     //01072021(start)
  //     // for(int i_mult = 0 ; i_mult < maxMult ; i_mult++){
  //     for(int i_mult = 0 ; i_mult < maxMult_ref3StPicoEvt ; i_mult++){
  //       //01072021(finish)
  //       Nevt = hB_Nevt_ref3StPicoEvt->GetBinContent(i_mult);

  //       //20082021(start)
  //       int CentBin = getRefMult3StPicoEvtCent(i_mult, NameRuZr);
  //       // cout<<Form("%d, %d",i_mult, CentBin)<<endl;
  //       if(CentBin == -1) continue;

  //       // int CentBin = (int)i_mult/NmergedBin;
  //       // if(CentBin == -1) continue;
  //       //20082021(finish)

  //       CentEvent[CentBin] += Nevt;//denominator

  //       AvgCent[CentBin] += i_mult*Nevt;

  //       CumSumB[CentBin] += (hB_C_ref3StPicoEvt[i_CRK]->GetBinContent(i_mult) * Nevt);//numerator
  //       CumErrSumB[CentBin] += TMath::Power(hB_C_ref3StPicoEvt[i_CRK]->GetBinError(i_mult) * Nevt, 2);
  //       // cout<<"i_CRK = "<<i_CRK<<", CentBin = "<<CentBin<<",  CumSumB[CentBin] = "<<CumSumB[CentBin]<<",  CumErrSumB[CentBin] = "<<CumErrSumB[CentBin]<<endl;
  //       // CumErrSumB[CentBin] += TMath::Power(hB_C_ref3StPicoEvt_err[i_CRK]->GetBinContent(i_mult) * Nevt, 2);
  //     }//mult loop ends
  //     //16082021(start)
  //     // for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
  //     //20082021(start)
  //     for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
  //     // for(int i_cent = 0 ; i_cent < NmultBin ; i_cent++){
  //       //20082021(finish)
  //       //16082021(finish)
  //       // cout<<"diagonal i_CRK & CentEvent[i_cent]: "<<i_CRK<<",\t"<<CentEvent[i_cent]<<endl;
  //       AvgCent[i_cent] /= CentEvent[i_cent];
  //       // cout<<Form("AvgCent[%d] = %f", i_cent, AvgCent[i_cent])<<endl;
  //       CumSumB[i_cent] /= CentEvent[i_cent];
  //       // CumErrSumB[i_cent] = ( TMath::Sqrt(CumErrSumB[i_cent]) / CentEvent[i_cent] );
  //       // cout<<"i_CRK = "<<i_CRK<<", i_cent = "<<i_cent<<",  CentEvent[i_cent] = "<<CentEvent[i_cent]<<",  CumErrSumB[i_cent] = "<<CumErrSumB[i_cent]<<endl;
  //       CumErrSumB[i_cent] = ( TMath::Sqrt(CumErrSumB[i_cent]) / CentEvent[i_cent] );
  //       // grB_C_ref3StPicoEvt[i_CRK]->SetPoint(i_cent, centrality_ref3StPicoEvt[i_cent], CumSumB[i_cent]);
  //       grB_C_ref3StPicoEvt[i_CRK]->SetPoint(i_cent, AvgCent[i_cent], CumSumB[i_cent]);
  //       grB_C_ref3StPicoEvt[i_CRK]->SetPointError(i_cent, 0, CumErrSumB[i_cent]);

  //     }//centrality bin loop ends

  //   }//CRK loop ends

  //   WriteRefMult3StPicoEvtCBWCcum(Form("output/cum/%s_eff_corr_RefMult3StPicoEvt_CBWC_cum.root", NameRuZr));
  
  // }

  // //////////////////////////////////////////

  // void Cum::WriteRefMult3StPicoEvtCBWCcum(const char* outFile){
  //   TFile *f_out = new TFile(outFile, "RECREATE");
  //   f_out->cd();
  //   // for(int i = 0 ; i < Ncums ; i++){
  //   for(int i = 0 ; i < N_CRK_noErr ; i++){
  //     grB_C_ref3StPicoEvt[i]->Write();
  //   }
  //   f_out->Close();

  // }

  // //////////////////////////////////////////

  // //======================================//

int Cum::getCent(int mult, const char* NameRuZr){
  // cout<<mult<<endl;
  // for(int i_cent = 0 ; i_cent < Ncent ; i_cent++) cout<<centrality[i_cent]<<endl;
  // int centrality[Ncent] = {35, 32, 27, 23, 19, 15, 12, 9, 7};

  if(string(NameRuZr) == "Ru") {
    // hybrid: Glauber up to 300 and data after that
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/mult_data_sim_hybrid_net_proton_epd.C
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/epd

    // redone in 20012022: // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/cendiv.py /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3/Ru_hybrid.root h1_ref3_hyb_ru   
    // redone in 20012022: // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/cendiv.py /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3/Zr_hybrid.root h1_ref3_hyb_zr   
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/cendiv.py /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3/Zr_hybrid.root h1_ref3_hyb_zr
    centrality[0] = 440;
    centrality[1] = 367;
    centrality[2] = 256;
    centrality[3] = 175;
    centrality[4] = 116;
    centrality[5] = 73;
    centrality[6] = 44;
    centrality[7] = 25;
    centrality[8] = 13;
  }
  if(string(NameRuZr) == "Zr") {
    // hybrid: Glauber up to 300 and data after that
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/mult_data_sim_hybrid_net_proton_epd.C
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/epd

    // redone in 20012022: // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/cendiv.py /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3/Ru_hybrid.root h1_ref3_hyb_ru   
    // redone in 20012022: // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/cendiv.py /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3/Zr_hybrid.root h1_ref3_hyb_zr   
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/cendiv.py /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3/Zr_hybrid.root h1_ref3_hyb_zr
    centrality[0] = 436;
    centrality[1] = 361;
    centrality[2] = 248;
    centrality[3] = 167;
    centrality[4] = 109;
    centrality[5] = 68;
    centrality[6] = 41;
    centrality[7] = 23;
    centrality[8] = 12;
  }

  //20082021(start)
  if(string(NameRuZr) == "RuZr") {
    // hybrid: Glauber up to 300 and data after that
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/mult_data_sim_hybrid_net_proton_epd.C
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/epd
    centrality[0] = 426;
    centrality[1] = 359;
    centrality[2] = 251;
    centrality[3] = 172;
    centrality[4] = 114;
    centrality[5] = 73;
    centrality[6] = 44;
    centrality[7] = 25;
    centrality[8] = 14;
  }
  //20082021(finish)
  //16082021(start)
  // if      (mult>=centrality[0] && mult<maxMult) return 0;    // 0-5%
  // else if (mult>=centrality[1] && mult<centrality[0]) return 1;   // 5-10%
  // else if (mult>=centrality[2] && mult<centrality[1]) return 2;    // 10-20%
  // else if (mult>=centrality[3] && mult<centrality[2]) return 3;    // 20-30%
  // else if (mult>=centrality[4] && mult<centrality[3]) return 4;    // 30-40%
  // else if (mult>=centrality[5] && mult<centrality[4]) return 5;    // 40-50%
  // else if (mult>=centrality[6] && mult<centrality[5]) return 6;    // 50-60%
  // else if (mult>=centrality[7] && mult<centrality[6]) return 7;    // 60-70%
  // else if (mult>=centrality[8] && mult<centrality[7]) return 8;    // 70-80%
  int topcentmerged[Ncent];
  for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
    topcentmerged[i_cent] = (i_cent < MergeTopCent) ? MergeTopCent : i_cent;
  }  
  if      ((mult>=centrality[0] && mult<maxMult)) return topcentmerged[0];    // 0-5%
  else if ((mult>=centrality[1] && mult<centrality[0])) return topcentmerged[1];   // 5-10%
  else if ((mult>=centrality[2] && mult<centrality[1])) return topcentmerged[2];    // 10-20%
  else if ((mult>=centrality[3] && mult<centrality[2])) return topcentmerged[3];    // 20-30%
  else if ((mult>=centrality[4] && mult<centrality[3])) return topcentmerged[4];    // 30-40%
  else if ((mult>=centrality[5] && mult<centrality[4])) return topcentmerged[5];    // 40-50%
  else if ((mult>=centrality[6] && mult<centrality[5])) return topcentmerged[6];    // 50-60%
  else if ((mult>=centrality[7] && mult<centrality[6])) return topcentmerged[7];    // 60-70%
  else if ((mult>=centrality[8] && mult<centrality[7])) return topcentmerged[8];    // 70-80%
  //16082021(finish)
  else    return -1;

}

int Cum::getRefMult3Cent(int mult, const char* NameRuZr){

  if(string(NameRuZr) == "Ru") {
    //07102021(start)
    // hybrid: Glauber up to 300 and data after that
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/mult_data_sim_hybrid_net_proton_ref3.C
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3

    // redone in 20012022: // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/cendiv.py /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3/Ru_hybrid.root h1_ref3_hyb_ru   
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/cendiv.py /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3/Zr_hybrid.root h1_ref3_hyb_zr
 
    // redone in 09022022:
    // hybrid: Glauber up to 150 and data after that
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/mult_data_sim_hybrid_net_proton_ref3.C
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3
    // data: /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/sys_unc/cum/output/net_proton/mean/lumi_dep/29012022/29012022_1/merged_hist_Ru_good123/Ru_hist_mergedFile_dca_10_nhitsfit_20_nsp_20_m2low_60_m2up_120_ptmax_20_ymax_5_detefflow_err_100_deteffhigh_err_100.root
    // sim: /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/output/ref3/Ru/03082021/03082021_1/Ratio_npp4.100_k4.000_x0.123_eff0.031.root
    //    ➜  draw ./cendiv.py output/hybrid/ref3/Ru_hybrid.root h1_ref3_hyb_ru

    centrality_ref3[0] = 439;
    centrality_ref3[1] = 367;
    centrality_ref3[2] = 258;
    centrality_ref3[3] = 178;
    centrality_ref3[4] = 118;
    centrality_ref3[5] = 75;
    centrality_ref3[6] = 45;
    centrality_ref3[7] = 26;
    centrality_ref3[8] = 14;

    //04122021(start)
    //to compare with narrow refmult3 cut
    //no glauber fit used. only data
    // //w/o refmult3 cut
    // centrality_ref3[0] = 462;
    // centrality_ref3[1] = 403;
    // centrality_ref3[2] = 308;
    // centrality_ref3[3] = 235;
    // centrality_ref3[4] = 177;
    // centrality_ref3[5] = 130;
    // centrality_ref3[6] = 93;
    // centrality_ref3[7] = 64;
    // centrality_ref3[8] = 41;
      
    //w/ refmult3 cut
    // centrality_ref3[0] = 448;
    // centrality_ref3[1] = 379;
    // centrality_ref3[2] = 273;
    // centrality_ref3[3] = 196;
    // centrality_ref3[4] = 140;
    // centrality_ref3[5] = 100;
    // centrality_ref3[6] = 71;
    // centrality_ref3[7] = 50;
    // centrality_ref3[8] = 33;
    //04122021(finish)
      
    // centrality_ref3[2] = 300;

    // //Let's use data centrality for 0~50%
    // //And use glauber to the rest 50~80%
    // //It is to take trigger efficiency into account for peripheral
    // //But the glauber fitting for now is not perfect
    // //especially the central classes
    // //original(glauber)
    // centrality_ref3[0] = 439;
    // centrality_ref3[1] = 367;
    // centrality_ref3[2] = 256;
    // centrality_ref3[3] = 176;
    // centrality_ref3[4] = 117;
    // centrality_ref3[5] = 74;
    // /*
    //   5% : 459
    //   10% : 400
    //   20% : 306
    //   30% : 233
    //   40% : 175
    //   50% : 129
    //   60% : 92
    //   70% : 63
    //   80% : 40
    // */

    // //data
    // // centrality_ref3[0] = 459;
    // // centrality_ref3[1] = 400;
    // // centrality_ref3[2] = 306;
    // // centrality_ref3[3] = 233;
    // // centrality_ref3[4] = 175;
    // // centrality_ref3[5] = 129;

    // //original(glauber)
    // centrality_ref3[6] = 45;
    // centrality_ref3[7] = 25;
    // centrality_ref3[8] = 13;

    // //not using...
    // Npart_mean_ref3[0] = 155.237;
    // Npart_mean_ref3[1] = 132.155;
    // Npart_mean_ref3[2] = 99.5216;
    // Npart_mean_ref3[3] = 73.9516;
    // Npart_mean_ref3[4] = 46.9137;
    // Npart_mean_ref3[5] = 27.4521;
    // Npart_mean_ref3[6] = 16.9075;//too low res?
    // Npart_mean_ref3[7] = 16.9075;//too low res?
    // Npart_mean_ref3[8] = 5.81803;

    //07102021(finish)
  }
  if(string(NameRuZr) == "Zr") {
    //07102021(start)
    // hybrid: Glauber up to 300 and data after that
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/mult_data_sim_hybrid_net_proton.C
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3

    // redone in 20012022: // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/cendiv.py /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3/Zr_hybrid.root h1_ref3_hyb_zr   
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/cendiv.py /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3/Zr_hybrid.root h1_ref3_hyb_zr

    // redone in 09022022:
    // hybrid: Glauber up to 150 and data after that
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/mult_data_sim_hybrid_net_proton_ref3.C
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3
    // data: /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/sys_unc/cum/output/net_proton/mean/lumi_dep/29012022/29012022_1/merged_hist_Zr_good123/Zr_hist_mergedFile_dca_10_nhitsfit_20_nsp_20_m2low_60_m2up_120_ptmax_20_ymax_5_detefflow_err_100_deteffhigh_err_100.root
    // sim: /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/output/ref3/Zr/03082021/03082021_1/Ratio_npp4.100_k4.000_x0.123_eff0.031.root
    //    ➜  draw ./cendiv.py output/hybrid/ref3/Zr_hybrid.root h1_ref3_hyb_zr

    centrality_ref3[0] = 435;
    centrality_ref3[1] = 363;
    centrality_ref3[2] = 252;
    centrality_ref3[3] = 171;
    centrality_ref3[4] = 113;
    centrality_ref3[5] = 71;
    centrality_ref3[6] = 42;
    centrality_ref3[7] = 24;
    centrality_ref3[8] = 13;

    //04122021(start)
    //to compare with narrow refmult3 cut
    //no glauber fit used. only data
    // //w/o refmult3 cut
    // centrality_ref3[0] = 460;
    // centrality_ref3[1] = 401;
    // centrality_ref3[2] = 306;
    // centrality_ref3[3] = 233;
    // centrality_ref3[4] = 174;
    // centrality_ref3[5] = 128;
    // centrality_ref3[6] = 92;
    // centrality_ref3[7] = 63;
    // centrality_ref3[8] = 40;
      
    //w/ refmult3 cut
    // centrality_ref3[0] = 446;
    // centrality_ref3[1] = 377;
    // centrality_ref3[2] = 270;
    // centrality_ref3[3] = 193;
    // centrality_ref3[4] = 138;
    // centrality_ref3[5] = 98;
    // centrality_ref3[6] = 70;
    // centrality_ref3[7] = 49;
    // centrality_ref3[8] = 32;
    //04122021(finish)

    // centrality_ref3[2] = 300;

    // //Let's use data centrality for 0~50%
    // //And use glauber to the rest 50~80%
    // //It is to take trigger efficiency into account for peripheral
    // //But the glauber fitting for now is not perfect
    // //especially the central classes
    // //original(glauber)
    // centrality_ref3[0] = 435;
    // centrality_ref3[1] = 363;
    // centrality_ref3[2] = 250;
    // centrality_ref3[3] = 169;
    // centrality_ref3[4] = 111;
    // centrality_ref3[5] = 69;
    // /*
    //   5% : 457
    //   10% : 398
    //   20% : 303
    //   30% : 230
    //   40% : 173
    //   50% : 127
    //   60% : 91
    //   70% : 62
    //   80% : 40
    // */

    // //data
    // // centrality_ref3[0] = 457;
    // // centrality_ref3[1] = 398;
    // // centrality_ref3[2] = 303;
    // // centrality_ref3[3] = 230;
    // // centrality_ref3[4] = 173;
    // // centrality_ref3[5] = 127;

    // //original(glauber)
    // centrality_ref3[6] = 42;
    // centrality_ref3[7] = 24;
    // centrality_ref3[8] = 13;

    // //not using...
    // Npart_mean_ref3[0] = 155.72;
    // Npart_mean_ref3[1] = 132.676;
    // Npart_mean_ref3[2] = 100.158;
    // Npart_mean_ref3[3] = 65.6358;
    // Npart_mean_ref3[4] = 47.2649;
    // Npart_mean_ref3[5] = 27.6657;
    // Npart_mean_ref3[6] = 16.9715;
    // Npart_mean_ref3[7] = 27.6657;
    // Npart_mean_ref3[8] = 5.78433;

    //07102021(finish)
  }
  if(string(NameRuZr) == "RuZr") {
    //07102021(start)
    // hybrid: Glauber up to 300 and data after that
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/mult_data_sim_hybrid_net_proton.C
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3


    // redone in 09022022:
    // hybrid: Glauber up to 150 and data after that
    // Just combined Ru and Zr in mult_data_sim_hybrid_net_proton_ref3.C
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/mult_data_sim_hybrid_net_proton_ref3.C
    // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3
    // data: /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/sys_unc/cum/output/net_proton/mean/lumi_dep/29012022/29012022_1/merged_hist_RuZr_good123/RuZr_hist_mergedFile_dca_10_nhitsfit_20_nsp_20_m2low_60_m2up_120_ptmax_20_ymax_5_detefflow_err_100_deteffhigh_err_100.root
    // sim: /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/output/ref3/RuZr/03082021/03082021_1/Ratio_npp4.100_k4.000_x0.123_eff0.031.root
    //    ➜  draw ./cendiv.py output/hybrid/ref3/RuZr_hybrid.root h1_ref3_hyb_ruzr
    centrality_ref3[0] = 437;
    centrality_ref3[1] = 365;
    centrality_ref3[2] = 255;
    centrality_ref3[3] = 174;
    centrality_ref3[4] = 115;
    centrality_ref3[5] = 73;
    centrality_ref3[6] = 44;
    centrality_ref3[7] = 25;
    centrality_ref3[8] = 13;

    // //Temporary values. Need updates
    // //As of 03092021, ref3 and ref3StPicoEvt (parasite) have the same cent cuts

    // //14092021(start)

    // // centrality_ref3[0] = 461;
    // // centrality_ref3[1] = 402;
    // // centrality_ref3[2] = 307;
    // // centrality_ref3[3] = 234;
    // // centrality_ref3[4] = 176;
    // // centrality_ref3[5] = 130;
    // // centrality_ref3[6] = 93;
    // // centrality_ref3[7] = 64;
    // // centrality_ref3[8] = 41;

    // //Ru(glauber)
    // // centrality_ref3[0] = 439;
    // // centrality_ref3[1] = 367;
    // // centrality_ref3[2] = 256;
    // // centrality_ref3[3] = 176;
    // // centrality_ref3[4] = 117;
    // // centrality_ref3[5] = 74;
    // // centrality_ref3[6] = 45;
    // // centrality_ref3[7] = 25;
    // // centrality_ref3[8] = 13;

    // //Zr(glauber)
    // // centrality_ref3[0] = 435;
    // // centrality_ref3[1] = 363;
    // // centrality_ref3[2] = 250;
    // // centrality_ref3[3] = 169;
    // // centrality_ref3[4] = 111;
    // // centrality_ref3[5] = 69;
    // // centrality_ref3[6] = 42;
    // // centrality_ref3[7] = 24;
    // // centrality_ref3[8] = 13;

    // //Ru+Zr gluaber combined from /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/output/ref3/Ru (Zr) /03082021/03082021_1/
    // //check /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/Ratio_with_Npart.C
    // // centrality_ref3[0] = 0.5*(439+435);
    // // centrality_ref3[1] = 0.5*(367+363);
    // // centrality_ref3[2] = 0.5*(256+250);
    // // centrality_ref3[3] = 0.5*(176+169);
    // // centrality_ref3[4] = 0.5*(117+111);
    // // centrality_ref3[5] = 0.5*(74+69);
    // // centrality_ref3[6] = 0.5*(45+42);
    // // centrality_ref3[7] = 0.5*(25+24);
    // // centrality_ref3[8] = 0.5*(13+13);
    // //from data Ru+Zr combined
    // centrality_ref3[0] = 461;
    // centrality_ref3[1] = 402;
    // centrality_ref3[2] = 307;
    // centrality_ref3[3] = 234;
    // centrality_ref3[4] = 176;
    // centrality_ref3[5] = 130;
    // centrality_ref3[6] = 93;
    // centrality_ref3[7] = 64;
    // centrality_ref3[8] = 41;
    // //14092021(finish)

    //07102021(finish)
  }
  //16082021(start)
  // if      (mult>=centrality_ref3[0] && mult<maxMult_ref3) return 0;    // 0-5%
  // else if (mult>=centrality_ref3[1] && mult<centrality_ref3[0]) return 1;   // 5-10%
  // else if (mult>=centrality_ref3[2] && mult<centrality_ref3[1]) return 2;    // 10-20%
  // else if (mult>=centrality_ref3[3] && mult<centrality_ref3[2]) return 3;    // 20-30%
  // else if (mult>=centrality_ref3[4] && mult<centrality_ref3[3]) return 4;    // 30-40%
  // else if (mult>=centrality_ref3[5] && mult<centrality_ref3[4]) return 5;    // 40-50%
  // else if (mult>=centrality_ref3[6] && mult<centrality_ref3[5]) return 6;    // 50-60%
  // else if (mult>=centrality_ref3[7] && mult<centrality_ref3[6]) return 7;    // 60-70%
  // else if (mult>=centrality_ref3[8] && mult<centrality_ref3[7]) return 8;    // 70-80%
  int topcentmerged[Ncent];
  for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
    topcentmerged[i_cent] = (i_cent < MergeTopCent) ? MergeTopCent : i_cent;
  }  
  if      (mult>=centrality_ref3[0] && mult<maxMult_ref3) return topcentmerged[0];    // 0-5%
  else if (mult>=centrality_ref3[1] && mult<centrality_ref3[0]) return topcentmerged[1];   // 5-10%
  else if (mult>=centrality_ref3[2] && mult<centrality_ref3[1]) return topcentmerged[2];    // 10-20%
  else if (mult>=centrality_ref3[3] && mult<centrality_ref3[2]) return topcentmerged[3];    // 20-30%
  else if (mult>=centrality_ref3[4] && mult<centrality_ref3[3]) return topcentmerged[4];    // 30-40%
  else if (mult>=centrality_ref3[5] && mult<centrality_ref3[4]) return topcentmerged[5];    // 40-50%
  else if (mult>=centrality_ref3[6] && mult<centrality_ref3[5]) return topcentmerged[6];    // 50-60%
  else if (mult>=centrality_ref3[7] && mult<centrality_ref3[6]) return topcentmerged[7];    // 60-70%
  else if (mult>=centrality_ref3[8] && mult<centrality_ref3[7]) return topcentmerged[8];    // 70-80%

  // if      (mult>=centrality_ref3[0] && mult<maxMult_ref3) return 4;    // 0-5%
  // else if (mult>=centrality_ref3[1] && mult<centrality_ref3[0]) return 4;   // 5-10%
  // else if (mult>=centrality_ref3[2] && mult<centrality_ref3[1]) return 4;    // 10-20%
  // else if (mult>=centrality_ref3[3] && mult<centrality_ref3[2]) return 4;    // 20-30%
  // else if (mult>=centrality_ref3[4] && mult<centrality_ref3[3]) return 4;    // 30-40%
  // else if (mult>=centrality_ref3[5] && mult<centrality_ref3[4]) return 6;    // 40-50%
  // else if (mult>=centrality_ref3[6] && mult<centrality_ref3[5]) return 6;    // 50-60%
  // else if (mult>=centrality_ref3[7] && mult<centrality_ref3[6]) return 8;    // 60-70%
  // else if (mult>=centrality_ref3[8] && mult<centrality_ref3[7]) return 8;    // 70-80%
  //16082021(finish)
  else    return -1;

}

  // int Cum::getRefMult3StPicoEvtCent(int mult, const char* NameRuZr){

  //   if(string(NameRuZr) == "Ru") {
  //     //Let's use data centrality for 0~50%
  //     //And use glauber to the rest 50~80%
  //     //It is to take trigger efficiency into account for peripheral
  //     //But the glauber fitting for now is not perfect
  //     //especially the central classes
  //     //original(glauber)
  //     centrality_ref3StPicoEvt[0] = 439;
  //     centrality_ref3StPicoEvt[1] = 367;
  //     centrality_ref3StPicoEvt[2] = 256;
  //     centrality_ref3StPicoEvt[3] = 176;
  //     centrality_ref3StPicoEvt[4] = 117;
  //     centrality_ref3StPicoEvt[5] = 74;
  //     /*
  //       5% : 459
  //       10% : 400
  //       20% : 306
  //       30% : 233
  //       40% : 175
  //       50% : 129
  //       60% : 92
  //       70% : 63
  //       80% : 40
  //     */

  //     //data
  //     // centrality_ref3StPicoEvt[0] = 459;
  //     // centrality_ref3StPicoEvt[1] = 400;
  //     // centrality_ref3StPicoEvt[2] = 306;
  //     // centrality_ref3StPicoEvt[3] = 233;
  //     // centrality_ref3StPicoEvt[4] = 175;
  //     // centrality_ref3StPicoEvt[5] = 129;

  //     //original(glauber)
  //     centrality_ref3StPicoEvt[6] = 45;
  //     centrality_ref3StPicoEvt[7] = 25;
  //     centrality_ref3StPicoEvt[8] = 13;

  //     //not using...
  //     Npart_mean_ref3StPicoEvt[0] = 155.237;
  //     Npart_mean_ref3StPicoEvt[1] = 132.155;
  //     Npart_mean_ref3StPicoEvt[2] = 99.5216;
  //     Npart_mean_ref3StPicoEvt[3] = 73.9516;
  //     Npart_mean_ref3StPicoEvt[4] = 46.9137;
  //     Npart_mean_ref3StPicoEvt[5] = 27.4521;
  //     Npart_mean_ref3StPicoEvt[6] = 16.9075;//too low res?
  //     Npart_mean_ref3StPicoEvt[7] = 16.9075;//too low res?
  //     Npart_mean_ref3StPicoEvt[8] = 5.81803;
  //   }
  //   if(string(NameRuZr) == "Zr") {
  //     //Let's use data centrality for 0~50%
  //     //And use glauber to the rest 50~80%
  //     //It is to take trigger efficiency into account for peripheral
  //     //But the glauber fitting for now is not perfect
  //     //especially the central classes
  //     //original(glauber)
  //     centrality_ref3StPicoEvt[0] = 435;
  //     centrality_ref3StPicoEvt[1] = 363;
  //     centrality_ref3StPicoEvt[2] = 250;
  //     centrality_ref3StPicoEvt[3] = 169;
  //     centrality_ref3StPicoEvt[4] = 111;
  //     centrality_ref3StPicoEvt[5] = 69;
  //     /*
  //       5% : 457
  //       10% : 398
  //       20% : 303
  //       30% : 230
  //       40% : 173
  //       50% : 127
  //       60% : 91
  //       70% : 62
  //       80% : 40
  //     */

  //     //data
  //     // centrality_ref3StPicoEvt[0] = 457;
  //     // centrality_ref3StPicoEvt[1] = 398;
  //     // centrality_ref3StPicoEvt[2] = 303;
  //     // centrality_ref3StPicoEvt[3] = 230;
  //     // centrality_ref3StPicoEvt[4] = 173;
  //     // centrality_ref3StPicoEvt[5] = 127;

  //     //original(glauber)
  //     centrality_ref3StPicoEvt[6] = 42;
  //     centrality_ref3StPicoEvt[7] = 24;
  //     centrality_ref3StPicoEvt[8] = 13;

  //     //not using...
  //     Npart_mean_ref3StPicoEvt[0] = 155.72;
  //     Npart_mean_ref3StPicoEvt[1] = 132.676;
  //     Npart_mean_ref3StPicoEvt[2] = 100.158;
  //     Npart_mean_ref3StPicoEvt[3] = 65.6358;
  //     Npart_mean_ref3StPicoEvt[4] = 47.2649;
  //     Npart_mean_ref3StPicoEvt[5] = 27.6657;
  //     Npart_mean_ref3StPicoEvt[6] = 16.9715;
  //     Npart_mean_ref3StPicoEvt[7] = 27.6657;
  //     Npart_mean_ref3StPicoEvt[8] = 5.78433;
  //   }
  //   if(string(NameRuZr) == "RuZr") {
  //     //Temporary values. Need updates
  //     // centrality_ref3StPicoEvt[0] = (int)(439+435)/2;
  //     // centrality_ref3StPicoEvt[1] = (int)(367+363)/2;
  //     // centrality_ref3StPicoEvt[2] = (int)(256+250)/2;
  //     // centrality_ref3StPicoEvt[3] = (int)(176+169)/2;
  //     // centrality_ref3StPicoEvt[4] = (int)(117+111)/2;
  //     // centrality_ref3StPicoEvt[5] = (int)(74+69)/2;
  //     // centrality_ref3StPicoEvt[6] = (int)(45+42)/2;
  //     // centrality_ref3StPicoEvt[7] = (int)(25+24)/2;
  //     // centrality_ref3StPicoEvt[8] = (int)(1]3+13)/2;

  //     //From merged RefMult3StPicoEvt using Cendiv with 690 as maxmult.
  //     //Same as RefMult with intime-pileup removed. (/Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/cum/effcorr/output/net_proton/mean/20082021/20082021_1/RefMult3StPicoEvt_intimePileup_removed_RuZr.root)
  //     centrality_ref3StPicoEvt[0] = 458;
  //     centrality_ref3StPicoEvt[1] = 399;
  //     centrality_ref3StPicoEvt[2] = 305;
  //     centrality_ref3StPicoEvt[3] = 232;
  //     centrality_ref3StPicoEvt[4] = 174;
  //     centrality_ref3StPicoEvt[5] = 128;
  //     centrality_ref3StPicoEvt[6] = 92;
  //     centrality_ref3StPicoEvt[7] = 63;
  //     centrality_ref3StPicoEvt[8] = 40;
  //   }
  //   //16082021(start)
  //   // if      (mult>=centrality_ref3StPicoEvt[0] && mult<maxMult_ref3StPicoEvt) return 0;    // 0-5%
  //   // else if (mult>=centrality_ref3StPicoEvt[1] && mult<centrality_ref3StPicoEvt[0]) return 1;   // 5-10%
  //   // else if (mult>=centrality_ref3StPicoEvt[2] && mult<centrality_ref3StPicoEvt[1]) return 2;    // 10-20%
  //   // else if (mult>=centrality_ref3StPicoEvt[3] && mult<centrality_ref3StPicoEvt[2]) return 3;    // 20-30%
  //   // else if (mult>=centrality_ref3StPicoEvt[4] && mult<centrality_ref3StPicoEvt[3]) return 4;    // 30-40%
  //   // else if (mult>=centrality_ref3StPicoEvt[5] && mult<centrality_ref3StPicoEvt[4]) return 5;    // 40-50%
  //   // else if (mult>=centrality_ref3StPicoEvt[6] && mult<centrality_ref3StPicoEvt[5]) return 6;    // 50-60%
  //   // else if (mult>=centrality_ref3StPicoEvt[7] && mult<centrality_ref3StPicoEvt[6]) return 7;    // 60-70%
  //   // else if (mult>=centrality_ref3StPicoEvt[8] && mult<centrality_ref3StPicoEvt[7]) return 8;    // 70-80%
  //   int topcentmerged[Ncent];
  //   for(int i_cent = 0 ; i_cent < Ncent ; i_cent++){
  //     topcentmerged[i_cent] = (i_cent < MergeTopCent) ? MergeTopCent : i_cent;
  //   }  
  //   if      (mult>=centrality_ref3StPicoEvt[0] && mult<maxMult_ref3StPicoEvt) return topcentmerged[0];    // 0-5%
  //   else if (mult>=centrality_ref3StPicoEvt[1] && mult<centrality_ref3StPicoEvt[0]) return topcentmerged[1];   // 5-10%
  //   else if (mult>=centrality_ref3StPicoEvt[2] && mult<centrality_ref3StPicoEvt[1]) return topcentmerged[2];    // 10-20%
  //   else if (mult>=centrality_ref3StPicoEvt[3] && mult<centrality_ref3StPicoEvt[2]) return topcentmerged[3];    // 20-30%
  //   else if (mult>=centrality_ref3StPicoEvt[4] && mult<centrality_ref3StPicoEvt[3]) return topcentmerged[4];    // 30-40%
  //   else if (mult>=centrality_ref3StPicoEvt[5] && mult<centrality_ref3StPicoEvt[4]) return topcentmerged[5];    // 40-50%
  //   else if (mult>=centrality_ref3StPicoEvt[6] && mult<centrality_ref3StPicoEvt[5]) return topcentmerged[6];    // 50-60%
  //   else if (mult>=centrality_ref3StPicoEvt[7] && mult<centrality_ref3StPicoEvt[6]) return topcentmerged[7];    // 60-70%
  //   else if (mult>=centrality_ref3StPicoEvt[8] && mult<centrality_ref3StPicoEvt[7]) return topcentmerged[8];    // 70-80%
  //   //16082021(finish)
  //   else    return -1;

  // }

