#include <iostream>

#include "Cum.h"

#include "TFile.h"

using namespace std;

void calcum(const char* NameRuZr, int dca_cut, int nhitsfit_cut, int nsp_cut, int m2low_cut, int m2up_cut, int ptmax_cut, int ymax_cut, int detefflow_err, int deteffhigh_err, int MergeTopCent = 0){
  int maxMult = 700;
  int maxMult_ref3 = 700;
  int maxMult_ref3StPicoEvt = 700;
  Cum *cum = new Cum(maxMult, maxMult_ref3, maxMult_ref3StPicoEvt, NameRuZr, MergeTopCent);

  if(string(NameRuZr) != "RuZr"){
    //Currently using for ref3StPicoEvt vz&lumi-uncorrected
    cum->GetEPDmultMeans(Form("/Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/sys_unc/cum/output/net_proton/mean/lumi_dep/29012022/29012022_1/merged_mTerms_f_%s_good123/%s_mTerms_f_mergedFile_dca_%d_nhitsfit_%d_nsp_%d_m2low_%d_m2up_%d_ptmax_%d_ymax_%d_detefflow_err_%d_deteffhigh_err_%d.root", NameRuZr, NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, detefflow_err, deteffhigh_err));
    cum->CalculateEffCorrEPDmultCum(NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, detefflow_err, deteffhigh_err);
    cum->CalculateEPDmultCBWCcum(NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, detefflow_err, deteffhigh_err);

    //ref3StPicoEvt vz&lumi-corrected
    cum->GetRefMult3Means(Form("/Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/sys_unc/cum/output/net_proton/mean/lumi_dep/29012022/29012022_1/corr/merged_mTerms_f_%s_good123/%s_mTerms_f_mergedFile_dca_%d_nhitsfit_%d_nsp_%d_m2low_%d_m2up_%d_ptmax_%d_ymax_%d_detefflow_err_%d_deteffhigh_err_%d.root", NameRuZr, NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, detefflow_err, deteffhigh_err));
    cum->CalculateEffCorrRefMult3Cum(NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, detefflow_err, deteffhigh_err);
    cum->CalculateRefMult3CBWCcum(NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, detefflow_err, deteffhigh_err);
  }

  else if(string(NameRuZr) == "RuZr"){

    //ref3StPicoEvt vz&lumi-corrected
    cum->CBWC_RuZr(Form("output/cum/%s_effcorr_ref3corr_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_ptmax_%d_ymax_%d_detefflow_%d_deteffhigh_%d_maxmult_%d_29012022_1_%d.root", "Ru", dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, detefflow_err, deteffhigh_err, maxMult_ref3StPicoEvt, MergeTopCent), Form("output/cum/%s_effcorr_ref3corr_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_ptmax_%d_ymax_%d_detefflow_%d_deteffhigh_%d_maxmult_%d_29012022_1_%d.root", "Zr", dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, detefflow_err, deteffhigh_err, maxMult_ref3StPicoEvt, MergeTopCent), Form("output/cum/%s_effcorr_ref3corr_CBWC_cum_dca_%d_nhf_%d_nsp_%d_m2l_%d_m2u_%d_ptmax_%d_ymax_%d_detefflow_%d_deteffhigh_%d_maxmult_%d_29012022_1_%d.root", NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut, detefflow_err, deteffhigh_err, maxMult_ref3StPicoEvt, MergeTopCent));
  }

  delete cum;

  return;
}

int main(int argc, char** argv){

  int dca_cuts[5] = {8, 9, 10, 11, 12};
  int nhitsfit_cuts[5] = {15, 18, 20, 22, 25};
  int nsp_cuts[5] = {16, 18, 20, 22, 25};
  int m2low_cuts[5] = {50, 50, 60, 65, 70};
  int m2up_cuts[5] = {110, 115, 120, 125, 130};

  int ptmax_cuts[2] = {10, 20};
  int ymax_cuts[2] = {1, 5};

  int detefflow_err[3] = {95, 100, 105};
  int deteffhigh_err[3] = {95, 100, 105};
  

  calcum("Ru", dca_cuts[2], nhitsfit_cuts[2], nsp_cuts[2], m2low_cuts[2], m2up_cuts[2], ptmax_cuts[1], ymax_cuts[1], detefflow_err[1], deteffhigh_err[1], 4);
  calcum("Zr", dca_cuts[2], nhitsfit_cuts[2], nsp_cuts[2], m2low_cuts[2], m2up_cuts[2], ptmax_cuts[1], ymax_cuts[1], detefflow_err[1], deteffhigh_err[1], 4);

  // calcum("RuZr", dca_cuts[2], nhitsfit_cuts[2], nsp_cuts[2], m2low_cuts[2], m2up_cuts[2], ptmax_cuts[1], ymax_cuts[1], detefflow_err[1], deteffhigh_err[1], 0);
  // calcum("RuZr", dca_cuts[2], nhitsfit_cuts[2], nsp_cuts[2], m2low_cuts[2], m2up_cuts[2], ptmax_cuts[1], ymax_cuts[1], detefflow_err[1], deteffhigh_err[1], 1);
  // calcum("RuZr", dca_cuts[2], nhitsfit_cuts[2], nsp_cuts[2], m2low_cuts[2], m2up_cuts[2], ptmax_cuts[1], ymax_cuts[1], detefflow_err[1], deteffhigh_err[1], 2);
  // calcum("RuZr", dca_cuts[2], nhitsfit_cuts[2], nsp_cuts[2], m2low_cuts[2], m2up_cuts[2], ptmax_cuts[1], ymax_cuts[1], detefflow_err[1], deteffhigh_err[1], 3);
  calcum("RuZr", dca_cuts[2], nhitsfit_cuts[2], nsp_cuts[2], m2low_cuts[2], m2up_cuts[2], ptmax_cuts[1], ymax_cuts[1], detefflow_err[1], deteffhigh_err[1], 4);
  
  return 0;
}
