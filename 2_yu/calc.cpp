#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include "TROOT.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TTree.h"
#include <algorithm>
#include "TGraphErrors.h"
#include "ECorr.h"
#include <cmath>

using namespace std;

const int _nCent = 9;
//21012022(start)
// static const int MergeTopCent = 0;//0~5%, 5~10%, 10~20%, 20~30%, 30~40%, 40~50%, 50~60%, 60~70%, 70~80% 
int MergeTopCent = 0;//0~5%, 5~10%, 10~20%, 20~30%, 30~40%, 40~50%, 50~60%, 60~70%, 70~80% 
//21012022(finish)
//from to central to peripheral 
int centrality[_nCent];
int npart[_nCent];
//21012022(start)
// int centrality_ru[_nCent] = {441, 368, 258, 177, 118, 75, 45, 26, 14};//Ru
// int centrality_zr[_nCent] = {436, 362, 250, 169, 111, 70, 42, 24, 13};//Zr
// int npart_ru[_nCent]      = {167, 145, 115, 83, 59, 40, 26, 16, 10};//Ru
// int npart_zr[_nCent]      = {166, 144, 113, 81, 57, 38, 25, 15, 9};//Zr
// redone in 20012022: // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/cendiv.py /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3/Ru_hybrid.root h1_ref3_hyb_ru   
// redone in 20012022: // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/cendiv.py /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3/Zr_hybrid.root h1_ref3_hyb_zr   
  // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/cendiv.py /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3/Zr_hybrid.root h1_ref3_hyb_zr

//redone in 09022022:
  // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/mult_data_sim_hybrid_net_proton_ref3.C
  // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/output/hybrid/ref3
  // ./cendiv.py output/hybrid/ref3/RuZr_hybrid.root h1_ref3_hyb_ruzr
 // data: /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/sys_unc/cum/output/net_proton/mean/lumi_dep/29012022/29012022_1/merged_hist_Ru_good123/Ru_hist_mergedFile_dca_10_nhitsfit_20_nsp_20_m2low_60_m2up_120_ptmax_20_ymax_5_detefflow_err_100_deteffhigh_err_100.root
// sim: /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/output/ref3/Ru/03082021/03082021_1/Ratio_npp4.100_k4.000_x0.123_eff0.031.root

int centrality_ru[_nCent] = {439, 367, 258, 178, 118, 75, 45, 26, 14};//Ru
int centrality_zr[_nCent] = {435, 363, 252, 171, 113, 71, 42, 24, 13};//Zr
int centrality_ruzr[_nCent] = {437, 365, 255, 174, 115, 73, 44, 25, 13};//RuZr   // Just combined Ru and Zr in mult_data_sim_hybrid_net_proton_ref3.C

  // /Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/GlauberFit/draw/Ratio_with_Npart_ref3_temp.C
int npart_ru[_nCent]      = {(int) 167.128, (int) 145.048, (int) 115.214, (int) 83.7428, (int) 59.2716, (int) 40.3641, (int) 26.2462, (int) 16.2812, (int) 9.49322};//Ru
int npart_zr[_nCent]      = {(int) 166.177, (int) 144.168, (int) 114.057, (int) 82.0014, (int) 57.4968, (int) 38.8873, (int) 24.9411, (int) 15.2306, (int) 8.84488};//Zr
int npart_ruzr[_nCent]      = {(int) 166.656, (int) 144.612, (int) 114.635, (int) 82.7754, (int) 58.212, (int) 39.5512, (int) 25.7144, (int) 15.8489, (int) 9.03226};//RuZr
//21012022(finish)
const int MaxMult = 700;
const int LowEventCut = 1;

int getCent(int mult, const char* NameRuZr){

  // int centrality[9];
  if(string(NameRuZr) == "Ru") {
    for(int i_cent = 0 ; i_cent <_nCent ; i_cent++) {
      centrality[i_cent] = centrality_ru[i_cent];
      npart[i_cent] = npart_ru[i_cent];
    }
  }
  if(string(NameRuZr) == "Zr") {
    for(int i_cent = 0 ; i_cent <_nCent ; i_cent++) {
      centrality[i_cent] = centrality_zr[i_cent];
      npart[i_cent] = npart_zr[i_cent];
    }
  }
  if(string(NameRuZr) == "RuZr") {
    for(int i_cent = 0 ; i_cent <_nCent ; i_cent++) {
      centrality[i_cent] = centrality_ruzr[i_cent];
      npart[i_cent] = npart_ruzr[i_cent];
    }
  }

  int topcentmerged[_nCent];
  for(int i_cent = 0 ; i_cent < _nCent ; i_cent++){
     topcentmerged[i_cent] = (i_cent < MergeTopCent) ? MergeTopCent : i_cent;
  }
  
  if(mult >= centrality[0])      return topcentmerged[0];
  else if(mult >= centrality[1]) return topcentmerged[1];
  else if(mult >= centrality[2]) return topcentmerged[2];
  else if(mult >= centrality[3]) return topcentmerged[3];
  else if(mult >= centrality[4]) return topcentmerged[4];
  else if(mult >= centrality[5]) return topcentmerged[5];
  else if(mult >= centrality[6]) return topcentmerged[6];
  else if(mult >= centrality[7]) return topcentmerged[7];
  else if(mult >= centrality[8]) return topcentmerged[8];
  else return -1;
}

void ConvertTerms(const char* NameRuZr, int dca_cuts, int nhitsfit_cuts, int nsp_cuts, int m2low_cuts, int m2up_cuts, int ptmax_cuts, int ymax_cuts, int detefflow_err, int deteffhigh_err){

  CumECorr *proton_ec= new CumECorr("ProCorr", MaxMult, LowEventCut);
  // CumECorr *proton_ec= new CumECorr("Pro", MaxMult, LowEventCut);
  proton_ec->Init();

  proton_ec->ReadTerms(Form("/Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/sys_unc/cum/output/net_proton/mean/light/27012022/27012022_1/corr/merged_mTerms_f_%s_good123/%s_mTerms_f_mergedFile_dca_%d_nhitsfit_%d_nsp_%d_m2low_%d_m2up_%d_ptmax_%d_ymax_%d_detefflow_err_%d_deteffhigh_err_%d.root", NameRuZr, NameRuZr, dca_cuts, nhitsfit_cuts, nsp_cuts, m2low_cuts, m2up_cuts, ptmax_cuts, ymax_cuts, detefflow_err, deteffhigh_err));//ref3Cut (Ref3St vs nBTOFmatch & Ref3St vs beta) ref3corr
  // proton_ec->ReadTerms(Form("/Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/sys_unc/cum/output/net_proton/mean/light/27012022/27012022_1/merged_mTerms_f_%s_good123/%s_mTerms_f_mergedFile_dca_10_nhitsfit_20_nsp_20_m2low_60_m2up_120_ptmax_20_ymax_5_detefflow_err_6000_deteffhigh_err_14000.root", NameRuZr, NameRuZr));//ref3Cut (Ref3St vs nBTOFmatch & Ref3St vs beta) ref3 uncorr

  proton_ec->Calculate();
  proton_ec->Update(Form("output/cum/noCbwc_f%s_corr_dca_%d_nhitsfit_%d_nsp_%d_m2low_%d_m2up_%d_ptmax_%d_ymax_%d_detefflow_err_%d_deteffhigh_err_%d_maxmult_%d_27012022_1.root",NameRuZr, dca_cuts, nhitsfit_cuts, nsp_cuts, m2low_cuts, m2up_cuts, ptmax_cuts, ymax_cuts, detefflow_err, deteffhigh_err, MaxMult));
  TFile* inf = TFile::Open(Form("output/cum/noCbwc_f%s_corr_dca_%d_nhitsfit_%d_nsp_%d_m2low_%d_m2up_%d_ptmax_%d_ymax_%d_detefflow_err_%d_deteffhigh_err_%d_maxmult_%d_27012022_1.root",NameRuZr, dca_cuts, nhitsfit_cuts, nsp_cuts, m2low_cuts, m2up_cuts, ptmax_cuts, ymax_cuts, detefflow_err, deteffhigh_err, MaxMult));
    // proton_ec->Update(Form("output/cum/noCbwc_f%s_27012022_1.root",NameRuZr));
    // TFile* inf = TFile::Open(Form("output/cum/noCbwc_f%s_27012022_1.root",NameRuZr));

  const int _nCums=23;
  TGraphErrors *Cums[_nCums];
  TGraphErrors *nEvents[_nCums];
  TH1D *sCumulants[_nCums];
  const char *cnames[_nCums]={"C1","C2","C3","C4","C5","C6",
    "R21","R31","R32","R42","R51","R62",
    "k1","k2","k3","k4","k5","k6",
    "k21","k31","k41","k51","k61"
  };
  const char *labels[_nCums]={"C_{1}","C_{2}","C_{3}","C_{4}","C_{5}","C_{6}",
    "C_{2}/C_{1}","C_{3}/C_{1}","C_{3}/C_{2}","C_{4}/C_{2}","C_{5}/C_{1}","C_{6}/C_{2}",
    "#kappa_{1}","#kappa_{2}","#kappa_{3}","#kappa_{4}","#kappa_{5}","#kappa_{6}",
    "#kappa_{2}/#kappa_{1}","#kappa_{3}/#kappa_{1}","#kappa_{4}/#kappa_{1}","#kappa_{5}/#kappa_{1}","#kappa_{6}/#kappa_{1}"
  };
  for(int i=0; i<_nCums; ++i){
    sCumulants[i] = (TH1D*)inf->Get(Form("%s%s", "ProCorr", cnames[i]));
    // sCumulants[i] = (TH1D*)inf->Get(Form("%s%s", "Pro", cnames[i]));
    Cums[i] = new TGraphErrors(_nCent);
    Cums[i]->SetName(Form("%s_%s", "Pro",cnames[i]));
    Cums[i]->SetTitle(labels[i]);
    Cums[i]->SetMarkerStyle(20);
    nEvents[i] = new TGraphErrors(_nCent);
    nEvents[i]->SetName(Form("%s_%s", "nEvt",cnames[i]));
    nEvents[i]->SetTitle(labels[i]);
    nEvents[i]->SetMarkerStyle(20);
  }

  const int LowMultCut  = 1;
  double CentEvent[_nCent]={0};
  double AvgCent[_nCent] = {0.};
  double vSum[_nCent];
  double eSum[_nCent];

  TH1D *hEntries = (TH1D*) inf->Get(Form("%shEntries", "ProCorr"));
  // TH1D *hEntries = (TH1D*) inf->Get(Form("%shEntries", "Pro"));
  for(int iCum=0; iCum<_nCums; ++iCum){

    int  nEvent =  0;
    for(int iCent=0; iCent<_nCent; ++iCent) {
      vSum[iCent] = 0;
      eSum[iCent] = 0;
      CentEvent[iCent] = 0;
      AvgCent[iCent] = 0;
    }

    for(int iMult = LowMultCut; iMult <= MaxMult; ++iMult){
      nEvent = hEntries->GetBinContent(iMult);

      int CurrCent = getCent(iMult, NameRuZr);
      if(CurrCent == -1) continue;

      // if(isnan(CurrCent) || isnan(nEvent) || isnan(sCumulants[iCum]->GetBinContent(iMult)) || isnan(sCumulants[iCum]->GetBinError(iMult)) )cout<<CurrCent<<", "<<nEvent<<", "<<sCumulants[iCum]->GetBinContent(iMult)<<", "<<sCumulants[iCum]->GetBinError(iMult)<<endl;
      
      vSum[CurrCent] += (sCumulants[iCum]->GetBinContent(iMult) * nEvent);
      eSum[CurrCent] += pow((sCumulants[iCum]->GetBinError(iMult)*nEvent),2);
      CentEvent[CurrCent] += nEvent;
      AvgCent[CurrCent] += iMult*nEvent;
    }
    for(int iCent=0; iCent<_nCent; ++iCent){
      if(CentEvent[iCent] != 0) {//cout<<"CentEvent["<<iCent<<"] = "<<CentEvent[iCent]<<endl;
	vSum[iCent] /= CentEvent[iCent];
	eSum[iCent] = sqrt(eSum[iCent]) / CentEvent[iCent];
	AvgCent[iCent] /= CentEvent[iCent];
	//20012022(start)
	// Cums[iCum]->SetPoint(iCent, npart[iCent], vSum[iCent]);
	//30012022(start)
	// Cums[iCum]->SetPoint(iCent, centrality[iCent], vSum[iCent]);
	// //20012022(finish)
	// Cums[iCum]->SetPointError(iCent, 0, eSum[iCent]);
	Cums[iCum]->SetPoint(iCent, AvgCent[iCent], vSum[iCent]);
	Cums[iCum]->SetPointError(iCent, 0, eSum[iCent]);
	//30012022(finish)
	nEvents[iCum]->SetPoint(iCent, AvgCent[iCent], CentEvent[iCent]);
      }
    }
  }
  TFile *out = new TFile(Form("output/cum/cbwc_f%s_corr_dca_%d_nhitsfit_%d_nsp_%d_m2low_%d_m2up_%d_ptmax_%d_ymax_%d_detefflow_err_%d_deteffhigh_err_%d_maxmult_%d_27012022_1_%d.root",NameRuZr, dca_cuts, nhitsfit_cuts, nsp_cuts, m2low_cuts, m2up_cuts, ptmax_cuts, ymax_cuts, detefflow_err, deteffhigh_err, MaxMult, MergeTopCent), "update");
  // TFile *out = new TFile(Form("output/cum/cbwc_f%s_27012022_1_%d.root",NameRuZr,MergeTopCent), "update");

  out->cd();
  for(int i=0;i<_nCums;++i) {
    Cums[i]->Write();
nEvents[i]->Write();
  }
  out->Close();


  delete gROOT->FindObject("ProCorrhEntries");
  for(int i = 0 ; i < _nCums ; i++){
    delete gROOT->FindObject(Form("ProCorr%s", cnames[i]));
  }
  delete gROOT->FindObject("ProCorrk32");
  delete gROOT->FindObject("ProCorrk43");
  delete gROOT->FindObject("ProCorrk54");
  delete gROOT->FindObject("ProCorrk65");

  // delete proton_ec;
  // delete inf;
  // // delete Cums;
  // // delete sCumulants;
  // // delete cnames;
  // // delete labels;
  // delete hEntries;
  // delete out;
    
  
  return;
}

void ConvertTerms_RuZr(const char* NameRuZr, int dca_cuts, int nhitsfit_cuts, int nsp_cuts, int m2low_cuts, int m2up_cuts, int ptmax_cuts, int ymax_cuts, int detefflow_err, int deteffhigh_err){

  CumECorr *proton_ec_Ru= new CumECorr("ProCorr", MaxMult, LowEventCut);
  // CumECorr *proton_ec_Ru= new CumECorr("Pro", MaxMult, LowEventCut);
  //14022022(start)
  // proton_ec_Ru->Init();
  proton_ec_Ru->Init("_Ru");
  //14022022(finish)

  proton_ec_Ru->ReadTerms(Form("/Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/sys_unc/cum/output/net_proton/mean/light/27012022/27012022_1/corr/merged_mTerms_f_%s_good123/%s_mTerms_f_mergedFile_dca_%d_nhitsfit_%d_nsp_%d_m2low_%d_m2up_%d_ptmax_%d_ymax_%d_detefflow_err_%d_deteffhigh_err_%d.root", "Ru", "Ru", dca_cuts, nhitsfit_cuts, nsp_cuts, m2low_cuts, m2up_cuts, ptmax_cuts, ymax_cuts, detefflow_err, deteffhigh_err));//ref3Cut (Ref3St vs nBTOFmatch & Ref3St vs beta) ref3corr
  // proton_ec_Ru->ReadTerms(Form("/Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/sys_unc/cum/output/net_proton/mean/light/27012022/27012022_1/merged_mTerms_f_%s_good123/%s_mTerms_f_mergedFile_dca_10_nhitsfit_20_nsp_20_m2low_60_m2up_120_ptmax_20_ymax_5_detefflow_err_6000_deteffhigh_err_14000.root", "Ru", "Ru"));//ref3Cut (Ref3St vs nBTOFmatch & Ref3St vs beta) ref3 uncorr

  proton_ec_Ru->Calculate();
  proton_ec_Ru->Update(Form("output/cum/noCbwc_f%s_corr_dca_%d_nhitsfit_%d_nsp_%d_m2low_%d_m2up_%d_ptmax_%d_ymax_%d_detefflow_err_%d_deteffhigh_err_%d_maxmult_%d_27012022_1.root", "Ru", dca_cuts, nhitsfit_cuts, nsp_cuts, m2low_cuts, m2up_cuts, ptmax_cuts, ymax_cuts, detefflow_err, deteffhigh_err, MaxMult));
  TFile* inf_Ru = TFile::Open(Form("output/cum/noCbwc_f%s_corr_dca_%d_nhitsfit_%d_nsp_%d_m2low_%d_m2up_%d_ptmax_%d_ymax_%d_detefflow_err_%d_deteffhigh_err_%d_maxmult_%d_27012022_1.root", "Ru", dca_cuts, nhitsfit_cuts, nsp_cuts, m2low_cuts, m2up_cuts, ptmax_cuts, ymax_cuts, detefflow_err, deteffhigh_err, MaxMult));
  // proton_ec_Ru->Update(Form("output/cum/noCbwc_f%s_27012022_1.root","Ru"));
  // TFile* inf_Ru = TFile::Open(Form("output/cum/noCbwc_f%s_27012022_1.root","Ru"));

  delete proton_ec_Ru;
  cout<<"hey 2"<<endl;

  CumECorr *proton_ec_Zr= new CumECorr("ProCorr", MaxMult, LowEventCut);
  cout<<"hey 3"<<endl;
  // CumECorr *proton_ec_Zr= new CumECorr("Pro", MaxMult, LowEventCut);
  //14022022(start)
  // proton_ec_Zr->Init();
  proton_ec_Zr->Init("_Zr");
  cout<<"hey 4"<<endl;
  //14022022(finish)
  proton_ec_Zr->ReadTerms(Form("/Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/sys_unc/cum/output/net_proton/mean/light/27012022/27012022_1/corr/merged_mTerms_f_%s_good123/%s_mTerms_f_mergedFile_dca_%d_nhitsfit_%d_nsp_%d_m2low_%d_m2up_%d_ptmax_%d_ymax_%d_detefflow_err_%d_deteffhigh_err_%d.root", "Zr", "Zr", dca_cuts, nhitsfit_cuts, nsp_cuts, m2low_cuts, m2up_cuts, ptmax_cuts, ymax_cuts, detefflow_err, deteffhigh_err));//ref3Cut (Ref3St vs nBTOFmatch & Ref3St vs beta) ref3corr
  // proton_ec_Zr->ReadTerms(Form("/Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/sys_unc/cum/output/net_proton/mean/light/27012022/27012022_1/merged_mTerms_f_%s_good123/%s_mTerms_f_mergedFile_dca_10_nhitsfit_20_nsp_20_m2low_60_m2up_120_ptmax_20_ymax_5_detefflow_err_6000_deteffhigh_err_14000.root", "Zr", "Zr"));//ref3Cut (Ref3St vs nBTOFmatch & Ref3St vs beta) ref3 uncorr
  proton_ec_Zr->Calculate();
  cout<<"hey 5"<<endl;
  proton_ec_Zr->Update(Form("output/cum/noCbwc_f%s_corr_dca_%d_nhitsfit_%d_nsp_%d_m2low_%d_m2up_%d_ptmax_%d_ymax_%d_detefflow_err_%d_deteffhigh_err_%d_maxmult_%d_27012022_1.root", "Zr", dca_cuts, nhitsfit_cuts, nsp_cuts, m2low_cuts, m2up_cuts, ptmax_cuts, ymax_cuts, detefflow_err, deteffhigh_err, MaxMult));
  cout<<"hey 6"<<endl;
  TFile* inf_Zr = TFile::Open(Form("output/cum/noCbwc_f%s_corr_dca_%d_nhitsfit_%d_nsp_%d_m2low_%d_m2up_%d_ptmax_%d_ymax_%d_detefflow_err_%d_deteffhigh_err_%d_maxmult_%d_27012022_1.root", "Zr", dca_cuts, nhitsfit_cuts, nsp_cuts, m2low_cuts, m2up_cuts, ptmax_cuts, ymax_cuts, detefflow_err, deteffhigh_err, MaxMult));
  // proton_ec_Zr->Update(Form("output/cum/noCbwc_f%s_27012022_1.root","Zr"));
  // TFile* inf_Zr = TFile::Open(Form("output/cum/noCbwc_f%s_27012022_1.root","Zr"));

  delete proton_ec_Zr;
  cout<<"hey 7"<<endl;

  const int _nCums=23;
  TGraphErrors *Cums[_nCums];
  TGraphErrors *nEvents[_nCums];
  TH1D *sCumulants_Ru[_nCums];
  TH1D *sCumulants_Zr[_nCums];
  const char *cnames[_nCums]={"C1","C2","C3","C4","C5","C6",
    "R21","R31","R32","R42","R51","R62",
    "k1","k2","k3","k4","k5","k6",
    "k21","k31","k41","k51","k61"
  };
  const char *labels[_nCums]={"C_{1}","C_{2}","C_{3}","C_{4}","C_{5}","C_{6}",
    "C_{2}/C_{1}","C_{3}/C_{1}","C_{3}/C_{2}","C_{4}/C_{2}","C_{5}/C_{1}","C_{6}/C_{2}",
    "#kappa_{1}","#kappa_{2}","#kappa_{3}","#kappa_{4}","#kappa_{5}","#kappa_{6}",
    "#kappa_{2}/#kappa_{1}","#kappa_{3}/#kappa_{1}","#kappa_{4}/#kappa_{1}","#kappa_{5}/#kappa_{1}","#kappa_{6}/#kappa_{1}"
  };
  for(int i=0; i<_nCums; ++i){
    //14042022(start)
    // sCumulants_Ru[i] = (TH1D*)inf_Ru->Get(Form("%s%s", "ProCorr", cnames[i]));
    // // sCumulants_Ru[i] = (TH1D*)inf_Ru->Get(Form("%s%s", "Pro", cnames[i]));
    // sCumulants_Zr[i] = (TH1D*)inf_Zr->Get(Form("%s%s", "ProCorr", cnames[i]));
    // // sCumulants_Zr[i] = (TH1D*)inf_Zr->Get(Form("%s%s", "Pro", cnames[i]));
    sCumulants_Ru[i] = (TH1D*)inf_Ru->Get(Form("%s%s%s", "ProCorr", cnames[i], "_Ru"));
    // sCumulants_Ru[i] = (TH1D*)inf_Ru->Get(Form("%s%s_%s", "Pro", cnames[i], "Ru"));
    sCumulants_Zr[i] = (TH1D*)inf_Zr->Get(Form("%s%s%s", "ProCorr", cnames[i], "_Zr"));
    // sCumulants_Zr[i] = (TH1D*)inf_Zr->Get(Form("%s%s_%s", "Pro", cnames[i], "Zr"));
    //14042022(finish)
    Cums[i] = new TGraphErrors(_nCent);
    Cums[i]->SetName(Form("%s_%s", "Pro",cnames[i]));
    Cums[i]->SetTitle(labels[i]);
    Cums[i]->SetMarkerStyle(20);
    nEvents[i] = new TGraphErrors(_nCent);
    nEvents[i]->SetName(Form("%s_%s", "nEvt",cnames[i]));
    nEvents[i]->SetTitle(labels[i]);
    nEvents[i]->SetMarkerStyle(20);
  }

  const int LowMultCut  = 1;
  double CentEvent[_nCent]={0};
  double AvgCent[_nCent] = {0.};
  double vSum[_nCent];
  double eSum[_nCent];

  TH1D *hEntries_Ru = (TH1D*) inf_Ru->Get(Form("%shEntries_Ru", "ProCorr"));
  // TH1D *hEntries_Ru = (TH1D*) inf_Ru->Get(Form("%shEntries_Ru", "Pro"));
  TH1D *hEntries_Zr = (TH1D*) inf_Zr->Get(Form("%shEntries_Zr", "ProCorr"));
  // TH1D *hEntries_Zr = (TH1D*) inf_Zr->Get(Form("%shEntries_Zr", "Pro"));
  for(int iCum=0; iCum<_nCums; ++iCum){

    int  nEvent_Ru =  0;
    int  nEvent_Zr =  0;
    for(int iCent=0; iCent<_nCent; ++iCent) {
      vSum[iCent] = 0;
      eSum[iCent] = 0;
      CentEvent[iCent] = 0;
      AvgCent[iCent] = 0;
    }

    for(int iMult = LowMultCut; iMult <= MaxMult; ++iMult){
      nEvent_Ru = hEntries_Ru->GetBinContent(iMult);
      nEvent_Zr = hEntries_Zr->GetBinContent(iMult);

      int CurrCent = getCent(iMult, NameRuZr);
      if(CurrCent == -1) continue;
      int CurrCent_Ru = getCent(iMult, "Ru");
      int CurrCent_Zr = getCent(iMult, "Zr");

      // if(isnan(CurrCent) || isnan(nEvent_Ru) || isnan(sCumulants_Ru[iCum]->GetBinContent(iMult)) || isnan(sCumulants_Ru[iCum]->GetBinError(iMult)) )cout<<CurrCent<<", "<<nEvent_Ru<<", "<<sCumulants_Ru[iCum]->GetBinContent(iMult)<<", "<<sCumulants_Ru[iCum]->GetBinError(iMult)<<endl;
      
      // vSum[CurrCent] += (sCumulants_Ru[iCum]->GetBinContent(iMult) * nEvent_Ru) + (sCumulants_Zr[iCum]->GetBinContent(iMult) * nEvent_Zr);
      // eSum[CurrCent] += pow((sCumulants_Ru[iCum]->GetBinError(iMult) * nEvent_Ru),2) + pow((sCumulants_Zr[iCum]->GetBinError(iMult) * nEvent_Zr),2);
      // CentEvent[CurrCent] += (nEvent_Ru + nEvent_Zr);
      // AvgCent[CurrCent] += iMult * (nEvent_Ru + nEvent_Zr);

      if(CurrCent == CurrCent_Ru && CurrCent != CurrCent_Zr){
	if(iCum == 0) cout<<iMult<<", "<<CurrCent<<", "<<CurrCent_Ru<<", "<<CurrCent_Zr<<endl;
        vSum[CurrCent] += (sCumulants_Ru[iCum]->GetBinContent(iMult) * nEvent_Ru);
	eSum[CurrCent] += pow((sCumulants_Ru[iCum]->GetBinError(iMult) * nEvent_Ru),2);
	CentEvent[CurrCent] += (nEvent_Ru);
	AvgCent[CurrCent] += iMult * (nEvent_Ru);
      }
      else if(CurrCent != CurrCent_Ru && CurrCent == CurrCent_Zr){
	if(iCum == 0) cout<<iMult<<", "<<CurrCent<<", "<<CurrCent_Ru<<", "<<CurrCent_Zr<<endl;
        vSum[CurrCent] += (sCumulants_Zr[iCum]->GetBinContent(iMult) * nEvent_Zr);
	eSum[CurrCent] += pow((sCumulants_Zr[iCum]->GetBinError(iMult) * nEvent_Zr),2);
	CentEvent[CurrCent] += (nEvent_Zr);
	AvgCent[CurrCent] += iMult * (nEvent_Zr);
      }
      else if(CurrCent == CurrCent_Ru && CurrCent == CurrCent_Zr){
	if(iCum == 0) cout<<iMult<<", "<<CurrCent<<", "<<CurrCent_Ru<<", "<<CurrCent_Zr<<endl;
	// vSum[CurrCent] += (sCumulants_Ru[iCum]->GetBinContent(iMult) + sCumulants_Zr[iCum]->GetBinContent(iMult)) * (nEvent_Zr + nEvent_Ru);
	vSum[CurrCent] += sCumulants_Ru[iCum]->GetBinContent(iMult) * nEvent_Zr + sCumulants_Zr[iCum]->GetBinContent(iMult) * nEvent_Ru;
	eSum[CurrCent] += pow((sCumulants_Ru[iCum]->GetBinError(iMult) * nEvent_Ru),2) + pow((sCumulants_Zr[iCum]->GetBinError(iMult) * nEvent_Zr),2);
	CentEvent[CurrCent] += (nEvent_Ru + nEvent_Zr);
	AvgCent[CurrCent] += iMult * (nEvent_Ru + nEvent_Zr);
      }
      else{cout<<"CurrCent = "<<CurrCent<<", CurrCent_Ru = "<<CurrCent_Ru<<"CurrCent_Zr ="<<CurrCent_Zr<<endl;}

    }
    for(int iCent=0; iCent<_nCent; ++iCent){
      if(CentEvent[iCent] != 0) {//cout<<"CentEvent["<<iCent<<"] = "<<CentEvent[iCent]<<endl;
	vSum[iCent] /= CentEvent[iCent];
	eSum[iCent] = sqrt(eSum[iCent]) / CentEvent[iCent];
	AvgCent[iCent] /= CentEvent[iCent];
	//20012022(start)
	// Cums[iCum]->SetPoint(iCent, npart[iCent], vSum[iCent]);
	//30012022(start)
	// Cums[iCum]->SetPoint(iCent, centrality[iCent], vSum[iCent]);
	// //20012022(finish)
	// Cums[iCum]->SetPointError(iCent, 0, eSum[iCent]);
	Cums[iCum]->SetPoint(iCent, AvgCent[iCent], vSum[iCent]);
	Cums[iCum]->SetPointError(iCent, 0, eSum[iCent]);
	//30012022(finish)
	nEvents[iCum]->SetPoint(iCent, AvgCent[iCent], CentEvent[iCent]);
      }
    }
  }
  TFile *out = new TFile(Form("output/cum/cbwc_f%s_corr_dca_%d_nhitsfit_%d_nsp_%d_m2low_%d_m2up_%d_ptmax_%d_ymax_%d_detefflow_err_%d_deteffhigh_err_%d_maxmult_%d_27012022_1_%d.root",NameRuZr, dca_cuts, nhitsfit_cuts, nsp_cuts, m2low_cuts, m2up_cuts, ptmax_cuts, ymax_cuts, detefflow_err, deteffhigh_err, MaxMult, MergeTopCent), "recreate");
  // TFile *out = new TFile(Form("output/cum/cbwc_f%s_27012022_1_%d.root",NameRuZr,MergeTopCent), "update");

  out->cd();
  for(int i=0;i<_nCums;++i) {
    Cums[i]->Write();
    nEvents[i]->Write();
  }
  out->Close();


  delete gROOT->FindObject("ProCorrhEntries");
  for(int i = 0 ; i < _nCums ; i++){
    delete gROOT->FindObject(Form("ProCorr%s", cnames[i]));
  }
  delete gROOT->FindObject("ProCorrk32");
  delete gROOT->FindObject("ProCorrk43");
  delete gROOT->FindObject("ProCorrk54");
  delete gROOT->FindObject("ProCorrk65");

  // delete inf_Ru;
  // // delete Cums;
  // // delete sCumulants_Ru;
  // // delete cnames;
  // // delete labels;
  // delete hEntries;
  // delete out;
    
  
  return;
}

//------------------------------------
int main(int argc, char** argv)
{
  const char* NameRuZr = argv[1];
  MergeTopCent = atoi(argv[2]);

  int dca_cuts[5] = {8, 9, 11, 12, 10};
  int nhitsfit_cuts[5] = {15, 18, 22, 25, 20};
  int nsp_cuts[5] = {16, 18, 22, 25, 20};
  int m2low_cuts[5] = {50, 50, 65, 70, 60};
  int m2up_cuts[5] = {110, 115, 125, 130, 120};

  int ptmax_cuts[2] = {10, 20};
  int ymax_cuts[2] = {1, 5};

  int detefflow_err[3] = {95, 105, 100};
  int deteffhigh_err[3] = {95, 105, 100};

  if(string(NameRuZr) != "RuZr"){
  
    ConvertTerms(NameRuZr, dca_cuts[4], nhitsfit_cuts[4], nsp_cuts[4], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[2], deteffhigh_err[2]);

    for(int i_dca = 0 ; i_dca < 4 ; i_dca++){
      ConvertTerms(NameRuZr, dca_cuts[i_dca], nhitsfit_cuts[4], nsp_cuts[4], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[2], deteffhigh_err[2]);
    }
    for(int i_nhf = 0 ; i_nhf < 4 ; i_nhf++){
      ConvertTerms(NameRuZr, dca_cuts[4], nhitsfit_cuts[i_nhf], nsp_cuts[4], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[2], deteffhigh_err[2]);
    }
    for(int i_nsp = 0 ; i_nsp < 4 ; i_nsp++){
      ConvertTerms(NameRuZr, dca_cuts[4], nhitsfit_cuts[4], nsp_cuts[i_nsp], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[2], deteffhigh_err[2]);
    }
    for(int i_m2 = 0 ; i_m2 < 4 ; i_m2++){
      ConvertTerms(NameRuZr, dca_cuts[4], nhitsfit_cuts[4], nsp_cuts[4], m2low_cuts[i_m2], m2up_cuts[i_m2], ptmax_cuts[1], ymax_cuts[1], detefflow_err[2], deteffhigh_err[2]);
    }

    ConvertTerms(NameRuZr, dca_cuts[4], nhitsfit_cuts[4], nsp_cuts[4], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[1], deteffhigh_err[1]);
    ConvertTerms(NameRuZr, dca_cuts[4], nhitsfit_cuts[4], nsp_cuts[4], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[0], deteffhigh_err[0]);
    ConvertTerms(NameRuZr, dca_cuts[4], nhitsfit_cuts[4], nsp_cuts[4], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[1], deteffhigh_err[0]);
    ConvertTerms(NameRuZr, dca_cuts[4], nhitsfit_cuts[4], nsp_cuts[4], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[0], deteffhigh_err[1]);

  }

  else if(string(NameRuZr) == "RuZr"){

    ConvertTerms_RuZr(NameRuZr, dca_cuts[4], nhitsfit_cuts[4], nsp_cuts[4], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[2], deteffhigh_err[2]);

    for(int i_dca = 0 ; i_dca < 4 ; i_dca++){
      ConvertTerms_RuZr(NameRuZr, dca_cuts[i_dca], nhitsfit_cuts[4], nsp_cuts[4], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[2], deteffhigh_err[2]);
    }
    for(int i_nhf = 0 ; i_nhf < 4 ; i_nhf++){
      ConvertTerms_RuZr(NameRuZr, dca_cuts[4], nhitsfit_cuts[i_nhf], nsp_cuts[4], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[2], deteffhigh_err[2]);
    }
    for(int i_nsp = 0 ; i_nsp < 4 ; i_nsp++){
      ConvertTerms_RuZr(NameRuZr, dca_cuts[4], nhitsfit_cuts[4], nsp_cuts[i_nsp], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[2], deteffhigh_err[2]);
    }
    for(int i_m2 = 0 ; i_m2 < 4 ; i_m2++){
      ConvertTerms_RuZr(NameRuZr, dca_cuts[4], nhitsfit_cuts[4], nsp_cuts[4], m2low_cuts[i_m2], m2up_cuts[i_m2], ptmax_cuts[1], ymax_cuts[1], detefflow_err[2], deteffhigh_err[2]);
    }

    ConvertTerms_RuZr(NameRuZr, dca_cuts[4], nhitsfit_cuts[4], nsp_cuts[4], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[1], deteffhigh_err[1]);
    ConvertTerms_RuZr(NameRuZr, dca_cuts[4], nhitsfit_cuts[4], nsp_cuts[4], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[0], deteffhigh_err[0]);
    ConvertTerms_RuZr(NameRuZr, dca_cuts[4], nhitsfit_cuts[4], nsp_cuts[4], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[1], deteffhigh_err[0]);
    ConvertTerms_RuZr(NameRuZr, dca_cuts[4], nhitsfit_cuts[4], nsp_cuts[4], m2low_cuts[4], m2up_cuts[4], ptmax_cuts[1], ymax_cuts[1], detefflow_err[0], deteffhigh_err[1]);

  }
  return 0;
}
