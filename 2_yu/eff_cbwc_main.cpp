//16082021: added static const int MergeTopCent = 3; to merge top 4 centrality bins to one
// #include <iostream>

// #include "ECorr.h"

// #include "TFile.h"
#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TTree.h"
#include <algorithm>
#include "TGraphErrors.h"
#include "ECorr.h"
#include <cmath>

using namespace std;

const int _nCent = 9;
//from to central to peripheral 
int centrality[_nCent] = {441, 368, 258, 177, 118, 75, 45, 26, 14};//Ru
// int centrality[_nCent] = {436, 362, 250, 169, 111, 70, 42, 24, 13};//Zr
int npart[_nCent]      = {326,282,219,157,107,70,47,0,0};
const int MaxMult = 700;
const int LowEventCut = 1;

int getCent(int mult){
  if(mult >= centrality[0])      return 0;
  else if(mult >= centrality[1]) return 1;
  else if(mult >= centrality[2]) return 2;
  else if(mult >= centrality[3]) return 3;
  else if(mult >= centrality[4]) return 4;
  else if(mult >= centrality[5]) return 5;
  else if(mult >= centrality[6]) return 6;
  else if(mult >= centrality[7]) return 7;
  else if(mult >= centrality[8]) return 8;
  else return -1;
}

void calcum(const char* NameRuZr, int dca_cut, int nhitsfit_cut, int nsp_cut, int m2low_cut, int m2up_cut, int ptmax_cut, int ymax_cut){
  // int maxMult = 700;
  // int maxMult_ref3 = 700;
  // int maxMult_ref3StPicoEvt = 700;

  int index = 0;
  
  ECorr *proton_ec= new ECorr("Pro", MaxMult, LowEventCut);
  cout<<"hey2"<<endl;
  proton_ec->Init();
  cout<<"hey3"<<endl;
  proton_ec->ReadTerms(Form("/Users/hosanko/playground/star/anal/Coll/isobar/ZrZrRuRu200_2018/charge/sys_unc/cum/output/net_proton/mean/30112021/30112021_1/merged_ref3StPicoEvt_%s_good/%s_mean_refMult3StPicoEvt_dca_%d_nhitsfit_%d_nsp_%d_m2low_%d_m2up_%d.root", NameRuZr, NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut));
  cout<<"hey4"<<endl;
  proton_ec->Calculate();
  cout<<"hey5"<<endl;
  proton_ec->Update(Form("noCbwc_f%d.root",index));
  cout<<"hey6"<<endl;

  TFile* inf = TFile::Open(Form("noCbwc_f%d.root",index));

  const int _nCums=22;
  TGraphErrors *Cums[_nCums];
  TH1D *sCumulants[_nCums];
  const char *cnames[_nCums]={"C1","C2","C3","C4","C5","C6",
    "R21","R32","R42","R51","R62",
    "k1","k2","k3","k4","k5","k6",
    "k21","k31","k41","k51","k61"
  };
  const char *labels[_nCums]={"C_{1}","C_{2}","C_{3}","C_{4}","C_{5}","C_{6}",
    "C_{2}/C_{1}","C_{3}/C_{2}","C_{4}/C_{2}","C_{5}/C_{1}","C_{6}/C_{2}",
    "#kappa_{1}","#kappa_{2}","#kappa_{3}","#kappa_{4}","#kappa_{5}","#kappa_{6}",
    "#kappa_{2}/#kappa_{1}","#kappa_{3}/#kappa_{1}","#kappa_{4}/#kappa_{1}","#kappa_{5}/#kappa_{1}","#kappa_{6}/#kappa_{1}"
  };
  for(int i=0; i<_nCums; ++i){
    sCumulants[i] = (TH1D*)inf->Get(Form("%s%s", "Pro", cnames[i]));
    Cums[i] = new TGraphErrors(_nCent);
    Cums[i]->SetName(Form("%s_%s", "Pro",cnames[i]));
    Cums[i]->SetTitle(labels[i]);
    Cums[i]->SetMarkerStyle(20);
  }

  const int LowMultCut  = 1;
  double CentEvent[_nCent]={0};
  double vSum[_nCent];
  double eSum[_nCent];

  TH1D *hEntries = (TH1D*) inf->Get(Form("%shEntries", "Pro"));
  for(int iCum=0; iCum<_nCums; ++iCum){

    int  nEvent =  0;
    for(int iCent=0; iCent<_nCent; ++iCent) {
      vSum[iCent] = 0;
      eSum[iCent] = 0;
      CentEvent[iCent] = 0;
    }

    for(int iMult = LowMultCut; iMult <= MaxMult; ++iMult){
      nEvent = hEntries->GetBinContent(iMult);

      int CurrCent = getCent(iMult);
      if(CurrCent == -1) continue;

      vSum[CurrCent] += (sCumulants[iCum]->GetBinContent(iMult) * nEvent);
      eSum[CurrCent] += pow((sCumulants[iCum]->GetBinError(iMult)*nEvent),2);
      CentEvent[CurrCent] += nEvent;
    }
    for(int iCent=0; iCent<_nCent; ++iCent){
      vSum[iCent] /= CentEvent[iCent];
      eSum[iCent] = sqrt(eSum[iCent]) / CentEvent[iCent];
      Cums[iCum]->SetPoint(iCent, npart[iCent], vSum[iCent]);
      Cums[iCum]->SetPointError(iCent, 0, eSum[iCent]);
    }
  }
  TFile *out = new TFile(Form("cbwc_f%d.root",index), "update");
  out->cd();
  for(int i=0;i<_nCums;++i) Cums[i]->Write();
  out->Close();


  // cum->GetRefMult3Means();
  // cum->CalculateEffCorrRefMult3Cum(NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut);
  // cum->CalculateRefMult3CBWCcum(NameRuZr, dca_cut, nhitsfit_cut, nsp_cut, m2low_cut, m2up_cut, ptmax_cut, ymax_cut);
  // delete cum;

  return;
}

int main(int argc, char** argv){
  int dca_cuts[5] = {8, 9, 10, 11, 12};
  int nhitsfit_cuts[5] = {15, 18, 20, 22, 25};
  int nsp_cuts[5] = {16, 18, 20, 22, 25};
  int m2low_cuts[5] = {50, 50, 60, 65, 70};
  int m2up_cuts[5] = {110, 115, 120, 125, 130};

  int ptmax_cuts[2] = {10, 20};
  int ymax_cuts[2] = {1, 5};

  calcum("Ru", dca_cuts[2], nhitsfit_cuts[2], nsp_cuts[2], m2low_cuts[2], m2up_cuts[2], ptmax_cuts[1], ymax_cuts[1]);
  // calcum("Zr", dca_cuts[2], nhitsfit_cuts[2], nsp_cuts[2], m2low_cuts[2], m2up_cuts[2], ptmax_cuts[1], ymax_cuts[1]);

  return 0;
}
